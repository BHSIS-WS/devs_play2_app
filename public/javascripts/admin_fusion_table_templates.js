/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function GoogleAdminModel(clientId, redirect) {
    this.clientId = clientId;
    this.redirect = redirect;
    this.selectedTemplateArrayIndex = 0;
    this.changeMode="readonly";
    this.init();
    this.extractToken();
}

GoogleAdminModel.prototype.init = function() {
    //set up hash detection
    $(window).bind('hashchange', function(e) {
        console.log("hash change= " + location.hash);
    });

};

GoogleAdminModel.prototype.authenticate = function() {
    var OAUTHURL = 'https://accounts.google.com/o/oauth2/auth?';
    var VALIDURL = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';
    var SCOPE = 'https://www.googleapis.com/auth/fusiontables https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email';
    //var CLIENTID = '389003939086-f5jqnik2s9viu9fnbbf3r8q343l26v0b.apps.googleusercontent.com';
    var CLIENTID = this.clientId;
    //var REDIRECT = 'http://bhs1dev.smdi.com/devs/gadmintemplate'
    var REDIRECT = this.redirect;
    var LOGOUT = 'http://accounts.google.com/Logout';
    var TYPE = 'token';
    var _url = OAUTHURL + 'scope=' + SCOPE + '&client_id=' + CLIENTID + '&redirect_uri=' + REDIRECT + '&response_type=' + TYPE;
    console.log("_url=" + _url);
    console.log("window.location.href :" + window.location.href);
    window.location.href = _url;
};

GoogleAdminModel.gup = function(url, name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\#&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    if (results === null)
        return "";
    else
        return results[1];
};

GoogleAdminModel.prototype.extractToken = function() {
    if (location.hash === undefined || location.hash === "") {
        return;
    }

    var url = location.hash;

    this.accessToken = GoogleAdminModel.gup(url, 'access_token');
    this.tokenType = GoogleAdminModel.gup(url, 'token_type');
    this.expiresIn = GoogleAdminModel.gup(url, 'expires_in');

    $('#access-token-id').val(this.accessToken);

};

GoogleAdminModel.prototype.getTemplates = function() {

    var fusionTableId = $('#fusion-table-id').val();
    // Construct the URL
    var url = ['https://www.googleapis.com/fusiontables/v1/tables/'];
    url.push(fusionTableId);
    url.push('/templates');
//    url.push('?callback=?');//for jsonp
    url.push('?maxResults=100');//default is 5


    var token = $('#access-token-id').val();
    console.log("token=" + token);

    var thisModel = this;
    // Send the JSONP request using jQuery
    $.ajax({
        url: url.join(''),
//        dataType: 'jsonp',// setRequestHeader in beforeSend won't work
        beforeSend: function(xhr)
        {
            xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        },
        success: function(data) {
            console.log("on getTemplates sucess() " + data);
            if ( thisModel.hasMoreTemplateToBeFetched(data) ){
                console.log("There are more template to be fetched...");
            }
            thisModel.setTemplatesJsonData(data);
        }
    });

};

GoogleAdminModel.prototype.hasMoreTemplateToBeFetched = function(data) {
    //
//    console.log("on hasMoreTemplateToBeFetched() pending " + data);
//    var nItem = data.items.length;
//    var totalItem = data.totalItems;
//    var nextPageToken = data.nextPageToken;
//        console.log ("nItem="+nItem+", totalItem="+totalItem+",nextPageToken="+nextPageToken);
    return (data.nextPageToken!==undefined);


};

GoogleAdminModel.prototype.refreshTemplateContentDisplay = function() {
    if (this.changeMode === 'edit' || this.changeMode === 'new') {
        throw Error("can not refresh templateContentDisplay while the mode in 'edit' or 'new' current mode="+this.changeMode);
    }

    console.log("refreshTemplateContentDisplay.. index="+this.selectedTemplateArrayIndex);

    //populate template choices
    var optionText = '';
    for (var i = 0; i < this.nTemplates; i++) {
        var templateId = this.templateArray[i].templateId;
        var text = "<option " + "value='" + i + "'>"
            + "Template-" + templateId
            + "</option>";
        optionText += text;
    }

    //    console.log("selectText=" + optionText)
    $('#map-style-select').html(optionText);


    //set selected items
    $('#map-style-select').val( this.selectedTemplateArrayIndex );


    //populate template content display
    var iTemplate = this.selectedTemplateArrayIndex;
    var templateSelectedText = JSON.stringify(this.templateArray[iTemplate], null, '  ');
    $('#style-text-area').val(templateSelectedText);
//    console.log("changed textArea Text to \n\t:"+templateSelectedText);


};

GoogleAdminModel.prototype.setTemplatesJsonData = function(data) {
    this.templateData = data;
    this.totalTemplates = data.totalItems;
    this.templateArray = data.items;
    this.nTemplates = data.items.length;
    this.refreshTemplateContentDisplay();
};

GoogleAdminModel.prototype.showButtonGroupAsChangeMode = function() {
    $('#style-edit').hide();
    $('#style-new').hide();
    $('#style-save').show();
    $('#style-cancel').show();
    $('#style-content-label').css("color", 'green');
    $('#style-text-area').removeAttr('readonly');
    $('#map-style-select').attr('disabled', 'disabled');
};

GoogleAdminModel.prototype.showButtonGroupAsInitialMode = function() {
    console.log("\tshowButtonGroupAsInitialMode in");//debug
    $('#style-edit').show();
    $('#style-new').show();
    $('#style-save').hide();
    $('#style-cancel').hide();
    $('#style-content-label').css("color", '');
    $('#style-text-area').attr('readonly', 'readonly');
    $('#map-style-select').removeAttr('disabled');
    this.changeMode = 'readonly';
    console.log("\t\t\tmode ->"+this.changeMode);//debug
};

GoogleAdminModel.prototype.setModeTemplateEdit = function() {
    this.showButtonGroupAsChangeMode();
    this.changeMode = 'edit';
    console.log("\t\t\tmode ->"+this.changeMode);//debug
};

GoogleAdminModel.prototype.setModeTemplateNew = function() {
    this.showButtonGroupAsChangeMode();

    //0.set mode : new
    this.changeMode = 'new';
    console.log("setModeTemplateNew()\t\t\tmode ->"+this.changeMode);//debug

    //1.template choices -set to blank
    $('#map-style-select').val( [] );

    //2.clear template content
    $('#style-text-area').val("");

};

GoogleAdminModel.prototype.setModeTemplateCancel = function() {
    this.showButtonGroupAsInitialMode();
    //restore template content display
    this.setTemplatesJsonData(this.templateData);
};
GoogleAdminModel.prototype.setModeTemplateSave = function() {

    if (this.changeMode === 'new') {
        this.createNewTemplate();
    } else if (this.changeMode === 'edit') {
        this.saveTemplate();
    } else {
        throw Error("Saving template content change failed because unknow changeMode : " + this.changeMode);
    }

};

GoogleAdminModel.prototype.createNewTemplate = function() {

    var fusionTableId = $('#fusion-table-id').val();
    // Construct the URL
    var url = ['https://www.googleapis.com/fusiontables/v1/tables/'];
    url.push(fusionTableId);
    url.push('/templates');

    var token = $('#access-token-id').val();
    console.log("creating new template token=" + token);

    var templateContent = $('#style-text-area').val();

    $.ajax({
        url: url.join(''),
        type: 'POST',
        context: this,
        data: templateContent,
        beforeSend: function(xhr)
        {
            xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            xhr.setRequestHeader('Content-Type', 'application/json');
        },
        success: function(data) {
            console.log("on createNewTemplate sucess " + data);
            this.showButtonGroupAsInitialMode();
        },
        error: function() {
            console.log("on createNewTemplate ERROR ");
        }
    });

};
GoogleAdminModel.prototype.getSelectedTemplateId = function() {
    var selectTemplateArrayIndex = this.getSelectedTemplateArrayIndex();
    var template = this.templateArray[selectTemplateArrayIndex];
    return template.templateId;
};
GoogleAdminModel.prototype.getSelectedTemplateArrayIndex = function() {
    return this.selectedTemplateArrayIndex = $('#map-style-select').val();
};
GoogleAdminModel.prototype.selectedTemplateChanged = function() {

    this.selectedTemplateArrayIndex = this.getSelectedTemplateArrayIndex();
    console.log("selectedTemplateChanged.. index="+this.selectedTemplateArrayIndex);
    this.refreshTemplateContentDisplay();

};


GoogleAdminModel.prototype.saveTemplate = function() {

    var selectTemplateId = this.getSelectedTemplateId();
    if (selectTemplateId <= 2) {
        alert("only allow to save template with templateId >2 current templateId = " + selectTemplateId);
        return;
    }
    var fusionTableId = $('#fusion-table-id').val();
    // Construct the URL
    var url = ['https://www.googleapis.com/fusiontables/v1/tables/'];
    url.push(fusionTableId);
    url.push('/templates/');
    url.push(selectTemplateId);

    var token = $('#access-token-id').val();
    console.log("token=" + token);

    var templateContent = $('#style-text-area').val();
    //var templateJson = JSON.parse(templateContent);

    $.ajax({
        url: url.join(''),
        type: 'PUT',
        context: this,
        data: templateContent,
        beforeSend: function(xhr)
        {
            xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            xhr.setRequestHeader('Content-Type', 'application/json');
        },
        success: function(data) {
            console.log("on saveTemplate sucess " + data);
            this.showButtonGroupAsInitialMode();
        },
        error: function() {
            console.log("on saveTemplate ERROR ");
        }
    });
};