/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function GoogleAdminModel(clientId, redirect) {
    this.clientId = clientId;
    this.redirect = redirect;
    this.selectedStyleArrayIndex = 0;
    this.changeMode="readonly";
    this.init();
    this.extractToken();
}

GoogleAdminModel.prototype.init = function() {
    //set up hash detection
    $(window).bind('hashchange', function(e) {
        console.log("hash change= " + location.hash);
    });

};

GoogleAdminModel.prototype.authenticate = function() {
    var OAUTHURL = 'https://accounts.google.com/o/oauth2/auth?';
    var VALIDURL = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';
    var SCOPE = 'https://www.googleapis.com/auth/fusiontables https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email';
    //var CLIENTID = '389003939086-f5jqnik2s9viu9fnbbf3r8q343l26v0b.apps.googleusercontent.com';
    //var CLIENTID = '631931670155-1uk1useul9las7i68fb98h5mverbni2j.apps.googleusercontent.com';
    var CLIENTID = this.clientId;
    //var REDIRECT = 'http://bhs1dev.smdi.com/devs/gadminstyle';
    //var REDIRECT = 'http://localhost:9062/devs/gadminstyle';
    var REDIRECT = this.redirect;
    var LOGOUT = 'http://accounts.google.com/Logout';
    var TYPE = 'token';
    var _url = OAUTHURL + 'scope=' + SCOPE + '&client_id=' + CLIENTID + '&redirect_uri=' + REDIRECT + '&response_type=' + TYPE;
    console.log("_url=" + _url);
    console.log("window.location.href :" + window.location.href);
    window.location.href = _url;
};

GoogleAdminModel.gup = function(url, name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\#&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    if (results === null)
        return "";
    else
        return results[1];
};

GoogleAdminModel.prototype.extractToken = function() {
    if (location.hash === undefined || location.hash === "") {
        return;
    }

    var url = location.hash;

    this.accessToken = GoogleAdminModel.gup(url, 'access_token');
    this.tokenType = GoogleAdminModel.gup(url, 'token_type');
    this.expiresIn = GoogleAdminModel.gup(url, 'expires_in');

    $('#access-token-id').val(this.accessToken);

};

GoogleAdminModel.prototype.getStyles = function() {

    var fusionTableId = $('#fusion-table-id').val();
    // Construct the URL
    var url = ['https://www.googleapis.com/fusiontables/v1/tables/'];
    url.push(fusionTableId);
    url.push('/styles');
//    url.push('?callback=?');//for jsonp
    url.push('?maxResults=100');//default is 5


    var token = $('#access-token-id').val();
    console.log("token=" + token);

    var thisModel = this;
    // Send the JSONP request using jQuery
    $.ajax({
        url: url.join(''),
//        dataType: 'jsonp',// setRequestHeader in beforeSend won't work
        beforeSend: function(xhr)
        {
            xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        },
        success: function(data) {
            console.log("on getStyles sucess() " + data);
            if ( thisModel.hasMoreStyleToBeFetched(data) ){
                console.log("There are more style to be fetched...");
            }
            thisModel.setStylesJsonData(data);
        }
    });

};

GoogleAdminModel.prototype.hasMoreStyleToBeFetched = function(data) {
    //
//    console.log("on hasMoreStyleToBeFetched() pending " + data);
//    var nItem = data.items.length;
//    var totalItem = data.totalItems;
//    var nextPageToken = data.nextPageToken;
//        console.log ("nItem="+nItem+", totalItem="+totalItem+",nextPageToken="+nextPageToken);
    return (data.nextPageToken!==undefined);


};

GoogleAdminModel.prototype.refreshStyleContentDisplay = function() {
    if (this.changeMode === 'edit' || this.changeMode === 'new') {
        throw Error("can not refresh styleContentDisplay while the mode in 'edit' or 'new' current mode="+this.changeMode);
    }

    console.log("refreshStyleContentDisplay.. index="+this.selectedStyleArrayIndex);

    //populate style choices
    var optionText = '';
    for (var i = 0; i < this.nStyles; i++) {
        var styleId = this.styleArray[i].styleId;
        var text = "<option " + "value='" + i + "'>"
            + "Style-" + styleId
            + "</option>";
        optionText += text;
    }

    //    console.log("selectText=" + optionText)
    $('#map-style-select').html(optionText);


    //set selected items
    $('#map-style-select').val( this.selectedStyleArrayIndex );


    //populate style content display
    var iStyle = this.selectedStyleArrayIndex;
    if ( this.styleArray!==undefined){
        var styleSelectedText = JSON.stringify(this.styleArray[iStyle], null, '  ');
        $('#style-text-area').val(styleSelectedText);
    }
//    console.log("changed textArea Text to \n\t:"+styleSelectedText);


};

GoogleAdminModel.prototype.setStylesJsonData = function(data) {
    this.styleData = data;
    this.totalStyles = data.totalItems;
    this.styleArray = data.items;
    if (data.items !== undefined){
        this.nStyles = data.items.length;
    }
    this.refreshStyleContentDisplay();
};

GoogleAdminModel.prototype.showButtonGroupAsChangeMode = function() {
    $('#style-edit').hide();
    $('#style-new').hide();
    $('#style-save').show();
    $('#style-cancel').show();
    $('#style-content-label').css("color", 'green');
    $('#style-text-area').removeAttr('readonly');
    $('#map-style-select').attr('disabled', 'disabled');
};

GoogleAdminModel.prototype.showButtonGroupAsInitialMode = function() {
    console.log("\tshowButtonGroupAsInitialMode in");//debug
    $('#style-edit').show();
    $('#style-new').show();
    $('#style-save').hide();
    $('#style-cancel').hide();
    $('#style-content-label').css("color", '');
    $('#style-text-area').attr('readonly', 'readonly');
    $('#map-style-select').removeAttr('disabled');
    this.changeMode = 'readonly';
    console.log("\t\t\tmode ->"+this.changeMode);//debug
};

GoogleAdminModel.prototype.setModeStyleEdit = function() {
    this.showButtonGroupAsChangeMode();
    this.changeMode = 'edit';
    console.log("\t\t\tmode ->"+this.changeMode);//debug
};

GoogleAdminModel.prototype.setModeStyleNew = function() {
    this.showButtonGroupAsChangeMode();

    //0.set mode : new
    this.changeMode = 'new';
    console.log("setModeStyleNew()\t\t\tmode ->"+this.changeMode);//debug

    //1.style choices -set to blank
    $('#map-style-select').val( [] );

    //2.clear style content
    $('#style-text-area').val("");

};

GoogleAdminModel.prototype.setModeStyleCancel = function() {
    this.showButtonGroupAsInitialMode();
    //restore style content display
    this.setStylesJsonData(this.styleData);
};
GoogleAdminModel.prototype.setModeStyleSave = function() {

    if (this.changeMode === 'new') {
        this.createNewStyle();
    } else if (this.changeMode === 'edit') {
        this.saveStyle();
    } else {
        throw Error("Saving style content change failed because unknow changeMode : " + this.changeMode);
    }

};

GoogleAdminModel.prototype.createNewStyle = function() {

    var fusionTableId = $('#fusion-table-id').val();
    // Construct the URL
    var url = ['https://www.googleapis.com/fusiontables/v1/tables/'];
    url.push(fusionTableId);
    url.push('/styles');

    var token = $('#access-token-id').val();
    console.log("creating new style token=" + token);

    var styleContent = $('#style-text-area').val();

    $.ajax({
        url: url.join(''),
        type: 'POST',
        context: this,
        data: styleContent,
        beforeSend: function(xhr)
        {
            xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            xhr.setRequestHeader('Content-Type', 'application/json');
        },
        success: function(data) {
            console.log("on createNewStyle sucess " + data);
            this.showButtonGroupAsInitialMode();
        },
        error: function() {
            console.log("on createNewStyle ERROR ");
        }
    });

};
GoogleAdminModel.prototype.getSelectedStyleId = function() {
    var selectStyleArrayIndex = this.getSelectedStyleArrayIndex();
    var style = this.styleArray[selectStyleArrayIndex];
    return style.styleId;
};
GoogleAdminModel.prototype.getSelectedStyleArrayIndex = function() {
    return this.selectedStyleArrayIndex = $('#map-style-select').val();
};
GoogleAdminModel.prototype.selectedStyleChanged = function() {

    this.selectedStyleArrayIndex = this.getSelectedStyleArrayIndex();
    console.log("selectedStyleChanged.. index="+this.selectedStyleArrayIndex);
    this.refreshStyleContentDisplay();

};


GoogleAdminModel.prototype.saveStyle = function() {

    var selectStyleId = this.getSelectedStyleId();
//    if (selectStyleId <= 2) {
//        alert("only allow to save style with styleId >2 current styleId = " + selectStyleId);
//        return;
//    }
    var fusionTableId = $('#fusion-table-id').val();
    // Construct the URL
    var url = ['https://www.googleapis.com/fusiontables/v1/tables/'];
    url.push(fusionTableId);
    url.push('/styles/');
    url.push(selectStyleId);

    var token = $('#access-token-id').val();
    console.log("token=" + token);

    var styleContent = $('#style-text-area').val();
    //var styleJson = JSON.parse(styleContent);

    $.ajax({
        url: url.join(''),
        type: 'PUT',
        context: this,
        data: styleContent,
        beforeSend: function(xhr)
        {
            xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            xhr.setRequestHeader('Content-Type', 'application/json');
        },
        success: function(data) {
            console.log("on saveStyle sucess " + data);
            this.showButtonGroupAsInitialMode();
        },
        error: function() {
            console.log("on saveStyle ERROR ");
        }
    });
};