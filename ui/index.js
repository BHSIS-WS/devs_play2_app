import React from 'react';
import { render } from 'react-dom';
import { Provider} from 'react-redux';
import store from './redux/store';
import StateChangeListener from './others/StateChangeListener';

import SideContainerController from './containers/SideContainerController';
import ResultDataTableController from './containers/ResultDataTableController';
import MapLegendController from './containers/MapLegendController';
import D3MapToolOptionPanel1 from './containers/mapd3tools/D3MapToolOptionPanel1';
import ContextMenuController from './containers/ContextMenuController';
import DropMarkerController from './containers/DropMarkerController';
import SaveLayerConfigController from './containers/SaveLayerConfigController';
import SavePdfController from './containers/SavePdfController';
import TimerModelController from './containers/TimerModelController';
import RenderStatusPanelController from './containers/RenderStatusPanelController';
import CanvasAttributeController from './containers/CanvasAttributesController';
import RemoveConfigDialogController from './containers/RemoveConfigDialogController';
import MinimapPanelController from "./containers/MinimapPanelController";
import ToolbarController from './containers/ToolbarController';
import InfoBoxDialogController from "./containers/InfoBoxDialogController";
import HeaderButtonsController from "./containers/HeaderButtonsController";
import RefreshMapController from './containers/RefreshMapController';

store.dispatch({type: 'INIT_STATE_REF'});
store.dispatch({type: 'INIT_STATE_HELP'});
store.dispatch({type: 'FETCH_SAVED_LAYER_CONFIG_LIST'});

render(
    <Provider store={store}>
        <SideContainerController />
    </Provider>,
      document.getElementById('sideLayerContainer')
);

render(
    <Provider store={store}>
        <ResultDataTableController />
    </Provider>,
    document.getElementById('layerData')
);

render(
    <Provider store={store}>
        <TimerModelController />
    </Provider>,
    document.getElementById('timerModelContainer')
);

render(
    <Provider store={store}>
        <MapLegendController/>
    </Provider>,
    document.getElementById('mapLegend')
);

render(
    <Provider store={store}>
        <InfoBoxDialogController/>
    </Provider>,
    document.getElementById('infoBoxDialog')
);

render(
    <Provider store={store}>
        <ContextMenuController/>
    </Provider>,
    document.getElementById('contextMenuContainer')
);

render(
    <Provider store={store}>
        <D3MapToolOptionPanel1 />
    </Provider>,
    document.getElementById('mapToolOptionContainer')
);

render(
    <Provider store={store}>
        <DropMarkerController />
    </Provider>,
    document.getElementById('dropMarkerContainer')
);

render(
    <Provider store={store}>
        <SaveLayerConfigController />
    </Provider>,
    document.getElementById('saveSnapshotContainer')
);

render(
    <Provider store={store}>
        <CanvasAttributeController/>
    </Provider>,
    document.getElementById('canvasAttributesContainer')
);

render(
    <Provider store={store}>
        <SavePdfController />
    </Provider>,
    document.getElementById('savePdfContainer')
);

render(
    <Provider store={store}>
        <RenderStatusPanelController/>
    </Provider>,
    document.getElementById('renderStatusPanelContainer')
);

render(
    <Provider store={store}>
        <RemoveConfigDialogController/>
    </Provider>,
    document.getElementById('removeConfigDialogContainer')
);

render(
    <Provider store={store}>
        <HeaderButtonsController/>
    </Provider>,
    document.getElementById('headerTabButtons')
);

if (appConfig.printShowTab) {
    render(
        <Provider store={store}>
            <ToolbarController/>
        </Provider>,
        document.getElementById('mapPrintMenu')
    );
}

render(
    <Provider store={store}>
        <MinimapPanelController/>
    </Provider>,
    document.getElementById('minimapPanelContainer')
);


render(
    <Provider store={store}>
        <RefreshMapController/>
    </Provider>,
    document.getElementById('refreshMapContainer')
);
