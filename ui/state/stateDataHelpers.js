
const isLayerCriteriaMapDataDifferent = function (normalLayerCriteriaIdMap1, normalLayerCriteriaIdMap2) {
    //selectedCriteriaData: Map ( [criteriaCodeid,set(CriteriaId)], [], ...] )
    //key: criteriaCodeid; value:set(CriteriaId)
    if (!normalLayerCriteriaIdMap1||!normalLayerCriteriaIdMap2) return true;
    if (normalLayerCriteriaIdMap1.size!=normalLayerCriteriaIdMap2.size) return true;

    //iterate over normalLayerCriteriaIdMap1
    for (let entry of normalLayerCriteriaIdMap1.entries()){
        let criteriaCodeid = entry[0];
        let selectedCriterIdSet1 = entry[1];
        let selectedCriterIdSet2 = normalLayerCriteriaIdMap2.get(criteriaCodeid);

        if (!selectedCriterIdSet2 && selectedCriterIdSet1) return true;
        if (selectedCriterIdSet2 && !selectedCriterIdSet1) return true;

        if (selectedCriterIdSet2 && selectedCriterIdSet1) {

            if (selectedCriterIdSet2.size != selectedCriterIdSet1.size) return true;

            let setDiff = new Set(
                [...selectedCriterIdSet2].filter(x => !selectedCriterIdSet1.has(x))
            );

            if (setDiff&&setDiff.size>0) {
                return true;
            }
        }
    }
    return false;
}

const isGradFilterArrayDataDifferent = function (gradLayerFilterArray1, gradLayerFilterArray2) {
    if (!gradLayerFilterArray1 || !gradLayerFilterArray2) return true;
    if (gradLayerFilterArray1.length !== gradLayerFilterArray2.length) return true;

    let filterEntry1;
    let filterEntry2;
    for (let i = 0; i < gradLayerFilterArray1.length; i++) {
        filterEntry1 = gradLayerFilterArray1[i];
        filterEntry2 = gradLayerFilterArray2.find(function(filterEntry) {
            return (filterEntry.filterSign == filterEntry1.filterSign &&
                filterEntry.filterValue == filterEntry1.filterValue);
        });
        if (typeof filterEntry2 == "undefined") {return true;}
    }
    return false;
}

export {isLayerCriteriaMapDataDifferent};
export {isGradFilterArrayDataDifferent};