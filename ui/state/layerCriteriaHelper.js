"use strict";

const getCriteriaData = function(criteriaCodeId, layerId, filterCriteriaSet, layerArray, layerFilteringCriteriaArray, criteriaCodeLookupMap) {
    let cData = {};
    //cData.creteriaCodeId = criteriaCodeId;
    criteriaCodeId=Math.trunc(criteriaCodeId);
    cData.criteria = criteriaCodeLookupMap.get(criteriaCodeId);
    cData.layer = layerArray.find(function(element){
        return element.id == layerId;
    });
    cData.filterCriteriaArray = layerFilteringCriteriaArray.filter(aFilteringcriteria => {
        return (
            aFilteringcriteria.fusion_map_layer_id == layerId&& aFilteringcriteria.criteria_code_id== criteriaCodeId && filterCriteriaSet.has(aFilteringcriteria.id)
        );
    });
    //cData.filterCriteriaSet = filterCriteriaSet;
    return cData;
};

const buildCriteriaQuery = function(criteriaCodeId, layerId, filterCriteriaSet, layerArray, layerFilteringCriteriaArray, criteriaCodeLookupMap) {
    let cData = getCriteriaData(criteriaCodeId, layerId, filterCriteriaSet, layerArray, layerFilteringCriteriaArray, criteriaCodeLookupMap);
    let criteria = "";
    if (cData.layer != null) {
        if (cData.filterCriteriaArray != null && cData.filterCriteriaArray.length > 0) {
           if (cData.layer.query_type === 'in') {
               let cVal = "";
               cData.filterCriteriaArray.forEach(fc=>{
                   if (cVal.length > 0) {
                       cVal += ", ";
                   }
                   cVal += "'" + fc.criteria_value + "'";
               });
               criteria = cData.criteria.criteria_name + " in (" + cVal + ")";
           } else {
               cData.filterCriteriaArray.forEach(fc => {
                   if (criteria.length > 0) {
                       criteria += " and ";
                   }
                   criteria += fc.criteria_value + "='1'";
               });
           }
        }
    }
    return criteria;
};

const buildNormalLayerQuery = function(selectedLayerIdSet, selectedCriteriaIdMap, selectedCriteriaCodeIdMap, criteriaCodeArray, layerArray, layerFilteringCriteriaArray, criteriaCodeLookupMap, criteriaConditionsMap) {
    let layersArray = [];
    if (selectedLayerIdSet != null && selectedLayerIdSet.size > 0) {
        let oneLayer = {};
        let oneCriteriaMap = null;
        let oneFilterSet = null;
        selectedLayerIdSet.forEach((val1, layerId, set) => {
            oneLayer = {"fusion_map_layer_id": layerId,
                        "criteria": ""};
            oneCriteriaMap = null;
            oneFilterSet = null;
            if (selectedCriteriaIdMap != null && selectedCriteriaIdMap.size > 0) {
                oneCriteriaMap = selectedCriteriaIdMap.get(layerId);
                oneFilterSet = null;
                if (oneCriteriaMap != null && oneCriteriaMap.size > 0) {
                    oneCriteriaMap.forEach((filterSet, criteriaId, map) => {
                        if (oneLayer.criteria.length > 0 && filterSet.size > 0) {
                            oneLayer.criteria += " and ";
                        }
                        oneLayer.criteria += buildCriteriaQuery(criteriaId, layerId, filterSet, layerArray, layerFilteringCriteriaArray, criteriaCodeLookupMap);
                    });
                }
            }
            if (criteriaConditionsMap != null && criteriaConditionsMap.size > 0 && selectedCriteriaCodeIdMap != null && selectedCriteriaCodeIdMap.size > 0) {
                let selectedCriteriaCodeIdSet = selectedCriteriaCodeIdMap.get(layerId);
                if (selectedCriteriaCodeIdSet != null) {
                    let conditionsWhereString = buildNormalLayerConditionsQuery(criteriaConditionsMap, selectedCriteriaCodeIdSet, criteriaCodeArray);
                    if (oneLayer.criteria !== "" && conditionsWhereString !== "") {
                        conditionsWhereString = " AND " + conditionsWhereString;
                    }
                    oneLayer.criteria += conditionsWhereString;
                }
            }
            layersArray.push(oneLayer);
        });
    }

console.log ("!!!!! buildNormalLayerQuery(): ",layersArray);

    return layersArray;
};

const buildNormalLayerQueryCarto = function(selectedLayerIdSet, selectedCriteriaIdMap, selectedCriteriaCodeIdMap, criteriaCodeArray, layerArray, layerFilteringCriteriaArray, criteriaCodeLookupMap, criteriaConditionsMap) {
    let layersArray = [];
    let selectedGeomTypes = [];
    if (selectedLayerIdSet != null && selectedLayerIdSet.size > 0) {
        let oneLayer = {};
        let oneCriteriaMap = null;
        let oneFilterSet = null;
        let selectedGeomType;
        selectedLayerIdSet.forEach((val1, layerId, set) => {
            oneLayer = {"fusion_map_layer_id": layerId,
                "criteria": ""};
            oneCriteriaMap = null;
            oneFilterSet = null;
            if (selectedCriteriaIdMap != null && selectedCriteriaIdMap.size > 0) {
                oneCriteriaMap = selectedCriteriaIdMap.get(layerId);
                oneFilterSet = null;
                if (oneCriteriaMap != null && oneCriteriaMap.size > 0) {
                    oneCriteriaMap.forEach((filterSet, criteriaId, map) => {
                        if (oneLayer.criteria.length > 0 && filterSet.size > 0) {
                            oneLayer.criteria += " and ";
                        }
                        oneLayer.criteria += buildCriteriaQuery(criteriaId, layerId, filterSet, layerArray, layerFilteringCriteriaArray, criteriaCodeLookupMap);
                    });
                }
            }
            if (criteriaConditionsMap != null && criteriaConditionsMap.size > 0 && selectedCriteriaCodeIdMap != null && selectedCriteriaCodeIdMap.size > 0) {
                let selectedCriteriaCodeIdSet = selectedCriteriaCodeIdMap.get(layerId);
                if (selectedCriteriaCodeIdSet != null) {
                    let conditionsWhereString = buildNormalLayerConditionsQuery(criteriaConditionsMap, selectedCriteriaCodeIdSet, criteriaCodeArray);
                    if (oneLayer.criteria !== "" && conditionsWhereString !== "") {
                        conditionsWhereString = " AND " + conditionsWhereString;
                    }
                    oneLayer.criteria += conditionsWhereString;
                }
            }
            selectedGeomTypes.push(selectedGeomType);
            layersArray.push(oneLayer);
        });
    }

    console.log ("!!!!! buildNormalLayerQuery(): ",layersArray);

    return layersArray;
};

const buildGradientLayerQuery = function (selectedGradLayerIdSet, gradientLayerArray, gradFilterMap) {
    let layersArray = [];
    let oneLayer = {};
    if (selectedGradLayerIdSet != null &&  selectedGradLayerIdSet.size > 0) {
        selectedGradLayerIdSet.forEach((val1, layerId, set) => {
            oneLayer = {};

            let layer = gradientLayerArray.find(function(layerElement) {
                return layerElement.id == layerId;
            });
            oneLayer.gradientMapLayerId = layerId;
            oneLayer.layer_key = layerId + "." + layer.field_column_name;
            oneLayer.criteria = "";
            let gradFilters = gradFilterMap.get(layerId);
            if (gradFilters && gradFilters.length >= 1) {
                oneLayer.criteria = layer.field_column_name + " " + gradFilters[0].filterSign + " " + gradFilters[0].filterValue;
                if (gradFilters.length > 1) {
                    for (let i = 1; i < gradFilters.length; i++) {
                        oneLayer.criteria += " AND " + layer.field_column_name + " " + gradFilters[i].filterSign + " " + gradFilters[i].filterValue;
                    }
                }
            }
            layersArray.push(oneLayer);
        });
    }
    return layersArray;
};

const buildNormalLayerConditionsQuery = function(criteriaConditionsMap, selectedCriteriaCodeIdSet, criteriaCodeArray) {

    let whereString = "";
    let relevantCriteriaConditions = [];
    selectedCriteriaCodeIdSet.forEach((criteriaCodeId) => {
        if (criteriaConditionsMap.has(criteriaCodeId)) {
            let criteriaConditionsArray = criteriaConditionsMap.get(criteriaCodeId);
            criteriaConditionsArray.forEach(criteriaCondition => {
                if (criteriaCondition.applied) {
                    relevantCriteriaConditions.push(criteriaCondition);
                }
            });

        }
    });
    if (relevantCriteriaConditions.length > 0) {
        whereString += " ( ";
        for(let i = 0; i < relevantCriteriaConditions.length; i++) {
            let criteriaCondition = relevantCriteriaConditions[i];
            let criteriaColumn = criteriaCodeArray.find(criteriaCode => {
                return criteriaCode.id === criteriaCondition.criteriaId;
            });
            if (typeof criteriaColumn !== 'undefined') {
                whereString += "( " + criteriaColumn.display_column_name + "::int > " + criteriaCondition.lowerBound +
                    " AND " + criteriaColumn.display_column_name + "::int < " + criteriaCondition.upperBound + " ) ";
                if (i !== relevantCriteriaConditions.length -1) {
                    whereString += " OR ";
                }
            } else {
                whereString += " (1 = 1) ";
                break;
            }
        }
        whereString += " )";
    }
    return whereString;
};

export {
    buildNormalLayerQuery,
    buildGradientLayerQuery,
    buildCriteriaQuery,
    getCriteriaData,
    buildNormalLayerConditionsQuery,
    buildNormalLayerQueryCarto
}
