import svgShapeArray from './svgShapeArray'
//import idleTimeout from './idleTimeoutState';

let initialState = {
    selectedMapLegendLayerId: '-1',
    selectedMapTab: "Carto",
    mapCanvasD3: {
        width: 1250,
        height: 700,
        zoom: 1.7,
        projection: 1,
        center_lat: 38.648911,
        center_lng: -94.921875,
        translateX: -80,
        translateY: 20,
        fromMinimap: false
    },
    colorPalette: {
        show: false,
        selectedShapeConfiguration: {},
        defaultFillColor: "none",
        defaultBorderColor: "#000000"
    },
    layerCategories: {expandMap: new Map([["L",true],["B",true]])},
    normalLayers: {
        expandedCriteriaIdSet: new Set (),

        expandedLayerIdSet: new Set (),
        //selectedLayerIdSet: new Set ([ 1,38,39 ]),
        selectedLayerIdSet: new Set (),
        //selectedCriteriaIdSet: set ( [CriteriaId1,CriteriaId2,CriteriaId3,...] )
        selectedCriteriaIdSet: new Set (),
        //selectedCriteriaCodeIdMap: Map( [[layerid, criteriaCodeid],......] )
        selectedCriteriaCodeIdMap: new Map(),
        //selectedCriteriaIdMap: Map ( [layerId, [criteriaCodeid,set(CriteriaId)],...............] )
        selectedCriteriaIdMap: new Map(),
        criteriaConditionsMap: new Map(),
        expandSelectedLayerId: null,
        cartoInclusiveWhereString: new Map()
    },
    gradientLayers: {
        expanded: true,
        expandedDataSetIdSet: new Set([6]),
        filterExpandedLayerIdSet: new Set([1]),
        selectedGradLayerIdSet: new Set(),
//         selectedGradLayerIdSet: new Set([1]),
        //[[<gradLayerId: array[{filterSign: <filter sign>, filterValue: <filter value>}]>]]
        gradFilterMap: new Map()
    },
    selectedPrintControlLayerId: 'normal-39',
    //todo: replace layers with a bettername such as: d3layers or d3rendering
    layers: {
        visibleLayerKeySet: new Set(),
        borderWidthMap: new Map()
    },
    d3LayerConfig: {
        disabledLayerIdSet: new Set(),
        layerConfigMap: new Map(),
        disabledGradientLayerIdSet: new Set(),
        gradientLayerConfigMap: new Map()
    },
    d3MapToolOptions: {
        d3MapTextTool: {
            optionDialogOpen: false,
            defaultTextToolConfig: {fontSize: "16", fontFamily: "Times New Roman", bold: false,
                italic: false, rotate: 0, fillColor: "#000",
                textContent: "This could be a title (or any text annotation) for the Map", underline: false},
            activeTextId: 0,
            //textToolConfigMap: Map([[<text ID>, <config JSON object>]])
            textToolConfigMap: new Map()
        },
        d3MapRectangleTool: {
            drawingCanvasOn: false,
            rectOptionDialogOpen: false,
            defaultStroke: "#0000FF",
            defaultStrokeWidth: "7",
            activeRectId: 0,
            //rectToolConfigMap: Map([[<rectangle ID>, <config JSON object>]])
            rectToolConfigMap: new Map()
        }
    },
    layerDataListing: {
        page: 1,
        pageSize: 10,
        search: null,
        order: {
            name: "ogc_fid", //for index (column name, example: fcntycd)
            dir: "asc" //or desc
        },
        // tableColumns: [],
        selectedFPLayerIdSet: new Set(),
        selectedFPColumnIdSet: new Set(),
        //selectedFPColumnIdMap: Map([<layer ID>, Set([<column ID>,...])])
        selectedFPColumnIdMap: new Map(),
        fetchingStart: false,
        layerDataQueryResults: {},
        expandedFieldPickerLayerIdSet: new Set(),
        showStateCountyDataPicker: false,
        dataLevelExpandMap: new Map([["state", true], ["county", true]]),
        expandedDataSetIdSet: new Set(),
        selectedAddtnlStateGradLayerIdSet: new Set(),
        selectedAddtnlCountyGradLayerIdSet: new Set(),
        autoRefresh: true,
        invertQuery: false,
        shouldUpdate: false,
        infoWindowSelectedId: null
    },
    ref : {
        svgShapes: svgShapeArray,
        dataInitialized: false,
        layerGroupArray: [],
        layerArray: [],
        layerFilteringCriteriaArray: [],
        criteriaCodeArray: [],
        criteriaCodeLookupMap: new Map(),
        gradientDataSetArray: [],
        gradientLayerArray: []
    },
    layerConfigHistory: {
        savedLayerConfigList: []
    },
    toolbarOptions: {
        showSaveLayerConfigDialog: false,
        showSavePdfDialog: false,
        showRenderStatusPanel: false,
        showPrintMapLegend: false,
        showCanvasAttributesDialog: false,
        showSaveConfigDuplicateNameError: false,
        showMinimapPanel: false,
        renderStatusLayerList: [],
        disableMouseZoom: false,
        showSelectedLayer: false
    },
    sidebarOptions: {
        showSidebarButtons: true,
        showSavedLayerConfigPanel: false,
        showSelectLayersPanel: true,
        showLayerPrintControlPanel: false,
        showFeedbackPanel: false,
        showHelpPanel: false,
        showRemoveConfigDialog: false,
        userSavedLayerConfig: null,
        helpList: []
    },
    mapVariables: {
        center: {
            lat: 38.648911,
            lng: -94.921875
        },
        minZoomLevel: 3,
        zoomLevel: 5,
        shouldZoomMap: true,
        shouldRefresh: false,
        rightClickContextMenuPosition: null,
        rightClickCoordinates: null,
        rightClickedMarker: null,
        rightClickedShape: null,
        mapMarkers: [],
        mapMarkerLocations: [],
        mapShapes: [],
        enteredLat: null,
        enteredLng: null,
        dropMarkerDialogVisible: false,
        dropMarkerType: null,
        infoWindow: null
    },
    feedbackForm: {
        result: null,
        successMessage: null
    },
    captcha: {
        gRecaptchaResponse: null
    },
    idleTimeout: {
        finalTimeout: 15,
        warnTimeout: 10,
        isIdle: false,
        remaining: null,
        lastActive: null,
        elapsed: null
    },
    printMapLegend: {
        expandedLayerIdSet: new Set(),
        expandedGradLayerIdSet: new Set()
    },
    printInfoBox: {
        maxInfoBox: 5,
        printInfoBoxSet: new Set(),
        maxLimitDialogBoxOpen: false
    }
    //idleTimeout: idleTimeout
};

export default initialState
