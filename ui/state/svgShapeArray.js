let svgShapeArray = [
 {
    "id": 1,
    "groupType": "POINT",
    "name": "Circle",
    "description": "",
//    "svgTag": "circle",
//    "svgAttributes": [{
//        name: "r",
//        value: "1"
//     }],
//     "svgOtherAttributes" : [{
//          name: "cx",
//          value: "1"
//     },{
//         name: "cy",
//         value: "1"
//     },{
//         name: "r",
//         value: "0.8"
//     }]
     "svgTag": "path",
     "svgAttributes": [{
         name: "d",
         value: "M0.19999999999999996,1a0.8,0.8 0 1,0 1.6,0a0.8,0.8 0 1,0 -1.6,0"
     }]
 },
 {
    "id": 2,
    "groupType": "POINT",
    "name": "Square",
    "description": "",
//    "svgTag": "rect",
//    "svgAttributes": [{
//        name: "height",
//        value: "2"
//    },{
//        name: "width",
//        value: "2"
//    }],
//     "svgOtherAttributes" : [{
//         name: "y",
//         value: "0.0"
//     },{
//         name: "x",
//         value: "0.0"
//     }]
    "svgTag": "path",
    "svgAttributes": [{
        name: "d",
        value: "m0.20506,0.20844l0,1.59884l1.5911,0l0,-1.59884l-1.5911,0z"
    }]
 },
 {
    "id": 3,
    "groupType": "POINT",
    "name": "Triangle",
    "description": "",
    // "svg": "<rect stroke=\"black\" height=\"10\" width=\"10\" y=\"0\" x=\"0\" fill=\"none\"/>"
     "svgTag": "path",
     "svgAttributes": [{
         name: "d",
         value: "M1 0.2 L0.2 1.8 L1.8 1.8 Z"
     }]
 },
 {
   "id": 4,
   "groupType": "POLYGON",
   "name": "Polygon",
   "svg": "<path stroke=\"#000000\" fill=\"none\" d=\"m4,7c2,0 5,15 5,15c0,0 15,-3 16,-3c1,0 7,-9 7,-9c0,0 -10,-8 -10,-8c-1,0 -18,6 -19,6l1,-1z\" id=\"svg_2\"/>",
   "description": ""
 },
 {
     "id": 5,
     "groupType": "POINT",
     "name": "Pentagon",
     "svg": "<path stroke=\"#000000\" fill=\"#FF0000\" stroke-width=\"0.5\" stroke-dasharray=\"null\" stroke-linejoin=\"null\" stroke-linecap=\"null\" d=\"m10.02185,2.7043c0,0 -7.21028,7.28617 -7.21028,7.28617c0,0 2.73231,8.08309 2.73231,8.08309c0,0 9.03181,0 9.03181,0c0,0 2.69436,-8.08309 2.69436,-8.08309c0,0 -7.24822,-7.28617 -7.24822,-7.28617l0.00001,0z\" id=\"svg_1\"/>",
     "description": "",
     "svgTag": "path",
     "svgAttributes": [{
         name: "d",
         value: "m1.00219,0.27043c0,0 -0.72103,0.72862 -0.72103,0.72862c0,0 0.27323,0.80831 0.27323,0.80831c0,0 0.90318,0 0.90318,0c0,0 0.26943,-0.80831 0.26943,-0.80831c0,0 -0.72482,-0.72862 -0.72482,-0.72862l0,0l0,0l0.00001,0z"
     }]
 },
 {
     "id": 6,
     "groupType": "POINT",
     "name": "Star",
     "svg": "<path fill=\"#FF0000\" stroke=\"#000000\" stroke-width=\"0.5\" stroke-dasharray=\"null\" stroke-linejoin=\"null\" stroke-linecap=\"null\" d=\"m10.01862,2.01717c0,0 -1.96393,5.7651 -1.96393,5.7651c0,0 -6.14521,0.15838 -6.16467,0.15793c-0.01945,-0.00045 4.86594,3.6749 4.86594,3.6749c0,0 -1.7422,5.86012 -1.76165,5.85968c-0.01945,-0.00045 5.02432,-3.51563 5.02432,-3.51563c0,0 5.03654,3.51607 5.01709,3.51563c-0.01945,-0.00045 -1.81778,-5.85968 -1.83723,-5.86013c-0.01945,-0.00045 4.92929,-3.67401 4.90983,-3.67446c-0.01945,-0.00045 -6.09408,-0.09458 -6.09408,-0.09458c0,0 -1.99561,-5.82845 -1.99561,-5.82845l-0.00001,0.00001z\" id=\"svg_11\"/>",
     "description": "",
     "svgTag": "path",
     "svgAttributes": [{
         name: "d",
         value: "m1.00186,0.20172c0,0 -0.19639,0.57651 -0.19639,0.57651c0,0 -0.61452,0.01584 -0.61647,0.01579c-0.00194,-0.00005 0.4866,0.36749 0.4866,0.36749c0,0 -0.17422,0.58601 -0.17617,0.58597c-0.00194,-0.00005 0.50243,-0.35156 0.50243,-0.35156c0,0 0.50366,0.3516 0.50171,0.35156c-0.00194,-0.00005 -0.18178,-0.58597 -0.18372,-0.58601c-0.00195,-0.00005 0.49293,-0.36741 0.49098,-0.36745c-0.00194,-0.00005 -0.60941,-0.00946 -0.60941,-0.00946c0,0 -0.19956,-0.58284 -0.19956,-0.58284l0,0z"
     }]
 },
 {
     "id": 7,
     "groupType": "POINT",
     "name": "Hexagon",
     "svg": "<path fill=\"none\" stroke-width=\"null\" stroke-dasharray=\"null\" stroke-linejoin=\"null\" stroke-linecap=\"null\" d=\"m0.82292,10.0734l3.94597,-8.2497l10.52258,0l3.94597,8.2497l-3.94597,8.2497l-10.52258,0l-3.94597,-8.2497z\" id=\"svg_1\" stroke=\"#000000\"/>",
     "description": "",
     "svgTag": "path",
     "svgAttributes": [{
         name: "d",
         value: "m0.50021,0.3016l0.99887,-0.00226l0.3041,0.69371l-0.3041,0.70797l-1.00255,0l-0.29935,-0.70321l0.30303,-0.69621l0,0z"
     }]
 },
 {
     "id": 8,
     "groupType": "POINT",
     "name": "Trapezoid",
     "svg": "<path id=\"svg_1\" d=\"m3.04158,19.05568l2.61858,-17.34456l8.7286,0l2.61858,17.34456l-13.96575,0z\" stroke-linecap=\"null\" stroke-linejoin=\"null\" stroke-dasharray=\"null\" stroke-width=\"null\" stroke=\"#000000\" fill=\"none\"/>",
     "description": "",
     "svgTag": "path",
     "svgAttributes": [{
         name: "d",
         value: "m0.30416,1.90557l0.26186,-1.73446l0.87286,0l0.26185,1.73446l-1.39657,0l0,0z"
     }]
 },
 {
     "id": 9,
     "groupType": "POINT",
     "name": "Diamond",
     "svg": "<path id=\"svg_1\" d=\"m9.98874,2.94213l-5.99048,7.08168l5.96923,6.98288l6.08186,-6.98288l-6.06061,-7.08168z\" stroke-linecap=\"null\" stroke-linejoin=\"null\" stroke-dasharray=\"null\" stroke-width=\"0.5\" stroke=\"#000000\" fill=\"#FF0000\"/>",
     "description": "",
     "svgTag": "path",
     "svgAttributes": [{
         name: "d",
         value: "m0.99887,0.29421l-0.59904,0.70817l0.59692,0.69829l0.60819,-0.69829l-0.60607,-0.70817z"
     }]
 },
 {
     "id": 10,
     "groupType": "POINT",
     "name": "Left Triangle",
     "svg": "<path id=\"svg_1\" d=\"m14.98375,1.88625l-0.03251,16.10592c0,0 -11.92004,-7.9123 -11.92004,-7.95454c0,-0.04224 11.95254,-8.15138 11.95254,-8.15138z\" stroke-linecap=\"null\" stroke-linejoin=\"null\" stroke-dasharray=\"null\" stroke-width=\"0.5\" stroke=\"#000000\" fill=\"#FF0000\"/>",
     "description": "",
     "svgTag": "path",
     "svgAttributes": [{
         name: "d",
         value: "m1.49838,0.18863l-0.00326,1.61059c0,0 -1.192,-0.79123 -1.192,-0.79546c0,-0.00422 1.19525,-0.81513 1.19525,-0.81513l0.00001,0z"
     }]
 },
 {
     "id": 11,
     "groupType": "POINT",
     "name": "Right Triangle",
     "svg": "<path id=\"svg_1\" d=\"m3.99416,1.92004l-0.02406,16.1566l13.09289,-8.06691l-13.06883,-8.08969z\" stroke-linecap=\"null\" stroke-linejoin=\"null\" stroke-dasharray=\"null\" stroke-width=\"0.5\" stroke=\"#000000\" fill=\"#FF0000\"/>",
     "description": "",
     "svgTag": "path",
     "svgAttributes": [{
         name: "d",
         value: "m0.39942,0.192l-0.00241,1.61566l1.30929,-0.80669l-1.30688,-0.80897z"
     }]
 },
 {
     "id": 12,
     "groupType": "POINT",
     "name": "Bottom Triangle",
     "svg": "<path id=\"svg_1\" d=\"m2.89323,3.95577l14.11345,-0.01382l-6.98288,13.06474l-7.13057,-13.05091z\" stroke-linecap=\"null\" stroke-linejoin=\"null\" stroke-dasharray=\"null\" stroke-width=\"0.5\" stroke=\"#000000\" fill=\"#FF0000\"/>",
     "description": "",
     "svgTag": "path",
     "svgAttributes": [{
         name: "d",
         value: "m0.28932,0.39558l1.41135,-0.00138l-0.69829,1.30647l-0.71306,-1.30509l0,0z"
     }]
 }
];

export default svgShapeArray;