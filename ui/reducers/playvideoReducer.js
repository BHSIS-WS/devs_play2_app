import initialState from '../state/initialState'

const playvideoReducer = (state = initialState.playvideoOptions , action) => {

    switch(action.type) {
        case "TOGGLE_VISIBILITY_FOR_PLAYVIDEO":
            return {
                ...state,
                showPlayvideoPanel: !state.showPlayvideoPanel
            };
        default:
            return state;
    }
};


export default playvideoReducer;