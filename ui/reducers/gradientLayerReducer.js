import initialState from '../state/initialState'


const cloneGradientFilterMap=function(gradientFilterMap) {
    if (!gradientFilterMap) return gradientFilterMap;
    let newGradientFilterMap = new Map();
      gradientFilterMap.forEach( (filterObjectArray,layerId,map)=>{
          let newFilterObjectArray = filterObjectArray.map ( filterObj =>{
              return {...filterObj};
          });
          newGradientFilterMap.set(layerId,newFilterObjectArray);
    });
    return newGradientFilterMap;
};

const gradientLayerReducer = (state = initialState.gradientLayers, action) => {

    let newFilterExpandedLayerIdSet,gradLayerId,newExpandedDataSetIdSet,dataSetId,
        newSelectedGradLayerIdSet, newGradFilterMap, filterEntryIndex, filterEntryArray, filterEntry;

    switch(action.type) {
        case 'TOGGLE_GRADIENT_CATEGORY':
            return {...state, expanded: !state.expanded};
        case 'TOGGLE_GRADIENT_FILTER':
            newFilterExpandedLayerIdSet = new Set(state.filterExpandedLayerIdSet);
            gradLayerId = action.gradLayerId;
            if (newFilterExpandedLayerIdSet.has(gradLayerId)) {
                newFilterExpandedLayerIdSet.delete(gradLayerId);
            } else {
                newFilterExpandedLayerIdSet.add(gradLayerId);
            }
            return {...state, filterExpandedLayerIdSet: newFilterExpandedLayerIdSet};
        case 'ADD_GRADIENT_FILTER_ENTRY':
            gradLayerId = action.gradLayerId;
            newGradFilterMap = cloneGradientFilterMap (state.gradFilterMap);
            let newFilterEntry = {filterSign: "<", filterValue: ""};
            if (newGradFilterMap.has(gradLayerId)) {
                filterEntryArray = newGradFilterMap.get(gradLayerId);
                filterEntryArray.push(newFilterEntry);
            } else {
                let firstFilterEntry = {filterSign: action.firstFilterSign, filterValue: action.firstFilterValue};
                filterEntryArray = [firstFilterEntry];
                filterEntryArray.push(newFilterEntry);
                newGradFilterMap.set(gradLayerId, filterEntryArray);
            }
            return {...state, gradFilterMap: newGradFilterMap};
        case 'DELETE_GRADIENT_FILTER_ENTRY':
            gradLayerId = action.gradLayerId;
            filterEntryIndex = action.filterEntryIndex;
            newGradFilterMap = cloneGradientFilterMap(state.gradFilterMap);
            filterEntryArray = newGradFilterMap.get(gradLayerId);
            filterEntryArray.splice(filterEntryIndex, 1);
            return {...state, gradFilterMap: newGradFilterMap};
        case 'CHANGE_GRADIENT_FILTER_ENTRY_SIGN':
            gradLayerId = action.gradLayerId;
            filterEntryIndex = action.filterEntryIndex;
            let filterSign = action.filterSign;
            newGradFilterMap = cloneGradientFilterMap(state.gradFilterMap);
            if (newGradFilterMap.has(gradLayerId)) {
                filterEntryArray = newGradFilterMap.get(gradLayerId);
                filterEntry = filterEntryArray[filterEntryIndex];
                filterEntry.filterSign = filterSign;
            } else {
                let newFilterEntry = {filterSign: filterSign, filterValue: ""};
                filterEntryArray = [newFilterEntry];
                newGradFilterMap.set(gradLayerId, filterEntryArray);
            }
            return {...state, gradFilterMap: newGradFilterMap};
        case 'CHANGE_GRADIENT_FILTER_ENTRY_VALUE':
            gradLayerId = action.gradLayerId;
            filterEntryIndex = action.filterEntryIndex;
            let filterValue = action.filterValue;
            newGradFilterMap = cloneGradientFilterMap(state.gradFilterMap);

            if (newGradFilterMap.has(gradLayerId)) {
                filterEntryArray = newGradFilterMap.get(gradLayerId);
                filterEntry = filterEntryArray[filterEntryIndex];
                filterEntry.filterValue = filterValue;
            } else {
                let newFilterEntry = {filterSign: "<", filterValue: filterValue};
                filterEntryArray = [newFilterEntry];
                newGradFilterMap.set(gradLayerId, filterEntryArray);
            }
            return {...state, gradFilterMap: newGradFilterMap};
        case 'TOGGLE_GRADIENT_DATASET':
            newExpandedDataSetIdSet = new Set(state.expandedDataSetIdSet);
            dataSetId = action.dataSetId;
            if(newExpandedDataSetIdSet.has(dataSetId)) {
                newExpandedDataSetIdSet.delete(dataSetId);
            } else {
                newExpandedDataSetIdSet.add(dataSetId);
            }
            return {...state, expandedDataSetIdSet: newExpandedDataSetIdSet};
        case 'TOGGLE_GRADIENT_LAYER':
            newSelectedGradLayerIdSet = new Set(state.selectedGradLayerIdSet);
            gradLayerId = action.gradLayerId;
            if (newSelectedGradLayerIdSet.has(gradLayerId)) {
                newSelectedGradLayerIdSet.delete(gradLayerId);
            } else {
                newSelectedGradLayerIdSet.add(gradLayerId);
            }
            return {...state, selectedGradLayerIdSet: newSelectedGradLayerIdSet};
        case 'CLEAR_GRADIENT_LAYER_SELECT':
             newSelectedGradLayerIdSet = new Set(state.selectedGradLayerIdSet);
             newSelectedGradLayerIdSet.clear();
             return {...state, selectedGradLayerIdSet: newSelectedGradLayerIdSet};
        case "SET_GRADIENT_LAYERS_STATE_FROM_CONFIG":
            return {
                expanded: action.gradientLayers.expanded,
                expandedDataSetIdSet: action.gradientLayers.expandedDataSetIdSet,
                filterExpandedLayerIdSet: action.gradientLayers.filterExpandedLayerIdSet,
                gradFilterMap: action.gradientLayers.gradFilterMap,
                selectedGradLayerIdSet: action.gradientLayers.selectedGradLayerIdSet
            };
        default:
            return state;
    }
};

export default gradientLayerReducer