import initialState from '../state/initialState'



const feedbackReducer = (state = initialState.feedbackForm, action) => {
    switch(action.type) {
        case 'FEEDBACK_VALIDATION_ERROR':
            return {
                result: null,
                successMessage: null
            }
        case 'PROCESS_FEEDBACK_FORM_SUCCESS':
            const result = action.result != null && action.result.length > 0 ? action.result : null;
            const resultMsg = action.message != null && action.message.length > 0 ? action.message : null;
            return {
                result: result,
                successMessage: resultMsg
            }
        default:
            return state;
    }
}

export default feedbackReducer;