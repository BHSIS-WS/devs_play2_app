import initialState from '../state/initialState'
import mapContentModel from "../others/MapContentModel";
import printContentModel from "../others/PrintContentModel";



//const getFileteringCriteriaIdArray = (layerCriteriaArray, layerId, criteriaCodeId) => {
//    if (!layerCriteriaArray) return null;
//    let criteriaArray = layerCriteriaArray.filter(criteria=>
//        (criteria.fusion_map_layer_id==layerId && criteria.criteria_code_id==criteriaCodeId));
//    let criteriaIdArray = criteriaArray.map(criteria => criteria.id);
//    return criteriaIdArray;
//}

const deleteCriteriaIdFromSelectedCriteriaIdMap = (selectedCriteriaIdMap, layerId, criteriaCodeId, criteriaIdToDelete) => {
    if (!selectedCriteriaIdMap) return null;
    let criteriaIdToCriteriaCodeMap = selectedCriteriaIdMap.get(layerId);
    if(criteriaIdToCriteriaCodeMap) {
        let criteriaIdSet = criteriaIdToCriteriaCodeMap.get(criteriaCodeId);
        if (criteriaIdSet) {
            criteriaIdSet.delete(criteriaIdToDelete);
        }
    }

    return selectedCriteriaIdMap;
};

const addCriteriaIdToSelectedCriteriaIdMap = (selectedCriteriaIdMap, layerId, criteriaCodeId, criteriaIdToAdd) => {
    if (!selectedCriteriaIdMap) return null;
    let criteriaIdToCriteriaCodeMap = selectedCriteriaIdMap.get(layerId);
    if(!criteriaIdToCriteriaCodeMap) {
        criteriaIdToCriteriaCodeMap = new Map([[criteriaCodeId, new Set([criteriaIdToAdd])]]);
        selectedCriteriaIdMap.set(layerId, criteriaIdToCriteriaCodeMap);
    } else {
        let criteriaIdSet = criteriaIdToCriteriaCodeMap.get(criteriaCodeId);
        if (!criteriaIdSet) {
            criteriaIdSet = new Set();
            criteriaIdToCriteriaCodeMap.set(criteriaCodeId, criteriaIdSet);
        }
        criteriaIdSet.add(criteriaIdToAdd);
    }

    return selectedCriteriaIdMap;
};

const getSelectedCriteriaIdSetForLayer = (selectedCriteriaIdMap, layerId) => {
    if (!selectedCriteriaIdMap) return null;
    let criteriaIdToCriteriaCodeMap = selectedCriteriaIdMap.get(layerId);
    let criteriaIdSet = new Set();
    if(criteriaIdToCriteriaCodeMap) {
        criteriaIdToCriteriaCodeMap.forEach((criteriaIdSet, criteriaCodeId, map) => {
            criteriaIdSet.forEach(criteriaId => criteriaIdSet.add(criteriaId));
        });
    }
    return criteriaIdSet;
};

const getSelectedCriteriaIdSetForLayerNCriteriaCode = (selectedCriteriaIdMap, layerId, criteriaCodeId) => {
    if (!selectedCriteriaIdMap) return null;
    let criteriaIdToCriteriaCodeMap = selectedCriteriaIdMap.get(layerId);
    let criteriaIdSet = null;
    if (criteriaIdToCriteriaCodeMap) {
        criteriaIdSet = criteriaIdToCriteriaCodeMap.get(criteriaCodeId);
    }
    return criteriaIdSet;
};

const getCriteriaIdSetSizeInSelectedCriteriaIdMap = (selectedCriteriaIdMap, layerId) => {
    if(!selectedCriteriaIdMap) return 0;
    let criteriaIdToCriteriaCodeMap = selectedCriteriaIdMap.get(layerId);
    if(!criteriaIdToCriteriaCodeMap) {
        return 0;
    } else {
        let criteriaIdCount = 0;
        criteriaIdToCriteriaCodeMap.forEach((criteriaIdSet, criteriaCodeId, map) => {
            criteriaIdCount += criteriaIdSet.size;
        });
        return criteriaIdCount;
    }
};

const cloneSelectedCriteriaIdMap = (selectedCriteriaIdMap) => {
    let newSelectedCriteriaIdMap = new Map();
    let newCriteriaIdToCriteriaCodeMap = null;
    let newCriteriaIdSet = null;
    selectedCriteriaIdMap.forEach((criteriaIdToCriteriaCodeMap, layerId, map) => {
        newCriteriaIdToCriteriaCodeMap = new Map();
        newSelectedCriteriaIdMap.set(layerId, newCriteriaIdToCriteriaCodeMap);
        criteriaIdToCriteriaCodeMap.forEach((criteriaIdSet, criteriaCodeId, map) => {
            newCriteriaIdSet = new Set();
            newCriteriaIdToCriteriaCodeMap.set(criteriaCodeId, newCriteriaIdSet);
            criteriaIdSet.forEach(criteriaId => {
                newCriteriaIdSet.add(criteriaId);
            });
        });
    });
    return newSelectedCriteriaIdMap;
};



const normalLayerReducer = (state = initialState.normalLayers , action) => {
//    console.log("in layerReducer", action);

    let layerId,criteriaCodeId,expandedLayerIdSet,layerCriteriaIdArray, newLayerIdSet,selectedLayerIdSet,newCriteriaCodeIdMap,
        selectedCriteriaIdSet, selectedCriteriaIdMap, expandSelectedLayerId, selectedCriteriaCodeIdMapSet, newExpandedCriteriaIdSet,
        newCriteriaConditionsMap, criteriaConditionArray, newselectedLayerIdSet, newexpandSelectedLayerId, newCartoWhereStringMap;

    let criteriaIdSet;

    switch (action.type) {
        case 'TOGGLE_CRITERIA_DISPLAY':
            layerId=action.layerId;
//            expandedLayerIdSet = state.expandedLayerIdSet;
            expandedLayerIdSet = state.expandedLayerIdSet;
            newLayerIdSet = new Set(expandedLayerIdSet);
            if ( newLayerIdSet.has (layerId) ){
                newLayerIdSet.delete(layerId);
            }else {
                newLayerIdSet.add(layerId);
            }
            state={...state, expandedLayerIdSet: newLayerIdSet };
            return state;

        case 'CHANGE_CRITERIA_CODE':
            layerId=action.layerId;
            criteriaCodeId=action.criteriaCodeId;

            newCriteriaCodeIdMap= new Map(state.selectedCriteriaCodeIdMap);
            selectedCriteriaCodeIdMapSet = newCriteriaCodeIdMap.get(layerId);
            if (selectedCriteriaCodeIdMapSet) {
                newCriteriaCodeIdMap.set(layerId,new Set([criteriaCodeId,  ...selectedCriteriaCodeIdMapSet]));
            } else {
                newCriteriaCodeIdMap.set(layerId,new Set([criteriaCodeId]));
            }

            newExpandedCriteriaIdSet = new Set(state.expandedCriteriaIdSet);
            newExpandedCriteriaIdSet.clear();
            newExpandedCriteriaIdSet.add(criteriaCodeId);

            state={...state, selectedCriteriaCodeIdMap: newCriteriaCodeIdMap, expandedCriteriaIdSet:  newExpandedCriteriaIdSet };
            return state;

        case 'REMOVE_CRITERIA':
            layerId=action.layerId;
            criteriaCodeId=action.criteriaCodeId;
            newCriteriaIdSet = new Set(state.selectedCriteriaIdSet);
            newCriteriaIdMap = cloneSelectedCriteriaIdMap(state.selectedCriteriaIdMap);
            newCriteriaCodeIdMap= new Map(state.selectedCriteriaCodeIdMap);
            selectedCriteriaCodeIdMapSet = newCriteriaCodeIdMap.get(layerId);
            if (selectedCriteriaCodeIdMapSet) {
                selectedCriteriaCodeIdMapSet.delete(criteriaCodeId);
                if (selectedCriteriaCodeIdMapSet.size === 0) {
                    newCriteriaCodeIdMap.delete(layerId);
                } else {
                    newCriteriaCodeIdMap.set(layerId, selectedCriteriaCodeIdMapSet);
                }
            }

            criteriaIdSet = getSelectedCriteriaIdSetForLayerNCriteriaCode(newCriteriaIdMap, layerId, criteriaCodeId);
            if (criteriaIdSet && criteriaIdSet.size > 0) {
                criteriaIdSet.forEach(criteriaId => {
                    criteriaIdSet.delete(criteriaId);
                    newCriteriaIdSet.delete(criteriaId);
                });
            }

            newExpandedCriteriaIdSet = new Set(state.expandedCriteriaIdSet);
            newExpandedCriteriaIdSet.delete(criteriaCodeId);

            state={...state, selectedCriteriaCodeIdMap: newCriteriaCodeIdMap, selectedCriteriaIdSet: newCriteriaIdSet,
                selectedCriteriaIdMap: newCriteriaIdMap, expandedCriteriaIdSet: newExpandedCriteriaIdSet };
            return state;

        case 'TOGGLE_LAYER_SELECT':
            layerId=action.layerId;
            selectedLayerIdSet=state.selectedLayerIdSet;
            newLayerIdSet = new Set(selectedLayerIdSet);
            selectedCriteriaIdMap = state.selectedCriteriaIdMap;
            selectedCriteriaIdSet = state.selectedCriteriaIdSet;
            newCriteriaIdSet = new Set(state.selectedCriteriaIdSet);
            if ( newLayerIdSet.has (layerId) ){
                newLayerIdSet.delete(layerId);
                let criteriaIdSet = getSelectedCriteriaIdSetForLayer(selectedCriteriaIdMap, layerId);
                criteriaIdSet.forEach(elem => {newCriteriaIdSet.delete(elem);});
            }else {
                newLayerIdSet.add(layerId);
            }

//            newCriteriaIdMap = new Map(selectedCriteriaIdMap);
            newCriteriaIdMap = cloneSelectedCriteriaIdMap(selectedCriteriaIdMap);
//            if (newCriteriaIdMap.has(layerId)) {
//                newCriteriaIdMap.delete(layerId);
//            } else {
//                newCriteriaIdMap.set(layerId, new Map());
//            }
            if (!newCriteriaIdMap.has(layerId)) {
                newCriteriaIdMap.set(layerId, new Map());
            }
            expandSelectedLayerId = newLayerIdSet.has(state.expandSelectedLayerId) ? state.expandSelectedLayerId : null;
            state={...state, selectedLayerIdSet: newLayerIdSet, selectedCriteriaIdSet: newCriteriaIdSet,
                selectedCriteriaIdMap: newCriteriaIdMap, expandSelectedLayerId: expandSelectedLayerId};
            return state ;

        case 'CLEAR_LAYER_SELECT':
            newselectedLayerIdSet=new Set(state.selectedLayerIdSet);
            newselectedLayerIdSet.clear();
            let newselectedCriteriaIdMap = new Set(state.selectedCriteriaIdMap);
            newselectedCriteriaIdMap.clear();
            newCriteriaCodeIdMap= new Map(state.selectedCriteriaCodeIdMap);
            newCriteriaCodeIdMap.clear();
            let newselectedCriteriaIdSet = new Set(state.selectedCriteriaIdSet);
            newselectedCriteriaIdSet.clear();
            state={...state, selectedLayerIdSet: newselectedLayerIdSet, selectedCriteriaIdMap: newselectedCriteriaIdMap,
                    selectedCriteriaCodeIdMap: newCriteriaCodeIdMap, selectedCriteriaIdSet: newselectedCriteriaIdSet};
            return state ;

        case "TOGGLE_CRITERIA_EXPANDED":

            newExpandedCriteriaIdSet = new Set(state.expandedCriteriaIdSet);
            if (newExpandedCriteriaIdSet.has(action.criteriaCodeId)) {
                //newExpandedCriteriaIdSet.delete(action.criteriaCodeId);
                newExpandedCriteriaIdSet.clear();
            } else {
                newExpandedCriteriaIdSet.clear();
                newExpandedCriteriaIdSet.add(action.criteriaCodeId);
            }

            state={...state, expandedCriteriaIdSet: newExpandedCriteriaIdSet};
            return state;

        case 'TOGGLE_CRITERIA_SELECT':
            let criteriaId=action.criteriaId;
            selectedCriteriaIdSet=state.selectedCriteriaIdSet;
            let newCriteriaIdSet = new Set(selectedCriteriaIdSet);
            layerId = action.layerId;
            selectedLayerIdSet = state.selectedLayerIdSet;
            newLayerIdSet = new Set(selectedLayerIdSet);
            criteriaCodeId = action.criteriaCodeId;
            selectedCriteriaIdMap = state.selectedCriteriaIdMap;

            let newCriteriaIdMap = cloneSelectedCriteriaIdMap(selectedCriteriaIdMap);
            if ( newCriteriaIdSet.has (criteriaId) ){
                newCriteriaIdSet.delete(criteriaId);
                deleteCriteriaIdFromSelectedCriteriaIdMap(newCriteriaIdMap, layerId, criteriaCodeId, criteriaId);
            }else {
                newCriteriaIdSet.add(criteriaId);
                addCriteriaIdToSelectedCriteriaIdMap(newCriteriaIdMap, layerId, criteriaCodeId, criteriaId);
                if (!newLayerIdSet.has(layerId)) {
                    newLayerIdSet.add(layerId);
                }
            }
            expandSelectedLayerId = newLayerIdSet.has(state.expandSelectedLayerId) ? state.expandSelectedLayerId : null;
            state={...state, selectedCriteriaIdSet: newCriteriaIdSet, selectedLayerIdSet: newLayerIdSet,
                selectedCriteriaIdMap: newCriteriaIdMap, expandSelectedLayerId: expandSelectedLayerId};
            return state;

        case 'SELECT_ALL_CRITERIA':
            layerId = action.layerId;
            criteriaCodeId = action.criteriaCodeId;
            layerCriteriaIdArray = action.layerCriteriaIdArray;
            newLayerIdSet = new Set(state.selectedLayerIdSet);
            newCriteriaIdSet = new Set(state.selectedCriteriaIdSet);
//            newCriteriaIdMap = new Map(state.selectedCriteriaIdMap);
            newCriteriaIdMap = cloneSelectedCriteriaIdMap(state.selectedCriteriaIdMap);

            if(!newLayerIdSet.has(layerId)) {
                newLayerIdSet.add(layerId);
            }
            layerCriteriaIdArray.forEach(criteriaId => {
                newCriteriaIdSet.add(criteriaId);
                addCriteriaIdToSelectedCriteriaIdMap(newCriteriaIdMap, layerId, criteriaCodeId, criteriaId);
            });

            state = {...state, selectedCriteriaIdSet: newCriteriaIdSet, selectedLayerIdSet: newLayerIdSet,
                selectedCriteriaIdMap: newCriteriaIdMap};
            return state;


        case 'SELECT_NONE_CRITERIA':
            layerId = action.layerId;
            criteriaCodeId = action.criteriaCodeId;
            newCriteriaIdSet = new Set(state.selectedCriteriaIdSet);
//            newCriteriaIdMap = new Map(state.selectedCriteriaIdMap);
            newCriteriaIdMap = cloneSelectedCriteriaIdMap(state.selectedCriteriaIdMap);
            criteriaIdSet = getSelectedCriteriaIdSetForLayerNCriteriaCode(newCriteriaIdMap, layerId, criteriaCodeId);
            if (criteriaIdSet && criteriaIdSet.size > 0) {
                criteriaIdSet.forEach(criteriaId => {
                    criteriaIdSet.delete(criteriaId);
                    newCriteriaIdSet.delete(criteriaId);
                });
            }

            state = {...state, selectedCriteriaIdSet: newCriteriaIdSet, selectedCriteriaIdMap: newCriteriaIdMap};
            return state;


        case 'SELECT_REVERSE_CRITERIA':
            layerId = action.layerId;
            criteriaCodeId = action.criteriaCodeId;
            layerCriteriaIdArray = action.layerCriteriaIdArray;
            newCriteriaIdSet = new Set(state.selectedCriteriaIdSet);
//            newCriteriaIdMap = new Map(state.selectedCriteriaIdMap);
            newCriteriaIdMap = cloneSelectedCriteriaIdMap(state.selectedCriteriaIdMap);
            layerCriteriaIdArray.forEach(criteriaId => {
                if(newCriteriaIdSet.has(criteriaId)) {
                    newCriteriaIdSet.delete(criteriaId);
                    deleteCriteriaIdFromSelectedCriteriaIdMap(newCriteriaIdMap, layerId, criteriaCodeId, criteriaId);
                } else {
                    newCriteriaIdSet.add(criteriaId);
                    addCriteriaIdToSelectedCriteriaIdMap(newCriteriaIdMap, layerId, criteriaCodeId, criteriaId);
                }
            });

            state = {...state, selectedCriteriaIdSet: newCriteriaIdSet, selectedCriteriaIdMap: newCriteriaIdMap};
            return state;

        case 'TOGGLE_SELECTED_LAYER':
            expandSelectedLayerId = action.layerId;
            return {...state, expandSelectedLayerId: expandSelectedLayerId};
        case "SET_NORMAL_LAYERS_STATE_FROM_CONFIG":
            return {
                expandSelectedLayerId: action.normalLayers.expandSelectedLayerId,
                expandedLayerIdSet: action.normalLayers.expandedLayerIdSet,
                selectedCriteriaCodeIdMap: action.normalLayers.selectedCriteriaCodeIdMap,
                selectedCriteriaIdMap: action.normalLayers.selectedCriteriaIdMap,
                selectedCriteriaIdSet: action.normalLayers.selectedCriteriaIdSet,
                selectedLayerIdSet: action.normalLayers.selectedLayerIdSet,
                expandedCriteriaIdSet: action.normalLayers.expandedCriteriaIdSet,
                criteriaConditionsMap: action.normalLayers.criteriaConditionsMap
            };
        case "INITIALIZE_DYNAMIC_FILTER":
            newCriteriaConditionsMap = new Map(state.criteriaConditionsMap);
            if (!newCriteriaConditionsMap.get(action.criteriaId)) {
                newCriteriaConditionsMap.set(action.criteriaId, [{
                    criteriaId: action.criteriaId,
                    conditionId: 0,
                    lowerBound: 0,
                    upperBound: 1,
                    applied: false
                }]);
            }
            return {
                ...state, criteriaConditionsMap: newCriteriaConditionsMap
            };
        case "REMOVE_DYNAMIC_CRITERIA_CONDITION":
            newCriteriaConditionsMap = new Map(state.criteriaConditionsMap);
            criteriaConditionArray = newCriteriaConditionsMap.get(action.criteriaId);
            criteriaConditionArray = criteriaConditionArray.filter((condition) => {
                return condition.conditionId !== action.conditionId
            });
            newCriteriaConditionsMap.set(action.criteriaId, criteriaConditionArray);
            return {
                ...state, criteriaConditionsMap: newCriteriaConditionsMap
            };
        case "ADD_DYNAMIC_CRITERIA_CONDITION":
            newCriteriaConditionsMap = new Map(state.criteriaConditionsMap);
            criteriaConditionArray = newCriteriaConditionsMap.get(action.criteriaId).slice();
            criteriaConditionArray.push({
                criteriaId: action.criteriaId,
                conditionId: action.conditionId,
                lowerBound: 0,
                upperBound: 1,
                applied: false
            });
            newCriteriaConditionsMap.set(action.criteriaId, criteriaConditionArray);
            return {
                ...state, criteriaConditionsMap: newCriteriaConditionsMap
            };
        case "REMOVE_DYNAMIC_CRITERIA":
            newCriteriaConditionsMap = new Map(state.criteriaConditionsMap);
            newCriteriaConditionsMap.delete(action.criteriaId);

            return {
                ...state, criteriaConditionsMap: newCriteriaConditionsMap
            };
        case "UPDATE_DYNAMIC_CONDITION_BOUND":
            newCriteriaConditionsMap = new Map(state.criteriaConditionsMap);
            criteriaConditionArray = newCriteriaConditionsMap.get(action.criteriaId).slice();
            criteriaConditionArray.map((criteriaCondition) => {
                if (criteriaCondition.conditionId === action.conditionId) {
                    criteriaCondition[action.bound] = action.value;
                }
            });
            newCriteriaConditionsMap.set(action.criteriaId, criteriaConditionArray);
            return {
                ...state, criteriaConditionsMap: newCriteriaConditionsMap
            };
        case "TOGGLE_DYNAMIC_CONDITION":
            newLayerIdSet = new Set(state.selectedLayerIdSet);
            newCriteriaConditionsMap = new Map(state.criteriaConditionsMap);
            criteriaConditionArray = newCriteriaConditionsMap.get(action.criteriaId).slice();
            criteriaConditionArray.map((criteriaCondition) => {
                if (criteriaCondition.conditionId === action.conditionId) {
                    criteriaCondition.applied = action.applied;
                }
            });
            newCriteriaConditionsMap.set(action.criteriaId, criteriaConditionArray);

            if (!newLayerIdSet.has(action.layerId) && action.applied) {
                newLayerIdSet.add(action.layerId);
            }
            return {
                ...state, criteriaConditionsMap: newCriteriaConditionsMap, selectedLayerIdSet: newLayerIdSet
            };
        case 'SET_CARTO_INCLUSIVE_WHERE_STRING':
            let responseCarto = action.whereString;
            layerId = action.layerId;
            newCartoWhereStringMap = new Map(state.cartoInclusiveWhereString);
            return {
                ...state,
                cartoInclusiveWhereString: newCartoWhereStringMap.set(layerId, responseCarto)
            };
        case 'REMOVE_INCLUSIVE_WHERE_STRING_FOR_CARTO':
            let responseCartoRemove = action.whereString;
            layerId = action.layerId;
            newCartoWhereStringMap = new Map(state.cartoInclusiveWhereString);
            newCartoWhereStringMap.delete(layerId);
            return {
                ...state,
                cartoInclusiveWhereString: newCartoWhereStringMap
            }
        case 'DO_NOT_FETCH_INCLUSIVE_WHERE_STRING_FOR_CARTO':
            mapContentModel.refreshGoogleMapLayers();
            return {
                ...state
            }
        default:
            return state;
    }
};

export default normalLayerReducer