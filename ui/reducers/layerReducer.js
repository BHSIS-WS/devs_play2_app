import initialState from '../state/initialState'

const layerReducer = (state = initialState.layers, action) => {
    let layerKey, newVisibleLayerKeySet, newBorderWidthMap;
    switch(action.type) {
        case 'TOGGLE_LAYER_VISIBILITY':
            layerKey = action.layerKey;
            newVisibleLayerKeySet = new Set(state.visibleLayerKeySet);
            if (newVisibleLayerKeySet.has(layerKey)) {
                newVisibleLayerKeySet.delete(layerKey);
            } else {
                newVisibleLayerKeySet.add(layerKey);
            }
            return {...state, visibleLayerKeySet: newVisibleLayerKeySet};
        case 'HIDE_LAYER':
            layerKey = action.layerKey;
            newVisibleLayerKeySet = new Set(state.visibleLayerKeySet);
            newVisibleLayerKeySet.delete(layerKey);
            return {...state, visibleLayerKeySet: newVisibleLayerKeySet};
        case 'CHANGE_BORDER_WEIGHT':
            layerKey = action.layerKey;
            newBorderWidthMap = new Map(state.borderWidthMap);
            newBorderWidthMap.set(layerKey, action.borderWidth);
            return {...state, borderWidthMap: newBorderWidthMap};
        case "SET_LAYER_STATE_FROM_CONFIG":
            return {
                ...state,
                visibleLayerKeySet:action.layers.visibleLayerKeySet,
                borderWidthMap: action.layers.borderWidthMap
            };
        default:
            return state;
    }
};


export default layerReducer