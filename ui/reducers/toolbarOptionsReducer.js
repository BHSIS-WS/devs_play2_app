import initialState from '../state/initialState'

const toolbarOptionsReducer = (state = initialState.toolbarOptions , action) => {
    let renderStatusLayerList = state.renderStatusLayerList.slice();
    switch(action.type) {
        case "TOGGLE_VISIBILITY_FOR_SAVE_LAYER_CONFIG_DIALOG":
            return {
                ...state,
                showSaveLayerConfigDialog: !state.showSaveLayerConfigDialog
            };
        case "TOGGLE_VISIBILITY_FOR_SAVE_PDF_DIALOG":
            return {
                ...state,
                showSavePdfDialog: !state.showSavePdfDialog
            };
        case "TOGGLE_VISIBILITY_FOR_RENDER_STATUS_PANEL":
            if (action.isVisible != null) {
                return {
                    ...state,
                    showRenderStatusPanel: action.isVisible
                }
            }
            return {
                ...state,
                showRenderStatusPanel: !state.showRenderStatusPanel
            };
        case "TOGGLE_VISIBILITY_FOR_PRINT_MAP_LEGEND":
            return {
                ...state,
                showPrintMapLegend: !state.showPrintMapLegend
            };
        case "TOGGLE_VISIBILITY_FOR_CANVAS_ATTRIBUTES_DIALOG":
            return {
                ...state,
                showCanvasAttributesDialog: !state.showCanvasAttributesDialog
            };
        case "TOGGLE_VISIBILITY_FOR_MINIMAP_PANEL":
            return {
                ...state,
                showMinimapPanel: !state.showMinimapPanel
            };
        case "SHOW_REMOVE_CONFIG_DIALOG":
            return {
                ...state,
                showRemoveConfigDialog: true,
                userSavedLayerConfig: action.userSavedLayerConfig
            };
        case "HIDE_SAVE_PDF_DIALOG":
            return {
                ...state,
                showSavePdfDialog: false
            };
        case "HIDE_SAVE_LAYER_CONFIG_DIALOG":
            return {
                ...state,
                showSaveLayerConfigDialog: false
            };
        case "HIDE_REMOVE_CONFIG_DIALOG":
            return {
                ...state,
                showRemoveConfigDialog: false,
                userSavedLayerConfig: null
            };
        case "HIDE_CANVAS_ATTRIBUTES_DIALOG":
            return {
                ...state,
                showCanvasAttributesDialog: false
            };
        case "SAVE_PDF":
            return {
                ...state,
                showSavePdfDialog: false
            };
        case "ADD_OR_REMOVE_LAYER_IN_RENDER_STATUS_LAYER_LIST":
            let removed = false;
            for (let i = 0; i < renderStatusLayerList.length; i++) {
                if (renderStatusLayerList[i].layerId === action.layerId && renderStatusLayerList[i].layerType === action.layerType) {
                    renderStatusLayerList.splice(i, 1);
                    removed = true;
                    break;
                }
            }
            if (!removed) {
                renderStatusLayerList.push({
                    layerId: action.layerId,
                    layerType: action.layerType,
                    status: "Loading"
                })
            }
            return {
                ...state,
                renderStatusLayerList: renderStatusLayerList
            };
        case "UPDATE_RENDER_STATUS_FOR_LAYER":
            for (let i = 0; i < renderStatusLayerList.length; i++) {
                if (renderStatusLayerList[i].layerId === action.layerId && renderStatusLayerList[i].layerType === action.layerType) {
                    renderStatusLayerList[i].status = action.status;
                }
            }
            return {
                ...state,
                renderStatusLayerList: renderStatusLayerList
            };
        case "SET_TOOLBAR_OPTIONS_STATE_FROM_CONFIG":
            return {
                ...state,
                renderStatusLayerList: action.toolbarOptions.renderStatusLayerList
            };
        case "SHOW_SAVE_CONFIG_DUPLICATE_NAME_ERR":
            return {
                ...state,
                showSaveConfigDuplicateNameError: action.status
            };
        case "TOGGLE_MOUSE_ZOOM_FOR_PRINT_MAP":
            return {
                ...state,
                disableMouseZoom: !state.disableMouseZoom
            };
        default:
            return state;
    }
};


export default toolbarOptionsReducer;