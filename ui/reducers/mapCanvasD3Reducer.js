import initialState from '../state/initialState'

const mapCanvasD3Reducer = (state = initialState.mapCanvasD3 , action) => {
    let canvasWidth, canvasHeight;

    switch(action.type) {
        case 'RESIZE_CANVAS':
            canvasWidth = action.width;
            canvasHeight = action.height;
            return {...state, width: canvasWidth, height: canvasHeight};
        case "HEIGHT_D3_CHANGE":
            return {...state, height: parseFloat(action.value)};
        case "WIDTH_D3_CHANGE":
            return {...state, width: parseFloat(action.value)};
        case 'ZOOM_D3_CHANGE':
            return {...state, zoom: parseFloat(action.value)};
        case 'TRANSLATE_D3_CHANGE':
            return{
                ...state,
                translateX: action.translateX,
                translateY: action.translateY,
                fromMinimap: action.fromMinimap
            };
        case 'PROJECTION_D3_CHANGE':
            return {...state, projection: action.value};
        case 'CENTER_D3_CHANGE':
            let strs = action.value.split(",");
            if (strs[0] && strs[1]) {
                let lat = parseFloat(strs[0]);
                let long = parseFloat(strs[1]);
                return {...state, center_lat: lat, center_lng: long};
            }
            break;
        case "SET_MAP_CANVAS_D3_STATE_FROM_CONFIG":
            let zoom = parseFloat(action.mapCanvasD3.zoom);
            if (state.projection < 3 && action.mapCanvasD3.projection === 3) {
                zoom = 4.5;
            } else if (state.projection === 3 && action.mapCanvasD3.projection < 3) {
                zoom = 1.7;
            }
            return {
                ...action.mapCanvasD3,
                zoom: zoom
            };
        default:
            return state;
    }
};

export default mapCanvasD3Reducer