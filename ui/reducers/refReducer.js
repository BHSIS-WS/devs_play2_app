import initialState from '../state/initialState'

const refReducer = (state = initialState.ref , action) => {

    let layerGroupArray,layerArray, layerFilteringCriteriaArray, criteriaCodeArray, criteriaCodeLookupMap,
        gradientDataSetArray, gradientLayerArray, refArray;

    switch(action.type) {
        case 'ASSIGN_STATE_REF':
            refArray = action.params.ref;
            criteriaCodeArray =refArray[0].criteriacodes;
            layerFilteringCriteriaArray= refArray[1].layerfilteringcriteria;
            gradientDataSetArray = refArray[2].gradientlayerdatasets;
            layerGroupArray = refArray[3].fusionmaplayergroups;
            layerArray = refArray[4].fusionmaplayers;
            gradientLayerArray = refArray[5].gradientmaplayers;
            criteriaCodeLookupMap = new Map(criteriaCodeArray.map(item => {return [item.id,item]}));

            return {...state, dataInitialized: true, layerGroupArray: layerGroupArray, layerArray: layerArray,
                layerFilteringCriteriaArray: layerFilteringCriteriaArray, criteriaCodeArray: criteriaCodeArray,
                criteriaCodeLookupMap: criteriaCodeLookupMap, gradientDataSetArray: gradientDataSetArray,
                gradientLayerArray: gradientLayerArray};
        case 'INIT_STATE_REF':
            return state;
        default:
            return state;
    }
};


export default refReducer