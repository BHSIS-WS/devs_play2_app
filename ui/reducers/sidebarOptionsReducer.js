import initialState from '../state/initialState'

const sidebarOptionsReducer = (state = initialState.sidebarOptions , action) => {
 let helpArray;
 switch(action.type) {
     case "TOGGLE_VISIBILITY_SIDEBAR_BUTTONS":
         return {
             ...state,
             showSidebarButtons: !state.showSidebarButtons
         }
     case "TOGGLE_VISIBILITY_FOR_SAVED_LAYER_CONFIG_PANEL":
         return {
             ...state,
             showSavedLayerConfigPanel: !state.showSavedLayerConfigPanel,
             showSelectLayersPanel: false,
             showLayerPrintControlPanel: false,
             showHelpPanel: false,
             showFeedbackPanel: false
         };
     case "TOGGLE_VISIBILITY_FOR_SELECT_LAYERS_PANEL":
         return {
             ...state,
             showSelectLayersPanel: !state.showSelectLayersPanel,
             showLayerPrintControlPanel: !state.showSelectLayersPanel ? true : false,
             showSavedLayerConfigPanel: false,
             showHelpPanel: false,
             showFeedbackPanel: false
         };
     case "TOGGLE_VISIBILITY_FOR_LAYER_PRINT_CONTROL_PANEL":
         return {
             ...state,
             showLayerPrintControlPanel: !state.showLayerPrintControlPanel
         }
     case "TOGGLE_VISIBILITY_FOR_FEEDBACK_PANEL":
         return {
             ...state,
             showFeedbackPanel: !state.showFeedbackPanel,
             showSelectLayersPanel: false,
             showLayerPrintControlPanel: false,
             showSavedLayerConfigPanel: false,
             showHelpPanel: false
         };
     case "TOGGLE_VISIBILITY_FOR_HELP_PANEL":
         return {
             ...state,
             showHelpPanel: !state.showHelpPanel,
             showSelectLayersPanel: false,
             showLayerPrintControlPanel: false,
             showSavedLayerConfigPanel: false,
             showFeedbackPanel: false
         };
     case "TOGGLE_VISIBILITY_ShowAnswer":
         return {
             ...state,
             helpList: state.helpList.map((item) => {
                 return item.devsHelpTopicId === action.devsHelpTopicId ? {...item, showAnswer: !item.showAnswer} : item
             })
         };
     case "TOGGLE_VISIBILITY_ShowPlayButtons":
         return {
             ...state,
             helpList: state.helpList.map((item) => {
                 return item.devsHelpTopicId === action.devsHelpTopicId ? {...item, showPlayButtons: !item.showPlayButtons} : item
             })
         };
     case "SET_VISIBILITY_PLAYPANE":
           return {
               ...state,
               helpList: state.helpList.map((item) => {
                   return item.devsHelpTopicId === action.devsHelpTopicId ? {...item, showPlayPane: !item.showPlayPane} : item
               })
         };
      case "CLOSE_PLAYPANE":
           return {
               ...state,
               helpList: state.helpList.map((item) => {
                   return item.devsHelpTopicId === action.devsHelpTopicId ? {...item, showPlayPane: false} : item
               })
         };
     case "INIT_STATE_HELP":
         return state;
     case 'ASSIGN_STATE_HELP':
         let helpList = action.params.HelpTopicDesc;
         helpList.forEach(helpTopic => {
             helpTopic.showAnswer = false;
             helpTopic.showPlayPane = false;
             helpTopic.showPlayButtons = false;
         });
          return {
            ...state,
            helpList: helpList
          };
     default:
         return state;
 }
};


export default sidebarOptionsReducer;