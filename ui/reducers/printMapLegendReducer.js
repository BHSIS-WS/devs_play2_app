import initialState from "../state/initialState";

const printMapLegendReducer = (state = initialState.printMapLegend, action) => {
    let newLayerIdSet;

    switch(action.type) {
        case "TOGGLE_NORMAL_LAYER_LEGEND_DISPLAY":
            newLayerIdSet = new Set(state.expandedLayerIdSet);
            newLayerIdSet.has(action.layerId) ? newLayerIdSet.delete(action.layerId) : newLayerIdSet.add(action.layerId);
            state={...state, expandedLayerIdSet: newLayerIdSet};
            return state;
        case "TOGGLE_GRADIENT_LAYER_LEGEND_DISPLAY":
            newLayerIdSet = new Set(state.expandedGradLayerIdSet);
            newLayerIdSet.has(action.layerId) ? newLayerIdSet.delete(action.layerId) : newLayerIdSet.add(action.layerId);
            state={...state, expandedGradLayerIdSet: newLayerIdSet};
            return state;
        default:
            return state;

    }
};

export default printMapLegendReducer;