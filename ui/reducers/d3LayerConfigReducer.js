import initialState from '../state/initialState'


const cloneLayerConfigMap = (layerConfigMap) => {
    return new Map(layerConfigMap);
};

const deepClone = (map) => {
    let result = new Map();

    map.forEach((value, key) => {
        if(value instanceof Map) {
            result.set(key, deepClone(value));
        } else if (typeof value === "object") {
            let newValue = Object.assign({}, value);
            Object.keys(newValue).forEach((objKey) => {
                if (newValue[objKey] instanceof Map) {
                    newValue[objKey] = deepClone(newValue[objKey]);
                } else if (newValue[objKey] instanceof Set) {
                    newValue[objKey] = new Set(newValue[objKey]);
                } else if(newValue[objKey] instanceof Array) {
                    newValue[objKey]  = [...value[objKey]];
                }
            });
            result.set(key, newValue);
        } else if (value instanceof Set) {
            result.set(key, new Set(value));
        } else if(value instanceof Array) {
            result.set(key, [...value]);
        } else {
            result.set(key, value);
        }
    });
    return result;
};

const getLayerConfigForNormalLayer = (layerConfig) => {
    let layerConfigJson = JSON.parse(layerConfig.layer_config_json_string);
    layerConfigJson.shapeConfigMap = new Map();

    if (layerConfig.style_column_json !== null) {
        if (layerConfig.geom_type === "POINT") {
            for (let i = 0; i < layerConfig.style_column_json.length; i++) {
                let styleJson = JSON.parse(layerConfig.style_column_json[i]);
                if (styleJson.name.includes("custom")) {
                    styleJson.name = styleJson.descrpition;
                    let configList = JSON.parse(styleJson.value);
                    let configMap = new Map();
                    configList.forEach(config => {
                        configMap.set(config.name, {
                            fillColor: config.fillColor,
                            fillOpacity: config.fillOpacity,
                            shapeId: config.shapeId,
                            strokeColor: config.strokeColor,
                            strokeOpacity: config.strokeOpacity,
                            strokeSize: config.strokeSize
                        });
                    });
                    layerConfigJson.shapeConfigMap.set(styleJson.descrpition, configMap);
                } else {
                    layerConfigJson.shapeConfigMap.set(i, layerConfig.style_column_json[i]);
                }
            }
        } else {
            for (let i = 0; i < layerConfig.style_column_json.length; i++) {
                let styleJson = JSON.parse(layerConfig.style_column_json[i]);
                if (!styleJson.name.includes("custom")) {
                    layerConfigJson.shapeConfigMap.set(i, layerConfig.style_column_json[i]);
                }
            }
        }
    }
    layerConfigJson.legendDisplayIncluded = true;
    return layerConfigJson;
};

const getLayerConfigForGradientLayer = (gradLayerConfig) => {
    let colorBoundariesJsonString = JSON.parse(gradLayerConfig.color_boundaries_json_string);
    return {gradientColorConfigArray: colorBoundariesJsonString.boundaries, legendDisplayIncluded: true, expanded: true};
};


const d3LayerConfigReducer = (state = initialState.d3LayerConfig, action) => {

    let layerId, newLayerConfigMap, layerConfig, newLayerConfig, newShapeConfigMap, shapeConfig,
        newShapeConfig, selectedColorConfig, newDisabledLayerIdSet, newCriteriaConfig, criteriaConfig, newBorderStyle,
        newGradientColorConfigArray, newHideLegendConfigSet;

    switch(action.type) {
        case 'TOGGLE_D3_LAYER_CONFIG_PANEL':
            layerId = action.layerId;
            newLayerConfigMap = cloneLayerConfigMap(action.isGradientLayer ? state.gradientLayerConfigMap : state.layerConfigMap);
            layerConfig = newLayerConfigMap.get(layerId);
            if (typeof layerConfig === 'undefined') {
                newLayerConfig = {
                };
                newLayerConfigMap.set(layerId, newLayerConfig);
            } else {
                layerConfig.expanded = !layerConfig.expanded;
            }
            if (action.isGradientLayer) {
                return {...state, gradientLayerConfigMap: newLayerConfigMap};
            } else {
                return {...state, layerConfigMap: newLayerConfigMap};
            }
        case 'TOGGLE_D3_LAYER_DISABLE':
            layerId = action.layerId;
            newDisabledLayerIdSet = new Set(action.isGradientLayer ? state.disabledGradientLayerIdSet: state.disabledLayerIdSet);
            if (newDisabledLayerIdSet.has(layerId)) {
                newDisabledLayerIdSet.delete(layerId);
            } else {
                newDisabledLayerIdSet.add(layerId);
            }
            if (action.isGradientLayer) {
                return {...state, disabledGradientLayerIdSet: newDisabledLayerIdSet};
            } else {
                return {...state, disabledLayerIdSet: newDisabledLayerIdSet};
            }
        case 'INITIALIZE_LAYER_CONFIG_MAP':
            newLayerConfigMap = cloneLayerConfigMap(state.layerConfigMap);
            layerConfig = newLayerConfigMap.get(action.layerId);
            if (typeof layerConfig === 'undefined') {
                newLayerConfig = action.value;
                newLayerConfigMap.set(action.layerId, newLayerConfig);
            }
            return {...state, layerConfigMap: newLayerConfigMap};
        case 'INITIALIZE_D3_LAYER_CONFIGS':
            console.log("INITING: " + action);
            newLayerConfigMap = new Map();
            action.layerArray.map(layer => {
                if (layer.layer_config_json_string != null) {
                    newLayerConfig = getLayerConfigForNormalLayer(layer);
                    newLayerConfigMap.set(layer.id, newLayerConfig);
                }
            });
            newGradientLayerConfigMap = new Map();
            action.gradientLayerArray.map((gradLayer) => {
                if (gradLayer.color_boundaries_json_string != null){
                    newLayerConfig = getLayerConfigForGradientLayer(gradLayer);
                    newGradientLayerConfigMap.set(gradLayer.id, newLayerConfig);
                }
            });
            return {...state, layerConfigMap: newLayerConfigMap, gradientLayerConfigMap: newGradientLayerConfigMap};
        case 'UPDATE_CONFIG_TAB':
            layerId = action.layerId;
            newLayerConfigMap = cloneLayerConfigMap(action.isGradientLayer ? state.gradientLayerConfigMap : state.layerConfigMap);
            layerConfig = newLayerConfigMap.get(layerId);
            if (typeof layerConfig === 'undefined') {
                newLayerConfig = {selectedTab: action.value};
                newLayerConfigMap.set(layerId, newLayerConfig);
            } else {
                layerConfig.selectedTab = action.value;
            }
            if (action.isGradientLayer) {
                return {...state, gradientLayerConfigMap: newLayerConfigMap};
            } else {
                return {...state, layerConfigMap: newLayerConfigMap};
            }
        case 'UPDATE_SHAPE_FOR_ALL':
            layerId = action.layerId;
            newLayerConfigMap = cloneLayerConfigMap(action.isGradientLayer ? state.gradientLayerConfigMap : state.layerConfigMap);
            layerConfig = newLayerConfigMap.get(layerId);
            if (typeof layerConfig === 'undefined') {
                newLayerConfig = {useSameShapeStyleForAll: action.value};
                newLayerConfigMap.set(layerId, newLayerConfig);
            } else {
                layerConfig.useSameShapeStyleForAll = action.value;
            }
            return {...state, layerConfigMap: newLayerConfigMap};
        case 'UPDATE_SHAPE_BY_VARIABLE':
            layerId = action.layerId;
            newLayerConfigMap = cloneLayerConfigMap(action.isGradientLayer ? state.gradientLayerConfigMap : state.layerConfigMap);
            layerConfig = newLayerConfigMap.get(layerId);
            if (typeof layerConfig === 'undefined') {
                newLayerConfig = {configShapeStyleByVariable: action.value};
                newLayerConfigMap.set(layerId, newLayerConfig);
            } else {
                layerConfig.configShapeStyleByVariable = action.value;
            }
            return {...state, layerConfigMap: newLayerConfigMap};
        case 'UPDATE_CONFIG_CODE_ID':
            layerId = action.layerId;
            newLayerConfigMap = cloneLayerConfigMap(state.layerConfigMap);
            layerConfig = newLayerConfigMap.get(layerId);
            if (typeof layerConfig === 'undefined') {
                newLayerConfig = {selectedVariableForStyle: action.value};
                newLayerConfigMap.set(layerId, newLayerConfig);
            } else {
                layerConfig.selectedVariableForStyle = action.value;
            }
            return {...state, layerConfigMap: newLayerConfigMap};
        case "UPDATE_GENERAL_TYPE":
            layerId = action.layerId;
            newLayerConfigMap = cloneLayerConfigMap(action.isGradientLayer ? state.gradientLayerConfigMap : state.layerConfigMap);
            layerConfig = newLayerConfigMap.get(layerId);
            let variableName = action.variableName;
            if (typeof layerConfig === 'undefined') {
                newLayerConfig = {variableName: action.value};
                newLayerConfigMap.set(layerId, newLayerConfig);
            } else {
                layerConfig[variableName] = action.value;
            }
            if (action.isGradientLayer) {
                return {...state, gradientLayerConfigMap: newLayerConfigMap};
            } else {
                return {...state, layerConfigMap: newLayerConfigMap};
            }
        case 'UPDATE_COORDINATE_TOLERANCE':
            layerId = action.layerId;
            newLayerConfigMap = cloneLayerConfigMap(action.isGradientLayer ? state.gradientLayerConfigMap : state.layerConfigMap);
            layerConfig = newLayerConfigMap.get(layerId);
            if (typeof layerConfig === 'undefined') {
                newLayerConfig = {coordinateAccuracyTolerance: action.value};
                newLayerConfigMap.set(layerId, newLayerConfig);
            } else {
                layerConfig.coordinateAccuracyTolerance = action.value;
            }
            if (action.isGradientLayer) {
                return {...state, gradientLayerConfigMap: newLayerConfigMap};
            } else {
                return {...state, layerConfigMap: newLayerConfigMap};
            }
        case 'UPDATE_SHAPE_CONFIGURATION':
            selectedColorConfig = action.value;
            layerId = action.layerId;
            newLayerConfigMap = cloneLayerConfigMap(state.layerConfigMap);
            layerConfig = newLayerConfigMap.get(layerId);
            if(action.hasSameShapeStyle) {
                let newShapeForAllStyle = Object.assign({}, layerConfig.shapeForAllStyle);
                newShapeForAllStyle.fillColor = selectedColorConfig.fillColor;
                newShapeForAllStyle.strokeColor = selectedColorConfig.strokeColor;
                newShapeForAllStyle.shapeId = parseInt(selectedColorConfig.shapeId);
                newShapeForAllStyle.strokeSize = selectedColorConfig.strokeSize;
                newShapeForAllStyle.strokeOpacity = selectedColorConfig.strokeOpacity;
                newShapeForAllStyle.fillOpacity = selectedColorConfig.fillOpacity;
                layerConfig.shapeForAllStyle = newShapeForAllStyle;
            } else {
                newShapeConfigMap = cloneLayerConfigMap(layerConfig.shapeConfigMap);
                shapeConfig = newShapeConfigMap.get(action.categoryCodeKey);
                if (typeof shapeConfig === 'undefined') {
                    newShapeConfig = new Map();
                    newCriteriaConfig = {
                        fillColor: selectedColorConfig.fillColor,
                        strokeColor: selectedColorConfig.strokeColor,
                        shapeId: parseInt(selectedColorConfig.shapeId),
                        strokeSize: selectedColorConfig.strokeSize,
                        strokeOpacity: selectedColorConfig.strokeOpacity,
                        fillOpacity: selectedColorConfig.fillOpacity
                    };
                    newShapeConfig.set(action.criteriaKey, newCriteriaConfig);
                    newShapeConfigMap.set(action.categoryCodeKey, newShapeConfig);
                } else {
                    newShapeConfig = cloneLayerConfigMap(shapeConfig);
                    criteriaConfig = shapeConfig.get(action.criteriaKey);
                    if (typeof criteriaConfig === 'undefined') {
                        newCriteriaConfig = {
                            fillColor: selectedColorConfig.fillColor,
                            strokeColor: selectedColorConfig.strokeColor,
                            shapeId: parseInt(selectedColorConfig.shapeId),
                            strokeSize: selectedColorConfig.strokeSize,
                            strokeOpacity: selectedColorConfig.strokeOpacity,
                            fillOpacity: selectedColorConfig.fillOpacity
                        };
                        newShapeConfig.set(action.criteriaKey, newCriteriaConfig);
                    } else {
                        criteriaConfig.fillColor = selectedColorConfig.fillColor;
                        criteriaConfig.strokeColor = selectedColorConfig.strokeColor;
                        criteriaConfig.shapeId = parseInt(selectedColorConfig.shapeId);
                        criteriaConfig.strokeSize = selectedColorConfig.strokeSize;
                        criteriaConfig.strokeOpacity = selectedColorConfig.strokeOpacity;
                        criteriaConfig.fillOpacity = selectedColorConfig.fillOpacity;
                    }
                    newShapeConfigMap.set(action.categoryCodeKey, newShapeConfig);
                }
                layerConfig.shapeConfigMap = newShapeConfigMap;
            }
            return {...state, layerConfigMap: newLayerConfigMap};
        case 'TOGGLE_LEGEND_FOR_CATEGORICAL_CONFIG':
            newLayerConfigMap = cloneLayerConfigMap(action.isGradientLayer ? state.gradientLayerConfigMap : state.layerConfigMap);
            layerConfig = newLayerConfigMap.get(action.layerId);
            newHideLegendConfigSet = layerConfig!== undefined && layerConfig.hideLegendConfigSet ? new Set(layerConfig.hideLegendConfigSet) : new Set();
            if (newHideLegendConfigSet.has(action.criteriaKey)) {
                newHideLegendConfigSet.delete(action.criteriaKey);
            } else {
                newHideLegendConfigSet.add(action.criteriaKey);
            }
            layerConfig.hideLegendConfigSet = newHideLegendConfigSet;
            if (action.isGradientLayer) {
                return {...state, gradientLayerConfigMap: newLayerConfigMap};
            } else {
                return {...state, layerConfigMap: newLayerConfigMap};
            }
        case 'INITIALIZE_GRADIENT_CONFIG_MAP':
            newLayerConfigMap = cloneLayerConfigMap(state.gradientLayerConfigMap);
            layerConfig = newLayerConfigMap.get(action.layerId);
            if (typeof layerConfig === 'undefined') {
                //newLayerConfig = {gradientColorConfigArray: action.value.boundaries, legendDisplayIncluded: true};
                newLayerConfig = {gradientColorConfigArray: action.value.boundaries, legendDisplayIncluded: true, expanded: true};
                newLayerConfigMap.set(action.layerId, newLayerConfig);
            } else {
                layerConfig.gradientColorConfigArray = action.value.boundaries;
                layerConfig.legendDisplayIncluded = true;
                layerConfig.expanded = true;
            }
            return {...state, gradientLayerConfigMap: newLayerConfigMap};
        case 'ADD_NEW_GRADIENT_CONFIG_VALUE':
            newLayerConfigMap = cloneLayerConfigMap(state.gradientLayerConfigMap);
            layerConfig = newLayerConfigMap.get(action.layerId);
            newGradientColorConfigArray = [...layerConfig.gradientColorConfigArray];
            newGradientColorConfigArray.push({value: 0, color: "none"});
            layerConfig.gradientColorConfigArray = newGradientColorConfigArray;
            return {...state, gradientLayerConfigMap: newLayerConfigMap};
        case 'REMOVE_GRADIENT_CONFIG_VALUE':
            newLayerConfigMap = cloneLayerConfigMap(state.gradientLayerConfigMap);
            layerConfig = newLayerConfigMap.get(action.layerId);
            newGradientColorConfigArray = [...layerConfig.gradientColorConfigArray];
            newGradientColorConfigArray.splice(action.idx, 1);
            layerConfig.gradientColorConfigArray = newGradientColorConfigArray;
            return {...state, gradientLayerConfigMap: newLayerConfigMap};
        case 'UPDATE_GRADIENT_CONFIG_VALUE':
            newLayerConfigMap = cloneLayerConfigMap(state.gradientLayerConfigMap);
            layerConfig = newLayerConfigMap.get(action.layerId);
            newGradientColorConfigArray = [...layerConfig.gradientColorConfigArray];
            newGradientColorConfigArray[action.idx].value = action.value;
            layerConfig.gradientColorConfigArray = newGradientColorConfigArray;
            return {...state, gradientLayerConfigMap: newLayerConfigMap};
        case 'UPDATE_GRADIENT_STROKE_CONFIGURATION':
            selectedColorConfig = action.value;
            layerId = action.layerId;
            newLayerConfigMap = cloneLayerConfigMap(state.gradientLayerConfigMap);
            layerConfig = newLayerConfigMap.get(layerId);
            newBorderStyle = Object.assign({}, layerConfig.borderStyle);
            newBorderStyle.color = selectedColorConfig.strokeColor;
            newBorderStyle.size = selectedColorConfig.strokeSize;
            newBorderStyle.opacity = selectedColorConfig.strokeOpacity;
            layerConfig.borderStyle = newBorderStyle;
            return {...state, gradientLayerConfigMap: newLayerConfigMap};
        case 'UPDATE_GRADIENT_FILL_CONFIGURATION':
            selectedColorConfig = action.value;
            newLayerConfigMap = cloneLayerConfigMap(state.gradientLayerConfigMap);
            layerConfig = newLayerConfigMap.get(action.layerId);
            newGradientColorConfigArray = [...layerConfig.gradientColorConfigArray];
            newGradientColorConfigArray[action.criteriaKey].color = selectedColorConfig.fillColor;
            newGradientColorConfigArray[action.criteriaKey].opacity = selectedColorConfig.fillOpacity;
            layerConfig.gradientColorConfigArray = newGradientColorConfigArray;
            return {...state, gradientLayerConfigMap: newLayerConfigMap};
        case 'SET_D3_LAYER_CONFIG_STATE_FROM_CONFIG':
            let newGradientLayerConfigMap = deepClone(action.d3LayerConfig.gradientLayerConfigMap);
            newLayerConfigMap = deepClone(action.d3LayerConfig.layerConfigMap);
            return {
                ...state,
                disabledGradientLayerIdSet: action.d3LayerConfig.disabledGradientLayerIdSet,
                disabledLayerIdSet: action.d3LayerConfig.disabledLayerIdSet,
                gradientLayerConfigMap: newGradientLayerConfigMap,
                layerConfigMap: newLayerConfigMap
            };
        default:
            return state;
    }
};


export default d3LayerConfigReducer;