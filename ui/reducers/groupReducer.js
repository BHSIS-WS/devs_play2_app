import initialState from '../state/initialState'

const groupReducer = (state = initialState.layerCategories, action) => {
//    console.log("in groupReducer action=", action,);
    let layerType, expandMap, newExpandMap;

    switch (action.type) {
        case 'TOGGLE_GROUPS':
            layerType = action.layerType;
            expandMap = state.expandMap;
            let flag = expandMap.get(layerType)? false : true;
            newExpandMap = new Map(expandMap);
            newExpandMap.set(layerType, flag);
            return Object.assign({},state,{expandMap: newExpandMap});
        default:
            return state
    }
};

export default groupReducer