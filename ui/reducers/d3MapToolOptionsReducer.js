import initialState from '../state/initialState';

const cloneToolConfigMap = function(toolConfigMap) {
    if (!toolConfigMap) {
        return toolConfigMap;
    } else {
        let newToolConfigMap = new Map();
        let newToolConfig;
        toolConfigMap.forEach((config, id, map) => {
            newToolConfig = Object.assign({}, config);
            newToolConfigMap.set(id, newToolConfig);
        });
        return newToolConfigMap;
    }
};

const d3MapToolOptionsReducer = (state = initialState.d3MapToolOptions, action) => {

    let textToolOptionDialogOpen, textToolFontSize, textToolRotate, textToolFillColor,
        textToolTextContent, newTextToolConfigMap, newTextToolConfig,
        rectToolDrawingCanvasOn, rectToolOptionDialogOpen, newRectToolConfigMap, newRectToolConfig, newD3MapToolOptions;

    switch(action.type) {
        case 'TOGGLE_D3_MAP_TEXT_TOOL_OPTION_DIALOG':
            textToolOptionDialogOpen = state.d3MapTextTool.optionDialogOpen;
            return {
                ...state,
                d3MapTextTool: {
                    ...state.d3MapTextTool,
                    activeTextId: action.textId,
                    optionDialogOpen: action.value !== undefined ? action.value : !textToolOptionDialogOpen
                }
            };
        case 'CLOSE_D3_MAP_TEXT_TOOL_OPTION_DIALOG':
            return {...state, d3MapTextTool: {...state.d3MapTextTool, optionDialogOpen: false}};
        case 'APPLY_D3_MAP_TEXT_TOOL_OPTIONS':
            newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
            newTextToolConfig = {
                fontSize: action.fontSize,
                rotate: action.rotate,
                fillColor: action.fillColor,
                textContent: action.textContent,
                fontFamily: action.fontFamily,
                bold: action.bold,
                italic: action.italic,
                underline: action.underline,
                width: action.width,
                height: action.height
            };
            if (state.d3MapTextTool.activeTextId === 0) {
                newTextToolConfigMap.set(action.textId, newTextToolConfig);
                return {...state, d3MapTextTool: {...state.d3MapTextTool, activeTextId: action.textId, textToolConfigMap: newTextToolConfigMap}};
            } else {
                newTextToolConfigMap.set(state.d3MapTextTool.activeTextId, newTextToolConfig);
                return {...state, d3MapTextTool: {...state.d3MapTextTool, textToolConfigMap: newTextToolConfigMap}};
            }
        case 'ADD_D3_MAP_TEXT':  //may not needed
            newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
            newTextToolConfig = {
                fontSize: state.d3MapTextTool.defaultFontSize,
                rotate: state.d3MapTextTool.defaultRotate,
                fillColor: state.d3MapTextTool.defaultFillColor,
                textContent: state.d3MapTextTool.defaultTextContent,
                underline: state.d3MapTextTool.defaultUnderline
            };
            newTextToolConfigMap.set(action.textId, newTextToolConfig);
            return {...state, d3MapTextTool: {...state.d3MapTextTool, activeTextId: action.textId,
                    textToolConfigMap: newTextToolConfigMap}};
        case 'REMOVE_D3_MAP_TEXT':
            newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
            newTextToolConfigMap.delete(state.d3MapTextTool.activeTextId);
            return {...state, d3MapTextTool: {...state.d3MapTextTool, activeTextId: 0,
                    textToolConfigMap: newTextToolConfigMap}};
        case 'CHANGE_D3_MAP_TEXT_TOOL_FONT_SIZE':
            if (state.d3MapTextTool.activeTextId !== 0) {
                newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
                newTextToolConfig = newTextToolConfigMap.get(state.d3MapTextTool.activeTextId);
                textToolFontSize = action.value;
                newTextToolConfig = {...newTextToolConfig, fontSize: textToolFontSize};
                newTextToolConfigMap.set(state.d3MapTextTool.activeTextId, newTextToolConfig);
                return {...state, d3MapTextTool: {...state.d3MapTextTool, textToolConfigMap: newTextToolConfigMap}};
            }
            return state;
        case 'CHANGE_D3_MAP_TEXT_TOOL_ROTATE':
            if (state.d3MapTextTool.activeTextId !== 0) {
                newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
                newTextToolConfig = newTextToolConfigMap.get(state.d3MapTextTool.activeTextId);
                textToolRotate = action.value;
                newTextToolConfig = {...newTextToolConfig, rotate: textToolRotate};
                newTextToolConfigMap.set(state.d3MapTextTool.activeTextId, newTextToolConfig);
                return {...state, d3MapTextTool: {...state.d3MapTextTool, textToolConfigMap: newTextToolConfigMap}};
            }
            return state;
        case 'CHANGE_D3_MAP_TEXT_TOOL_FILL_COLOR':
            if (state.d3MapTextTool.activeTextId !== 0) {
                newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
                newTextToolConfig = newTextToolConfigMap.get(state.d3MapTextTool.activeTextId);
                textToolFillColor = action.value;
                newTextToolConfig = {...newTextToolConfig, fillColor: textToolFillColor};
                newTextToolConfigMap.set(state.d3MapTextTool.activeTextId, newTextToolConfig);
                return {...state, d3MapTextTool: {...state.d3MapTextTool, textToolConfigMap: newTextToolConfigMap}};
            }
            return state;
        case 'CHANGE_D3_MAP_TEXT_TOOL_TEXT_CONTENT':
            if (state.d3MapTextTool.activeTextId !== 0) {
                newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
                newTextToolConfig = newTextToolConfigMap.get(state.d3MapTextTool.activeTextId);
                textToolTextContent = action.value;
                newTextToolConfig = {...newTextToolConfig, textContent: textToolTextContent};
                newTextToolConfigMap.set(state.d3MapTextTool.activeTextId, newTextToolConfig);
                return {...state, d3MapTextTool: {...state.d3MapTextTool, textToolConfigMap: newTextToolConfigMap}};
            }
            return state;
        case 'CHANGE_D3_MAP_TEXT_TOOL_FONT_FAMILY':
            if (state.d3MapTextTool.activeTextId !== 0) {
                newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
                newTextToolConfig = newTextToolConfigMap.get(state.d3MapTextTool.activeTextId);
                newTextToolConfig = {...newTextToolConfig, fontFamily: action.value};
                newTextToolConfigMap.set(state.d3MapTextTool.activeTextId, newTextToolConfig);
                return {...state, d3MapTextTool: {...state.d3MapTextTool, textToolConfigMap: newTextToolConfigMap}};
            }
            return state;
        //The followings may not be needed.
        case 'TOGGLE_D3_MAP_TEXT_TOOL_BOLD':
            if (state.d3MapTextTool.activeTextId !== 0) {
                newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
                newTextToolConfig = newTextToolConfigMap.get(state.d3MapTextTool.activeTextId);
                newTextToolConfig = {...newTextToolConfig, bold: !newTextToolConfig.bold};
                newTextToolConfigMap.set(state.d3MapTextTool.activeTextId, newTextToolConfig);
                return {...state, d3MapTextTool: {...state.d3MapTextTool, textToolConfigMap: newTextToolConfigMap}};
            }
            return state;
        case 'TOGGLE_D3_MAP_TEXT_TOOL_ITALIC':
            if (state.d3MapTextTool.activeTextId !== 0) {
                newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
                newTextToolConfig = newTextToolConfigMap.get(state.d3MapTextTool.activeTextId);
                newTextToolConfig = {...newTextToolConfig, italic: !newTextToolConfig.italic};
                newTextToolConfigMap.set(state.d3MapTextTool.activeTextId, newTextToolConfig);
                return {...state, d3MapTextTool: {...state.d3MapTextTool, textToolConfigMap: newTextToolConfigMap}};
            }
            return state;
        case 'TOGGLE_D3_MAP_TEXT_TOOL_UNDERLINE':
            if (state.d3MapTextTool.activeTextId !== 0) {
                newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
                newTextToolConfig = newTextToolConfigMap.get(state.d3MapTextTool.activeTextId);
                newTextToolConfig = {...newTextToolConfig, underline: !newTextToolConfig.underline};
                newTextToolConfigMap.set(state.d3MapTextTool.activeTextId, newTextToolConfig);
                return {...state, d3MapTextTool: {...state.d3MapTextTool, textToolConfigMap: newTextToolConfigMap}};
            }
            return state;

//=================================================== Rectangle Tool ==================================================

        case 'TOGGLE_D3_MAP_RECT_TOOL':
            rectToolDrawingCanvasOn = state.d3MapRectangleTool.drawingCanvasOn;
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool,
                    drawingCanvasOn: !rectToolDrawingCanvasOn}};
        case 'TOGGLE_D3_MAP_RECT_TOOL_OPTION_DIALOG':
            rectToolOptionDialogOpen = state.d3MapRectangleTool.rectOptionDialogOpen;
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool, activeRectId: action.rectId,
                    rectOptionDialogOpen: !rectToolOptionDialogOpen}};
        case 'CLOSE_D3_MAP_RECT_TOOL_OPTION_DIALOG':
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool, rectOptionDialogOpen: false}};
        case 'APPLY_D3_MAP_RECT_TOOL_OPTIONS':
            newRectToolConfigMap = cloneToolConfigMap(state.d3MapRectangleTool.rectToolConfigMap);
            newRectToolConfig = {
                width: action.width,
                height: action.height,
                stroke: action.stroke,
                strokeWidth: action.strokeWidth
            };
            newRectToolConfigMap.set(state.d3MapRectangleTool.activeRectId, newRectToolConfig);
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool,
                    rectToolConfigMap: newRectToolConfigMap}};
        case 'ADD_D3_MAP_RECT':
            newRectToolConfigMap = cloneToolConfigMap(state.d3MapRectangleTool.rectToolConfigMap);
            newRectToolConfig = {
                width: action.width,
                height: action.height,
                stroke: state.d3MapRectangleTool.defaultStroke,
                strokeWidth: state.d3MapRectangleTool.defaultStrokeWidth
            };
            newRectToolConfigMap.set(action.rectId, newRectToolConfig);
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool, activeRectId: action.rectId,
                    rectToolConfigMap: newRectToolConfigMap}};
        case 'REMOVE_D3_MAP_RECT':
            newRectToolConfigMap = cloneToolConfigMap(state.d3MapRectangleTool.rectToolConfigMap);
            newRectToolConfigMap.delete(state.d3MapRectangleTool.activeRectId);
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool, rectToolConfigMap: newRectToolConfigMap}};
        case 'CHANGE_D3_MAP_RECT_TOOL_WIDTH':
            newRectToolConfigMap = cloneToolConfigMap(state.d3MapRectangleTool.rectToolConfigMap);
            newRectToolConfig = newRectToolConfigMap.get(state.d3MapRectangleTool.activeRectId);
            newRectToolConfig = {...newRectToolConfig, width: action.value};
            newRectToolConfigMap.set(state.d3MapRectangleTool.activeRectId, newRectToolConfig);
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool, rectToolConfigMap: newRectToolConfigMap}};
        case 'CHANGE_D3_MAP_RECT_TOOL_HEIGHT':
            newRectToolConfigMap = cloneToolConfigMap(state.d3MapRectangleTool.rectToolConfigMap);
            newRectToolConfig = newRectToolConfigMap.get(state.d3MapRectangleTool.activeRectId);
            newRectToolConfig = {...newRectToolConfig, height: action.value};
            newRectToolConfigMap.set(state.d3MapRectangleTool.activeRectId, newRectToolConfig);
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool, rectToolConfigMap: newRectToolConfigMap}};
        case 'CHANGE_D3_MAP_RECT_TOOL_STROKE':
            newRectToolConfigMap = cloneToolConfigMap(state.d3MapRectangleTool.rectToolConfigMap);
            newRectToolConfig = newRectToolConfigMap.get(state.d3MapRectangleTool.activeRectId);
            newRectToolConfig = {...newRectToolConfig, stroke: action.value};
            newRectToolConfigMap.set(state.d3MapRectangleTool.activeRectId, newRectToolConfig);
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool, rectToolConfigMap: newRectToolConfigMap}};
        case 'CHANGE_D3_MAP_RECT_TOOL_STROKE_WIDTH':
            newRectToolConfigMap = cloneToolConfigMap(state.d3MapRectangleTool.rectToolConfigMap);
            newRectToolConfig = newRectToolConfigMap.get(state.d3MapRectangleTool.activeRectId);
            newRectToolConfig = {...newRectToolConfig, strokeWidth: action.value};
            newRectToolConfigMap.set(state.d3MapRectangleTool.activeRectId, newRectToolConfig);
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool, rectToolConfigMap: newRectToolConfigMap}};
        case 'SET_D3_MAP_TOOL_OPTIONS_STATE_FROM_CONFIG':
            newD3MapToolOptions = action.d3MapToolOptions;
            if (typeof newD3MapToolOptions !== 'undefined') {
                return {
                    ...state,
                    d3MapTextTool: {
                        ...state.d3MapTextTool,
                        optionDialogOpen: newD3MapToolOptions.d3MapTextTool.optionDialogOpen,
                        activeTextId: newD3MapToolOptions.d3MapTextTool.activeTextId,
                        textToolConfigMap: newD3MapToolOptions.d3MapTextTool.textToolConfigMap
                    },
                    d3MapRectangleTool: {
                        ...state.d3MapRectangleTool,
                        drawingCanvasOn: newD3MapToolOptions.d3MapRectangleTool.drawingCanvasOn,
                        rectOptionDialogOpen: newD3MapToolOptions.d3MapRectangleTool.rectOptionDialogOpen,
                        activeRectId: newD3MapToolOptions.d3MapRectangleTool.activeRectId,
                        rectToolConfigMap: newD3MapToolOptions.d3MapRectangleTool.rectToolConfigMap
                    }
                };
            } else {
                return {...state};
            }
        case 'SAVE_MAP_TOOL_ELEMENT_COORDINATES':
            newTextToolConfigMap = cloneToolConfigMap(state.d3MapTextTool.textToolConfigMap);
            newRectToolConfigMap = cloneToolConfigMap(state.d3MapRectangleTool.rectToolConfigMap);
            action.data.textCoordsMap.forEach((coord, textId, map) => {
                newTextToolConfig = newTextToolConfigMap.get(textId);
                newTextToolConfig = {...newTextToolConfig, x: coord.x, y: coord.y};
                newTextToolConfigMap.set(textId, newTextToolConfig);
            });
            action.data.rectCoordsMap.forEach((coord, rectId, map) => {
                newRectToolConfig = newRectToolConfigMap.get(rectId);
                newRectToolConfig = {...newRectToolConfig, x: coord.x, y: coord.y};
                newRectToolConfigMap.set(rectId, newRectToolConfig);
            });
            return {...state, d3MapRectangleTool: {...state.d3MapRectangleTool, rectToolConfigMap: newRectToolConfigMap},
                d3MapTextTool: {...state.d3MapTextTool, textToolConfigMap: newTextToolConfigMap}};
        default:
            return state;
    }
};

export default d3MapToolOptionsReducer;