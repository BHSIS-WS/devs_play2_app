import initialState from '../state/initialState'

const colorPaletteReducer = (state = initialState.colorPalette, action) => {
    let newSelectedShapeConfiguration, existingShapeConf;

    switch(action.type) {
        case 'TOGGLE_PALETTE':
            return {
                ...state,
                show: action.value,
                criteriaKey: action.criteriaKey,
                categoryCodeKey: action.categoryCodeKey,
                layerId: action.layerId,
                layerGroupType: action.groupType,
                topPosition: action.topPosition,
                leftPosition: action.leftPosition,
                visibleSection: action.visibleSection,
                actionType: action.actionType
            };
        case 'CHANGE_FILL_COLOR':
            newSelectedShapeConfiguration = Object.assign({}, state.selectedShapeConfiguration);
            newSelectedShapeConfiguration.fillColor = action.value;
            return {...state, selectedShapeConfiguration:  newSelectedShapeConfiguration};
        case 'CHANGE_BORDER_COLOR':
            newSelectedShapeConfiguration = Object.assign({}, state.selectedShapeConfiguration);
            newSelectedShapeConfiguration.strokeColor = action.value;
            return {...state, selectedShapeConfiguration:  newSelectedShapeConfiguration};
        case 'CHANGE_SHAPE_STYLE':
            newSelectedShapeConfiguration = Object.assign({}, state.selectedShapeConfiguration);
            newSelectedShapeConfiguration.shapeId = action.value;
            return {...state, selectedShapeConfiguration:  newSelectedShapeConfiguration};
        case 'CHANGE_STROKE_SIZE':
            newSelectedShapeConfiguration = Object.assign({}, state.selectedShapeConfiguration);
            newSelectedShapeConfiguration.strokeSize = action.value;
            return {...state, selectedShapeConfiguration:  newSelectedShapeConfiguration};
        case 'CHANGE_STROKE_OPACITY':
            newSelectedShapeConfiguration = Object.assign({}, state.selectedShapeConfiguration);
            newSelectedShapeConfiguration.strokeOpacity = action.value;
            return {...state, selectedShapeConfiguration:  newSelectedShapeConfiguration};
        case 'CHANGE_FILL_OPACITY':
            newSelectedShapeConfiguration = Object.assign({}, state.selectedShapeConfiguration);
            newSelectedShapeConfiguration.fillOpacity = action.value;
            return {...state, selectedShapeConfiguration:  newSelectedShapeConfiguration};
        case 'UPDATE_SELECTED_SHAPE_CONFIGURATION':
            newSelectedShapeConfiguration = Object.assign({}, state.selectedShapeConfiguration);
            if (action.hasSameShapeStyle) {
                existingShapeConf = action.layerConfig.shapeForAllStyle;
                if (typeof existingShapeConf !== 'undefined') {
                    newSelectedShapeConfiguration.fillColor = existingShapeConf.fillColor;
                    newSelectedShapeConfiguration.strokeColor = existingShapeConf.strokeColor;
                    newSelectedShapeConfiguration.shapeId = existingShapeConf.shapeId;
                    newSelectedShapeConfiguration.strokeSize = existingShapeConf.strokeSize;
                    newSelectedShapeConfiguration.strokeOpacity = existingShapeConf.strokeOpacity;
                    newSelectedShapeConfiguration.fillOpacity = existingShapeConf.fillOpacity;
                }
            } else {
                existingShapeConf = action.layerConfig.shapeConfigMap !== undefined ?
                    action.layerConfig.shapeConfigMap.get(state.categoryCodeKey) : undefined;
                if (typeof existingShapeConf !== 'undefined') {
                    existingShapeConf = existingShapeConf.get(state.criteriaKey);
                    if (typeof existingShapeConf !== 'undefined') {
                        newSelectedShapeConfiguration.fillColor = existingShapeConf.fillColor;
                        newSelectedShapeConfiguration.strokeColor = existingShapeConf.strokeColor;
                        newSelectedShapeConfiguration.shapeId = existingShapeConf.shapeId;
                        newSelectedShapeConfiguration.strokeSize = existingShapeConf.strokeSize;
                        newSelectedShapeConfiguration.strokeOpacity = existingShapeConf.strokeOpacity;
                        newSelectedShapeConfiguration.fillOpacity = existingShapeConf.fillOpacity;
                    }
                }
            }
            return{...state, selectedShapeConfiguration: newSelectedShapeConfiguration, hasSameShapeStyle: action.hasSameShapeStyle};
        case 'UPDATE_SELECTED_GRADIENT_SHAPE_CONFIGURATION':
            newSelectedShapeConfiguration = Object.assign({}, state.selectedShapeConfiguration);
            let existingLayerConf = action.layerConfig;
            if (typeof existingLayerConf.borderStyle !== 'undefined') {
                newSelectedShapeConfiguration.strokeColor = existingLayerConf.borderStyle.color;
                newSelectedShapeConfiguration.strokeSize = existingLayerConf.borderStyle.size;
                newSelectedShapeConfiguration.strokeOpacity = existingLayerConf.borderStyle.opacity;
            }
            existingShapeConf = existingLayerConf.gradientColorConfigArray[state.criteriaKey];
            if (typeof existingShapeConf !== 'undefined') {
                newSelectedShapeConfiguration.fillColor = existingShapeConf.color;
                newSelectedShapeConfiguration.fillOpacity = existingShapeConf.opacity;
            }

            return{...state, selectedShapeConfiguration: newSelectedShapeConfiguration};
        default:
            return state;
    }
};

export default colorPaletteReducer