"use strict";
import initialState from '../state/initialState';
import mapContentModel from "../others/MapContentModel";

const generateShapesFromCoordinateArray = (coordinateArray) => {
    let shapeArray = [];
    coordinateArray.forEach(function (coordinateString, index) {
        let newShape = mapContentModel.createGoogleMapsShapeFromCoordinateString(coordinateString, ++index);
        shapeArray.push(newShape);
    });
    return shapeArray;
};


const mapVariablesReducer = (state = initialState.mapVariables , action) => {

    let shapes = [];
    let markers = [];
    let mapMarkerLocations = [];
    switch(action.type) {
        case 'SET_RIGHT_CLICK_VALUES':
            return {
                ...state,
                rightClickCoordinates: action.rightClickCoordinates,
                rightClickContextMenuPosition: action.rightClickContextMenuPosition,
                rightClickedMarker: action.rightClickedMarker,
                rightClickedShape: action.rightClickedShape
            };
        case "SET_RIGHT_CLICK_VALUES_NULL":
            return {
                ...state,
                rightClickCoordinates: null,
                rightClickContextMenuPosition: null,
                rightClickedMarker: null,
                rightClickedShape: null
            };
        case "SET_VISIBILITY_FOR_DROP_MARKER_DIALOG":
            return {
                ...state,
                dropMarkerDialogVisible: action.visible,
                dropMarkerType: action.dialogType,
                rightClickCoordinates: null,
                rightClickContextMenuPosition: null,
                rightClickedMarker: null
            };
        case "REMOVE_RIGHT_CLICKED_MARKER":
            markers = state.mapMarkers.slice();
            for(let i = 0; i < markers.length; i++) {
                if (markers[i].id === state.rightClickedMarker.id) {
                    markers.splice(i, 1);
                    mapMarkerLocations.splice(i, 1);
                    break;
                }
            }
            return {
                ...state,
                mapMarkers: markers,
                mapMarkerLocations: mapMarkerLocations,
                rightClickCoordinates: null,
                rightClickContextMenuPosition: null,
                rightClickedMarker: null
            };
        case "REMOVE_RIGHT_CLICKED_SHAPE":
            shapes = state.mapShapes.slice();
            for (let i = 0; i < shapes.length; i++) {
                if (shapes[i].id === state.rightClickedShape.id) {
                    shapes.splice(i, 1);
                    break;
                }
            }
            return {
                ...state,
                mapShapes: shapes,
                rightClickCoordinates: null,
                rightClickContextMenuPosition: null,
                rightClickedShape: null
            };
        case "TOGGLE_EDITABLE_RIGHT_CLICKED_SHAPE":
            return {
                ...state,
                rightClickCoordinates: null,
                rightClickContextMenuPosition: null,
                rightClickedShape: null
            };
        case "TOGGLE_DRAGGABLE_RIGHT_CLICKED_SHAPE":
            return {
                ...state,
                rightClickCoordinates: null,
                rightClickContextMenuPosition: null,
                rightClickedShape: null
            };
        case "REMOVE_ALL_MARKERS":
            return {
                ...state,
                mapMarkers: [],
                rightClickCoordinates: null,
                rightClickContextMenuPosition: null,
                rightClickedMarker: null
            };
        case "REMOVE_ALL_SHAPES":
            return {
                ...state,
                mapShapes: [],
                rightClickCoordinates: null,
                rightClickContextMenuPosition: null,
                rightClickedMarker: null
            };
        case "ADD_MARKER_TO_MARKER_LIST":
            markers = state.mapMarkers.slice();
            mapMarkerLocations = state.mapMarkerLocations.slice();
            markers.push(action.marker);
            mapMarkerLocations.push(action.markerLocation);
            return {
                ...state,
                mapMarkers: markers,
                mapMarkerLocations: mapMarkerLocations
            };
        case "ADD_SHAPE_TO_SHAPE_LIST":
            shapes = state.mapShapes.slice();
            shapes.push(action.shape);
            return {
                ...state,
                mapShapes: shapes
            };
        case "DROP_MARKER_BY_LAT_LNG":
            return {
                ...state
            };
        case "SET_MAP_ZOOM_LEVEL":
            return {
                ...state,
                zoomLevel: action.zoomLevel,
                shouldZoomMap: action.shouldZoomMap
            };
        case "SET_MAP_CENTER":
            return {
                ...state,
                center: action.center
            };
        case "HIDE_MAINTENANCE_MESSAGE":
            return {
                ...state,
                hideMaintenanceMessage: true
            };
        case "SET_MAP_VARIABLES_STATE_FROM_CONFIG":
            return {
                center: action.mapVariables.center,
                dropMarkerDialogVisible: false,
                dropMarkerType: null,
                enteredLat: null,
                enteredLng: null,
                mapMarkerLocations: action.mapVariables.mapMarkerLocations,
                mapMarkers: action.mapVariables.mapMarkers,
                mapShapes: generateShapesFromCoordinateArray(action.mapVariables.mapShapes),
                minZoomLevel: action.mapVariables.minZoomLevel,
                rightClickContextMenuPosition: null,
                rightClickCoordinates: null,
                rightClickedMarker: null,
                rightClickedShape: null,
                shouldZoomMap: true,
                zoomLevel: action.mapVariables.zoomLevel
            };
        case "SELECT_ITEM":
            return {...state, infoWindow: action.infoWindow};
        case "SETUP_INFO_WINDOW":
            return {...state, infoWindow: {latLng: action.latLng, htmlToDisplay: action.htmlToDisplay, position: action.position}};
        case "CLOSE_INFO_WINDOW":
            return {...state, infoWindow: null};
        default:
            return state;
    }
};

export default mapVariablesReducer;