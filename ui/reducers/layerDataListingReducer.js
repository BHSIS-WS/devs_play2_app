"use strict";

import initialState from '../state/initialState';

const getResponse = function(data) {
    let resp = null;
    if (data.status == 200) {
        resp = data.response;
    }
    return resp;
};

const cloneTableColumns = function(tableColumns) {
    if (!tableColumns) return tableColumns;
    let newTableColumns = tableColumns.map((tableColumnObj) => {
        let newselectedTableColumnIds = tableColumnObj.selectedTableColumnIds.slice(0);
        return {
            fusionMapLayerId: tableColumnObj.fusionMapLayerId,
            selectedTableColumnIds: newselectedTableColumnIds
        };
    });
};

const cloneSelectedFPColumnIdMap = function(selectedFPColumnIdMap) {
    if (!selectedFPColumnIdMap) {
        return selectedFPColumnIdMap;
    } else {
        let newSelectedFPColumnIdMap = new Map();
        let newColumnIdSet = null;
        selectedFPColumnIdMap.forEach((columnIdSet, layerId, map) => {
            newColumnIdSet = new Set();
            columnIdSet.forEach((columnId) => {
                newColumnIdSet.add(columnId);
            });
            newSelectedFPColumnIdMap.set(layerId, newColumnIdSet);
        });
        return newSelectedFPColumnIdMap;
    }
};

const getAllColumnIdsForLayerId = function(state, layerId) {
    let allColumnIdSet = new Set();
    state.layerDataQueryResults.layerColumns.forEach((layer) => {
        if (layer.fusionMapLayerId == layerId) {
            layer.columns.forEach((column) => {
                allColumnIdSet.add(column.columnId);
            });
        }
    });
    return allColumnIdSet;
};

const layerDataListingReducer = (state = initialState.layerDataListing, action) => {

    let layerId, columnId, expandedFieldPickerLayerIdSet, newExpandedFieldPickerLayerIdSet, newSelectedFPLayerIdSet,
        newSelectedFPColumnIdSet, newSelectedFPColumnIdMap, seletedColumnIdsForLayerId;

    switch (action.type) {
        case 'UPDATE_LAYER_DATA_LISTING':
            let response = getResponse(action.data);
            let selectedRowIndex;
            if (state.sendMarker && response && response.rows) {
                //if this was triggered by clicking a marker on the map
                response.rows.forEach((row) => {
                    if (row._lat_ === state.markerLat && row._lng_ === state.markerLng) {
                        selectedRowIndex = row._irow;
                    }
                });
            } else if (state.infoWindowSelectedId !== null && response && response.rows) {
                response.rows.forEach((row) => {
                    if (row._ogc_fid1_ === state.infoWindowSelectedId) {
                        selectedRowIndex = row._irow;
                    }
                });
            }
            return {
                ...state,
                fetchingStart: false,
                layerDataQueryResults: response,
                shouldUpdate: false,
                page: response ? response.page : state.page, 
                sendMarker: false,
                infoWindowSelectedId: null,
                selectedDataTableRow: selectedRowIndex ? selectedRowIndex : null
            };
        case 'TOGGLE_FIELD_PICKER':
            layerId = action.layerId;
            newExpandedFieldPickerLayerIdSet = new Set(state.expandedFieldPickerLayerIdSet);
            if (newExpandedFieldPickerLayerIdSet.has(layerId)) {
                newExpandedFieldPickerLayerIdSet.delete(layerId);
            } else {
                newExpandedFieldPickerLayerIdSet.add(layerId);
            }
            return {
                ...state,
                expandedFieldPickerLayerIdSet: newExpandedFieldPickerLayerIdSet
            };
        case 'TOGGLE_FIELD_PICKER_LAYER_SELECT':
            layerId = action.layerId;
            newSelectedFPLayerIdSet = new Set(state.selectedFPLayerIdSet);
            if (newSelectedFPLayerIdSet.has(layerId)) {
                newSelectedFPLayerIdSet.delete(layerId);
            } else {
                newSelectedFPLayerIdSet.add(layerId);
            }
            return {...state, selectedFPLayerIdSet: newSelectedFPLayerIdSet, shouldUpdate: true};
        case 'TOGGLE_FIELD_PICKER_COLUMN_SELECT':
            layerId = action.layerId;
            columnId = action.columnId;
            newSelectedFPColumnIdSet = new Set(state.selectedFPColumnIdSet);
            newSelectedFPColumnIdMap = cloneSelectedFPColumnIdMap(state.selectedFPColumnIdMap);
            seletedColumnIdsForLayerId = newSelectedFPColumnIdMap.get(layerId);
            newSelectedFPLayerIdSet = new Set(state.selectedFPLayerIdSet);
            if (typeof seletedColumnIdsForLayerId === "undefined") {
                seletedColumnIdsForLayerId = new Set();
                newSelectedFPColumnIdMap.set(layerId, seletedColumnIdsForLayerId);
            }
            if (newSelectedFPColumnIdSet.has(columnId)) {
                newSelectedFPColumnIdSet.delete(columnId);
                seletedColumnIdsForLayerId.delete(columnId);
                if (seletedColumnIdsForLayerId.size === 0) {
                    newSelectedFPLayerIdSet.delete(layerId);
                }
            } else {
                newSelectedFPColumnIdSet.add(columnId);
                seletedColumnIdsForLayerId.add(columnId);
                if (!newSelectedFPLayerIdSet.has(layerId)) {
                    newSelectedFPLayerIdSet.add(layerId);
                }
            }
            return {...state, selectedFPColumnIdSet: newSelectedFPColumnIdSet,
                selectedFPColumnIdMap: newSelectedFPColumnIdMap, selectedFPLayerIdSet: newSelectedFPLayerIdSet, shouldUpdate: true};
        case 'FIELD_PICKER_SELECT_ALL_COLUMNS':
            layerId = action.layerId;
            newSelectedFPColumnIdSet = new Set(state.selectedFPColumnIdSet);
            newSelectedFPColumnIdMap = cloneSelectedFPColumnIdMap(state.selectedFPColumnIdMap);
            seletedColumnIdsForLayerId = getAllColumnIdsForLayerId(state, layerId);
            newSelectedFPColumnIdMap.set(layerId, seletedColumnIdsForLayerId);
            newSelectedFPColumnIdSet = new Set([...newSelectedFPColumnIdSet, ...seletedColumnIdsForLayerId]);
            newSelectedFPLayerIdSet = new Set(state.selectedFPLayerIdSet);
            if (!newSelectedFPLayerIdSet.has(layerId)) {
                newSelectedFPLayerIdSet.add(layerId);
            }
            return {...state, selectedFPColumnIdSet: newSelectedFPColumnIdSet,
                selectedFPColumnIdMap: newSelectedFPColumnIdMap, shouldUpdate: true, selectedFPLayerIdSet: newSelectedFPLayerIdSet};
        case 'FIELD_PICKER_SELECT_NONE_COLUMNS':
            layerId = action.layerId;
            newSelectedFPColumnIdSet = new Set(state.selectedFPColumnIdSet);
            newSelectedFPColumnIdMap = cloneSelectedFPColumnIdMap(state.selectedFPColumnIdMap);
            seletedColumnIdsForLayerId = newSelectedFPColumnIdMap.get(layerId);
            newSelectedFPLayerIdSet = new Set(state.selectedFPLayerIdSet);
            if (typeof seletedColumnIdsForLayerId === "undefined") {
                seletedColumnIdsForLayerId = new Set();
                newSelectedFPColumnIdMap.set(layerId, seletedColumnIdsForLayerId);
            } else {
                seletedColumnIdsForLayerId.forEach((columnId) => {
                    newSelectedFPColumnIdSet.delete(columnId);
                });
                seletedColumnIdsForLayerId.clear();
            }
            if (newSelectedFPLayerIdSet.has(layerId)) {
                newSelectedFPLayerIdSet.delete(layerId);
            }
            return {...state, selectedFPColumnIdSet: newSelectedFPColumnIdSet,
                selectedFPColumnIdMap: newSelectedFPColumnIdMap, shouldUpdate: true, selectedFPLayerIdSet: newSelectedFPLayerIdSet};
        case 'FIELD_PICKER_SELECT_REVERSE_COLUMNS':
            layerId = action.layerId;
            newSelectedFPColumnIdSet = new Set(state.selectedFPColumnIdSet);
            newSelectedFPColumnIdMap = cloneSelectedFPColumnIdMap(state.selectedFPColumnIdMap);
            seletedColumnIdsForLayerId = newSelectedFPColumnIdMap.get(layerId);
            let allColumnIdsForLayerId = getAllColumnIdsForLayerId(state, layerId);
            newSelectedFPLayerIdSet = new Set(state.selectedFPLayerIdSet);
            if (typeof seletedColumnIdsForLayerId === "undefined") {
                seletedColumnIdsForLayerId = new Set();
                newSelectedFPColumnIdMap.set(layerId, seletedColumnIdsForLayerId);
            }
            allColumnIdsForLayerId.forEach((columnId) => {
                if (newSelectedFPColumnIdSet.has(columnId)) {
                    newSelectedFPColumnIdSet.delete(columnId);
                    seletedColumnIdsForLayerId.delete(columnId);
                } else {
                    newSelectedFPColumnIdSet.add(columnId);
                    seletedColumnIdsForLayerId.add(columnId);
                }
            });
            if (newSelectedFPColumnIdSet.size === 0 && newSelectedFPLayerIdSet.has(layerId)) {
                newSelectedFPLayerIdSet.delete(layerId);
            } else if (newSelectedFPColumnIdSet.size > 0 && !newSelectedFPLayerIdSet.has(layerId)) {
                newSelectedFPLayerIdSet.add(layerId);
            }
            return {...state, selectedFPColumnIdSet: newSelectedFPColumnIdSet,
                selectedFPColumnIdMap: newSelectedFPColumnIdMap, shouldUpdate: true, selectedFPLayerIdSet: newSelectedFPLayerIdSet};
        case 'CHANGE_DATA_TABLE_PAGE_NUMBER':
            let pageNum = action.pageNum;
            if (pageNum < 1) {
                pageNum = 1;
            } else if (pageNum > state.layerDataQueryResults.totalPages) {
                pageNum = state.layerDataQueryResults.totalPages;
            }
            let shouldUpdate = action.shouldUpdate;
            return {...state, page: pageNum, shouldUpdate};
        case 'CHANGE_DATA_TABLE_PAGE_SIZE':
            return {...state, pageSize: action.pageSize, shouldUpdate: true};
        case 'TOGGLE_DT_STATE_COUNTY_DATA_PICKER':
            return {...state, showStateCountyDataPicker: !state.showStateCountyDataPicker};
        case 'TOGGLE_DT_ADDITIONAL_DATA_LEVEL':
            let newDataLevelExpandMap = new Map(state.dataLevelExpandMap);
            let dataLevelExpanded = newDataLevelExpandMap.get(action.dataLevel);
            newDataLevelExpandMap.set(action.dataLevel, !dataLevelExpanded);
            return {...state, dataLevelExpandMap: newDataLevelExpandMap};
        case 'TOGGLE_DT_DATA_SET':
            let newExpandedDataSetIdSet = new Set(state.expandedDataSetIdSet);
            if (newExpandedDataSetIdSet.has(action.dataSetId)) {
                newExpandedDataSetIdSet.delete(action.dataSetId);
            } else {
                newExpandedDataSetIdSet.add(action.dataSetId);
            }
            return {...state, expandedDataSetIdSet: newExpandedDataSetIdSet};
        case 'TOGGLE_DT_GRAD_LAYER':
            let newSelectedAddtnlGradLayerIdSet;
            newSelectedAddtnlGradLayerIdSet = new Set(state.selectedAddtnlCountyGradLayerIdSet);
            if (newSelectedAddtnlGradLayerIdSet.has(action.gradLayerId)) {
                newSelectedAddtnlGradLayerIdSet.delete(action.gradLayerId);
            } else {
                newSelectedAddtnlGradLayerIdSet.add(action.gradLayerId);
            }
            return {...state, selectedAddtnlCountyGradLayerIdSet: newSelectedAddtnlGradLayerIdSet, shouldUpdate: true};
        case 'OPEN_DATA_TABLE_EXCEL_FILE':
            return {...state};
        case 'TOGGLE_DATA_TABLE_AUTO_REFRESH':
            return {...state, autoRefresh: !state.autoRefresh};
        case 'TOGGLE_DATA_TABLE_INVERT_QUERY':
            return {...state, invertQuery: !state.invertQuery, shouldUpdate: true};
        case 'SELECT_ITEM':
            return {...state, selectedDataTableRow: action.index};
        case 'CHANGE_MAP_TAB':
            return {...state, selectedDataTableRow: null};
        case 'MARKER_CLICKED':
            return {...state, markerLat: action.lat, markerLng: action.lng, sendMarker: true};
        case 'INFO_WINDOW_LINK_CLICKED':
            return {...state, infoWindowSelectedId: action.infoWindowSelectedId};
        default:
            return state;
    }
};

export default layerDataListingReducer;
