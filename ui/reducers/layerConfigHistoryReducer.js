import initialState from '../state/initialState'

const layerConfigHistoryReducer = (state = initialState.layerConfigHistory , action) => {

    switch(action.type) {
        case "UPDATE_SAVED_LAYER_CONFIG_LIST":
            return {
                ...state,
                savedLayerConfigList: action.savedLayerConfigList
            };
        default:
            return state;
    }
};


export default layerConfigHistoryReducer;