import initialState from "../state/initialState";

const captchaReducer = (state = initialState.captcha, action) => {
    switch (action.type) {
        case 'CAPTCHA_CHANGE':
            let gRecaptchaResponse = action.gRecaptchaResponse;
            return {
                gRecaptchaResponse: gRecaptchaResponse
            };
        case 'PROCESS_FEEDBACK_FORM_SUCCESS':
            return {
                gRecaptchaResponse: 'reset'
            };
        default:
            return state;
    }
};

export default captchaReducer;