import initialState from "../state/initialState";

const idleTimerReducer = (state = initialState.idleTimeout, action) => {
    switch(action.type) {
        case "IDLE_ON":
            return {
                ...state,
                isIdle: true
            };
        case "IDLE_LOGOUT":
            return {
                ...state,
                isIdle: false,
                remaining: 0
            }
        case "IDLE_CONTINUE":
            return {
                ...state,
                isIdle: false
            }
        case "NON_IDLE_USER_ACTION":
            return {
                ...state,
                lastActive: action.lastActive,
                remaining: action.remaining,
                elapsed: action.elapsed
            }
        default:
            return state;
    }
}

export default idleTimerReducer;