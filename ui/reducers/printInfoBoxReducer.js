import initialState from "../state/initialState";

const printInfoBoxReducer = (state = initialState.printInfoBox, action) => {
    let newPrintInfoBoxSet = new Set(state.printInfoBoxSet);

    switch(action.type) {
        case "ADD_LAYER_INFO_BOX":
            newPrintInfoBoxSet.add(action.layerInfoBoxId);
            return {...state, printInfoBoxSet: newPrintInfoBoxSet};
        case "REMOVE_LAYER_INFO_BOX":
            newPrintInfoBoxSet.delete(action.layerInfoBoxId);
            return {...state, printInfoBoxSet: newPrintInfoBoxSet};
        case "TOGGLE_MAX_LIMIT_DIALOG":
            return {...state, maxLimitDialogBoxOpen: action.value};
        case "REMOVE_ALL_LAYER_INFO_BOX":
            newPrintInfoBoxSet.clear();
            return {...state, printInfoBoxSet: newPrintInfoBoxSet};
        default:
            return state;
    }
};

export default  printInfoBoxReducer;