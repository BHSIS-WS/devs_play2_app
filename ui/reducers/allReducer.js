import { combineReducers } from 'redux'
import initialState from '../state/initialState'
import normalLayerReducer from './normalLayerReducer'
import gradientLayerReducer from './gradientLayerReducer'
import mapCanvasD3Reducer from './mapCanvasD3Reducer'
import layerReducer from './layerReducer'
import d3LayerConfigReducer from './d3LayerConfigReducer'
import d3MapToolOptionsReducer from './d3MapToolOptionsReducer'
import colorPaletteReducer from './colorPaletteReducer'
import refReducer from './refReducer'
import groupReducer from './groupReducer'
import layerDataListingReducer from "./layerDataListingReducer";
import mapVariablesReducer from './mapVariablesReducer';
import layerConfigHistoryReducer from './layerConfigHistoryReducer';
import toolbarOptionsReducer from './toolbarOptionsReducer';
import sidebarOptionsReducer from './sidebarOptionsReducer';
import feedbackReducer from './feedbackReducer';
import captchaReducer from './CaptchaReducer';
import idleTimerReducer from './IdleTimerReducer';
import printMapLegendReducer from "./printMapLegendReducer";
import printInfoBoxReducer from "./printInfoBoxReducer";

const mapLegendReducer = (state = initialState.selectedMapLegendLayerId, action) => {
    switch(action.type) {
        case "SET_SELECTED_MAP_LEGEND_LAYER_ID":
            return  action.selectedMapLegendLayerId;
        default:
            return state;
    }
};

const layerPrintControlReducer = (state = initialState.selectedPrintControlLayerId, action) => {
    switch(action.type) {
        case "SET_SELECTED_PRINT_CONTROL_LAYER_ID":
            console.log(action.selectedPrintControlLayerId);
            return action.selectedPrintControlLayerId;
        default:
            return state;
    }
};

const mapTabReducer = (state = initialState.selectedMapTab, action) => {

  switch (action.type) {
    case 'CHANGE_MAP_TAB':
      let newTab=action.value;
      if (newTab === "Print" || newTab === "Carto"){
        return newTab;
      }
      break;
    default:
      return state
  }
};


const allReducer = combineReducers({
    selectedMapLegendLayerId: mapLegendReducer,
    selectedMapTab: mapTabReducer,
    selectedPrintControlLayerId: layerPrintControlReducer,
    mapCanvasD3: mapCanvasD3Reducer,
    colorPalette: colorPaletteReducer,
    groups: groupReducer,
    normalLayers: normalLayerReducer,
    gradientLayers: gradientLayerReducer,
    layers: layerReducer,
    d3LayerConfig: d3LayerConfigReducer,
    d3MapToolOptions: d3MapToolOptionsReducer,
    ref: refReducer,
    layerDataListing: layerDataListingReducer,
    mapVariables: mapVariablesReducer,
    layerConfigHistory: layerConfigHistoryReducer,
    toolbarOptions: toolbarOptionsReducer,
    sidebarOptions: sidebarOptionsReducer,
    feedbackForm: feedbackReducer,
    captcha: captchaReducer,
    idleTimeout: idleTimerReducer,
    printMapLegend: printMapLegendReducer,
    printInfoBox: printInfoBoxReducer
});

export default allReducer



