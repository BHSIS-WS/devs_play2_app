"use strict";

function processIdleOn() {
    return {
      type: "IDLE_ON"
    };
}

function processIdleLogout() {
    return {
        type: "IDLE_LOGOUT"
    };
}

function processContinueSession() {
    return {
        type: "IDLE_CONTINUE"
    };
}

function processUserAction(lastActiveTime, remainingTime, elapsedTime) {
    return {
        type: "NON_IDLE_USER_ACTION",
        lastActive: lastActiveTime,
        remaining: remainingTime,
        elapsed: elapsedTime
    };
}

export {
    processIdleOn,
    processIdleLogout,
    processContinueSession,
    processUserAction
}