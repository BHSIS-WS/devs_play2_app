"use strict";
import store from '../redux/store';
import { hideRemoveConfigDialog, showSaveConfigDuplicateNameError, hideSaveLayerConfigDialog } from "./ToolbarOptionsAction";

function fetchSavedLayerConfigList(ajaxObject) {
    if(ajaxObject) {
        let response = ajaxObject.response;
        if(response && response.error === true) {
            store.dispatch(showSaveConfigDuplicateNameError(true));
        } else {
            store.dispatch(showSaveConfigDuplicateNameError(false));
            store.dispatch(hideSaveLayerConfigDialog());
        }
    }
    return {
        type: "FETCH_SAVED_LAYER_CONFIG_LIST"
    };
}

function updateSavedLayerConfigList(savedLayerConfigList) {
    store.dispatch(hideRemoveConfigDialog());
    return {
        type: "UPDATE_SAVED_LAYER_CONFIG_LIST",
        savedLayerConfigList: savedLayerConfigList
    };
}

function toggleVisibilityForSavedLayerConfigPanel() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_SAVED_LAYER_CONFIG_PANEL"
    };
}

export {
    fetchSavedLayerConfigList,
    updateSavedLayerConfigList,
    toggleVisibilityForSavedLayerConfigPanel
};