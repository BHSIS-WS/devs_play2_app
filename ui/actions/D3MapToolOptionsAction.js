
function toggleD3MapTextToolOptionDialog(textId, value) {
    return {
        type: "TOGGLE_D3_MAP_TEXT_TOOL_OPTION_DIALOG",
        textId: textId,
        value: value
    };
}

function changeD3MapTextToolFontSize (value) {
    return {
        type: "CHANGE_D3_MAP_TEXT_TOOL_FONT_SIZE",
        value: value
    };
}

function changeD3MapTextToolRotate (value) {
    return {
        type: "CHANGE_D3_MAP_TEXT_TOOL_ROTATE",
        value: value
    };
}

function changeD3MapTextToolFillColor (value) {
    return {
        type: "CHANGE_D3_MAP_TEXT_TOOL_FILL_COLOR",
        value: value
    };
}

function changeD3MapTextToolTextContent (value) {
    return {
        type: "CHANGE_D3_MAP_TEXT_TOOL_TEXT_CONTENT",
        value: value
    };
}

function changeD3MapTextToolFontFamily(value) {
    return {
        type: "CHANGE_D3_MAP_TEXT_TOOL_FONT_FAMILY",
        value: value
    };
}

function toggleD3MapTextToolBold() {
    return {
        type: "TOGGLE_D3_MAP_TEXT_TOOL_BOLD"
    };
}

function toggleD3MapTextToolItalic() {
    return {
        type: "TOGGLE_D3_MAP_TEXT_TOOL_ITALIC"
    };
}

function toggleD3MapTextToolUnderline() {
    return {
        type: "TOGGLE_D3_MAP_TEXT_TOOL_UNDERLINE"
    };
}

function closeD3MapTextToolOptionDialog () {
    return {
        type: "CLOSE_D3_MAP_TEXT_TOOL_OPTION_DIALOG"
    };
}

function applyD3MapTextToolOptions(textId, fontSize, rotate, fillColor, textAreaFields, fontFamily, bold,
                                    italic, underline) {
    return {
        type: "APPLY_D3_MAP_TEXT_TOOL_OPTIONS",
        textId: textId,
        fontSize: fontSize,
        rotate: rotate,
        fillColor: fillColor,
        textContent: textAreaFields.textContent,
        fontFamily: fontFamily,
        bold: bold,
        italic: italic,
        underline: underline,
        width: textAreaFields.width,
        height: textAreaFields.height
    };
}

function addD3MapText(textId){
    return {
        type: "ADD_D3_MAP_TEXT",
        textId: textId
    };
}

function removeD3MapText() {
    return {
        type: "REMOVE_D3_MAP_TEXT",
    };
}

//======================= Rectangle Tool ============================

function toggleD3MapRectTool() {
    return {
        type: "TOGGLE_D3_MAP_RECT_TOOL"
    };
}

function toggleD3MapRectToolOptionDialog(rectId) {
    return {
        type: "TOGGLE_D3_MAP_RECT_TOOL_OPTION_DIALOG",
        rectId: rectId
    };
}

function closeD3MapRectToolOptionDialog() {
    return {
        type: "CLOSE_D3_MAP_RECT_TOOL_OPTION_DIALOG"
    };
}

function applyD3MapRectToolOptions(width, height, stroke, strokeWidth) {
    return {
        type: "APPLY_D3_MAP_RECT_TOOL_OPTIONS",
        width: width,
        height: height,
        stroke: stroke,
        strokeWidth: strokeWidth
    };
}

function addD3MapRect(width, height, rectId) {
    return {
        type: "ADD_D3_MAP_RECT",
        width: width,
        height: height,
        rectId: rectId
    };
}

function removeD3MapRect() {
    return {
        type: "REMOVE_D3_MAP_RECT"
    };
}

function changeD3MapRectToolWidth(value) {
    return {
        type: "CHANGE_D3_MAP_RECT_TOOL_WIDTH",
        value: value
    };
}

function changeD3MapRectToolHeight(value) {
    return {
        type: "CHANGE_D3_MAP_RECT_TOOL_HEIGHT",
        value: value
    };
}

function changeD3MapRectToolStroke(value) {
    return {
        type: "CHANGE_D3_MAP_RECT_TOOL_STROKE",
        value: value
    };
}

function changeD3MapRectToolStrokeWidth(value) {
    return {
        type: "CHANGE_D3_MAP_RECT_TOOL_STROKE_WIDTH",
        value: value
    };
}

function setD3MapToolOptionsStateFromConfig(data) {
    return {
        type: "SET_D3_MAP_TOOL_OPTIONS_STATE_FROM_CONFIG",
        d3MapToolOptions: data
    };
}

function saveMapToolElementCoordinates(data) {
    return {
        type: "SAVE_MAP_TOOL_ELEMENT_COORDINATES",
        data: data
    }
}

export {
    toggleD3MapTextToolOptionDialog,
    changeD3MapTextToolFontSize,
    changeD3MapTextToolRotate,
    changeD3MapTextToolFillColor,
    changeD3MapTextToolTextContent,
    changeD3MapTextToolFontFamily,
    toggleD3MapTextToolBold,
    toggleD3MapTextToolItalic,
    toggleD3MapTextToolUnderline,
    closeD3MapTextToolOptionDialog,
    applyD3MapTextToolOptions,
    addD3MapText,
    removeD3MapText,

    toggleD3MapRectTool,
    toggleD3MapRectToolOptionDialog,
    closeD3MapRectToolOptionDialog,
    applyD3MapRectToolOptions,
    addD3MapRect,
    removeD3MapRect,
    changeD3MapRectToolWidth,
    changeD3MapRectToolHeight,
    changeD3MapRectToolStroke,
    changeD3MapRectToolStrokeWidth,

    setD3MapToolOptionsStateFromConfig,
    saveMapToolElementCoordinates
}