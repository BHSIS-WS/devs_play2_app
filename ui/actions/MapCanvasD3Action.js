function setMapCanvasD3StateFromConfig(data) {
    return {
        type: "SET_MAP_CANVAS_D3_STATE_FROM_CONFIG",
        mapCanvasD3: data
    };
}

function resizeCanvas(height, width) {
    return {
        type: "RESIZE_CANVAS",
        height: height,
        width: width
    };
}

export {
    setMapCanvasD3StateFromConfig,
    resizeCanvas
}