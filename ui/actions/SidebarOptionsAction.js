"use strict";

function toggleVisibilitySidebarButtons() {
    return {
        type: "TOGGLE_VISIBILITY_SIDEBAR_BUTTONS"
    }
}
function toggleVisibilityForSavedLayerConfigPanel() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_SAVED_LAYER_CONFIG_PANEL"
    };
}

function toggleVisibilityForSelectLayersPanel() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_SELECT_LAYERS_PANEL"
    };
}

function toggleVisibilityForLayerPrintControlPanel() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_LAYER_PRINT_CONTROL_PANEL"
    };
}

function toggleVisibilityForFeedbackPanel() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_FEEDBACK_PANEL"
    };
}

function toggleVisibilityForHelpPanel() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_HELP_PANEL"
    };
}
function toggleVisibilityShowAnswer(devsHelpTopicId) {
    return {
        type: "TOGGLE_VISIBILITY_ShowAnswer",
        devsHelpTopicId: devsHelpTopicId
    };
}
function toggleVisibilityShowPlayButtons(devsHelpTopicId) {
    return {
        type: "TOGGLE_VISIBILITY_ShowPlayButtons",
        devsHelpTopicId: devsHelpTopicId
    };
}
function setVisibilityPlayPane(devsHelpTopicId) {
     return {
         type: "SET_VISIBILITY_PLAYPANE",
         devsHelpTopicId: devsHelpTopicId
     };
 }
 function setclosePlayPane(devsHelpTopicId) {
     return {
         type: "CLOSE_PLAYPANE",
         devsHelpTopicId: devsHelpTopicId
     };
 }
export {
    toggleVisibilitySidebarButtons,
    toggleVisibilityForSavedLayerConfigPanel,
    toggleVisibilityForSelectLayersPanel,
    toggleVisibilityForLayerPrintControlPanel,
    toggleVisibilityForFeedbackPanel,
    toggleVisibilityForHelpPanel,
    toggleVisibilityShowAnswer,
    toggleVisibilityShowPlayButtons,
    setVisibilityPlayPane,
    setclosePlayPane,
};