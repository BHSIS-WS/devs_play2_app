function toggleGroups(layerType) {
    return {
        type:"TOGGLE_GROUPS",
        layerType: layerType
    };
}

export {
    toggleGroups
}