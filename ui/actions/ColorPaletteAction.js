

function toggleColorPalette(value, criteriaKey, criteriaCodeKey, layerId, layerGroupType, topPosition, leftPosition, visibleSection, actionType) {
    return {
        type: "TOGGLE_PALETTE",
        value: value,
        criteriaKey: criteriaKey,
        categoryCodeKey: criteriaCodeKey,
        layerId: layerId,
        groupType: layerGroupType,
        topPosition: topPosition,
        leftPosition: leftPosition,
        visibleSection: visibleSection,
        actionType: actionType
    };
}

function updateSelectedGradientShapeConfiguration(layerConfig) {
    return {
        type: 'UPDATE_SELECTED_GRADIENT_SHAPE_CONFIGURATION',
        layerConfig: layerConfig
    };
}

function updateSelectedShapeConfiguration(layerConfig, hasSameShapeStyle) {
    return {
        type: 'UPDATE_SELECTED_SHAPE_CONFIGURATION',
        layerConfig: layerConfig,
        hasSameShapeStyle: hasSameShapeStyle
    };
}




export {
    toggleColorPalette,
    updateSelectedGradientShapeConfiguration,
    updateSelectedShapeConfiguration
}