"use strict";

function toggleVisibilityForPlayVideo() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_PLAYVIDEO"
    };
}

export {
    toggleVisibilityForPlayVideo
};