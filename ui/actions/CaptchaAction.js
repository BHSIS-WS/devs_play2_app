"use strict";

function captchaChange(gRecaptchaResponse) {
    return {
        type: "CAPTCHA_CHANGE",
        gRecaptchaResponse: gRecaptchaResponse
    }
}
export {
    captchaChange
};