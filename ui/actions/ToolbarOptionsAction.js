"use strict";

import printContentModel from "../others/PrintContentModel";
import store from "../redux/store";

function toggleVisibilityForSaveLayerConfigDialog() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_SAVE_LAYER_CONFIG_DIALOG"
    };
}

function toggleVisibilityForSavePdfDialog() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_SAVE_PDF_DIALOG"
    };
}

function toggleVisibilityForRenderStatusPanel(isVisible) {
    return {
        type: "TOGGLE_VISIBILITY_FOR_RENDER_STATUS_PANEL",
        isVisible: isVisible
    };
}

function toggleVisibilityForCanvasAttributesDialog() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_CANVAS_ATTRIBUTES_DIALOG"
    };
}

function toggleVisibilityForMinimapPanel() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_MINIMAP_PANEL"
    };
}

function showRemoveConfigDialog(userSavedLayerConfig) {
    return {
        type: "SHOW_REMOVE_CONFIG_DIALOG",
        userSavedLayerConfig: userSavedLayerConfig
    }
}

function hideRemoveConfigDialog() {
    return {
        type: "HIDE_REMOVE_CONFIG_DIALOG"
    }
}

function hideSavePdfDialog() {
    return {
        type: "HIDE_SAVE_PDF_DIALOG"
    };
}

function hideSaveLayerConfigDialog() {
    return {
        type: "HIDE_SAVE_LAYER_CONFIG_DIALOG"
    };
}

function savePdf(width, height) {
    printContentModel.performPDFGeneration(width, height);
    return {
        type: "SAVE_PDF"
    };
}

function saveLayerConfigForUser(name, description) {
    return {
        type: "SAVE_LAYER_CONFIG_FOR_USER",
        name: name,
        description: description
    };
}

function showSaveConfigDuplicateNameError(status) {
    return {
        type: "SHOW_SAVE_CONFIG_DUPLICATE_NAME_ERR",
        status: status
    };
}

function hideCanvasAttributesDialog() {
    return {
        type: "HIDE_CANVAS_ATTRIBUTES_DIALOG"
    };
}

function removeSavedLayerConfig(userSavedLayerConfig) {
    return {
        type: "REMOVE_SAVED_LAYER_CONFIG",
        userSavedLayerConfig: userSavedLayerConfig
    }
}

function updateRenderStatusForLayer(layerId, layerType, status) {
    return {
        type: "UPDATE_RENDER_STATUS_FOR_LAYER",
        layerId: layerId,
        layerType: layerType,
        status: status
    };
}

function setToolbarOptionsStateFromConfig(data) {
    return {
        type: "SET_TOOLBAR_OPTIONS_STATE_FROM_CONFIG",
        toolbarOptions: data
    };
}

function addOrRemoveLayerInRenderStatusLayerList (layerId, layerType) {
    return {
        type: "ADD_OR_REMOVE_LAYER_IN_RENDER_STATUS_LAYER_LIST",
        layerId: layerId,
        layerType: layerType
    };
}

function toggleVisibilityForPrintMapLegend() {
    return {
        type: "TOGGLE_VISIBILITY_FOR_PRINT_MAP_LEGEND"
    };
}

function performD3MapZoomIn() {
    printContentModel.performZoomIn();
}

function performD3MapZoomOut() {
    printContentModel.performZoomOut();
}

function downloadToImage() {
    printContentModel.downloadToImage(store.getState(), store.dispatch);
}

function downloadToPdf() {
    printContentModel.downloadToPdf(store.getState(), store.dispatch);
}

function refreshMapLayers(evt) {
    printContentModel.refreshMapLayers(evt, store.getState(), store.dispatch);
}

function toggleMouseZoomForPrintMap(disableMouseZoom) {
    printContentModel.toggleMouseZoom(disableMouseZoom);
    return {
        type: "TOGGLE_MOUSE_ZOOM_FOR_PRINT_MAP"
    };
}

export {
    toggleVisibilityForSaveLayerConfigDialog,
    toggleVisibilityForSavePdfDialog,
    toggleVisibilityForRenderStatusPanel,
    toggleVisibilityForCanvasAttributesDialog,
    toggleVisibilityForMinimapPanel,
    showRemoveConfigDialog,
    hideRemoveConfigDialog,
    hideSavePdfDialog,
    hideSaveLayerConfigDialog,
    hideCanvasAttributesDialog,
    savePdf,
    saveLayerConfigForUser,
    showSaveConfigDuplicateNameError,
    removeSavedLayerConfig,
    updateRenderStatusForLayer,
    setToolbarOptionsStateFromConfig,
    addOrRemoveLayerInRenderStatusLayerList,
    toggleVisibilityForPrintMapLegend,
    performD3MapZoomIn,
    performD3MapZoomOut,
    downloadToImage,
    downloadToPdf,
    refreshMapLayers,
    toggleMouseZoomForPrintMap
};