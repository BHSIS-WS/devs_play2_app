import store from "../redux/store";
import {addOrRemoveLayerInRenderStatusLayerList} from "./ToolbarOptionsAction";

function setGradientLayersStateFromConfig(data) {
    return {
        type: "SET_GRADIENT_LAYERS_STATE_FROM_CONFIG",
        gradientLayers: data
    };
}

function toggleGradientFilter(gradLayerId) {
    return{
        type:"TOGGLE_GRADIENT_FILTER",
        gradLayerId: gradLayerId
    };
}

function toggleGradientLayer(gradLayerId) {
    return{
        type:"TOGGLE_GRADIENT_LAYER",
        gradLayerId: gradLayerId
    };
}

function addGradientFilterEntry(gradLayerId, firstFilterSign, firstFilterValue) {
    return {
        type: "ADD_GRADIENT_FILTER_ENTRY",
        gradLayerId: gradLayerId,
        firstFilterSign: firstFilterSign,
        firstFilterValue: firstFilterValue
    };
}

function deleteGradientFilterEntry(gradLayerId, filterEntryIndex) {
    return {
        type: "DELETE_GRADIENT_FILTER_ENTRY",
        gradLayerId: gradLayerId,
        filterEntryIndex: filterEntryIndex
    };
}

function changeGradientFilterEntrySign(gradLayerId, filterEntryIndex, filterSign) {
    return {
        type: "CHANGE_GRADIENT_FILTER_ENTRY_SIGN",
        gradLayerId: gradLayerId,
        filterEntryIndex: filterEntryIndex,
        filterSign: filterSign
    };
}

function changeGradientFilterEntryValue(gradLayerId, filterEntryIndex, filterValue) {
    return {
        type: "CHANGE_GRADIENT_FILTER_ENTRY_VALUE",
        gradLayerId: gradLayerId,
        filterEntryIndex: filterEntryIndex,
        filterValue: filterValue
    };
}

export {
    setGradientLayersStateFromConfig,
    toggleGradientFilter,
    toggleGradientLayer,
    addGradientFilterEntry,
    deleteGradientFilterEntry,
    changeGradientFilterEntrySign,
    changeGradientFilterEntryValue
}