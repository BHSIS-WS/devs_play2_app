"use strict";

function validateError(response) {
    return {
        type: "FEEDBACK_VALIDATION_ERROR",
        result: response.result,
        message: response.message
    }
}

function processForm(response) {
    if (response && (response.result === "wrongCaptchaCode" || response.result === "error")) {
        return validateError(response);
    } else if (response && response.result === "succeeded") {
        return successResult(response);
    }
}

function successResult(response) {
    return {
        type: "PROCESS_FEEDBACK_FORM_SUCCESS",
        result: response.result,
        message: response.message
    }
}

export {
    validateError,
    processForm,
    successResult
};