"use strict";

function toggleNormalLayerLegendDisplay(layerId) {
    return {
        type: 'TOGGLE_NORMAL_LAYER_LEGEND_DISPLAY',
        layerId: layerId
    };
}

function toggleGradientLayerLegendDisplay(layerId) {
    return {
        type: 'TOGGLE_GRADIENT_LAYER_LEGEND_DISPLAY',
        layerId: layerId
    };
}

export {
    toggleNormalLayerLegendDisplay,
    toggleGradientLayerLegendDisplay
}