"use strict";

function setD3LayerConfigStateFromConfig(data) {
    return {
        type: "SET_D3_LAYER_CONFIG_STATE_FROM_CONFIG",
        d3LayerConfig: data
    };
}

function setSelectedPrintControlLayerId(layerId) {
    return {
        type: "SET_SELECTED_PRINT_CONTROL_LAYER_ID",
        selectedPrintControlLayerId: layerId
    };
}

function toggleD3LayerConfigPanel(layerId, isGradientLayer) {
    return {
        type: "TOGGLE_D3_LAYER_CONFIG_PANEL",
        layerId: layerId,
        isGradientLayer: isGradientLayer
    }
}

function toggleD3LayerDisable(layerId, isGradientLayer) {
    return {
        type: "TOGGLE_D3_LAYER_DISABLE",
        layerId: layerId,
        isGradientLayer: isGradientLayer
    }
}

function updateConfigTab(layerId, value, isGradientLayer) {
    return {
        type: "UPDATE_CONFIG_TAB",
        layerId: layerId,
        value: value,
        isGradientLayer: isGradientLayer
    }
}

function updateGeneralType(layerId, variableName, value, isGradientLayer) {
    return {
        type: "UPDATE_GENERAL_TYPE",
        layerId: layerId,
        variableName: variableName,
        value: value,
        isGradientLayer: isGradientLayer}
}

function updateCoordinateTolerance(layerId, value, isGradientLayer) {
    return {
        type: "UPDATE_COORDINATE_TOLERANCE",
        layerId: layerId,
        value: value,
        isGradientLayer: isGradientLayer
    }
}

function updateGradientConfigValue(index, value, layerId) {
    return {
        type: "UPDATE_GRADIENT_CONFIG_VALUE",
        idx: index,
        value: value,
        layerId: layerId
    };
}

function addNewGradientConfigValue(layerId) {
    return {
        type: "ADD_NEW_GRADIENT_CONFIG_VALUE",
        layerId: layerId
    };
}

function removeGradientConfigValue(index, layerId) {
    return {
        type: "REMOVE_GRADIENT_CONFIG_VALUE",
        idx: index,
        layerId: layerId
    };
}

function initializeGradientConfigMap(value, layerId) {
    return {
        type: "INITIALIZE_GRADIENT_CONFIG_MAP",
        value: value,
        layerId: layerId
    };
}

function initializeLayerConfigMap(value, layerId) {
    return {
        type: "INITIALIZE_LAYER_CONFIG_MAP",
        value: value,
        layerId: layerId
    };
}

function initializeD3LayerConfigs(data) {
    let refArray = data.ref;
    let layerArray = refArray[4].fusionmaplayers;
    let gradientLayerArray = refArray[5].gradientmaplayers;
    return {
        type: "INITIALIZE_D3_LAYER_CONFIGS",
        layerArray: layerArray,
        gradientLayerArray: gradientLayerArray
    };
}

function updateCodeConfigId(layerId, value) {
    return {
        type: "UPDATE_CONFIG_CODE_ID",
        layerId: layerId,
        value: value
    };
}

function updateShapeForAll(layerId, value) {
    return {
        type: "UPDATE_SHAPE_FOR_ALL",
        layerId: layerId,
        value: value
    }
}

function updateShapeByVariable(layerId, value) {
    return {
        type: "UPDATE_SHAPE_BY_VARIABLE",
        layerId: layerId,
        value: value
    }
}

function toggleDisplayLegendCategory (criteriaKey, layerId, isGradientLayer) {
    return {
        type: "TOGGLE_LEGEND_FOR_CATEGORICAL_CONFIG",
        criteriaKey: criteriaKey,
        layerId: layerId,
        isGradientLayer: isGradientLayer
    }
}


export {
    setD3LayerConfigStateFromConfig,
    setSelectedPrintControlLayerId,
    toggleD3LayerConfigPanel,
    toggleD3LayerDisable,
    updateConfigTab,
    updateGeneralType,
    updateCoordinateTolerance,
    updateGradientConfigValue,
    addNewGradientConfigValue,
    removeGradientConfigValue,
    initializeGradientConfigMap,
    initializeLayerConfigMap,
    initializeD3LayerConfigs,
    updateCodeConfigId,
    updateShapeByVariable,
    updateShapeForAll,
    toggleDisplayLegendCategory
};