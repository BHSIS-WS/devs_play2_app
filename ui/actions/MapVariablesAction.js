"use strict";
import mapContentModel from "../others/MapContentModel";
import store from "../redux/store";

function setRightClickValues(rightClickCoordinates, rightClickContextMenuPosition, rightClickedMarker, rightClickedShape) {
    return {
        type: "SET_RIGHT_CLICK_VALUES",
        rightClickCoordinates: rightClickCoordinates,
        rightClickContextMenuPosition: rightClickContextMenuPosition,
        rightClickedMarker: rightClickedMarker,
        rightClickedShape: rightClickedShape
    };
}


function dropMarkerForRightClickCoordinates() {
    mapContentModel.dropMarkerFromRightClickCoordinates(store.getState(), store.dispatch);
    return {
        type: "SET_RIGHT_CLICK_VALUES",
        rightClickCoordinates: null,
        rightClickContextMenuPosition: null,
        rightClickedMarker: null,
        rightClickedShape: null
    }
}

function showDropMarkerDialog(dialogType) {
    if (dialogType === "latlng") {
        mapContentModel.removeAutocomplete();
    } else if (dialogType === "location") {
        mapContentModel.initializeAutocomplete(store.dispatch);
    }
    return {
        type: "SET_VISIBILITY_FOR_DROP_MARKER_DIALOG",
        dialogType: dialogType,
        visible: true
    }
}

function hideDropMarkerDialog() {
    return {
        type: "SET_VISIBILITY_FOR_DROP_MARKER_DIALOG",
        dialogType: null,
        visible: false
    }
}

function zoomMapIn() {
    return setMapZoomLevel(store.getState().mapVariables.zoomLevel + 1, true);
}

function zoomMapOut() {
    return setMapZoomLevel(store.getState().mapVariables.zoomLevel - 1, true);
}

function centerMapOnRightClickCoordinates() {
    mapContentModel.centerMapOnRightClickLatLng(store.getState(), store.dispatch);
    return {
        type: "SET_RIGHT_CLICK_VALUES_NULL"
    };
}

function refreshMap() {
    mapContentModel.refreshGoogleMapLayers(store.getState(), store.dispatch);
    return {
        type: "SET_RIGHT_CLICK_VALUES_NULL"
    };
}

function addMarkerValuesToMarkerList(marker, markerLocation) {
    return {
        type: "ADD_MARKER_TO_MARKER_LIST",
        marker: marker,
        markerLocation: markerLocation
    };
}

function addShapeToShapeList(shape) {
    return {
        type: "ADD_SHAPE_TO_SHAPE_LIST",
        shape: shape
    }
}

function removeRightClickedMarker() {
    mapContentModel.removeRightClickedMarker(store.getState(), store.dispatch);
    return {
        type: "REMOVE_RIGHT_CLICKED_MARKER"
    };
}

function removeNonRightClickedMarkers() {
    mapContentModel.removeNonRightClickedMarkers(store.getState(), store.dispatch);
    return {
        type: "REMOVE_NON_RIGHT_CLICKED_MARKERS"
    };
}

function removeRightClickedShape() {
    mapContentModel.removeRightClickedShape(store.getState(), store.dispatch);
    return {
        type: "REMOVE_RIGHT_CLICKED_SHAPE"
    }
}

function toggleEditableRightClickedShape() {
    mapContentModel.toggleEditableRightClickedShape(store.getState(), store.dispatch);
    return {
        type: "TOGGLE_EDITABLE_RIGHT_CLICKED_SHAPE"
    }
}

function toggleDraggableRightClickedShape() {
    mapContentModel.toggleDraggableRightClickedShape(store.getState(), store.dispatch);
    return {
        type: "TOGGLE_DRAGGABLE_RIGHT_CLICKED_SHAPE"
    }
}

function removeAllMarkers() {
    mapContentModel.removeAllMarkers(store.getState(), store.dispatch);
    return {
        type: "REMOVE_ALL_MARKERS"
    };
}

function removeAllShapes() {
    mapContentModel.removeAllShapes(store.getState(), store.dispatch);
    return {
        type: "REMOVE_ALL_SHAPES"
    };
}

function dropMarkerByLatLng(latLngString) {
    if (typeof latLngString !== "undefined") {
        let latLngArray = latLngString.split(",");
        if (latLngArray.length !== 2) {
            return {
                type: "SET_VISIBILITY_FOR_DROP_MARKER_DIALOG",
                visible: true
            };
        } else {
            mapContentModel.dropMarker(store.getState(), store.dispatch, latLngArray[0], latLngArray[1]);
            return {
                type: "SET_VISIBILITY_FOR_DROP_MARKER_DIALOG",
                visible: false
            };
        }
    } else {
        return {
            type: "SET_VISIBILITY_FOR_DROP_MARKER_DIALOG",
            visible: true
        };
    }
}

function dropMarkerByLocation(locationString) {
    if (typeof locationString !== "undefined") {
        mapContentModel.dropMarkerFromLocation(store.getState(), store.dispatch, locationString);
        return {
            type: "SET_VISIBILITY_FOR_DROP_MARKER_DIALOG",
            visible: false
        }
    }
}

function setMapCenter(center) {
    return {
        type: "SET_MAP_CENTER",
        center: center
    };
}

function setMapCenterToStartAddress(center) {
    mapContentModel.centerMapOnCoordinate(store.getState(), center);
    return {
        type: "SET_MAP_CENTER",
        center: center
    }
}

function setMapZoomLevel(zoomLevel, shouldZoomMap) {
    return {
        type: "SET_MAP_ZOOM_LEVEL",
        shouldZoomMap: shouldZoomMap,
        zoomLevel: zoomLevel
    }
}

function hideMaintenanceMessage() {
    return {
        type: "HIDE_MAINTENANCE_MESSAGE"
    }
}

function setMapVariablesStateFromConfig(data) {
    return {
        type: "SET_MAP_VARIABLES_STATE_FROM_CONFIG",
        mapVariables: data
    };
}

// function dataTableRowClicked(item) {
//     let infoWindow = null;
//     if (item) {
//         infoWindow = mapContentModel.addInfoWindow(store.getState(), item);
//     }
//     return {
//         type: "SELECT_ITEM",
//         infoWindow
//     };
// }

function markerClicked(lat, lng) {
    return {
        type: "MARKER_CLICKED",
        lat,
        lng
    };
}

function setupInfoWindow(layerName, htmlList, columnList, data, latLng, position) {
    let htmlToDisplay = "<b>" + layerName + "</b><br/><br/>";
    for (let i = 0; i < columnList.length; i++) {
        let key = columnList[i];
        let value = data[key];
        if (value) {
            htmlToDisplay += htmlList[i].replace("{{" + key + "}}", value);
        }
    }

    return {
        type: "SETUP_INFO_WINDOW",
        latLng: latLng,
        position: position,
        htmlToDisplay: htmlToDisplay
    };
}

function closeInfoWindow() {
    return {
        type: "CLOSE_INFO_WINDOW"
    };
}

export {
    setRightClickValues,
    dropMarkerForRightClickCoordinates,
    dropMarkerByLatLng,
    dropMarkerByLocation,
    showDropMarkerDialog,
    zoomMapIn,
    zoomMapOut,
    centerMapOnRightClickCoordinates,
    refreshMap,
    removeRightClickedMarker,
    removeNonRightClickedMarkers,
    removeAllMarkers,
    removeAllShapes,
    addMarkerValuesToMarkerList,
    removeRightClickedShape,
    toggleEditableRightClickedShape,
    toggleDraggableRightClickedShape,
    addShapeToShapeList,
    hideDropMarkerDialog,
    setMapCenter,
    setMapCenterToStartAddress,
    setMapZoomLevel,
    hideMaintenanceMessage,
    setMapVariablesStateFromConfig,
    // dataTableRowClicked,
    markerClicked,
    setupInfoWindow,
    closeInfoWindow
};