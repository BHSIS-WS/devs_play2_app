"use strict";

function fetchLayerDataList() {
    return {
        type: "FETCH_LAYER_DATA_LIST"
    };
}

function downloadDataTableExcel() {
    return {
        type: "DOWNLOAD_DATA_TABLE_EXCEL"
    };
}

function openDataTableExcelFile(openExcelFileFunc, reqJson) {
    openExcelFileFunc(reqJson);
    return {
        type: "OPEN_DATA_TABLE_EXCEL_FILE"
    };
}

function updateLayerDataListing(data) {
    return {
        type: "UPDATE_LAYER_DATA_LISTING",
        data: data
    };
}

function changeDataTablePageNumber(pageNum, shouldUpdate) {
    return {
        type: "CHANGE_DATA_TABLE_PAGE_NUMBER",
        pageNum: pageNum,
        shouldUpdate
    };
}

function changeDataTablePageSize(pageSize) {
    return {
        type: "CHANGE_DATA_TABLE_PAGE_SIZE",
        pageSize: pageSize
    };
}

function toggleDtStateCountyDataPicker() {
    return {
        type: "TOGGLE_DT_STATE_COUNTY_DATA_PICKER"
    };
}

function toggleDtAdditionalDataLevel(dataLevel) {
    return {
        type: "TOGGLE_DT_ADDITIONAL_DATA_LEVEL",
        dataLevel: dataLevel
    };
}

function toggleDtDataSet(dataSetId) {
    return {
        type: "TOGGLE_DT_DATA_SET",
        dataSetId: dataSetId
    };
}

function toggleDtGradLayer(gradLayerId, dataLevel) {
    return {
        type: "TOGGLE_DT_GRAD_LAYER",
        gradLayerId: gradLayerId,
        dataLevel: dataLevel
    };
}

function toggleDataTableAutoRefresh() {
    return {
        type: "TOGGLE_DATA_TABLE_AUTO_REFRESH"
    };
}

function toggleDataTableInvertQuery() {
    return {
        type: "TOGGLE_DATA_TABLE_INVERT_QUERY"
    };
}

function dataTableRowClicked(index) {
    return {
        type: "SELECT_ITEM",
        index
    }
}

function infoWindowLinkClicked(id) {
    return {
        type: "INFO_WINDOW_LINK_CLICKED",
        infoWindowSelectedId: id
    }
}

export {
    fetchLayerDataList,
    downloadDataTableExcel,
    openDataTableExcelFile,
    updateLayerDataListing,
    changeDataTablePageNumber,
    changeDataTablePageSize,
    toggleDtStateCountyDataPicker,
    toggleDtAdditionalDataLevel,
    toggleDtDataSet,
    toggleDtGradLayer,
    toggleDataTableAutoRefresh,
    toggleDataTableInvertQuery,
    dataTableRowClicked,
    infoWindowLinkClicked
};