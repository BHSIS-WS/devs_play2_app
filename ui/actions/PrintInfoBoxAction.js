"use strict";

function addPrintLayerInfoBox(layerInfoBoxId) {
    return {
        type: "ADD_LAYER_INFO_BOX",
        layerInfoBoxId: layerInfoBoxId
    }
}

function removePrintLayerInfoBox(layerInfoBoxId) {
    return {
        type: "REMOVE_LAYER_INFO_BOX",
        layerInfoBoxId: layerInfoBoxId
    }
}

function toggleInfoBoxMaxLimitDialog(value) {
    return {
        type: "TOGGLE_MAX_LIMIT_DIALOG",
        value: value
    }
}

function removeAllPrintLayerInfoBox() {
    return {
        type: "REMOVE_ALL_LAYER_INFO_BOX"
    }
}

export {
    addPrintLayerInfoBox,
    removePrintLayerInfoBox,
    toggleInfoBoxMaxLimitDialog,
    removeAllPrintLayerInfoBox
};