function setLayersStateFromConfig (data) {
    return {
        type: "SET_LAYERS_STATE_FROM_CONFIG",
        layers: data
    }
}

export {
    setLayersStateFromConfig
}