import store from "../redux/store";
import {addOrRemoveLayerInRenderStatusLayerList} from "./ToolbarOptionsAction";
import D3MapNormalLayer from "../others/d3map/D3MapNormalLayer";
import {fetchLayerDataList} from "./LayerDatatListingAction";
import mapContentModel from "../others/MapContentModel";
import printContentModel from "../others/PrintContentModel";

function setNormalLayersStateFromConfig(data) {
    return {
        type: "SET_NORMAL_LAYERS_STATE_FROM_CONFIG",
        normalLayers: data
    };
}

function changeCriteriaCode(criteriaCodeId, layerId) {
    return {
        type: "CHANGE_CRITERIA_CODE",
        criteriaCodeId: criteriaCodeId,
        layerId: layerId
    }
}

function toggleCriteriaExpanded(criteriaCodeId) {
    return {
        type: "TOGGLE_CRITERIA_EXPANDED",
        criteriaCodeId: criteriaCodeId
    };
}

function toggleCriteriaSelect(layerId, selectedCriteriaCodeId, criteriaId) {
    if (!store.getState().toolbarOptions.renderStatusLayerList.some(layer => layer.layerId === layerId)) {
        store.dispatch(addOrRemoveLayerInRenderStatusLayerList(layerId, "normal"));
    }
    return {
        type: "TOGGLE_CRITERIA_SELECT",
        layerId: layerId,
        criteriaCodeId: selectedCriteriaCodeId,
        criteriaId: criteriaId
    }
}

function removeCriteria(layerId, criteriaId) {
    return {
        type: "REMOVE_CRITERIA",
        layerId: layerId,
        criteriaCodeId: criteriaId
    }
}

function selectAllCriteria(layerId, selectedCriteriaCodeId, layerCriteriaIdArray) {
    if (!store.getState().toolbarOptions.renderStatusLayerList.some(layer => layer.layerId === layerId)) {
        store.dispatch(addOrRemoveLayerInRenderStatusLayerList(layerId, "normal"));
    }
    return {
        type: "SELECT_ALL_CRITERIA",
        layerId: layerId,
        criteriaCodeId: selectedCriteriaCodeId,
        layerCriteriaIdArray: layerCriteriaIdArray
    }
}

function selectNoneCriteria(layerId, selectedCriteriaCodeId, layerCriteriaIdArray) {
    return {
        type: "SELECT_NONE_CRITERIA",
        layerId: layerId,
        criteriaCodeId: selectedCriteriaCodeId,
        layerCriteriaIdArray: layerCriteriaIdArray
    }
}

function selectReverseCriteria(layerId, selectedCriteriaCodeId, layerCriteriaIdArray) {
    return {
        type: "SELECT_REVERSE_CRITERIA",
        layerId: layerId,
        criteriaCodeId: selectedCriteriaCodeId,
        layerCriteriaIdArray: layerCriteriaIdArray
    }
}

function initializeDynamicFilter(criteriaId) {
    return {
        type: "INITIALIZE_DYNAMIC_FILTER",
        criteriaId: criteriaId
    };
}

function removeDynamicCriteriaCondition(criteriaId, conditionId) {
    return {
        type: "REMOVE_DYNAMIC_CRITERIA_CONDITION",
        conditionId: conditionId,
        criteriaId: criteriaId
    }
}

function addDynamicCriteriaCondition(criteriaId, conditionId) {
    return {
        type: "ADD_DYNAMIC_CRITERIA_CONDITION",
        criteriaId: criteriaId,
        conditionId: conditionId
    };
}

function removeDynamicCriteria(criteriaId){
    return {
        type: "REMOVE_DYNAMIC_CRITERIA",
        criteriaId: criteriaId
    };
}

function updateDynamicConditionBound(bound, conditionId, criteriaId, value) {
    return {
        type: "UPDATE_DYNAMIC_CONDITION_BOUND",
        bound: bound,
        conditionId: conditionId,
        criteriaId: criteriaId,
        value: value
    };
}

function toggleDynamicCondition(conditionId, applied, criteriaId, layerId) {
        return {
            type: "TOGGLE_DYNAMIC_CONDITION",
            conditionId: conditionId,
            applied: applied,
            criteriaId: criteriaId,
            layerId: layerId
        };
}

function determineInclusiveWhereStringForCarto(layerId) {
    let normalState = store.getState().normalLayers;
    let layerArray = store.getState().ref.layerArray;
    let layerGeomType = layerArray.find(lay => ((lay['id'] === layerId) ? lay['geom_type'] : '')).geom_type;
    let selectedLayerIdSet = normalState.selectedLayerIdSet;
    let selectedCriteriaIdMap = normalState.selectedCriteriaIdMap;
    let selectedCriteriaIdSet = normalState.selectedCriteriaIdSet;
    let layerParam;
    let fetchInclusiveWhereStringBoolean = false;

    // NOTE: This does NOT handle gradient layers (right now)
    if(selectedLayerIdSet.size >= 1 && selectedCriteriaIdMap.has(layerId)) {
        let selectedGeomType = layerArray.find(lay => ((lay['id'] === layerId) ? lay['geom_type'] : '')).geom_type;
        let selectedGeomTypes = [];
        let selectedLayersFilteredSet = new Set();
        selectedLayerIdSet.forEach(selectedLayer => {
            selectedGeomTypes.push(layerArray.find(lay => ((lay['id'] === selectedLayer) ? lay['geom_type'] : '')).geom_type);
        });
        // This is to make sure if multiple Point layers are selected, they don't try to 'show common' between the two of them
        if(selectedGeomType === "POINT") {
            selectedLayersFilteredSet.add(layerId);
            selectedLayerIdSet.forEach(selectedLayer => {
                let geomType = layerArray.find(lay => ((lay['id'] === selectedLayer) ? lay['geom_type'] : '')).geom_type;
                if (geomType !== "POINT" && !selectedLayersFilteredSet.has(selectedLayer)) {
                    selectedLayersFilteredSet.add(selectedLayer);
                }
            });
        }
        /* Make sure that only one polygon layer and all point layers use the "Show Common" inclusive where string. */
        if(!selectedGeomTypes.includes("POLYGON") || selectedGeomType === "POINT") {
            if(selectedLayersFilteredSet.size > 0) {
                layerParam = new D3MapNormalLayer(layerId, store, null).createRequestParameterJsonStringForCarto2(selectedLayersFilteredSet);
            } else {
                layerParam = new D3MapNormalLayer(layerId, store, null).createRequestParameterJsonStringForCarto();
            }
            fetchInclusiveWhereStringBoolean = true;
        }
    }

    if(fetchInclusiveWhereStringBoolean) {
        return {
            type: "FETCH_INCLUSIVE_WHERE_STRING_FOR_CARTO",
            layerId: layerId,
            layerParams: layerParam
        }
    } else {
        return {
            type: "DO_NOT_FETCH_INCLUSIVE_WHERE_STRING_FOR_CARTO"
        }
    }
}

function setInclusiveWhereString(layerId, ajaxObject) {
    let response = ajaxObject.response;
    return {
        type: "SET_CARTO_INCLUSIVE_WHERE_STRING",
        layerId: layerId,
        whereString: response
    };
}

function removeInclusiveWhereString(layerId) {
    return {
        type: "REMOVE_INCLUSIVE_WHERE_STRING_FOR_CARTO",
        layerId: layerId,
        whereString: ""
    }
}

export {
    setNormalLayersStateFromConfig,
    changeCriteriaCode,
    toggleCriteriaSelect,
    removeCriteria,
    selectAllCriteria,
    selectNoneCriteria,
    selectReverseCriteria,
    toggleCriteriaExpanded,
    initializeDynamicFilter,
    removeDynamicCriteriaCondition,
    addDynamicCriteriaCondition,
    removeDynamicCriteria,
    updateDynamicConditionBound,
    toggleDynamicCondition,
    determineInclusiveWhereStringForCarto,
    setInclusiveWhereString,
    removeInclusiveWhereString
}