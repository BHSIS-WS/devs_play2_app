import { applyMiddleware, createStore } from 'redux';
import allReducer from '../reducers/allReducer';
import allEpics from '../observables/allEpics';
import {createEpicMiddleware} from 'redux-observable';
import detectLayerSelectionMiddleware from './detectLayerSelectionMiddleware';

const middlewares = [createEpicMiddleware(allEpics), detectLayerSelectionMiddleware];

if (process.env.NODE_ENV !== 'production') {
    const { logger } = require('redux-logger');
    middlewares.push(logger);
}

//middleware
const store = createStore(allReducer, applyMiddleware(...middlewares));

export default store;