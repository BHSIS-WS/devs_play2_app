import React from 'react'
import layerSelectionSubject from '../observables/layerSelectionSubject';

const detectLayerSelectionMiddleware = store => next => action => {
    console.log("!->*********Middleware triggered:", action);
    switch (action.type) {
        case 'D3_CANVAS_CHANGE':
            console.log("action.type :" + action.type);
            layerSelectionSubject.next('D3_CANVAS_CHANGE');
            break;
    }
    next(action);
    console.log ("!<- exiting middleWare function.");
};

export default detectLayerSelectionMiddleware;