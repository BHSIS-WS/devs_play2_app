import React from "react";
import FusionLayerController from '../containers/FusionLayerController';

class FusionLayerGroup extends React.Component {

    render() {
        const {group, layerArray} = this.props;
        if (!group) return null;
        return (
            <li>
                <strong>{group.name}</strong>
                <ul className="fusion_layers">
                    {layerArray.map(layer => (<FusionLayerController key={layer.id} layer={layer}/>))}
                </ul>
            </li>
        );
    };

}

export default FusionLayerGroup;