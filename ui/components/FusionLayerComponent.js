import React from "react";
import FusionLayerFilterController from '../containers/FusionLayerFilterController';
import mapContentModel from '../others/MapContentModel';
import printContentModel from '../others/PrintContentModel';
import {removeAllPrintLayerInfoBox} from "../actions/PrintInfoBoxAction";
import {determineInclusiveWhereStringForCarto, removeInclusiveWhereString} from "../actions/NormalLayersAction";

class FusionLayerComponent extends React.Component {

    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
        this.onChangeLayerSelect = this.onChangeLayerSelect.bind(this);
    }

    getClassName() {
        let { expanded, layerCriteriaArray } = this.props;
        if (layerCriteriaArray.length === 0) {
            return '';
        }
        if (expanded) {
            return 'hascriteria open';
        }
        return 'hascriteria';
    }

    onClick(e) {
        let { dispatch, layerCriteriaArray, criteriaArray, layer } = this.props;
        e.preventDefault();
        if (layerCriteriaArray.length === 0) {
            return;
        }
        if (criteriaArray.length === 1) {
            dispatch({type: "CHANGE_CRITERIA_CODE", criteriaCodeId: criteriaArray[0], layerId: layer.id});
        }
        dispatch({type: "TOGGLE_CRITERIA_DISPLAY", layerId: layer.id});
    }

    onChangeLayerSelect(evt) {
        let { dispatch, dataTableAutoRefresh, layer, selectedMapTab, selectedLayerIdSet, layerArray } = this.props;
        dispatch(removeAllPrintLayerInfoBox());
        dispatch({type: "TOGGLE_LAYER_SELECT", layerId: layer.id});
        if(selectedMapTab === "Carto") {
            let selectedGeomType = layerArray.find(lay => ((lay['id'] === layer.id) ? lay['geom_type'] : '')).geom_type;
            /*
             * If the selected layer is a Point we do not want to refresh any/all other layers of any type, so just dispatch for this layer.
             * If the selected layer is a Boundary layer, we want to refresh each other layer so that it can get a new 'inclusive' string
             *   from the backend in order to Show Common. See: this.determineCartoWhereString
             * */
            if(selectedGeomType === "POINT") {
                dispatch(determineInclusiveWhereStringForCarto(layer.id));
            } else {
                /* This is done for POLYGON (Boundary) layers because we need to re-find the inclusive string
                 * for each POINT layer so it can 'Show Common'*/
                this.determineCartoWhereString(layer.id, selectedLayerIdSet, layerArray);
            }
            if(evt.target.checked === false) {
                selectedLayerIdSet.forEach(selectedLayer => {
                    dispatch(removeInclusiveWhereString(selectedLayer));
                });
            }
        }
        dispatch({
            type: "ADD_OR_REMOVE_LAYER_IN_RENDER_STATUS_LAYER_LIST",
            layerId: layer.id,
            layerType: "normal"
        });
        if (dataTableAutoRefresh) {
            dispatch({type: "FETCH_LAYER_DATA_LIST"});
        }
        mapContentModel.refreshGoogleMapLayers();
        printContentModel.refreshD3MapLayersFromState();
    }

    /* For each layer already selected, dispatch an action to fetch a new inclusive Show Common whereString.
     * Since at this point the current selected layer has not been added to the initial state, dispatch an action for that as well.
     */
    determineCartoWhereString(layerId, selectedLayerIdSet, layerArray) {
        let {dispatch} = this.props;
        selectedLayerIdSet.forEach(selectedLayer => {
            // let selectedGeomType = layerArray.find(lay => ((lay['id'] === selectedLayer) ? lay['geom_type'] : '')).geom_type;
            dispatch(determineInclusiveWhereStringForCarto(selectedLayer));
        });
        dispatch(determineInclusiveWhereStringForCarto(layerId))
    }

    render() {
        let { selected, layer, expanded } = this.props;
        if (!layer) return null;
        return (
            <li key={layer.id} className="fusionLayer">
                <input className="nav1" type="checkbox"
                       name="tbd" value="122"
                       onChange={(evt) => this.onChangeLayerSelect(evt)}
                       checked={selected ? "selected" : ""}
                />
                <a href="#" title={layer.layer_name} className={this.getClassName()} data-urlsource="tbd"
                   onClick={this.onClick}>
                    {layer.layer_name}
                </a>
                {expanded ? <FusionLayerFilterController layer={layer} /> : null}
            </li>
        )
    }

}

export default FusionLayerComponent

