import React from 'react';
import {GoggleTabIcon, PrintTabIcon, HistoryIcon, HelpIcon, DoubleArrowIcon, ArrowIcon, ClosedArrowIcon} from "../constant/SvgIcons";

class HeaderButtonsComponent extends React.Component {

    componentDidMount() {
        var cartoObj = document.getElementById("mapCarto");
        cartoObj.classList.add('selected');
    }

    updateVisibilityForSelectLayerPanel() {
        this.props.toggleVisibilityForSelectLayersPanel();
    }

    updateVisibilityForSavedLayerConfigPanel() {
        this.props.toggleVisibilityForSavedLayerConfigPanel();
    }

    updateVisibilityForFeedbackPanel() {
        this.props.toggleVisibilityForFeedbackPanel();
    }

    updateVisibilityForHelpPanel() {
        this.props.toggleVisibilityForHelpPanel();
    }

    updateVisibilityForSidebarButtons() {
        this.props.toggleVisibilitySidebarButtons();
    }

    updateMapTab(target) {
        this.props.changeMapTab(target);
    }

    render() {
        const {showSelectLayersPanel, showSavedLayerConfigPanel, showHelpPanel, showFeedbackPanel, showSidebarButtons} = this.props.sidebarOptions;
        const { selectedMapTab } = this.props;

        return (
            <div className="header-tab-buttons">
                <div className="left-tab-buttons tab-buttons">
                    <ul className='tabGroup'>
                        {/*<li><a className="tabLeft" id="mapGoogle" href="javascript:void(0)" onClick={() => this.updateMapTab("Google")}><span className="tabIcon"><GoggleTabIcon/></span>Google</a></li>*/}
                        <li><a className="tabLeft" id="mapCarto" href="javascript:void(0)" onClick={() => this.updateMapTab("Carto")}>Map</a></li>
                        {appConfig.printShowTab ?
                            <li><a className="tabLeft" id="mapPrint" href="javascript:void(0)"
                                   onClick={() => this.updateMapTab("Print")}><span className="tabIcon"><PrintTabIcon/></span>Print</a>
                            </li>
                            :
                            null
                        }
                    </ul>
                </div>
                <div className='right-tab-buttons tab-buttons'>
                    <ul className='tabGroup'>
                        <li className={showSelectLayersPanel ? 'active tabRight tabRightOpen' : showSidebarButtons ? 'tabRight tabRightClose' : 'tabRightHidden'}
                            onClick={() => this.updateVisibilityForSelectLayerPanel()}>
                            <a className={showSelectLayersPanel ? 'tabRightBtnOpen' : 'tabRightBtnClose'} role="button" href="javascript:void(0)">
                                {showSelectLayersPanel ? <ArrowIcon/> : <ClosedArrowIcon/>}Select Layers</a>
                        </li>
                        {appConfig.printShowTab ?
                            <li className={showSavedLayerConfigPanel ? 'active tabRight tabRightOpen' : showSidebarButtons ? 'tabRight tabRightClose' : 'tabRightHidden'}
                                onClick={() => this.updateVisibilityForSavedLayerConfigPanel()}>
                                <a className={showSavedLayerConfigPanel ? 'tabRightBtnOpen' : 'tabRightBtnClose'} role="button" href="javascript:void(0)">
                                    <HistoryIcon/><span>{showSavedLayerConfigPanel ? 'Hide History' : 'Show History'}</span>
                                </a>
                            </li>
                        : null}
                        {appConfig.printShowTab ?
                            <li className={showHelpPanel ? 'active tabRight tabRightOpen' : showSidebarButtons ? 'tabRight tabRightClose' : 'tabRightHidden'}
                                onClick={() => this.updateVisibilityForHelpPanel()}>
                                <a className={showHelpPanel ? 'tabRightBtnOpen' : 'tabRightBtnClose'} role="button" href="javascript:void(0)"><HelpIcon/><span>Help</span></a>
                            </li>
                        : null}
                        <li className={showFeedbackPanel ? 'active tabRight tabRightOpen' : showSidebarButtons ? 'tabRight tabRightClose' : 'tabRightHidden'}
                            onClick={() => this.updateVisibilityForFeedbackPanel()}>
                            <a className={showFeedbackPanel ? 'tabRightBtnOpen' : 'tabRightBtnClose'} role="button" href="javascript:void(0)">
                                {showFeedbackPanel ? <ArrowIcon/> : <ClosedArrowIcon/>}Feedback</a>
                        </li>
                        <li onClick={() => this.updateVisibilityForSidebarButtons()}>
                            <a id="toggle-controls-btn"  className={showSidebarButtons ? "" : "double-arrow-flip-icon"} role="button" href="javascript:void(0)"
                               title="Toggle Control Buttons" aria-label="Toggle Control Buttons"><DoubleArrowIcon/></a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default HeaderButtonsComponent;