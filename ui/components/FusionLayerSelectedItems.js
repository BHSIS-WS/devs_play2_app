import React from "react";
import {removeAllPrintLayerInfoBox} from "../actions/PrintInfoBoxAction";


class FusionLayerSelectedItems extends React.Component {
	getSelectedCriteriaClassName (layerId){
		if (this.props.expandSelectedLayerId === layerId){
			return 'dropDownArrow open';
		} else {
			return 'dropDownArrow'
		}
  	}
    handleclearAllSelectedServiceCode() {
        this.props.dispatch({type:"CLEAR_LAYER_SELECT"});
    }
	closeLayer(evt, layerId){
        this.props.dispatch(removeAllPrintLayerInfoBox());
	    this.props.dispatch({type:"TOGGLE_LAYER_SELECT", layerId: layerId});
	    this.props.dispatch({type: "HIDE_LAYER", layerKey: "normal_layer_"+layerId});
	}

	closeGradLayer(e, gradLayerId) {
        this.props.dispatch(removeAllPrintLayerInfoBox());
	    this.props.dispatch({type: "TOGGLE_GRADIENT_LAYER", gradLayerId: gradLayerId});
	    this.props.dispatch({type: "HIDE_LAYER", layerKey: "grad_layer_"+gradLayerId});
	}

	toggleCriteriaSelection(evt, layerId) {
		layerId = evt.currentTarget.classList.contains('open') ? null : layerId;
		this.props.dispatch({type:'TOGGLE_SELECTED_LAYER', layerId: layerId});
	}

	getSelectedCriteriaSpan(layer) {
		if(layer.hasSelectedCriteria) {
			return <span className={this.getSelectedCriteriaClassName(layer.id)} onClick={evt => this.toggleCriteriaSelection(evt, layer.id)} />
		}
	}

	render() {
	    const { selectedMapTab,layerArray,selectedLayerIdSet,gradientLayerArray,selectedGradLayerIdSet,
	        selectedCriteriaIdMap } = this.props;
		
		if (!selectedLayerIdSet.size > 0 || selectedMapTab !== "Carto") return null;
		
		this.selectedLayerArray = layerArray.filter(layer => {
			layer.hasSelectedCriteria = selectedCriteriaIdMap.get(layer.id) && selectedCriteriaIdMap.get(layer.id).size > 0;
			return selectedLayerIdSet.has(layer.id);
		});

	    this.selectedGradLayerArray = gradientLayerArray.filter(gradLayer => {
	        return selectedGradLayerIdSet.has(gradLayer.id);
	    });

	    return (
		    <div className="selectedItems">
		    	<span className="boldFont">Your Layer Selections:
		    	    <button className="link-button-Remove-All" title="Remove All Selected Services" onClick={() => this.handleclearAllSelectedServiceCode()}>Clear All</button>
		    	</span>
		    	{this.selectedLayerArray.map(layer => 
			    	<div key={layer.id} className="selected_layer">
				    	<span className="closeButton" onClick={evt=> this.closeLayer(evt,layer.id)} />
				    	<span>{layer.layer_name}</span>
				    	{this.getSelectedCriteriaSpan(layer)}
			    	</div>
		    	)}
		    	{selectedGradLayerIdSet.size > 0 ? <span className="boldFont">Your Gradient Layer Selections:</span> : ""}
		    	{selectedGradLayerIdSet.size > 0 ?
                    this.selectedGradLayerArray.map(gradLayer =>
                        <div key={gradLayer.id}>
                            <span className="closeButton" onClick={e=> this.closeGradLayer(e,gradLayer.id)} />
                            <span>{gradLayer.name}</span>
                        </div>
                    )
		    	:""}
		    </div>
	    );
	}

}

export default FusionLayerSelectedItems

