import React from "react";
import mapContentModel from '../others/MapContentModel';
import printContentModel from '../others/PrintContentModel';
import {
    addGradientFilterEntry, changeGradientFilterEntrySign, changeGradientFilterEntryValue, deleteGradientFilterEntry,
    toggleGradientFilter,
    toggleGradientLayer
} from "../actions/GradientLayersAction";
import {addOrRemoveLayerInRenderStatusLayerList} from "../actions/ToolbarOptionsAction";
import {fetchLayerDataList} from "../actions/LayerDatatListingAction";
import {removeAllPrintLayerInfoBox} from "../actions/PrintInfoBoxAction";

class GradientLayer extends React.Component {

  constructor() {
    super();
  }

  getGradientLayerById(gradientLayerArray, layerId){
    return gradientLayerArray.find(gradLayer=> gradLayer.id === layerId);
  }

  getFilterIconTitle(expanded) {
    let title = expanded? "Close": "Show";
    title += " Filter";
    return title;
  }

  getFilterTableDisplay(expanded) {
    return expanded?"block":"none";
  }

  toggleFilter(e, gradLayerId) {
    this.props.dispatch(toggleGradientFilter(gradLayerId));
  }

  isChecked(selectedGradLayerIdSet, gradLayerId) {
    if (!selectedGradLayerIdSet) {
        return false;
    }
    return selectedGradLayerIdSet.has(gradLayerId);
  }

  onChangeLayerSelect(e, gradLayerId) {
    this.props.dispatch(removeAllPrintLayerInfoBox());
    this.props.dispatch(toggleGradientLayer(gradLayerId));
    this.props.dispatch(addOrRemoveLayerInRenderStatusLayerList(gradLayerId, "gradient"));
    this.props.dispatch(fetchLayerDataList());
    mapContentModel.refreshGoogleMapLayers();
    printContentModel.refreshD3MapLayersFromState();
  }

  applyFilter(e, gradLayerId) {
    this.props.dispatch(removeAllPrintLayerInfoBox());
    mapContentModel.refreshGoogleGradientMapLayers();
    printContentModel.refreshD3MapLayersFromState();
  }

  addFilterEntry(e, gradLayerId, filterEntryArrayExists) {
    if (!filterEntryArrayExists) {
        let firstFilterSign = document.querySelectorAll('select.filter-dropdown')[0].value;
        let firstFilterValue = document.querySelectorAll('input.filter-textbox')[0].value;
        this.props.dispatch(addGradientFilterEntry(gradLayerId, firstFilterSign, firstFilterValue));
    } else {
        this.props.dispatch(addGradientFilterEntry(gradLayerId));
    }
//    let tbody = e.target.parentNode.parentNode.parentNode;
//    tbody.insertAdjacentHTML('afterend',
//        '<tr>'+
//            '<td>'+
//                '<select class="filter-dropdown">'+
//                    '<option value="<">&lt;</option>'+
//                    '<option value="=">=</option>'+
//                    '<option value="<">&gt;</option>'+
//                '</select>'+
//            '</td>'+
//            '<td><input class="filter-textbox" type="text" size="4"/></td>'+
//            '<td><a href="javascript:void(0)" class="icon-gradient-cancel" title="Delete Criteria" onclick="onclickDeleteCriteria()"></a></td>'+
//        '</tr>');
  }

  deleteFilterEntry(e, gradLayerId, filterEntryIndex) {
      this.props.dispatch(deleteGradientFilterEntry(gradLayerId, filterEntryIndex));
  }

  onChangeFilterSignChange(e, gradLayerId, filterEntryIndex) {
      this.props.dispatch(changeGradientFilterEntrySign(gradLayerId, filterEntryIndex, e.target.value));
  }

  onChangeFilterValueChange(e, gradLayerId, filterEntryIndex) {
      this.props.dispatch(changeGradientFilterEntryValue(gradLayerId, filterEntryIndex, e.target.value));
  }

  render() {
    const {gradientLayerArray, filterExpandedLayerIdSet, gradLayerId, selectedGradLayerIdSet, gradFilterMap} = this.props;
    let layer = this.getGradientLayerById(gradientLayerArray, gradLayerId);
    if (!layer) return null;
    let expanded = filterExpandedLayerIdSet.has(gradLayerId);
//    let gradFilterEntryArray = gradFilterMap.get(gradLayerId);
    let filterEntryArrayExists = gradFilterMap.has(gradLayerId);
//    this.newGradFilterEntryArray = new Array(gradFilterEntryArray);
//    let firstGradFilterEntry = {gradFilterEntryArray? gradientLayerArray[0] : false};
//    let firestGradFilterEntrySign = {firstGradFilterEntry};
    let firstGradFilterEntrySign = "<",
        firstGradFilterEntryValue = "",
        gradFilterEntryArray = null;
    if (filterEntryArrayExists) {
        firstGradFilterEntrySign = gradientLayerArray[0].filterSign;
        firstGradFilterEntryValue = gradientLayerArray[0].filterValue;
        gradFilterEntryArray = gradFilterMap.get(gradLayerId);
    }

    return(
      <li key={gradLayerId}>
        <input type="checkbox" name={"chkbox_grad_layer_"+layer.id}
            checked={this.isChecked(selectedGradLayerIdSet, gradLayerId)?"checked":""}
            onChange={(e)=>this.onChangeLayerSelect(e, gradLayerId)}/>
        <label htmlFor={"chkbox_grad_layer_"+layer.id} title={layer.data_source}>{layer.name}</label>
        <a href="javascript:void(0)" className="icon-gradient-filter" title={this.getFilterIconTitle(expanded)}
            onClick={e => this.toggleFilter(e,gradLayerId)} />
        <table style={{display: this.getFilterTableDisplay(expanded)}}>
          <tbody>
            <tr>
              <td>
                <select className="filter-dropdown" defaultValue = {firstGradFilterEntrySign}
                    onChange={(e) => this.onChangeFilterSignChange(e, gradLayerId, 0)}>
                    <option value="<">&lt;</option>
                    <option value="=">=</option>
                    <option value=">">&gt;</option>
                </select>
              </td>
              <td><input className="filter-textbox" type="text" size="4" defaultValue = {firstGradFilterEntryValue}
                    onChange={(e) => this.onChangeFilterValueChange(e, gradLayerId, 0)}/></td>
              <td><a href="javascript:void(0)" className="icon-gradient-check" title="Filter"
                    onClick={(e) => this.applyFilter(e, gradLayerId)} /></td>
              <td><a href="javascript:void(0)" className="icon-gradient-add" title="Add Criteria"
                    onClick={(e) => this.addFilterEntry(e, gradLayerId)} /></td>
            </tr>
            {filterEntryArrayExists? gradFilterEntryArray.map((filterEntry, index) => (
                index > 0 ?
                    <tr key={gradLayerId+"."+index}>
                        <td>
                            <select className="filter-dropdown" value={filterEntry.filterSign}
                                onChange={(e) => this.onChangeFilterSignChange(e, gradLayerId, index)}>
                                <option value="<">&lt;</option>
                                <option value="=">=</option>
                                <option value=">">&gt;</option>
                            </select>
                        </td>
                        <td>
                            <input className="filter-textbox" type="text" size="4" value={filterEntry.filterValue}
                                onChange={(e) => this.onChangeFilterValueChange(e, gradLayerId, index)}/>
                        </td>
                        <td><a href="javascript:void(0)" className="icon-gradient-cancel" title="Delete Criteria"
                            onClick={(e) => this.deleteFilterEntry(e, gradLayerId, index)} /></td>
                    </tr>
                : null
            )): null}
          </tbody>
        </table>
      </li>
    );
  }
}

export default GradientLayer;