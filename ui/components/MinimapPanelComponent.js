import React from "react";
import * as d3 from "d3";
import * as d3_composite from "d3-composite-projections";
import * as d3_geo from "d3-geo";

class MinimapPanelComponent extends React.Component {

    constructor(props) {
        super(props);
        this.calculateMinimapDimensions = this.calculateMinimapDimensions.bind(this);
        this.resetView = this.resetView.bind(this);
        this.state = {
            geoJson: null,
            projection: null
        };
    }

    componentWillMount() {

    }

    componentDidMount() {
        d3.request("/devs2/geoJson/" + 5)
            .header("Content-Type", "application/json")
            .post(
                JSON.stringify({
                    parameterJson: {
                        layerCriteria: {}
                    }}),
                xhr => {
                    this.setState({
                        geoJson: JSON.parse(xhr.response),
                        projection: this.getProjection(this.props.projection)
                    }, () => {
                        this.drawSvg();
                    });
                }
            );
    }

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        let { showMinimapPanel, projection, width, height, zoom, translateX, translateY } = this.props;
        if (nextProps.projection !== projection) {
            this.setState({
                projection: this.getProjection(nextProps.projection)
            });
        }
        else if ((nextProps.showMinimapPanel && nextProps.showMinimapPanel !== showMinimapPanel) ||
            nextProps.width !== width || nextProps.height !== height || nextProps.zoom !== zoom ||
            ((nextProps.translateX !== translateX || nextProps.translateY !== translateY) && !nextProps.fromMinimap)) {
            this.setState({
            }, () => {
                if(nextProps.showMinimapPanel) {
                    this.redrawSvg();
                }
            });
        }
    }

    calculateMinimapDimensions () {
        let { translateX, translateY, zoom } = this.props;
        let { projection } = this.state;
        let mapCanvas = document.getElementById("mapCanvas");
        let svg = d3.select("#mapCanvasSvg");
        let svgg = d3.select("#mapCanvasSvgG");
        let self = this;

        if (mapCanvas && svg && svgg && svg.attr("viewBox")) {
            let viewBox = svg.attr("viewBox").split(" ");

            // get the actual visible number of pixels based on the view box
            let offsetScale = Math.max((viewBox[2] / mapCanvas.offsetWidth), (viewBox[3] / mapCanvas.offsetHeight));

            let bbox = svgg.node().getBBox();

            // get the total width of the map with our predetermined boundaries
            let totalWidth = (mapCanvas.offsetWidth + 2 * bbox.width) * offsetScale;
            let totalHeight = (mapCanvas.offsetHeight + 2 * bbox.height) * offsetScale;

            // set the viewbox to that width and height
            d3.select("#minimap-svg").attr("viewBox", "0 0 " + totalWidth + " " + totalHeight);


            // get the left and top coordinate for the US map
            let leftCoord = (totalWidth  - zoom * bbox.width) / 2;
            let topCoord = (totalHeight  - zoom * bbox.height) / 2;

            d3.select("#states").attr("transform",
                "translate(" + leftCoord +
                "," + topCoord +
                ")scale(" + zoom + "," + zoom + ")");


            let rectTranslateX = leftCoord - translateX;
            let rectTranslateY = topCoord - translateY;

            // set the minimap rectangle group element position
            let minimapRectG = d3.select("#minimap-svg")
                .append("g")
                .attr("id", "minimap-rect-g")
                .attr("transform", "translate(" + rectTranslateX + "," + rectTranslateY + ")scale(1)");

            // get the visible width and height of the canvas
            let visibleWidth = mapCanvas.offsetWidth * offsetScale;
            let visibleHeight = mapCanvas.offsetHeight * offsetScale;

            // set the minimap rectangle width/height
            let minimapRect = minimapRectG.append("rect")
                .attr("width", visibleWidth)
                .attr("height", visibleHeight)
                .attr("fill", "black")
                .attr("fill-opacity", "0.1")
                .attr("stroke-width", "4px")
                .attr("stroke", "black");

            // reset scale when the minimap is zoomed
            let zoomed = function() {
                let newX = d3.event.transform.x;
                let newY = d3.event.transform.y;
                minimapRect.attr("transform", "translate(" + newX + "," + newY + ")scale(1)");
            };

            // once zooming finishes, update the main map with new position
            let zoomEnd = function() {
                let newX = d3.event.transform.x;
                let newY = d3.event.transform.y;
                self.props.dispatch({
                    type: "TRANSLATE_D3_CHANGE",
                    translateX: translateX - newX,
                    translateY: translateY - newY,
                    fromMinimap: true
                });
            };

            // console.log("TX: ", translateX, "TY: ", translateY, totalWidth - (translateX + bbox.width), totalHeight - (translateY + bbox.height));
            let zoomD3 = d3.zoom()
                // .translateExtent([
                //     [-translateX, -translateY],
                //     [totalWidth - (translateX + bbox.width), totalHeight - (translateY + bbox.height)]])
                .on("zoom", zoomed)
                .on("end", zoomEnd);

            minimapRectG.call(zoomD3).on("dblclick.zoom", null);
        }
    }

    drawSvg() {
        let { showMinimapPanel } = this.props;
        let { geoJson, projection } = this.state;
        if (geoJson && geoJson.features) {
            let g = d3.select("#minimap-svg").append("g").attr("id", "states");
            let layerElements = g.selectAll("path")
                .data(geoJson.features).enter();
            let pathElements = layerElements.append("path").attr("d", projection);
            pathElements.attr("vector-effect","non-scaling-stroke")
                .attr("stroke", "black")
                .attr("fill", "none");
            if(showMinimapPanel) {
                this.calculateMinimapDimensions();
            }
        }
    }

    redrawSvg() {
        document.getElementById("minimap-svg").innerHTML = "";
        this.drawSvg();
    }

    getProjection(projection) {
        console.log(projection);
        switch (projection){
            case 3:
                return d3.geoPath(d3.geoMercator());
            case 2:
                return d3_geo.geoPath().projection(d3_composite.geoAlbersUsaTerritories());
            case 1:
            default:
                return d3_geo.geoPath().projection(d3_composite.geoAlbersUsa());
        }
    }

    getTransformation(transform) {
        let g = document.createElementNS("http://www.w3.org/2000/svg", "g");
        g.setAttributeNS(null, "transform", transform);
        let matrix = g.transform.baseVal.consolidate().matrix;
        let {a, b, c, d, e, f} = matrix;
        let scaleX, scaleY, skewX;
        if (scaleX = Math.sqrt(a * a + b * b)) { a /= scaleX; b /= scaleX; }
        if (skewX = a * c + b * d) { c -= a * skewX; d -= b * skewX; }
        if (scaleY = Math.sqrt(c * c + d * d)) { c /= scaleY; d /= scaleY; }
        if (a * d < b * c) scaleX = -scaleX;

        return { translateX: e, translateY: f, scaleX: scaleX, scaleY: scaleY };
    }

    resetView() {
        let { zoom } = this.props;
        let mapCanvas = document.getElementById("mapCanvas");
        let svg = d3.select("#mapCanvasSvg");
        let svgg = d3.select("#mapCanvasSvgG");
        let bbox = svgg.node().getBBox();
        let viewBox = svg.attr("viewBox").split(" ");

        let offsetScale = Math.max((viewBox[2] / mapCanvas.offsetWidth), (viewBox[3] / mapCanvas.offsetHeight));
        let totalWidth = (mapCanvas.offsetWidth + 2 * bbox.width) * offsetScale;
        let totalHeight = (mapCanvas.offsetHeight + 2 * bbox.height) * offsetScale;
        let leftCoord = (totalWidth  - zoom * bbox.width) / 2;
        let topCoord = (totalHeight  - zoom * bbox.height) / 2;
        this.props.dispatch({
            type: "TRANSLATE_D3_CHANGE",
            translateX: -80,
            translateY: 20,
            fromMinimap: true
        });
        d3.select("#minimap-rect-g").attr("transform", "translate(" + leftCoord + "," + topCoord + ")scale(1)");
    }

    render() {
        let { geoJson } = this.state;
        let { showMinimapPanel } = this.props;
        if (geoJson) {
            return (
                <div id="minimap-panel" className='toolbar-popup-div' hidden={!showMinimapPanel}>
                    <h4>Minimap:</h4>
                    <div className='toolbar-popup-content-div'>
                        <svg id="minimap-svg" viewBox="0 0 1000 1000" preserveAspectRatio="xMinYMax meet">
                        </svg>
                        <div className='toolbar-popup-button-container'>
                            <button className='button' onClick={this.resetView}>Reset View</button>
                        </div>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }

}

export default MinimapPanelComponent;

