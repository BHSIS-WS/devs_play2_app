import React from "react";
import D3MapTextToolOptionDialog1 from "../../containers/mapd3tools/D3MapTextToolOptionDialog1";
import D3MapRectToolOptionDialogController from "../../containers/mapd3tools/D3MapRectToolOptionDialogController";

class D3MapToolOptionPanel extends React.Component {

    render() {
        return (
            <div id="d3MapToolOptionPanelContent">
                <D3MapTextToolOptionDialog1 />
                <D3MapRectToolOptionDialogController />
            </div>
        );
    }
}

export default D3MapToolOptionPanel;