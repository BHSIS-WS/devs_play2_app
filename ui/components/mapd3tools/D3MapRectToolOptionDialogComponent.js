import React from "react";
import { SketchPicker } from 'react-color';
import {
    closeD3MapRectToolOptionDialog,
    applyD3MapRectToolOptions,
    changeD3MapRectToolWidth,
    changeD3MapRectToolHeight,
    changeD3MapRectToolStroke,
    changeD3MapRectToolStrokeWidth
} from "../../actions/D3MapToolOptionsAction";

class D3MapRectToolOptionDialogComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            displayColorPicker: false
        };
    }

    widthKeyUp(event) {
        if (event.key === "Enter") {
            this.props.dispatch(changeD3MapRectToolWidth(event.target.value));
        }
    }

    heightKeyUp(event) {
        if (event.key === "Enter") {
            this.props.dispatch(changeD3MapRectToolHeight(event.target.value));
        }
    }

    strokeColorPickerClicked() {
        this.setState({ displayColorPicker: !this.state.displayColorPicker });
    }

    handleColorPickerClose() {
        this.setState({ displayColorPicker: false });
    }

    strokeColorChanged(color) {
        document.getElementById("rect-tool-stroke-input").value = color.hex;
        document.getElementById("rect-tool-selected-stroke-color").style.backgroundColor = color.hex;
    }

    strokeKeyUp(event) {
        if (event.key === "Enter") {
            this.props.dispatch(changeD3MapRectToolStroke(event.target.value));
        }
    }

    strokeWidthKeyUp(event) {
        if (event.key === "Enter") {
            this.props.dispatch(changeD3MapRectToolStrokeWidth(event.target.value));
        }
    }

    okClicked() {
        let width = document.getElementById("rect-tool-width-input").value,
            height = document.getElementById("rect-tool-height-input").value,
            stroke = document.getElementById("rect-tool-stroke-input").value,
            strokeWidth = document.getElementById("rect-tool-stroke-width-input").value;
        this.props.dispatch(applyD3MapRectToolOptions(width, height, stroke, strokeWidth));
        this.props.dispatch(closeD3MapRectToolOptionDialog());
    }

    applyClicked() {
        let width = document.getElementById("rect-tool-width-input").value,
            height = document.getElementById("rect-tool-height-input").value,
            stroke = document.getElementById("rect-tool-stroke-input").value,
            strokeWidth = document.getElementById("rect-tool-stroke-width-input").value;
        this.props.dispatch(applyD3MapRectToolOptions(width, height, stroke, strokeWidth));
    }

    cancelClicked() {
        this.props.dispatch(closeD3MapRectToolOptionDialog());
    }

    render() {
        const {rectOptionDialogOpen, activeRectId, rectToolConfigMap} = this.props;
        let rectToolConfig = rectToolConfigMap.get(activeRectId);

        return rectOptionDialogOpen? (
            <div id="d3-rect-property-panel-div">
                <h4>Rectangle Properties</h4>
                <div className="propertySection firstPropertySection">
                    <span id="rect-tool-width" className="" title="Width"/>
                    <input id="rect-tool-width-input" type="number" className="propertyInput"
                           defaultValue={rectToolConfig.width} onKeyUp={(e) => this.widthKeyUp(e)}/>
                    <label htmlFor="rect-tool-width-input"> px</label>
                    <span id="rect-tool-height" className="" title="Height"/>
                    <input id="rect-tool-height-input" type="number" className="propertyInput"
                           defaultValue={rectToolConfig.height} onKeyUp={(e) => this.heightKeyUp(e)} />
                    <label htmlFor="rect-tool-height-input"> px</label>
                </div>
                <div className="propertySection">
                    <span id="rect-tool-stroke-color-picker" title="Color picker"
                          onClick={(e) => this.strokeColorPickerClicked(e)}/>
                    <div id="rect-tool-selected-stroke-color" style={{backgroundColor: rectToolConfig.stroke}}
                         onClick={(e) => this.strokeColorPickerClicked(e)}/>
                    <input id="rect-tool-stroke-input" type="text" defaultValue={rectToolConfig.stroke}
                           size="7" onKeyUp={(e) => this.strokeKeyUp(e)} />
                    <span id="rect-tool-stroke-width" className="" title="Stroke width"/>
                    <input id="rect-tool-stroke-width-input" type="number" className="propertyInput"
                           defaultValue={rectToolConfig.strokeWidth} onKeyUp={(e) => this.strokeWidthKeyUp(e)} />
                    <label htmlFor="rect-tool-stroke-width-input"> px</label>
                </div>
                <div id="rect-tool-dialog-footer-div">
                    <button id="rect-tool-property-ok-btn" className="tool-prop-btn"
                            onClick={(e) => this.okClicked(e)}>OK</button>
                    <button id="rect-tool-property-apply-btn" className="tool-prop-btn"
                            onClick={(e) => this.applyClicked(e)}>Apply</button>
                    <button id="rect-tool-property-apply-btn" className="tool-prop-btn"
                            onClick={(e) => this.cancelClicked(e)}>Cancel</button>
                </div>
                { this.state.displayColorPicker ?
                    <div className="colorPickerTool">
                        <div className="closeCover" onClick={() => this.handleColorPickerClose()}/>
                        <SketchPicker color={rectToolConfig.stroke} onChange={(color) => this.strokeColorChanged(color)} />
                    </div>
                    : null }
            </div>
        ) : null;
    }
}

export default D3MapRectToolOptionDialogComponent;