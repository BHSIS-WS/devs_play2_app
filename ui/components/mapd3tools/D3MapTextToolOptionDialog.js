import React from "react";
import { SketchPicker } from 'react-color';
import {TextPropertyIcon, BoldIcon, ItalicIcon, UnderlineIcon, ColorPickerIcon, RotateIcon} from "../../constant/SvgIcons";
import {
    applyD3MapTextToolOptions,
    changeD3MapTextToolFillColor,
    changeD3MapTextToolFontSize,
    changeD3MapTextToolRotate,
    closeD3MapTextToolOptionDialog
} from "../../actions/D3MapToolOptionsAction";
import printContentModel from '../../others/PrintContentModel';

class D3MapTextToolOptionDialog extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            displayColorPicker: false
        };
    }

    onFontSizeChange(event) {
        this.props.dispatch(changeD3MapTextToolFontSize(event.target.value));
    }

    rotateKeyUp(event) {
        if (event.key === "Enter") {
            this.props.dispatch(changeD3MapTextToolRotate(event.target.value));
        }
    }

    fillColorPickerClicked() {
        this.setState({ displayColorPicker: !this.state.displayColorPicker });
    }

    fillColorChanged(color) {
        document.getElementById("text-tool-fill-color-input").value = color.hex;
        document.getElementById("text-tool-selected-fill-color").style.backgroundColor = color.hex;
    }

    handleColorPickerClose() {
        this.setState({ displayColorPicker: false });
    }

    fillColorKeyUp(event) {
        if (event.key === "Enter") {
            this.props.dispatch(changeD3MapTextToolFillColor(event.target.value));
        }
    }

    boldClicked() {
        let self = this,
            classList = document.getElementById("text-tool-bold").classList;
        if (!classList.contains("highlighted")) {
            classList.add("highlighted");
            self.isBold = true;
        } else {
            classList.remove("highlighted");
            self.isBold = false;
        }
    }

    italicClicked() {
        let self = this,
            classList = document.getElementById("text-tool-italic").classList;
        if (!classList.contains("highlighted")) {
            classList.add("highlighted");
            self.isItalic = true;
        } else {
            classList.remove("highlighted");
            self.isItalic = false;
        }
    }

    underlineClicked() {
        let self = this,
            classList = document.getElementById("text-tool-underline").classList;
        if (!classList.contains("highlighted")) {
            classList.add("highlighted");
            self.isUnderlined = true;
        } else {
            classList.remove("highlighted");
            self.isUnderlined = false;
        }
    }

    cancelClicked() {
        this.props.dispatch(closeD3MapTextToolOptionDialog());
        printContentModel.hideTextTypingTextArea();
    }

    okClicked() {
        let self = this;
        let textAreaFields = printContentModel.getTextToolTextFields();
        let textId = null;
        if (this.props.activeTextId === 0) {
            textId = printContentModel.getNewSvgTextId();
        }
        let fontSize = document.getElementById("text-tool-font-size-input").value;
        let rotate = document.getElementById("text-tool-rotate-input").value;
        let fillColor = document.getElementById("text-tool-fill-color-input").value;
        let fontFamily = document.getElementById("text-tool-font-select").value;
        if (this.props.activeTextId === 0) {
            printContentModel.createSvgText(textId, fontSize, rotate, fillColor, textAreaFields.textContent, fontFamily,
                self.isBold, self.isItalic, self.isUnderlined);
        }
        this.props.dispatch(applyD3MapTextToolOptions(textId, fontSize, rotate, fillColor, textAreaFields,
            fontFamily, self.isBold, self.isItalic, self.isUnderlined));
        this.props.dispatch(closeD3MapTextToolOptionDialog());
    }

    applyClicked() {
        let self = this;
        let textAreaFields = printContentModel.getTextToolTextFields();
        let textId = null;
        if (this.props.activeTextId === 0) {
            textId = printContentModel.getNewSvgTextId();
        }
        let fontSize = document.getElementById("text-tool-font-size-input").value;
        let rotate = document.getElementById("text-tool-rotate-input").value;
        let fillColor = document.getElementById("text-tool-fill-color-input").value;
        let fontFamily = document.getElementById("text-tool-font-select").value;
        if (this.props.activeTextId === 0) {
            printContentModel.createSvgText(textId, fontSize, rotate, fillColor, textAreaFields.textContent, fontFamily,
                self.isBold, self.isItalic, self.isUnderlined);
        }
        this.props.dispatch(applyD3MapTextToolOptions(textId, fontSize, rotate, fillColor, textAreaFields,
            fontFamily, self.isBold, self.isItalic, self.isUnderlined));
    }

    render() {
        const {optionDialogOpen, defaultTextToolConfig, activeTextId, textToolConfigMap} = this.props;

        let self = this,
            textToolConfig;
        if (activeTextId === 0) {
            textToolConfig = defaultTextToolConfig;
        } else {
            textToolConfig = textToolConfigMap.get(activeTextId);
        }
        self.isBold = textToolConfig.bold;
        self.isItalic = textToolConfig.italic;
        self.isUnderlined = textToolConfig.underline;

        return optionDialogOpen? (
            <div id="propertyBox">
                <h4>Text Properties</h4>
                <div className="propertySection firstPropertySection">
                    <span id="text-tool-text" className="propertyImage" title="Text font"><TextPropertyIcon/></span>
                    <select id="text-tool-font-select" title="Font" defaultValue={textToolConfig.fontFamily}>
                        <option value="Arial">Arial</option>
                        <option value="Arial Black">Arial Black</option>
                        <option value="Bookman">Bookman</option>
                        <option value="Comic Sans MS">Comic Sans MS</option>
                        <option value="Courier">Courier</option>
                        <option value="Courier New">Courier New</option>
                        <option value="Garamond">Garamond</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Helvetica">Helvetica</option>
                        <option value="Impact">Impact</option>
                        <option value="Palatino">Palatino</option>
                        <option value="Times">Times</option>
                        <option value="Times New Roman">Times New Roman</option>
                        <option value="Trebuchet MS">Trebuchet MS</option>
                        <option value="Verdana">Verdana</option>
                    </select>
                    <input id="text-tool-font-size-input" type="number" defaultValue={textToolConfig.fontSize}
                           min="1" onChange={(e) => this.onFontSizeChange(e)}/>
                    <label id="text-tool-font-size" htmlFor="text-tool-font-size"> px</label>
                </div>
                <div className="propertySection secondPrepertySection">
                    <span id="text-tool-bold" className={textToolConfig.bold ? "highlighted" : ""}
                          onClick={(e) => this.boldClicked(e)} title="Bold"><BoldIcon/></span>
                    <span id="text-tool-italic" className={textToolConfig.italic ? "highlighted" : ""}
                          onClick={(e) => this.italicClicked(e)} title="Italic"><ItalicIcon/></span>
                    <span id="text-tool-underline" className={textToolConfig.underline ? "highlighted" : ""}
                          onClick={(e) => this.underlineClicked(e)}
                          title="Underline"><UnderlineIcon/></span>
                    <span id="text-tool-fill-color-picker" title="Color picker"
                          onClick={(e) => this.fillColorPickerClicked(e)}><ColorPickerIcon/></span>
                    <div id="text-tool-selected-fill-color" style={{backgroundColor: textToolConfig.fillColor}}
                          onClick={(e) => this.fillColorPickerClicked(e)}/>
                    <input type="text" id="text-tool-fill-color-input" size="9" defaultValue={textToolConfig.fillColor}
                           title="Fill color" onKeyUp={(e) => this.fillColorKeyUp(e)}/>
                    <span id="text-tool-rotate" className="propertyImage" title="Rotate"><RotateIcon/></span>
                    <input type="number" id="text-tool-rotate-input" className="" defaultValue={textToolConfig.rotate}
                           onKeyUp={(e) => this.rotateKeyUp(e)} title="Rotate" min="0" max="360"/>
                    <label id="text-tool-rotate-input-symbol" htmlFor="text-tool-rotate-input"> °</label>
                </div>
                <div id="text-tool-dialog-footer-div">
                    <button id="text-tool-property-ok-btn" className="tool-prop-btn"
                            onClick={(e) => this.okClicked(e)}>OK</button>
                    <button id="text-tool-property-apply-btn" className="tool-prop-btn"
                            onClick={(e) => this.applyClicked(e)}>Apply</button>
                    <button id="text-tool-property-cancel-btn" className="tool-prop-btn"
                            onClick={(e) => this.cancelClicked(e)}>Cancel</button>
                </div>
                { this.state.displayColorPicker ?
                    <div className="colorPickerTool">
                        <div className="closeCover" onClick={() => this.handleColorPickerClose()}/>
                        <SketchPicker color={textToolConfig.fillColor} onChange={(color) => this.fillColorChanged(color)} />
                    </div>
                : null }
            </div>
        ) : null;
    }
}

export default D3MapTextToolOptionDialog;
