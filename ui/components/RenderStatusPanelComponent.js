import React from 'react';
import RenderStatusLayerController from '../containers/RenderStatusLayerController';

class RenderStatusPanelComponent extends React.Component {

    render() {
        let {showRenderStatusPanel, selectedLayers} = this.props;
        return (
            <div id="render-status-panel" hidden={!showRenderStatusPanel}>
                {selectedLayers.map(layerObject =>
                    <RenderStatusLayerController key={layerObject.layerType + layerObject.layer.id} layerObject={layerObject} />
                )}
            </div>
        );
    }
}

export default RenderStatusPanelComponent;