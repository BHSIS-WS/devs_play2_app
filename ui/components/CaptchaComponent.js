import React from "react";
import ReCAPTCHA from 'react-google-recaptcha';

class CaptchaComponent extends React.Component {
    constructor(props) {
        super(props);
        this.captchaChange = this.captchaChange.bind(this);
    }

    captchaChange(gRecaptchaResponse) {
        this.props.captchaChange(gRecaptchaResponse);
    }

    render() {
        const reCaptchaSiteKey = appConfig.reCaptcha;
        if (this.props.gRecaptchaResponse === 'reset') {
            window.grecaptcha.reset();
            this.captchaChange(null);
        }
        return (
            <ReCAPTCHA
                ref="recaptcha"
                sitekey={reCaptchaSiteKey}
                onChange={this.captchaChange}
            />
        );
    }
}

export default CaptchaComponent;