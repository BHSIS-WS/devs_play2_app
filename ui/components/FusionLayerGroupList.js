import React from "react";
import FusionLayerGroup1 from '../containers/FusionLayerGroup1';
import {toggleGroups} from '../actions/GroupAction';


class FusionLayerGroupList extends React.Component {

    getExpandCollapseClassName(startingName, expand){
       return expand ? startingName+" open" : startingName+" close";
    }

    filterGroups(layerGroupArray, layerType){
        if (layerGroupArray){
        return layerGroupArray.filter ( group => group.layer_type==layerType );
        }
        return layerGroupArray;
    }

    toggleExpand(e, layerType){
        this.props.dispatch(toggleGroups(layerType));
    }

render() {
    const { expandMap,layerType,groupArray,name } = this.props;
    let expand = expandMap.get(layerType);
    let filteredGroupArray =  this.filterGroups(groupArray,layerType);
    return (
    <div className="RtNav1">
        <div className="RtNavTitle">
          <a href="#" onClick={e=>this.toggleExpand(e,layerType)}
             className={this.getExpandCollapseClassName('',expand)}>{name}
          </a>
        </div>
        <ul>
          { expand
              ? filteredGroupArray.map( group => (<FusionLayerGroup1 key={group.id} group={group}/>))
              : ''
          }
        </ul>
    </div>
    );
  }

}


export default FusionLayerGroupList;

