import React from "react";
import {DownloadIcon, RefreshBottomIcon, ArrowUpDown, ArrowUp, ArrowDown} from "../constant/SvgIcons";
import {setupInfoWindow} from "../actions/MapVariablesAction";
import mapContentModel from '../others/MapContentModel';
import {dataTableRowClicked} from "../actions/LayerDatatListingAction";
import scrollIntoView from "scroll-into-view";

class ResultDataTable extends React.Component {

    constructor(props) {
        super(props);
        this.handleRefresh = this.handleRefresh.bind(this);
    }

    findItemInTable() {
        let searchVar = document.getElementById('txtSearch').value.trim();
        if (searchVar != null && searchVar != "") {
            this.props.layerDataListing.search = searchVar;
            this.props.changeDataTablePageNumber(1, true);
        }
    }
    handleRefresh(e) {
        e.preventDefault();
        this.props.fetchLayerDataList();
    }

    handleColumnIndex(e,name,dir) {
        this.props.layerDataListing.order.name=name;
        this.props.layerDataListing.order.dir=dir;
        let shouldUpdate = true;
        this.props.changeDataTablePageNumber(1, shouldUpdate);
    }

    pageSizeChanged(e) {
        this.props.changeDataTablePageSize(parseInt(e.target.value));
    }

    pageNumberChanged(e, pageNum) {
        let shouldUpdate = true;
        if (!pageNum) {
            pageNum = e.target.value;
            shouldUpdate = false;
        } 
        this.props.changeDataTablePageNumber(pageNum, shouldUpdate);
    }

    pageInputKeyUp(e) {
        if (e.key === "Enter") {
            this.props.changeDataTablePageNumber(parseInt(e.target.value), true);
        }
    }

    stateCountyBtnClicked() {
        this.props.toggleDtStateCountyDataPicker();
    }

    renderDataFields(layerColumns) {
        let columnPickerMenu = this.renderColumnPickerMenu(layerColumns);
        return (
            <div className="pullLeft">
                <strong>Choose Data Field:</strong>
                {columnPickerMenu}&nbsp;&nbsp;&nbsp;&nbsp;
                <strong>Search By Data Value:</strong>&nbsp;&nbsp;
                <input id="txtSearch" type="text" size="20" name="textfield-search" />&nbsp;&nbsp;
                <button id="btnSearch" title="Search By Data Value" onClick={(e) => this.findItemInTable()}>Search</button>&nbsp;&nbsp;
            </div>
        );
    }

    renderLayerFieldPicker() {
        let layerDataListing = this.props.layerDataListing;
        let stateCountyPickerMenu = null;
        if (layerDataListing.showStateCountyDataPicker) {
            stateCountyPickerMenu = this.renderStateCountyPickerMenu();
        }
        let layerFieldPicker = (
            <div id="main-layer-field-picker-div" className="pullRight">
                <span>
                    <button id="pgn_num_page1" className="pgn_num" disabled={(layerDataListing.page == 1 ) ? true : false} aria-label="Go to page 1" onClick={(e) => this.pageNumberChanged(e, 1)}>
                        <strong className={(layerDataListing.page == 1 ) ? "graycolor" : "whitecolor"}></strong>
                    </button>
                    <button id="pgn_num_prev" className="pgn_num" disabled={(layerDataListing.page == 1 ) ? true : false} aria-label="Go to previous page" onClick={(e) => this.pageNumberChanged(e, layerDataListing.page - 1)}>
                        <strong className={(layerDataListing.page == 1 ) ? "graycolor" : "whitecolor"} ></strong>
                    </button>
                    Page&nbsp;
                    <input id="textfield-page1" type="text" size="5" name="textfield-page"
                           value={layerDataListing.page} onChange={(e) => this.pageNumberChanged(e)} onKeyUp={(e) => this.pageInputKeyUp(e)}/>&nbsp;of&nbsp;
                    <label id="tatal-page-label">
                        {layerDataListing.layerDataQueryResults.totalPages}
                    </label>
                    <span className="fieldPickerPage">
                        <label htmlFor="select-page-size1">Show</label>
                        <select id="select-page-size1" name="select-page-size1" value={layerDataListing.pageSize}
                                onChange={(e) => this.pageSizeChanged(e)}>
                            <option id="pgn_size_10" value="10" >10</option>
                            <option id="pgn_size_20" value="20">20</option>
                            <option id="pgn_size_30" value="30">30</option>
                            <option id="pgn_size_50" value="50">50</option>
                            <option id="pgn_size_100" value="100">100</option>
                        </select>
                    </span>
                    <button id="pgn_num_next" className="pgn_num" disabled={(layerDataListing.page == layerDataListing.layerDataQueryResults.totalPages ) ? true : false} aria-label="Go to next page" onClick={(e) => this.pageNumberChanged(e, layerDataListing.page + 1)}>
                        <strong className={(layerDataListing.page == layerDataListing.layerDataQueryResults.totalPages ) ? "graycolor" : "whitecolor"} ></strong>
                    </button>
                    <button id="pgn_num_last" className="pgn_num" disabled={(layerDataListing.page == layerDataListing.layerDataQueryResults.totalPages ) ? true : false} aria-label="Go to last page" onClick={(e) => this.pageNumberChanged(e, layerDataListing.layerDataQueryResults.totalPages)}>
                        <strong className={(layerDataListing.page == layerDataListing.layerDataQueryResults.totalPages ) ? "graycolor" : "whitecolor"} ></strong>
                    </button>
                </span>
                {stateCountyPickerMenu}
            </div>
        );

        return layerFieldPicker;
    }

    downArrowClicked(event, layerId) {
        this.props.dispatch({type: "TOGGLE_FIELD_PICKER", layerId: layerId});
    }

    fpLayerClicked(event, layerId) {
        this.props.dispatch({type: "TOGGLE_FIELD_PICKER_LAYER_SELECT", layerId: layerId});
    }

    fpColumnClicked(event, layerId, columnId) {
        this.props.dispatch({type: "TOGGLE_FIELD_PICKER_COLUMN_SELECT", layerId: layerId, columnId: columnId});
    }

    fpSelectAllClicked(event, layerId) {
        this.props.dispatch({type: "FIELD_PICKER_SELECT_ALL_COLUMNS", layerId: layerId});
    }

    fpSelectNoneClicked(event, layerId) {
        this.props.dispatch({type: "FIELD_PICKER_SELECT_NONE_COLUMNS", layerId: layerId});
    }

    fpSelectReverseClicked(event, layerId) {
        this.props.dispatch({type: "FIELD_PICKER_SELECT_REVERSE_COLUMNS", layerId: layerId});
    }
    
    resultRefreshEvent(event, layerId, type) {
        this.props.dispatch({type: "FIELD_PICKER_SELECT_REVERSE_COLUMNS", layerId: layerId});
    }

    renderColumnPickerMenu(layerColumns) {
        if (layerColumns !== undefined && layerColumns !== null && layerColumns.length > 0) {
            let self = this,
                layerDataListing = this.props.layerDataListing,
                expandedFieldPickerLayerIdSet = layerDataListing.expandedFieldPickerLayerIdSet,
                selectedFPLayerIdSet = layerDataListing.selectedFPLayerIdSet,
                selectedFPColumnIdSet = layerDataListing.selectedFPColumnIdSet;
            let element = layerColumns.map(function(layer, index) {
                let pickerId  = "layer-field-picker" + layer.fusionMapLayerId;
                return (
                    <div key={pickerId} id={pickerId} className="inline-div">
                        <input className="lp-checkbox pointer" id={"lp-checkbox-"+layer.fusionMapLayerId} type="checkbox"
                               autoComplete="off" value={layer.fusionMapLayerId} name={layer.layerName}
                               onChange={(e) => self.fpLayerClicked(e, layer.fusionMapLayerId)}
                               checked={selectedFPLayerIdSet.has(layer.fusionMapLayerId)} />
                        <label className="lp-label pointer" title={layer.layerName} htmlFor={"img-"+layer.fusionMapLayerId}
                               onClick={(e) => self.downArrowClicked(e, layer.fusionMapLayerId)}>
                            {layer.layerName}
                        </label>
                        <img id={"img-"+layer.fusionMapLayerId} className="pointer"
                             title={"choose data fields for "+layer.layerName}
                             alt={"Icon to show datafield selection for "+layer.layerName}
                             src="/devs2/assets/images/downarrow_send.png"
                             onClick={(e) => self.downArrowClicked(e, layer.fusionMapLayerId)}/>
                        {expandedFieldPickerLayerIdSet.has(layer.fusionMapLayerId)?
                            <div className="data-picker-block-div">
                                <div className="all-none-reverse-block">
                                    <a className="select_all" href="javascript:void(0)"
                                       onClick={(e) => self.fpSelectAllClicked(e, layer.fusionMapLayerId)}>All</a>
                                    /
                                    <a className="select_none" href="javascript:void(0)"
                                       onClick={(e) => self.fpSelectNoneClicked(e, layer.fusionMapLayerId)}>None</a>
                                    /
                                    <a className="select_reverse" href="javascript:void(0)"
                                       onClick={(e) => self.fpSelectReverseClicked(e, layer.fusionMapLayerId)}>Reverse</a>
                                    <button id="closeButton" onClick={(e) => self.downArrowClicked(e, layer.fusionMapLayerId)}></button>
                                </div>
                                <ul className="data-picker-ul">
                                    {layer.columns.map((column, index) => {
                                        return (
                                            <li key={"lpf-checkbox-" + column.columnId}>
                                                <input className="lpf-checkbox pointer" id={"lpf-checkbox-" + column.columnId}
                                                       type="checkbox"
                                                       autoComplete="off" value={column.columnId}
                                                       name={layer.tableName + "." + column.columnName}
                                                       onChange={(e) => self.fpColumnClicked(e, layer.fusionMapLayerId,
                                                           column.columnId)}
                                                       checked={selectedFPColumnIdSet.has(column.columnId)}/>
                                                <label htmlFor={"lpf-checkbox-" + column.columnId} className="pointer">
                                                    {column.columnName}
                                                </label>
                                            </li>
                                        );
                                    })}
                                </ul>
                            </div>
                            :null}
                    </div>
                );
            });
            return element;
        } else {
            return null;
        }
    }

    toggleAdditionalDataLevel(e, dataLevel) {
        this.props.toggleDtAdditionalDataLevel(dataLevel);
    }

    toggleDtDataSet(e, dataSetId) {
        this.props.toggleDtDataSet(dataSetId);
    }

    toggleDtGradLayer(e, gradLayerId, dataLevel) {
        this.props.toggleDtGradLayer(gradLayerId, dataLevel);
    }

    isGradLayerSelected(gradLayerId, dataLevel) {
        let selectedAddtnlGradLayerIdSet;
        if (dataLevel === "state") {
            selectedAddtnlGradLayerIdSet = this.props.layerDataListing.selectedAddtnlStateGradLayerIdSet;
        } else if (dataLevel === "county") {
            selectedAddtnlGradLayerIdSet = this.props.layerDataListing.selectedAddtnlCountyGradLayerIdSet;
        }
        return selectedAddtnlGradLayerIdSet.has(gradLayerId);
    }

    getAdditionalDataRenderElement(dataLevel) {
        let self = this,
            gradientDataSetArray = this.props.gradientDataSetArray,
            gradientLayerArray = this.props.gradientLayerArray,
            expandedDataSetIdSet = this.props.layerDataListing.expandedDataSetIdSet;
        return gradientDataSetArray.map(dataSet => (
            dataSet.data_level == dataLevel?
                <li key={dataSet.id} data-id={dataSet.id}>
                    <span className="data-table-data-set-span" onClick={(e) => self.toggleDtDataSet(e, dataSet.id)}>
                        <strong>{dataSet.name}</strong>
                    </span>
                    {expandedDataSetIdSet.has(dataSet.id)?
                        <ul>
                            {gradientLayerArray.filter(layer => layer.data_set_id == dataSet.id).map(gradLayer => (
                                <li key={gradLayer.id}>
                                    <input type="checkbox" autoComplete="off" id={"chkbox_state_county_data_"+gradLayer.id}
                                           name={gradLayer.id+"."+gradLayer.field_column_name} value={gradLayer.id}
                                           onChange={(e) => this.toggleDtGradLayer(e, gradLayer.id, dataLevel)}
                                           checked={this.isGradLayerSelected(gradLayer.id, dataLevel)}/>
                                    <label className="data-table-grad-layer-label"
                                           onClick={(e) => {e.preventDefault();
                                                this.toggleDtGradLayer(e, gradLayer.id, dataLevel)}}
                                           htmlFor={"chkbox_state_county_data_"+gradLayer.id}>{gradLayer.name}</label>
                                </li>
                            ))}
                        </ul>
                        :null}
                </li>
                :null
        ));
    }

    handleDownload() {
        this.props.downloadDataTableExcel();
    }

    toggleDataTableAutoRefresh() {
        this.props.toggleDataTableAutoRefresh();
    }
    toggleDataTableInvertQuery() {
        this.props.toggleDataTableInvertQuery();
    }
    getExpandCollapseSrc(expand){
        return expand ? "/devs2/assets/images/Menu-expanded.png" : "/devs2/assets/images/Menu-normal.png";
    }
    
    rowClicked(index) {
        let {layerDataListing, layerArray} = this.props;
        let rows = layerDataListing.layerDataQueryResults.rows;
        let page = layerDataListing.page;
        let pageSize = layerDataListing.pageSize;
        let rowArrayIndex = (index - 1) - ((page -1 ) * pageSize);
        let item = rows[rowArrayIndex];
        item.layerId = layerDataListing.layerDataQueryResults.orderedLayerIds[0];

        let layer = layerArray.find(layer => layer.id == item.layerId);
        if (layer) {
            let columnsAndHtml = mapContentModel.getColumnsAndHtmlFromInfoWindowTemplate(layer.layer_info_window_template);

            let latLng = {};
            if (item._lng_ && item._lat_) {
                latLng = { lat: item._lat_, lng: item._lng_};
            } else if (item.lng && item.lat) {
                latLng = { lat: item.lat, lng: item.lng};
            }
            this.props.dispatch(dataTableRowClicked(index));
            this.props.dispatch(setupInfoWindow(layer.layer_name, columnsAndHtml.htmlList, columnsAndHtml.columnList, item, latLng, null));
        }
    }
    
    render() {
        let {layerDataListing} = this.props,
            hasDataColumns = false,
            columnHeaders = [],
            hasData = false,
            dataNotEmpty = false,
            layerColumns = null,
            columnSize=0;

        if (layerDataListing != null && layerDataListing.layerDataQueryResults != null) {
            if (layerDataListing.layerDataQueryResults.columns != null && layerDataListing.layerDataQueryResults.columns.length > 0) {
                hasDataColumns = true;
                columnHeaders = ['Index'].concat(layerDataListing.layerDataQueryResults.columns);
                columnSize = columnHeaders.length;
            }
            if (layerDataListing.layerDataQueryResults.layerColumns != null && layerDataListing.layerDataQueryResults.layerColumns.length > 0) {
                layerColumns = layerDataListing.layerDataQueryResults.layerColumns;
            }
        }
        if (layerDataListing != null && layerDataListing.layerDataQueryResults != null && layerDataListing.layerDataQueryResults.recordCount !== null) {
            hasData = true;
            if (layerDataListing.layerDataQueryResults.recordCount > 0) {
                dataNotEmpty = true;
            }
        }
        // display total records count or "select Data Layers to show data"
        let dataElement = null;
        if (!hasDataColumns) {
            dataElement = <span>Please select Data Layers to show data</span>;
        } else if (hasData) {
            dataElement = <span className="showTotalRecords">Displaying: <strong>{layerDataListing.layerDataQueryResults.recordCount} Records</strong></span>;
        }

        let chooseDataFields = null;
        let layerFieldPicker = null;
        if (layerColumns != null) {
            chooseDataFields = this.renderDataFields(layerColumns);
            layerFieldPicker = this.renderLayerFieldPicker();
        }
        let self = this;
        let ordername = this.props.layerDataListing.order.name;
        let orderdir = this.props.layerDataListing.order.dir;
        return(
            <div id="Result">
                <div className="toolbar">
                    <button onClick={this.handleRefresh} className="refresh-icon"></button>
                    <input type="checkbox" autoComplete="off" checked={layerDataListing.autoRefresh} name="auto-refresh"
                           className="pointer" id="chkbox_auto-refresh" onChange={(e) => this.toggleDataTableAutoRefresh(e)}/>
                    <label title="Auto refreshing data listing display" htmlFor="chkbox_auto-refresh"
                           className="pointer" onClick={(e) => {e.preventDefault(); this.toggleDataTableAutoRefresh(e);}}>
                        Auto Refresh
                    </label>
                    <input type="checkbox" autoComplete="off" checked={layerDataListing.invertQuery} name="invert-result"
                           id="chkbox_invert-query" className="pointer" onChange={(e) => this.toggleDataTableInvertQuery(e)}/>
                    <label title="Listing data based on 'inverse' of layer query" htmlFor="chkbox_invert-query"
                           className="pointer" onClick={(e) => {e.preventDefault(); this.toggleDataTableInvertQuery(e);}}>
                        Invert query
                    </label>
                    <button onClick={(e) => this.handleDownload(e)} className="download-icon"></button>
                    <button id="btn-pick-state-county" onClick={(e) => this.stateCountyBtnClicked(e)}>
                        {/*<strong>Pick Additional State or County level data >> </strong>*/}
                    </button>
                    {layerFieldPicker}
                </div>
                <div className="data-toolbar">
                    {dataElement}
                    {chooseDataFields}
                </div>
                <div id="result-table-div">
                <table className={dataNotEmpty && 'tbl_ResultLst'} summary="Search Results">
                    <thead>
                    <tr>
                    { hasDataColumns && columnHeaders.map(function(name) {
                        return (
                        <th key={name}> {name}&nbsp;
                            <span className="sort-icon" title="Asc order" style={{display: ordername === name ? "none" : "inline-block"}} onClick={(e) => self.handleColumnIndex(e,name,"asc")} ><ArrowUpDown/></span>
                            <span className="sort-iconUp" title="Desc order" style={{display: ordername === name && orderdir === "asc" ? "inline-block" : "none"}} onClick={(e) => self.handleColumnIndex(e,name,"desc")}><ArrowUp/></span>
                            <span className="sort-iconDown" title="Asc order" style={{display: ordername === name && orderdir === "desc" ? "inline-block" : "none"}} onClick={(e) => self.handleColumnIndex(e,name,"asc")}><ArrowDown/></span>
                        </th>
                        )
                    })}
                    </tr>
                    </thead>
                    <tbody>
                    { 
                        (dataNotEmpty &&
                            layerDataListing.layerDataQueryResults.rows.map((row, rindex) => {
                                return <tr id={"result-row-" + row["_irow"]} className={
                                    layerDataListing.selectedDataTableRow === row["_irow"] ? "pointer selectedDataTableRow" : "pointer"
                                } onClick={() => {
                                    this.rowClicked(row["_irow"])
                                }} key={'r' + rindex}>{
                                    columnHeaders.map((name, index) => {
                                        if ('Index' === name) {
                                            return <td key={name + rindex + index}>{row["_irow"]}</td>
                                        } else {
                                            return <td key={name + rindex + index}>{row[name]}</td>
                                        }
                                    })
                                }</tr>
                        })) || (!dataNotEmpty && <tr><td>No data available</td></tr>)
                    }
                    </tbody>
                </table>
                </div>
            </div>
        );
    }
    renderStateCountyPickerMenu() {
        let dataLevelExpandMap = this.props.layerDataListing.dataLevelExpandMap;
        let stateDataExpanded = dataLevelExpandMap.get("state");
        let countyDataExpanded = dataLevelExpandMap.get("county");
        let pickerPanel = (
            <div id="pick-state-county-data-div">
                <div className="addtnl-data-level-div" onClick={(e) => this.toggleAdditionalDataLevel(e, "county")}>
                    <img className="hideShowIcon" title="Show or hide item content"
                         alt="Show or hide item content image icon" src={this.getExpandCollapseSrc(countyDataExpanded)} />
                    <span className="hideShowIconDesc"> County Level
                    <button id="closeButtonData" onClick={(e) => this.stateCountyBtnClicked(e)}></button></span>
                </div>

                {countyDataExpanded?
                    <ul id="pick-county-data-ul">
                        {this.getAdditionalDataRenderElement("county")}
                    </ul>
                    :null
                }
            </div>
        );
        return pickerPanel;
    }
    componentDidUpdate() {
        let {layerDataListing, selectedMapTab} = this.props;
        let pickStateCountyBtn = document.getElementById("btn-pick-state-county");
        let pickStateCountyDiv = document.getElementById("pick-state-county-data-div");
        let selectedItem = document.getElementById("result-row-" + layerDataListing.selectedDataTableRow);
        if (pickStateCountyBtn !== null && pickStateCountyDiv !== null) {
            if (navigator.userAgent.search("Chrome") >= 0) {
                // pickStateCountyDiv.style.top = (pickStateCountyBtn.offsetTop + 91) + "px";
            }  else if (navigator.userAgent.search("Firefox") >= 0) {
                pickStateCountyDiv.style.top = (pickStateCountyBtn.offsetTop + 22) + "px";
            } else if (navigator.userAgent.search("MSIE") >= 0) {
                //TBD
            }
        }
        if (this.props.layerDataListing.autoRefresh && this.props.layerDataListing.shouldUpdate) {
            this.props.dispatch({type: "FETCH_LAYER_DATA_LIST"});
        }

        if (selectedItem !== null && layerDataListing.selectedDataTableRow !== null && selectedMapTab === "Print") {
            scrollIntoView(selectedItem, {
                time: 500,
                align: {
                    top: 0.1,
                    left: 0
                }
            });
        }
    }

    componentDidMount() {
        if (this.props.layerDataListing.autoRefresh) {
            this.props.dispatch({type: "FETCH_LAYER_DATA_LIST"});
        }
    }
}

export default ResultDataTable;