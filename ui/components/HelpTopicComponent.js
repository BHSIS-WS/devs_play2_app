import React from "react";
import Modal from "./PlayVideoComponent";
import {NewWindowIcon, PlayVideoIcon, VideoIcon} from "../constant/SvgIcons";
import {
    setclosePlayPane, setVisibilityPlayPane, toggleVisibilityShowAnswer,
    toggleVisibilityShowPlayButtons
} from "../actions/SidebarOptionsAction";

class HelpTopic extends React.Component {

    constructor(props) {
        super(props);
        this.showAnswer = this.showAnswer.bind(this);
        this.showPlayButtons = this.showPlayButtons.bind(this);
        this.setPlayPane = this.setPlayPane.bind(this);
        this.closePlayPane = this.closePlayPane.bind(this);
    }

    showAnswer() {
        let { dispatch, helpTopic } = this.props;
        dispatch(toggleVisibilityShowAnswer(helpTopic.devsHelpTopicId));
    }
    showPlayButtons() {
        let { dispatch, helpTopic } = this.props;
        dispatch(toggleVisibilityShowPlayButtons(helpTopic.devsHelpTopicId));
    }
    setPlayPane() {
        let { dispatch, helpTopic } = this.props;
        dispatch(setVisibilityPlayPane(helpTopic.devsHelpTopicId));
    }
    closePlayPane() {
        let { dispatch, helpTopic } = this.props;
        dispatch(setclosePlayPane(helpTopic.devsHelpTopicId));
    }

    getExpandCollapseSrc(showAnswer){
            return showAnswer ? "help-topic-arrow-open" : "help-topic-arrow-close";
    }

    render() {
        const { helpTopic } = this.props;
        return (
            <li>
                <div className={helpTopic.showAnswer ? "help-topic-header-open" : "help-topic-header"}>
                    <div id="help-topic-question-div" onClick={this.showAnswer}>
                        <span className={"help-topic-arrow "  + this.getExpandCollapseSrc(helpTopic.showAnswer)}  />
                        <p className='help-topic-question'>{helpTopic.topicTitle}</p>
                    </div>
                    {/*Disable the click for now as videos are not uploaded yet*/}
                    {/*<div id="help-topic-vid-icon-div" onClick={this.showPlayButtons}>*/}
                    <div id="help-topic-vid-icon-div">
                        <span className="gotolinklist"><VideoIcon/></span>
                    </div>
                </div>
                <p className="help-topic-answer" hidden={!helpTopic.showAnswer}>{helpTopic.content}</p>
                <div className="gotoLinks" hidden={!helpTopic.showPlayButtons}>
                    <button type="button" onClick={() => window.open (helpTopic.embedUrl)} title="Play in new window"><NewWindowIcon/>Play in a new window</button>
                    <button type="button" onClick={this.setPlayPane} title="Play video"><PlayVideoIcon/>Play in window center</button>
                </div>
                <Modal header="Video Tutorial" open={helpTopic.showPlayPane} src={helpTopic.sourceUrl} onClose={this.closePlayPane}>
                    <h1>Some Content</h1>
                </Modal>
             </li>
        );
    }
}

export default HelpTopic;