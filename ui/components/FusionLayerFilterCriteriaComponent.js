import React from "react";
import {
    removeCriteria, toggleCriteriaSelect, toggleCriteriaExpanded,
    selectAllCriteria, selectNoneCriteria, selectReverseCriteria
} from "../actions/NormalLayersAction";
import {fetchLayerDataList} from "../actions/LayerDatatListingAction";
import mapContentModel from "../others/MapContentModel";
import printContentModel from '../others/PrintContentModel';
import {removeAllPrintLayerInfoBox} from "../actions/PrintInfoBoxAction";
import {determineInclusiveWhereStringForCarto, removeInclusiveWhereString, refreshMapLayers} from "../actions/NormalLayersAction";
import D3MapNormalLayer from "../others/d3map/D3MapNormalLayer";

class FusionLayerFilterCriteriaComponent extends React.Component {

    constructor(props) {
        super(props);
        this.isCriteriaCheckboxSelected = this.isCriteriaCheckboxSelected.bind(this);
        this.criteriaCheckboxClicked = this.criteriaCheckboxClicked.bind(this);
        this.removeFilter = this.removeFilter.bind(this);
        this.toggleCriteriaExpanded = this.toggleCriteriaExpanded.bind(this);
        this.refreshMapLayers = this.refreshMapLayers.bind(this);
    }

    refreshMapLayers() {
        let {dispatch, dataTableAutoRefresh} = this.props;
        if (dataTableAutoRefresh) {
            dispatch(fetchLayerDataList());
        }
        mapContentModel.refreshGoogleMapLayers();
        printContentModel.refreshD3MapLayersFromState();
    }

    /* For each layer already selected, dispatch an action to fetch a new inclusive Show Common whereString.
     * Since at this point the current selected layer has not been added to the initial state, dispatch an action for that as well.
     */
    determineCartoWhereString(layerId, selectedLayerIdSet, layerArray) {
        let {dispatch} = this.props;
        let geomType = layerArray.find(lay => ((lay['id'] === layerId) ? lay['geom_type'] : '')).geom_type;
        if(geomType === "POINT") {
            this.refreshMapLayers();
        } else {
            selectedLayerIdSet.forEach(selectedLayer => {
                let selectedGeomType = layerArray.find(lay => ((lay['id'] === selectedLayer) ? lay['geom_type'] : '')).geom_type;
                dispatch(determineInclusiveWhereStringForCarto(selectedLayer));
            });
            //dispatch(determineInclusiveWhereStringForCarto(layerId));
        }
    }

    /*
     * In this case, always dispatch an action to fetch the inclusive Show Common whereString for each layer, or refresh the map.
     * See: this.determineCartoWhereString
     * */
    criteriaCheckboxClicked(e,criteria){
        let {dispatch, layer, selectedMapTab, selectedLayerIdSet, layerArray, dataTableAutoRefresh} = this.props;
        dispatch(removeAllPrintLayerInfoBox());
        dispatch(toggleCriteriaSelect(criteria.fusion_map_layer_id, criteria.criteria_code_id, criteria.id));
        if(selectedMapTab === "Carto") {
            this.determineCartoWhereString(criteria.fusion_map_layer_id, selectedLayerIdSet, layerArray);
            if(e.target.checked === false) {
                dispatch(removeInclusiveWhereString(criteria.fusion_map_layer_id));
            }
        }
        if (dataTableAutoRefresh) {
            dispatch({type: "FETCH_LAYER_DATA_LIST"});
        }
        this.refreshMapLayers();
    }

    isCriteriaCheckboxSelected(criteriaId){
        let { selectedCriteriaIdSet } = this.props;
        return selectedCriteriaIdSet.has(criteriaId);
    }

    selectAllCriteriaClicked(e) {
        let {dispatch, criteria, layer, criteriaCodeArray, selectedMapTab, selectedLayerIdSet, layerArray } = this.props;
        dispatch(removeAllPrintLayerInfoBox());
        let criteriaCodeIdArray = criteriaCodeArray.map(criteria => criteria.id);
        dispatch(selectAllCriteria(layer.id, criteria.id, criteriaCodeIdArray));
        if(selectedMapTab === "Carto") {
            this.determineCartoWhereString(layer.id, selectedLayerIdSet, layerArray);
        }
    }

    selectNoneCriteriaClicked(e) {
        let {dispatch, criteria, layer, criteriaCodeArray, selectedMapTab, selectedLayerIdSet, layerArray } = this.props;
        dispatch(removeAllPrintLayerInfoBox());
        let criteriaCodeIdArray = criteriaCodeArray.map(criteria => criteria.id);
        dispatch(selectNoneCriteria(layer.id, criteria.id, criteriaCodeIdArray));
        if(selectedMapTab === "Carto") {
            selectedLayerIdSet.forEach(selectedLayer => {
                let selectedGeomType = layerArray.find(lay => ((lay['id'] === selectedLayer) ? lay['geom_type'] : '')).geom_type;
                dispatch(removeInclusiveWhereString(selectedLayer));
                this.determineCartoWhereString(selectedLayer, selectedLayerIdSet, layerArray);
            });
        }
    }

    selectReverseCriteriaClicked(e) {
        let {dispatch, criteria, layer, criteriaCodeArray, selectedMapTab, selectedLayerIdSet, layerArray } = this.props;
        dispatch(removeAllPrintLayerInfoBox());
        let criteriaCodeIdArray = criteriaCodeArray.map(criteria => criteria.id);
        dispatch(selectReverseCriteria(layer.id, criteria.id, criteriaCodeIdArray));
        if(selectedMapTab === "Carto") {
            this.determineCartoWhereString(layer.id, selectedLayerIdSet, layerArray);
        }
    }

    removeFilter() {
        let {dispatch, criteria, layer, selectedMapTab, selectedLayerIdSet, layerArray} = this.props;
        dispatch(removeAllPrintLayerInfoBox());
        dispatch(removeCriteria(layer.id, criteria.id));
        if(selectedMapTab === "Carto") {
            selectedLayerIdSet.forEach(selectedLayer => {
                let selectedGeomType = layerArray.find(lay => ((lay['id'] === selectedLayer) ? lay['geom_type'] : '')).geom_type;
                dispatch(removeInclusiveWhereString(selectedLayer));
                this.determineCartoWhereString(selectedLayer, selectedLayerIdSet, layerArray);
            });
        }
    }

    toggleCriteriaExpanded() {
        let {dispatch, criteria} = this.props;
        dispatch(toggleCriteriaExpanded(criteria.id));
    }

    render(){
        let { criteriaCodeArray, criteria, expanded, layer } = this.props;
        return (
            <div className="fusion-layer-filter-criteria-div">
                <div className='fusion-layer-filter-criteria-header-div'>
                    <b>{criteria.criteria_name}</b>
                    <button className='remove-filter-button' title='Remove filter' onClick={this.removeFilter} />
                    <button className={expanded? 'collapse-filter-button' : 'expand-filter-button'} title='Toggle Filter Visibility' onClick={this.toggleCriteriaExpanded} />
                </div>
                <div className='fusion-layer-filter-criteria-body-div' hidden={!expanded}>
                    <div className='fusion-layer-filter-criteria-info-div'>
                        <a href="javascript:void(0)" className="select_all" onClick={this.selectAllCriteriaClicked.bind(this)}>All</a>
                        {' / '}
                        <a href="javascript:void(0)" className="select_none" onClick={this.selectNoneCriteriaClicked.bind(this)}>None</a>
                        {' / '}
                        <a href="javascript:void(0)" className="select_reverse" onClick={this.selectReverseCriteriaClicked.bind(this)}>Reverse</a>
                        <span>Type of search: {layer.query_type_display}</span>
                    </div>
                    <ul>
                        {criteriaCodeArray.map((criteriaCode) => {
                            return (
                                <li key={criteriaCode.id}>
                                    <input id={"chkbox_criteria_"+ criteriaCode.id} autoComplete="off" name={"chkbox_criteria_"+ criteriaCode.id}
                                           onChange={(e)=>this.criteriaCheckboxClicked(e,criteriaCode)}
                                           checked={this.isCriteriaCheckboxSelected(criteriaCode.id)}
                                           type="checkbox"/>
                                    <label htmlFor={"chkbox_criteria_"+ criteriaCode.id}>{criteriaCode.name}</label>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}

export default FusionLayerFilterCriteriaComponent;