import React from "react";
import {determineInclusiveWhereStringForCarto, removeInclusiveWhereString} from "../../actions/NormalLayersAction";

class D3PointLayerConfig extends React.Component {

    constructor(props) {
        super(props);
    }

    getClassName(expanded) {
        if (expanded) {
            return "open";
        } else {
            return "";
        }
    }

    onClick(e, layerId) {
        e.preventDefault();
        let { toggleD3LayerConfigPanel } = this.props;
        toggleD3LayerConfigPanel(layerId, false);
    }

    isLayerEnabled(disabledLayerIdSet, layerId) {
        if (disabledLayerIdSet.has(layerId)) {
            return "";
        } else {
            return "checked";
        }
    }

    layerCheckboxClicked(e, layerId) {
        this.props.toggleD3LayerDisable(layerId, false);
    }

    updateTab(evt, layerId) {
        this.props.updateConfigTab(layerId, evt.currentTarget.dataset.value, false);
    }

    updateShapeByVariable(isShapeByVariable, layerId) {
        let { layerConfig, updateShapeByVariable, updateCodeConfigId, layer } = this.props;
        if (layerConfig.selectedVariableForStyle === "" && layerConfig.shapeConfigMap && layerConfig.shapeConfigMap.size > 0) {
            let shapeConfigMapJson = JSON.parse(layerConfig.shapeConfigMap.get(0));
            if (shapeConfigMapJson) {
                updateCodeConfigId(layer.id, shapeConfigMapJson.name);
            }
        }
        updateShapeByVariable(layerId, isShapeByVariable);
    }

    updateGeneralType(evt, layerId) {
        let { dispatch } = this.props;
        const keyName = evt.target.dataset.stateVariable;
        this.props.updateGeneralType(layerId, keyName, evt.target.checked, false);
        if(keyName === "commonShapeForAllLayers") {
            dispatch(determineInclusiveWhereStringForCarto(layerId));
        }
    }


    updateConfigCodeId(evt, layerId) {
        this.props.updateCodeConfigId(layerId, evt.target.value);
    }

    toggleColorPalette(evt, showValue, layer, hasSameShapeStyle, layerConfig, criteriaName) {
        const position = evt.currentTarget.getBoundingClientRect(),
            topPosition = evt.currentTarget.parentElement.getBoundingClientRect().top - position.height + window.scrollY - 180,
            leftPosition = ((evt.currentTarget.parentElement.getBoundingClientRect().right - document.getElementById('mapControlD3').getBoundingClientRect().left) - 150) * -1;


        let actionType = "UPDATE_SHAPE_CONFIGURATION";
        let criteriaCodeKey = this.refs.choosePointCode ? this.refs.choosePointCode.value : null;

        this.props.toggleColorPalette(showValue, criteriaName, criteriaCodeKey, layer.id, layer.geom_type, topPosition, leftPosition, null, actionType);

        if(showValue) {
            this.props.updateSelectedShapeConfiguration(layerConfig, hasSameShapeStyle);
        }
    }

    renderShapeSvg(shapeStyle, svgShapes) {
        const shapeId = shapeStyle && shapeStyle.shapeId ? (shapeStyle.shapeId - 1) : 0;
        let svgTagProps  = Object.assign(...(svgShapes[shapeId].svgAttributes).map(attr => ({[attr["name"]]: attr["value"]}))),
            svgTagOtherProps  = (svgShapes[shapeId].svgOtherAttributes) ?
                Object.assign(...(svgShapes[shapeId].svgOtherAttributes).map(attr => ({[attr["name"]]: attr["value"]}))) : {},
            svgTag = React.createElement(svgShapes[shapeId].svgTag, {
                ...svgTagProps,
                ...svgTagOtherProps,
                "vectorEffect": "non-scaling-stroke",
                "stroke": shapeStyle && shapeStyle.strokeColor ? shapeStyle.strokeColor : "#000",
                "strokeWidth": shapeStyle && shapeStyle.strokeSize ? shapeStyle.strokeSize : "1",
                "strokeOpacity": shapeStyle && shapeStyle.strokeOpacity ? shapeStyle.strokeOpacity : "",
                "fill": shapeStyle && shapeStyle.fillColor ? shapeStyle.fillColor : "#fff",
                "fillOpacity": shapeStyle && shapeStyle.fillOpacity ? shapeStyle.fillOpacity : ""
            });

        return svgTag;
    }


    toggleDisplayLegendCategory(criteriaKey, layerId) {
        this.props.toggleDisplayLegendCategory(criteriaKey, layerId, false);
    }

    render() {
        const {disabledLayerIdSet, layerConfig, layer, colorPalette, svgShapes,
            expanded, selectedTab, isShapeByVariable, commonShapeForAllLayers, freezeShapeSizeWhenZoom,
            legendDisplayIncluded, shapeForAllStyle, selectedVariableForStyle, categoricalListArray, categoricalCodes} = this.props;

        return(
                <div ref="d3PointConfig" key={layer.id}>
                    <div className="d3PointLayerConfigLi">
                        <a href="javascript:void(0);" className={this.getClassName(expanded)}
                            onClick={e => this.onClick(e, layer.id)}>
                            {layer.layer_name}
                        </a>
                        <span id="toggleLayerOnMapDisplay">
                            <label>Show in the map</label>
                            <input id="toggle-layer-onmap-display" checked={this.isLayerEnabled(disabledLayerIdSet, layer.id)}
                                   onChange={(e) => this.layerCheckboxClicked(e, layer.id)}
                                   type="checkbox"/>
                        </span>
                    </div>
                    {expanded ?
                        <div id={"d3PointLayerExpanded-" + layer.id}>
                            <div className="d3PointLayerConfigPanelDiv general">
                                <div className="checkSec">
                                    <div>
                                        <input type="checkbox" id="generalCheck1" checked={commonShapeForAllLayers} data-state-variable="commonShapeForAllLayers" onChange={(evt) => this.updateGeneralType(evt, layer.id)}/>
                                        <label className="config-label" htmlFor="generalCheck1">Show only shape common to all layers</label>
                                    </div>
                                    <div>
                                        <input type="checkbox" id="generalCheck2" checked={legendDisplayIncluded} data-state-variable="legendDisplayIncluded" onChange={(evt) => this.updateGeneralType(evt, layer.id)}/>
                                        <label className="config-label" htmlFor="generalCheck2">Included for legend display</label>
                                    </div>
                                    <div>
                                        <input type="checkbox" id="generalCheck3" checked={freezeShapeSizeWhenZoom} data-state-variable="freezeShapeSizeWhenZoom" onChange={(evt) => this.updateGeneralType(evt, layer.id)}/>
                                        <label className="config-label" htmlFor="generalCheck3">Do not scale shape during zoom</label>
                                    </div>
                                </div>
                            </div>
                            {selectedTab === "shape" ?
                                <div className="d3PointLayerConfigPanelDiv">
                                    <div className="checkSec">
                                        <div>
                                            <input type="radio" id="infoRadio1" name={"infoShape" + layer.id} checked={!isShapeByVariable} onChange={() => this.updateShapeByVariable(false, layer.id)}/>
                                            <label className="config-label" htmlFor="infoRadio1">Use same shape for all data</label>
                                        </div>
                                        <div>
                                            <input type="radio" id="infoRadio2" name={"infoShape" + layer.id} checked={isShapeByVariable} onChange={() => this.updateShapeByVariable(true, layer.id)} disabled={!categoricalListArray}/>
                                            <label className="config-label" htmlFor="infoRadio2">Configuration shape using categorical variable</label>
                                        </div>
                                    </div>
                                    {isShapeByVariable && categoricalCodes ?
                                        <div className="chooseCate">
                                            <label className="config-label" htmlFor="choosePointCode">Choose categorical variable</label>
                                            <select ref="choosePointCode" id="choosePointCode" value={selectedVariableForStyle} onChange={(evt) => this.updateConfigCodeId(evt, layer.id)}>
                                                {categoricalCodes.map(categoryCode =>
                                                (
                                                    <option key={categoryCode} value={categoryCode}>
                                                        {categoryCode}
                                                    </option>
                                                )
                                                )}
                                            </select>
                                        </div>
                                    : null}
                                    <table>
                                        {!isShapeByVariable ?
                                            <tbody>
                                                <tr>
                                                    <td>Shape Style</td>
                                                    <td>
                                                        <svg width="20" height="20" viewBox="0 0 2 2" onClick={(evt) => this.toggleColorPalette(evt, !colorPalette.show, layer, !isShapeByVariable, layerConfig, null)}>
                                                            {this.renderShapeSvg(shapeForAllStyle, svgShapes)}
                                                        </svg>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        :
                                            <tbody>
                                                {categoricalListArray ? categoricalListArray.map(criteria => (
                                                    criteria.name ?
                                                        <tr key={criteria.id}>
                                                            <td>{criteria.name}</td>
                                                            <td>
                                                                <svg width="20" height="20" viewBox="0 0 2 2" onClick={(evt) => this.toggleColorPalette(evt, !colorPalette.show, layer, !isShapeByVariable, layerConfig, criteria.name)}>
                                                                    {this.renderShapeSvg(criteria.shapeConfigStyle, svgShapes)}
                                                                </svg>
                                                            </td>
                                                            <td className="legendConfig">
                                                            <div className="legendConfig">
                                                                <label className="legend-config-label" htmlFor={"legendConfigCheck-" + criteria.id}>Hide </label><label htmlFor={"legendConfigCheck-" + criteria.id} id="legend-config-icon"></label>
                                                                <input type="checkbox" id={"legendConfigCheck-" + criteria.id} onChange={() => this.toggleDisplayLegendCategory(selectedVariableForStyle + "-" + criteria.name, layer.id)}
                                                                checked={layerConfig.hideLegendConfigSet !== undefined && layerConfig.hideLegendConfigSet.has(selectedVariableForStyle + "-" + criteria.name)} />
                                                            </div>
                                                            </td>
                                                        </tr>
                                                    : null
                                                )
                                                ) : null }
                                            </tbody>

                                        }
                                    </table>
                                </div>
                            :
                                null
                            }
                        </div>
                    : null}
                </div>
        );
    }
}

export default D3PointLayerConfig;
