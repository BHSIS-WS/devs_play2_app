import React from "react";
import {determineInclusiveWhereStringForCarto} from "../../actions/NormalLayersAction";

class D3BoundaryLayerConfig extends React.Component {

    constructor(props) {
        super(props);
    }

    getClassName(expanded) {
        if (expanded) {
            return "open";
        } else {
            return "";
        }
    }

    onClick(e, layerId) {
        e.preventDefault();
        this.props.toggleD3LayerConfigPanel(layerId, false);
    }

    isLayerEnabled(disabledLayerIdSet, layerId) {
        if (disabledLayerIdSet.has(layerId)) {
            return "";
        } else {
            return "checked";
        }
     }

    layerCheckboxClicked(e, layerId) {
        this.props.toggleD3LayerDisable(layerId, false);
    }

    updateTab(evt, layerId) {
        this.props.updateConfigTab(layerId, evt.currentTarget.dataset.value, false);
    }

    updateShapeByVariable(isShapeByVariable, layerId) {
        let { layerConfig, updateShapeByVariable, updateCodeConfigId, layer } = this.props;
        if (layerConfig.selectedVariableForStyle === "" && layerConfig.shapeConfigMap && layerConfig.shapeConfigMap.size > 0) {
            let shapeConfigMapJson = JSON.parse(layerConfig.shapeConfigMap.get(0));
            if (shapeConfigMapJson) {
                updateCodeConfigId(layer.id, shapeConfigMapJson.name);
            }
        }
        updateShapeByVariable(layerId, isShapeByVariable);
    }

    updateGeneralType(evt, layerId) {
        let { dispatch } = this.props;
        const keyName = evt.target.dataset.stateVariable;
        this.props.updateGeneralType(layerId, keyName, evt.target.checked, false);
        if(keyName === "commonShapeForAllLayers") {
            dispatch(determineInclusiveWhereStringForCarto(layerId));
        }
    }

    updateCoordinateTolerance(evt, layerId) {
        this.props.updateCoordinateTolerance(layerId, evt.target.value, false);
    }

    updateConfigCodeId(evt, layerId) {
        this.props.updateCodeConfigId(layerId, evt.target.value);
    }

    toggleColorPalette(evt, showValue, layer, hasSameShapeStyle, layerConfig, criteriaName) {
        const position = evt.currentTarget.getBoundingClientRect(),
            topPosition = position.top + position.height + window.scrollY - 120,
            leftPosition = ((position.left - document.getElementById('mapControlD3').getBoundingClientRect().left + position.width) - position.left);

        let actionType = "UPDATE_SHAPE_CONFIGURATION";
        let criteriaCodeKey = this.refs.chooseBoundaryCode ? this.refs.chooseBoundaryCode.value : null;

        this.props.toggleColorPalette(showValue, criteriaName, criteriaCodeKey, layer.id, null, topPosition, leftPosition, null, actionType);

        if(showValue) {
            this.props.updateSelectedShapeConfiguration(layerConfig, hasSameShapeStyle);
        }
    }

    renderShapeSvg(shapeStyle) {
        if (shapeStyle) {
            return <path stroke={shapeStyle.strokeColor || "#000" } strokeWidth={shapeStyle.strokeSize || "1" } fill={shapeStyle.fillColor || "#fff" } strokeOpacity={shapeStyle.strokeOpacity} fillOpacity={shapeStyle.fillOpacity}
                        d="m4.13592,4.99998l2.87767,0.76744l2.29709,0.10963l1.96893,-0.54817l1.53981,0.76744l0.12621,1.35216l0.63107,0.43854l1.71651,-2.7774l0.65631,0.25581l0.05048,2.66777l-1.76699,2.63122l0.37864,1.75415l-0.83301,0.58472l1.06019,2.85049c0,0 -0.73204,0.14618 -0.73204,0.10963c0,-0.03655 -1.79223,-2.1196 -1.79223,-2.1196c0,0 -1.76699,0.58472 -1.76699,0.58472c0,0 -1.38835,-0.25581 -1.38835,-0.25581c0,0 -1.18641,1.82724 -1.18641,1.82724c0,0 -1.41359,-0.65781 -1.41359,-0.65781c0,0 -0.75728,-1.75415 -0.75728,-1.75415c0,0 -1.69126,-0.65781 -1.69126,-0.65781c0,0 -1.11068,-2.74086 -1.11068,-2.74086c0,0 0.47961,-2.99667 0.47961,-2.99667c0,0 0.65631,-2.19269 0.65631,-2.19269l0,0l0.00001,0.00001z"/>;
        } else {
            return <path stroke="#000" strokeWidth="1" fill="#fff" d="m4.13592,4.99998l2.87767,0.76744l2.29709,0.10963l1.96893,-0.54817l1.53981,0.76744l0.12621,1.35216l0.63107,0.43854l1.71651,-2.7774l0.65631,0.25581l0.05048,2.66777l-1.76699,2.63122l0.37864,1.75415l-0.83301,0.58472l1.06019,2.85049c0,0 -0.73204,0.14618 -0.73204,0.10963c0,-0.03655 -1.79223,-2.1196 -1.79223,-2.1196c0,0 -1.76699,0.58472 -1.76699,0.58472c0,0 -1.38835,-0.25581 -1.38835,-0.25581c0,0 -1.18641,1.82724 -1.18641,1.82724c0,0 -1.41359,-0.65781 -1.41359,-0.65781c0,0 -0.75728,-1.75415 -0.75728,-1.75415c0,0 -1.69126,-0.65781 -1.69126,-0.65781c0,0 -1.11068,-2.74086 -1.11068,-2.74086c0,0 0.47961,-2.99667 0.47961,-2.99667c0,0 0.65631,-2.19269 0.65631,-2.19269l0,0l0.00001,0.00001z"/>;
        }
    }

    toggleDisplayLegendCategory(criteriaKey, layerId) {
        this.props.toggleDisplayLegendCategory(criteriaKey, layerId, false);
    }

    render() {
        const {disabledLayerIdSet, layerConfig, layer, colorPalette,
            expanded, selectedTab, isShapeByVariable, coordinateAccuracyTolerance, commonShapeForAllLayers,
            legendDisplayIncluded, shapeForAllStyle, selectedVariableForStyle, categoricalListArray, categoricalCodes} = this.props;

        return(
            <div ref="d3BoundaryConfig" key={layer.id}>
                <div className="d3BoundaryLayerConfigLi">
                    <a href="javascript:void(0);" className={this.getClassName(expanded)}
                        onClick={e => this.onClick(e, layer.id)}>
                        {layer.layer_name}
                    </a>
                    <span id="toggleLayerOnMapDisplay">
                        <label>Show in the map</label>
                        <input id="toggle-layer-onmap-display" checked={this.isLayerEnabled(disabledLayerIdSet, layer.id)}
                               onChange={(e) => this.layerCheckboxClicked(e, layer.id)}
                               type="checkbox"/>
                    </span>
                </div>
                {expanded ?
                    <div>
                        <div className="d3BoundaryLayerConfigPanelDiv general">
                            <div className="checkSec">
                                <div>
                                    <input type="checkbox" id="generalBoundaryCheck1" checked={commonShapeForAllLayers} data-state-variable="commonShapeForAllLayers" onChange={(evt) => this.updateGeneralType(evt, layer.id)}/>
                                    <label className="config-label" htmlFor="generalBoundaryCheck1">Show only shape common to all layers</label>
                                </div>
                                <div>
                                    <input type="checkbox" id="generalBoundaryCheck2" checked={legendDisplayIncluded} data-state-variable="legendDisplayIncluded" onChange={(evt) => this.updateGeneralType(evt, layer.id)}/>
                                    <label className="config-label" htmlFor="generalBoundaryCheck2">Included for legend display</label>
                                </div>
                                <div>
                                    <label className="config-label" htmlFor="coordinateTolerance">Coordinate accuracy tolerance</label>
                                    <input type="text" size="3" id="coordinateTolerance" value={coordinateAccuracyTolerance} onChange={(evt) => this.updateCoordinateTolerance(evt, layer.id)}/>
                                </div>
                            </div>
                        </div>
                        {selectedTab === "shape" ?
                            <div className="d3BoundaryLayerConfigPanelDiv">
                                <div className="checkSec">
                                    <div>
                                        <input type="radio" id="infoBoundaryRadio1" name={"infoBoundaryShape" + layer.id } checked={!isShapeByVariable} onChange={() => this.updateShapeByVariable(false, layer.id)}/>
                                        <label className="config-label" htmlFor="infoBoundaryRadio1">Use same shape for all data</label>
                                    </div>
                                    <div>
                                        <input type="radio" id="infoBoundaryRadio2" name={"infoBoundaryShape" + layer.id } checked={isShapeByVariable} onChange={() => this.updateShapeByVariable(true, layer.id)} disabled={!categoricalListArray}/>
                                        <label className="config-label" htmlFor="infoBoundaryRadio2">Configuration shape using categorical variable</label>
                                    </div>
                                </div>
                                {isShapeByVariable && categoricalCodes ?
                                    <div className="chooseCate">
                                        <label className="config-label" htmlFor="chooseBoundaryCode">Choose categorical variable</label>
                                        <select ref="chooseBoundaryCode" id="chooseBoundaryCode" value={selectedVariableForStyle} onChange={(evt) => this.updateConfigCodeId(evt, layer.id)}>
                                            {categoricalCodes.map(categoryCode=>
                                                (
                                                    <option key={categoryCode} value={categoryCode}>
                                                        {categoryCode}
                                                    </option>
                                                )
                                            )}
                                        </select>
                                    </div>
                                : null}
                                <table>
                                    {!isShapeByVariable ?
                                        <tbody>
                                        <tr>
                                            <td>Shape Style</td>
                                            <td>
                                                <svg width="20" height="20"
                                                     onClick={(evt) => this.toggleColorPalette(evt, !colorPalette.show, layer, !isShapeByVariable, layerConfig, null)}>
                                                    {this.renderShapeSvg(shapeForAllStyle)}
                                                </svg>
                                            </td>
                                        </tr>
                                        </tbody>
                                        :
                                        <tbody>
                                        {categoricalListArray ? categoricalListArray.map(criteria => (
                                            criteria.name ?
                                                <tr key={criteria.id}>
                                                    <td>{criteria.name}</td>
                                                    <td>
                                                        <svg width="20" height="20"
                                                             onClick={(evt) => this.toggleColorPalette(evt, !colorPalette.show, layer, !isShapeByVariable, layerConfig, criteria.name)}>
                                                            {this.renderShapeSvg(criteria.shapeConfigStyle)}
                                                        </svg>
                                                    </td>
                                                    <td className="legendConfig">
                                                    <div className="legendConfig">
                                                        <label className="legend-config-label" htmlFor={"legendConfigCheck-" + criteria.id}>Hide </label><label htmlFor={"legendConfigCheck-" + criteria.id} id="legend-config-icon"></label>
                                                        <input type="checkbox" id={"legendConfigCheck-" + criteria.id} onChange={() => this.toggleDisplayLegendCategory(selectedVariableForStyle + "-" + criteria.name, layer.id)}
                                                        checked={layerConfig.hideLegendConfigSet !== undefined && layerConfig.hideLegendConfigSet.has(selectedVariableForStyle + "-" + criteria.name)} />
                                                    </div>
                                                    </td>
                                                </tr>
                                            : null
                                            )
                                        ) : null}
                                        </tbody>
                                    }
                                </table>
                            </div>
                        :
                            null
                        }
                    </div>
                : null}
            </div>
        );
    }
}

export default D3BoundaryLayerConfig;