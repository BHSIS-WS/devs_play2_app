import React from "react";

class D3GradientLayerConfig extends React.Component {

    getClassName(expanded) {
        if (expanded) {
            return "open";
        } else {
            return "";
        }
    }

    onClick(evt, layerId) {
        evt.preventDefault();
        this.props.toggleD3LayerConfigPanel(layerId, true);
    }

    isLayerEnabled(disabledLayerIdSet,  layerId) {
        if (disabledLayerIdSet.has(layerId)) {
            return "";
        } else {
            return "checked";
        }
    }

    layerCheckboxClicked(evt, layerId) {
        this.props.toggleD3LayerDisable(layerId, true);
    }

    updateTab(evt, layerId) {
        this.props.updateConfigTab(layerId, evt.currentTarget.dataset.value, true);
    }

    updateGeneralType(evt, layerId) {
        const keyName = evt.target.dataset.stateVariable;
        this.props.updateGeneralType(layerId, keyName, evt.target.checked, true);
    }

    updateCoordinateTolerance(evt, layerId) {
        this.props.updateCoordinateTolerance(layerId, evt.target.value, true);
    }

    updateGradientConfigValue(evt, index, layerId) {
        this.props.updateGradientConfigValue(index, evt.target.value, layerId);
    }

    addNewGradientConfigValue(layerId) {
        this.props.addNewGradientConfigValue(layerId);
    }

    removeGradientConfig(index, layerId) {
        this.props.removeGradientConfigValue(index, layerId);
    }

    toggleColorPalette(evt, gradLayer, gradLayerConfig, sectionName, gradientConfigIdx) {
        const position = evt.currentTarget.getBoundingClientRect(),
            topPosition = position.top + position.height + window.scrollY - 120,
            leftPosition = ((position.left - document.getElementById('mapControlD3').getBoundingClientRect().left + position.width) - position.left);

        let actionType = "UPDATE_GRADIENT_" + sectionName + "_CONFIGURATION";

        this.props.toggleColorPalette(true, gradientConfigIdx, null, gradLayer.id, null, topPosition, leftPosition, sectionName, actionType);
        this.props.updateSelectedGradientShapeConfiguration(gradLayerConfig, false);

    }

    getlinearGradient(gradientColorConfigArray, borderStyle) {
        const borderColor = borderStyle && borderStyle.color ? borderStyle.color : "#000",
            borderSize = borderStyle && borderStyle.size ? borderStyle.size + "px" : "1px",
            borderOpacity = borderStyle && borderStyle.opacity ? borderStyle.opacity : "",
            linearGradientArr = gradientColorConfigArray.map(c => this.hexToRgba(c.color, (c.opacity || "")));

        return ({
            background: "linear-gradient(to right, " + linearGradientArr.join(', ') + ")",
            border: borderSize + " solid " +  this.hexToRgba(borderColor, borderOpacity)
        });
    }

    hexToRgba(color, opacity) {
        const r = parseInt(color.length === 4  ? color.slice(1, 2).repeat(2) : color.slice(1, 3), 16),
            g = parseInt(color.length === 4  ? color.slice(2, 3).repeat(2) : color.slice(3, 5), 16),
            b = parseInt(color.length === 4  ? color.slice(3, 4).repeat(2) : color.slice(5, 7), 16);

        if (opacity) {
            return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + opacity + ')';
        } else {
            return 'rgb(' + r + ', ' + g + ', ' + b + ')';
        }
    }

    toggleDisplayLegendCategory(criteriaKey, layerId) {
        this.props.toggleDisplayLegendCategory(criteriaKey, layerId, true);
    }

    render() {
        const {disabledGradientLayerIdSet, gradientLayerConfigMap, gradientLayerArray, layer} = this.props;
        let gradLayerConfig = gradientLayerConfigMap.get(layer.id),
            expanded = typeof gradLayerConfig !== "undefined" ? gradLayerConfig.expanded : false,
            selectedTab = typeof gradLayerConfig !== "undefined" && gradLayerConfig.selectedTab !== undefined ? gradLayerConfig.selectedTab : "shape",
            gradLayer = gradientLayerArray.find(gradLayer => gradLayer.id === layer.id),
            coordinateAccuracyTolerance = typeof gradLayerConfig !== "undefined" && gradLayerConfig.coordinateAccuracyTolerance !== undefined ? gradLayerConfig.coordinateAccuracyTolerance : "",
            commonShapeForAllLayers = typeof gradLayerConfig !== "undefined" && gradLayerConfig.commonShapeForAllLayers !== undefined ? gradLayerConfig.commonShapeForAllLayers : false,
            legendDisplayIncluded = typeof gradLayerConfig !== "undefined" && gradLayerConfig.legendDisplayIncluded !== undefined ? gradLayerConfig.legendDisplayIncluded : false,
            freezeShapeSizeWhenZoom = typeof gradLayerConfig !== "undefined" && gradLayerConfig.freezeShapeSizeWhenZoom !== undefined ? gradLayerConfig.freezeShapeSizeWhenZoom : false,
            borderStyle = typeof gradLayerConfig !== "undefined" && gradLayerConfig.borderStyle !== undefined  ? gradLayerConfig.borderStyle : "";

        return(
            <div ref="d3GradientConfig" key={layer.id}>
                <div className="d3GradientLayerConfigLi">
                    <a href="javascript:void(0);" className={this.getClassName(expanded)}
                        onClick={evt => this.onClick(evt, layer.id)}>
                        {gradLayer.name}
                    </a>
                    <span id="toggleLayerOnMapDisplay">
                        <label>Show in the map</label>
                        <input id="toggle-layer-onmap-display" checked={this.isLayerEnabled(disabledGradientLayerIdSet, layer.id)}
                               onChange={(evt) => this.layerCheckboxClicked(evt, layer.id)}
                               type="checkbox"/>
                    </span>
                </div>
                {expanded ?
                    <div>
                        <div className="d3PointLayerConfigPanelDiv general">
                            <div className="checkSec">
                                <div>
                                    <input type="checkbox" id="generalCheck1" checked={commonShapeForAllLayers} data-state-variable="commonShapeForAllLayers" onChange={(evt) => this.updateGeneralType(evt, layer.id)}/>
                                    <label className="config-label" htmlFor="generalCheck1">Show only shape common to all layers</label>
                                </div>
                                <div>
                                    <input type="checkbox" id="generalCheck2" checked={legendDisplayIncluded} data-state-variable="legendDisplayIncluded" onChange={(evt) => this.updateGeneralType(evt, layer.id)}/>
                                    <label className="config-label" htmlFor="generalCheck2">Included for legend display</label>
                                </div>
                                <div>
                                    <input type="checkbox" id="generalCheck3" checked={freezeShapeSizeWhenZoom} data-state-variable="freezeShapeSizeWhenZoom" onChange={(evt) => this.updateGeneralType(evt, layer.id)}/>
                                    <label className="config-label" htmlFor="generalCheck3">Do not scale shape during zoom</label>
                                </div>
                            </div>
                        </div>
                        {selectedTab === "shape" ?
                            <div className="d3GradientLayerConfigPanelDiv">
                                <div className="chooseCateGrad">
                                    <label className="config-label" htmlFor="chooseBorderColor">Border:</label>
                                    <svg id="chooseBorderColor" transform="translate(0,4)" width="50" height="20" viewBox="0 0 5 2" onClick={(evt) => this.toggleColorPalette(evt, gradLayer, gradLayerConfig, "STROKE")}>
                                        <rect vectorEffect="non-scaling-stroke" height="1.5" width="5" y="0.25" x="0.0" fill="#fff" strokeWidth={borderStyle && borderStyle.size ? borderStyle.size : "1"}
                                            stroke={borderStyle && borderStyle.color ? borderStyle.color : "#000"} strokeOpacity={borderStyle && borderStyle.opacity ? borderStyle.opacity : ""}/>
                                    </svg>
                                </div>
                                <div className="chooseCateGrad">
                                    <label className="config-label">Fill:</label>
                                    <text className="colorSpectrum" style={this.getlinearGradient(gradLayerConfig.gradientColorConfigArray, borderStyle)}></text>
                                </div>
                                <div className="chooseCate">
                                    <label className="config-label">Fill Gradient Configuration:</label>
                                    <button onClick={() => this.addNewGradientConfigValue(layer.id)}> New Value</button>
                                    {gradLayerConfig.gradientColorConfigArray.length > 0 ?
                                        <table>
                                        <tbody>
                                        {gradLayerConfig.gradientColorConfigArray.map((colorConfig, index) => (
                                            <tr key={index}>
                                                <td><input type="text" size="6" value={colorConfig.value} onChange={((evt) => this.updateGradientConfigValue(evt, index, layer.id))}/></td>
                                                <td>
                                                    <svg width="50" height="20" viewBox="0 0 5 2" onClick={(evt) => this.toggleColorPalette(evt, gradLayer, gradLayerConfig, "FILL", index)}>
                                                        <rect height="1.5" width="5" y="0.25" x="0.0" stroke="#000" fill={colorConfig.color} fillOpacity={colorConfig.opacity} vectorEffect="non-scaling-stroke"/>
                                                    </svg>
                                                </td>
                                                <td className="legendConfig">
                                                <div className="legendConfig">
                                                    <label className="legend-config-label" htmlFor={"legendConfigCheck-" + index}>Hide </label><label htmlFor={"legendConfigCheck-" + index} id="legend-config-icon"></label>
                                                    <input type="checkbox" id={"legendConfigCheck-" + index} onChange={() => this.toggleDisplayLegendCategory(index, layer.id)}
                                                    checked={gradLayerConfig.hideLegendConfigSet !== undefined && gradLayerConfig.hideLegendConfigSet.has(index)} />
                                                </div>
                                                </td>
                                                <td><img className="shapeIcons" src="/devs2/assets/images/close_btn.png" alt="remove" onClick={() => this.removeGradientConfig(index, layer.id)}/></td>
                                            </tr>
                                        ))}
                                        </tbody>
                                        </table>
                                    : null}
                                </div>
                            </div>
                            :
                            null
                        }
                    </div>
                : null}
            </div>
        );
    }
}

export default D3GradientLayerConfig;