import React from "react";
import D3PointLayerConfig1 from '../../containers/mapd3control/D3PointLayerConfig1';
import D3BoundaryLayerConfig1 from '../../containers/mapd3control/D3BoundaryLayerConfig1';
import D3GradientLayerConfig1 from '../../containers/mapd3control/D3GradientLayerConfig1';

class D3LayerConfigList extends React.Component {

    componentWillMount() {
        // this.initializeGradientLayers(this.props.selectedGradLayerArray);
        // this.initializeNormalLayers(this.props.selectedLayerArray);
    }

    componentWillReceiveProps(nextProps) {
        let newGradLayers = [];
        nextProps.selectedGradLayerArray.forEach(newGradLayer => {
            let match = this.props.selectedGradLayerArray.find(oldGradLayer => {
                return oldGradLayer.id === newGradLayer.id;
            });
            if (match === undefined) {
                newGradLayers.push(newGradLayer);
            }
        });

        let newNormalLayers = [];
        nextProps.selectedLayerArray.forEach(newNormalLayer => {
            let match = this.props.selectedLayerArray.find(oldLayerArray => {
                return oldLayerArray.id === newNormalLayer.id;
            });
            if (match === undefined) {
                newNormalLayers.push(newNormalLayer);
            }
        });

    }


    setSelectedPrintControlLayerId(layerId) {
        this.props.dispatch({type:"SET_SELECTED_PRINT_CONTROL_LAYER_ID", selectedPrintControlLayerId: layerId});
    }

    returnJSX(layer, layerId) {
        if (layerId.split('-')[0] === 'normal') {
            if (layer.geom_type === 'POINT') {
                return (
                    <D3PointLayerConfig1
                        key={layer.id}
                        layer={layer}
                        toggleD3LayerConfigPanel={this.props.toggleD3LayerConfigPanel}
                        toggleD3LayerDisable={this.props.toggleD3LayerDisable}
                        updateConfigTab={this.props.updateConfigTab}
                        updateGeneralType={this.props.updateGeneralType}
                        updateShapeByVariable={this.props.updateShapeByVariable}
                        updateCodeConfigId={this.props.updateCodeConfigId}
                        updateSelectedShapeConfiguration={this.props.updateSelectedShapeConfiguration}
                        toggleColorPalette={this.props.toggleColorPalette}
                        toggleDisplayLegendCategory={this.props.toggleDisplayLegendCategory}
                    />);
            } else {
                return (
                    <D3BoundaryLayerConfig1
                        key={layer.id}
                        layer={layer}
                        toggleD3LayerConfigPanel={this.props.toggleD3LayerConfigPanel}
                        toggleD3LayerDisable={this.props.toggleD3LayerDisable}
                        updateConfigTab={this.props.updateConfigTab}
                        updateGeneralType={this.props.updateGeneralType}
                        updateShapeByVariable={this.props.updateShapeByVariable}
                        updateCodeConfigId={this.props.updateCodeConfigId}
                        updateCoordinateTolerance={this.props.updateCoordinateTolerance}
                        updateSelectedShapeConfiguration={this.props.updateSelectedShapeConfiguration}
                        toggleColorPalette={this.props.toggleColorPalette}
                        toggleDisplayLegendCategory={this.props.toggleDisplayLegendCategory}
                    />);
            }
        }
        else {
            return (
                <D3GradientLayerConfig1
                    key={layer.id}
                    layer={layer}
                    toggleD3LayerConfigPanel={this.props.toggleD3LayerConfigPanel}
                    toggleD3LayerDisable={this.props.toggleD3LayerDisable}
                    updateConfigTab={this.props.updateConfigTab}
                    updateGeneralType={this.props.updateGeneralType}
                    updateCoordinateTolerance={this.props.updateCoordinateTolerance}
                    toggleColorPalette={this.props.toggleColorPalette}
                    toggleDisplayLegendCategory={this.props.toggleDisplayLegendCategory}
                />);
        }
    }

    render() {
        const { selectedLayerArray, selectedGradLayerArray, selectedPrintControlLayerId, selectedPrintControlJson } = this.props;

        this.selectedLayerArray = selectedLayerArray;
        this.selectedGradLayerArray = selectedGradLayerArray;

        return (
          <div className='print-config-body'>
              <select id="layer-select" value={selectedPrintControlLayerId} onChange={(event) => this.setSelectedPrintControlLayerId(event.target.value)}>
                  <option value={'-1'}>-- Select an option below --</option>
                  {this.selectedLayerArray.map(layer =>
                      <option value={'normal-' + layer.id} key={'normal-' + layer.id}>{layer.layer_name == null? layer.name : layer.layer_name}</option>
                  )}
                  {this.selectedGradLayerArray.map(layer =>
                      <option value={'grad-' + layer.id} key={'grad-' + layer.id}>{layer.layer_name == null? layer.name : layer.layer_name}</option>
                  )}
              </select>
              <div id="layer-config-display">
                { (selectedPrintControlLayerId === '-1' || selectedPrintControlJson === null) ?
                    <p>Please make a selection using above field.</p>
                    :
                    this.returnJSX(selectedPrintControlJson, selectedPrintControlLayerId)
                }
              </div>
          </div>
        )
    }

}

export default D3LayerConfigList


