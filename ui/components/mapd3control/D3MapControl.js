import React from 'react';
import D3LayerConfigList1 from '../../containers/mapd3control/D3LayerConfigList1';
import Rx from 'rxjs';
import { SmallDoubleArrowIcon } from '../../../ui/constant/SvgIcons';
import {toggleVisibilityForLayerPrintControlPanel} from "../../actions/SidebarOptionsAction";

class InputValueSubject{
    constructor(dispatch, actionType){
        this.keyup = new Rx.Subject();
        let filtered = this.keyup
            .filter(function (text) {
                if (!text) return false;
                return text.trim(text).length>0; // Only if the text is longer than 2 characters
            })
            .debounceTime(1000 )
            .distinctUntilChanged(); // Only if the value has changed

        filtered.subscribe(
            function(data) {
              dispatch({type: actionType, value: data});
            }
        );
    }
    next(value){
        this.keyup.next(value);
    }
    onCompleted(){
        this.keyup.onCompleted();
    }
}

class D3MapControl extends React.Component {
    //new
    componentWillMount(){
        this.widthKeyup = new InputValueSubject(this.props.dispatch, "WIDTH_D3_CHANGE");
        this.heightKeyup = new InputValueSubject(this.props.dispatch, "HEIGHT_D3_CHANGE");
        this.zoomKeyup = new InputValueSubject(this.props.dispatch, "ZOOM_D3_CHANGE");
        this.projectionKeyup = new InputValueSubject(this.props.dispatch, "PROJECTION_D3_CHANGE");
        this.centerKeyup = new InputValueSubject(this.props.dispatch, "CENTER_D3_CHANGE");

    }

    componentWillUnmount() {
        this.widthKeyup.onCompleted();
        this.heightKeyup.onCompleted();
        this.zoomKeyup.onCompleted();
        this.projectionKeyup.onCompleted();
        this.centerKeyup.onCompleted();
    }

    toggleColorPalette(evt) {
        const { dispatch, colorPalette} = this.props;
        dispatch({type: "TOGGLE_PALETTE", value: !colorPalette.show, displayId: evt.target.dataset.id});
    }

    displaySelectedColor(fillColor, borderColor) {
        if(fillColor || borderColor) {
            return (
                <span className="displayColor">
                    <span className="fillColor" style={{background: fillColor, borderColor: borderColor,}}/>
                </span>
            )
        }
    }

    onChangeLayerVsbl(e, layerKey) {
        const {dispatch} = this.props;
        dispatch({type: "TOGGLE_LAYER_VISIBILITY", layerKey: layerKey});
    }

    isLayerVsbl(layerKey) {
        const {visibleLayerKeySet} = this.props;
        if(visibleLayerKeySet.has(layerKey)) {
            return true;
        } else {
            return false;
        }
    }

    getBorderWidth(layerKey) {
        const {borderWidthMap} = this.props;
        let borderWidth = borderWidthMap.get(layerKey);
        if (!borderWidth) {borderWidth = "0.1"}
        return borderWidth;
    }

    onChangeBorderWidth(e, layerKey) {
        const {dispatch} = this.props;
        dispatch({type: "CHANGE_BORDER_WEIGHT", borderWidth: e.target.value, layerKey: layerKey});
    }

    onChangeProjection(e) {
        this.props.dispatch({type: "PROJECTION_D3_CHANGE", value: parseInt(e.target.value)});
    }

    hideLayerPrintControl() {
        this.props.dispatch(toggleVisibilityForLayerPrintControlPanel());
    }

    render() {
        const { selectedMapTab, mapCanvasD3, dataInitialized, colorPalette, layerArray,
            selectedLayerIdSet, gradientLayerArray, selectedGradLayerIdSet, showSelectLayersPanel, showLayerPrintControlPanel } = this.props;

        if (!dataInitialized || selectedMapTab !== "Print") {
            return null;
        }
        this.selectedLayerArray = layerArray.filter(layer => {
            return selectedLayerIdSet.has(layer.id);
        });
        this.selectedGradLayerArray = gradientLayerArray.filter(gradLayer => {
            return selectedGradLayerIdSet.has(gradLayer.id);
        });

        return (
            <div id="mapControlD3" hidden={!showLayerPrintControlPanel}>
                <div className='print-config-header'>
                    <h3>Layer Print Control</h3>
                    <button onClick={() => this.hideLayerPrintControl()} className={'hide-d3-map-control-button'}><SmallDoubleArrowIcon/></button>
                </div>
                <D3LayerConfigList1> </D3LayerConfigList1>
            </div>
        );
    }

}

export default D3MapControl;