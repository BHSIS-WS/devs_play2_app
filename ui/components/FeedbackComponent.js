import React from 'react';
import CaptchaController from '../containers/CaptchaController';
import {toggleVisibilityForFeedbackPanel} from "../actions/SidebarOptionsAction";
import $ from "jquery";

class FeedbackComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            feedbackName: '',
            feedbackEmail: '',
            feedbackType: '',
            feedbackSubject: '',
            feedbackContent: '',
            formValid: false,
            formSubmitted: false,
            nameError: false,
            emailError: false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.validate = this.validate.bind(this);

    }

    validate() {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        var name = this.state.feedbackName,
            email = this.state.feedbackEmail,
            type = this.state.feedbackType,
            content = this.state.feedbackContent;
        var validEmail = pattern.test(email);
        var { gRecaptchaResponse } = this.props;
        if(name === '' || email === '' || content === '' || !validEmail || gRecaptchaResponse === null) {
            return false;
        } else {
            return true;
        }
    }

    handleNameChange(event) {
        if(this.state.feedbackName === '') {
            this.setState({nameError: true});
        } else {
            this.setState({nameError: false});
        }
        this.setState({feedbackName: event.target.value});
    }

    handleEmailChange(event) {
        var value = event.target.value;
        var regex = new RegExp(event.target.pattern);
        var validEmail = regex.test(value);
        this.setState({feedbackEmail: event.target.value});
        if(!validEmail) {
            this.setState({emailError: true});
        } else {
            this.setState({emailError: false});
        }
        this.setState({formValid: this.validate()});
    }

    handleTypeChange(event) {
        this.setState({feedbackType: event.target.value});
    }

    handleSubjectChange(event) {
        this.setState({feedbackSubject: event.target.value});
    }

    handleContentChange(event) {
        this.setState({feedbackContent: event.target.value});
    }

    handleCancel(event) {
        let { dispatch } = this.props;
        event.preventDefault();
        //this.handleReset(event);
        dispatch(toggleVisibilityForFeedbackPanel());
        $("#feedback-btn1").attr('class', "tabRightClose");
    }

    handleReset(event) {
        let { dispatch } = this.props;
        event.preventDefault();
        this.setState({
            feedbackName: '',
            feedbackEmail: '',
            feedbackType: '',
            feedbackSubject: '',
            feedbackContent: '',
            nameError: false,
            emailError: false,
            formValid: false
        });
    }

    handleSubmit(event) {
        var formDataObj = {},
            { successMessage, gRecaptchaResponse, dispatch } = this.props;

        event.preventDefault();

        var validForm = this.validate();
        if(!validForm) {
            dispatch({
                type: 'FEEDBACK_VALIDATION_ERROR',
                data: "Feedback form input error"
            });
        } else {
            formDataObj = this.state;
            dispatch({
                type: 'SUBMIT_FEEDBACK_FORM',
                data: {formDataObj: formDataObj, gRecaptchaResponse: gRecaptchaResponse}
            });
        }
        this.handleReset(event);
    }
    
    render() {
        const { successMessage, showFeedbackPanel } = this.props;
        let validForm = this.validate();
        return (
            <div className="feedbackBox" hidden={!showFeedbackPanel}>
                <header className="feedbackTitle">
                    <h3>Feedback</h3>
                    <button className="remove-box-button" onClick={this.handleCancel}/>
                </header>
                <div className="feedbackBody">
                    <div id="feedbackMainForm">
                        <form id="fbForm" onSubmit={this.handleSubmit}>
                            <label htmlFor="feedbackName">From: <span>*</span></label><br/>
                            <input type="text" id="feedbackName" placeholder="Your Name" className="inline-input"
                                   maxLength="45" tabIndex="100"
                                   value={this.state.feedbackName} onChange={this.handleNameChange.bind(this)} />
                            <label htmlFor="feedbackEmail" style={{'fontSize': 0}}>Email: </label>
                            <input type="text" id="feedbackEmail" placeholder="Your Email" className="inline-input"
                                   maxLength="95" tabIndex="101" pattern="[\w\-._%+!']+@[a-zA-Z0-9.-]+\.[a-zA-z0-9]{2,4}$"
                                   value={this.state.feedbackEmail} onChange={this.handleEmailChange.bind(this)} />
                            <div id="emailErrorMsg" className={this.state.emailError ? 'feedbackError' : 'hiddenMsg'}>Please enter a valid email address.</div><br/>
                            <label htmlFor="feedbackType">Feedback Type: <span>*</span></label><br/>
                            <select id="feedbackType" tabIndex="102" value={this.state.feedbackType} onChange={this.handleTypeChange.bind(this)}>
                                <option value="1">Ask a Question</option>
                                <option value="2">Make a Suggestion</option>
                                <option value="3">Report a Problem</option>
                            </select><br/>
                            <label htmlFor="feedbackSubject">Subject:</label><br/>
                            <input type="text" id="feedbackSubject" placeholder="Subject"
                                   maxLength="195" tabIndex="103"
                                   value={this.state.feedbackSubject} onChange={this.handleSubjectChange.bind(this)} /><br/>
                            <label htmlFor="feedbackContent">Content: <span>*</span></label><br/>
                            <textarea id="feedbackContent" placeholder="Your Content"
                                      maxLength="3950" tabIndex="104"
                                      value={this.state.feedbackContent} onChange={this.handleContentChange.bind(this)}/><br/>
                            <label >ReCaptcha: <span>*</span></label><br/>
                            <div id="captchaControl">
                                <CaptchaController/>
                                <div>
                                    {
                                        successMessage && successMessage !== null ?
                                            <div id="feedbackSuccessMsg">{successMessage}</div> :
                                            <div id="formErrorMsg" className={!this.validate() ? 'feedbackError' : 'hiddenMsg'}> </div>
                                    }
                                </div>
                            </div>
                            <footer className="toolbar-popup-button-container">
                                <button tabIndex="109" onClick={this.handleCancel}>Cancel</button>
                                <button type="reset" tabIndex="108" onClick={this.handleReset}>Reset</button>
                                <button disabled={!validForm} type="submit" id="submit" tabIndex="107">Send</button>
                            </footer>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default FeedbackComponent;