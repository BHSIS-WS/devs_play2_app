import React from "react";
import {removeAllPrintLayerInfoBox} from "../actions/PrintInfoBoxAction";

class FusionLayerSelectedCriteria extends React.Component {
	closeCriteria(evt, criteria) {
        this.props.dispatch(removeAllPrintLayerInfoBox());
		this.props.dispatch({
			type: "TOGGLE_CRITERIA_SELECT",
			layerId: this.props.expandSelectedLayerId,
			criteriaCodeId: criteria.criteria_code_id,
        	criteriaId: criteria.id
        });
	}

	render() {
		const { selectedMapTab, layerCriteriaArray, selectedCriteriaIdSet, expandSelectedLayerId, dispatch} = this.props;

		if (expandSelectedLayerId === null || selectedMapTab !== "Carto") return null;
		
		this.expandSelectedCriteriaArray = layerCriteriaArray.filter(criteria => {
			return criteria.fusion_map_layer_id===expandSelectedLayerId && selectedCriteriaIdSet.has(criteria.id);
		});
		//console.log(this.expandSelectedCriteriaArray);
		if (!this.expandSelectedCriteriaArray.length > 0) return null;
		
		return (
			<div className="selectedItemDetails">
				<span className="boldFont">Your Layer Filter Selections:</span>
				{this.expandSelectedCriteriaArray.map(criteria => 
			    	<div key={criteria.id} className="selected_items">
				    	<span className="closeButton" onClick={evt=> this.closeCriteria(evt,criteria)} />
				    	<span>{criteria.name}</span>
			    	</div>
		    	)}
			</div>
		);
	}
}

export default FusionLayerSelectedCriteria;