"use strict";

import React from 'react';

class ContextMenuListItemComponent extends React.Component {

    render() {
        let {id, message, cssClass, actionMethod} = this.props;
        return (
            <li className={cssClass} onClick={actionMethod}>
                <p id={"contextMenuItem" + id} >{message}</p>
            </li>
        );
    }
}

export default ContextMenuListItemComponent;