import React from "react";

class SavePdfComponent extends React.Component {

    constructor(props) {
        super(props);
        this.savePdf = this.savePdf.bind(this);
        this.updateWidth = this.updateWidth.bind(this);
        this.updateHeight = this.updateHeight.bind(this);
    }


    updateWidth(event) {
        let {height, width} = this.props;
        let newHeight = parseFloat(event.target.value);
        if(!isNaN(newHeight)) {
            document.getElementById("pdf-width-input").value = ((width / height) * newHeight).toFixed(2);
        }
    }

    updateHeight(event) {
        let {height,width} = this.props;
        let newWidth = parseFloat(event.target.value);
        if (!isNaN(newWidth)) {
            document.getElementById("pdf-height-input").value = ((height / width) * newWidth).toFixed(2);
        }
    }

    savePdf() {
        let {savePdf} = this.props;
        let width = document.getElementById("pdf-width-input");
        let height = document.getElementById("pdf-height-input");
        if (width.value !== "" && height.value !== "") {
            let widthValueFloat = parseFloat(width.value);
            let heightValueFloat = parseFloat(height.value);

            if (isNaN(widthValueFloat) || isNaN(heightValueFloat)) {
                alert("Invalid width or height.");
            } else {
                savePdf(widthValueFloat, heightValueFloat);
            }
        } else {
            alert("Width and height are required.");
        }
    }

    render() {
        let { showSavePdfDialog, hideSavePdfDialog } = this.props;
        return (
            <div id="save-pdf-dialog" className='toolbar-popup-div' hidden={!showSavePdfDialog}>
                <h4>Specify the PDF size</h4>
                <div className='toolbar-popup-content-div'>
                    <div className='toolbar-popup-input-container'>
                        <label htmlFor="pdf-width-input">Width in inches:</label>
                        <input onChange={(event) => this.updateHeight(event)} type="number" id="pdf-width-input"/>
                    </div>
                    <div className='toolbar-popup-input-container'>
                        <label htmlFor="pdf-height-input">Height in inches:</label>
                        <input onChange={(event) => this.updateWidth(event)} type="number" id="pdf-height-input"/>
                    </div>
                    <div className='toolbar-popup-button-container'>
                        <button onClick={hideSavePdfDialog} id="cancel-download-pdf-btn">Cancel</button>
                        <button onClick={this.savePdf} id="download-pdf-btn">Download</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default SavePdfComponent;