import React from "react";
import SavedLayerConfigController from '../containers/SavedLayerConfigController';
import {toggleVisibilityForSavedLayerConfigPanel} from "../actions/SidebarOptionsAction";

class SavedLayerConfigPanelComponent extends React.Component {

    constructor(props) {
        super(props);
        this.updateVisibleSavedLayerConfigList = this.updateVisibleSavedLayerConfigList.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.state = {
            visibleSavedLayerConfigList: [],
            sortByValue: 1,
            filterValue: ""
        };
    }

    componentWillReceiveProps(nextProps) {
        let { visibleSavedLayerConfigList, sortByValue, filterValue } = this.state;
        if (nextProps.configList !== visibleSavedLayerConfigList) {
            this.updateVisibleSavedLayerConfigList(sortByValue, filterValue, nextProps.configList);
        }
    }

    updateVisibleSavedLayerConfigList(sortByValue, filterValue, visibleSavedLayerConfigList) {
        let newVisibleSavedLayerConfigList = visibleSavedLayerConfigList.filter(layer => {
            return layer.configName.includes(filterValue);
        });

        newVisibleSavedLayerConfigList.sort(function(a, b) {
            if (sortByValue === 1) {
                return (a.lastModified < b.lastModified) ? 1 : (a.lastModified === b.lastModified) ? 0 : -1;
            } else if (sortByValue === 2) {
                let nameA = a.configName.toLowerCase(), nameB = b.configName.toLowerCase();
                return (nameA > nameB) ? 1 : (nameA === nameB) ? 0 : -1;
            }
        });

        this.setState({
            visibleSavedLayerConfigList: newVisibleSavedLayerConfigList,
            sortByValue: sortByValue,
            filterValue: filterValue
        });
    }

    handleCancel(event) {
        event.preventDefault();
        this.props.dispatch(toggleVisibilityForSavedLayerConfigPanel());
    }

    render() {
        let { configList, showSavedLayerConfigPanel, selectedMapTab, showSidebarButtons } = this.props;
        let { visibleSavedLayerConfigList, filterValue, sortByValue } = this.state;
        return (
            <div id="saved-layer-config-panel" className={showSidebarButtons ? "savedLayerConfigPanelTabsOpen" : "savedLayerConfigPanelTabsClosed"} hidden={!showSavedLayerConfigPanel}>
                <div id="historyheader">
                    <h3>Saved Layer Configurations</h3>
                    <button id="history-dialog-close" className="close-dialog-button" onClick={this.handleCancel}></button>
                </div>
                <div id="historyContent">
                    <div id='search-saved-layer-config-div' className='saved-layer-config-filter-div'>
                        <label>Find/Search</label>
                        <input type='text' placeholder='Enter your search' onChange={(event) => this.updateVisibleSavedLayerConfigList(sortByValue, event.target.value, configList)}/>
                        <button>Search</button>
                    </div>
                    <div id='sort-saved-layer-config-div' className='saved-layer-config-filter-div'>
                        <label>Sort By</label>
                        <select onChange={(event) => this.updateVisibleSavedLayerConfigList(parseInt(event.target.value), filterValue, configList)}>
                            <option value={1}>Date</option>
                            <option value={2}>Name</option>
                        </select>
                    </div>
                    <ul id="saved-layer-config-list">
                        {visibleSavedLayerConfigList.map(config =>
                            <SavedLayerConfigController key={config.userSavedLayerConfigId} config={config} />
                        )}
                    </ul>
                </div>
            </div>
        );
    }
}

export default SavedLayerConfigPanelComponent;