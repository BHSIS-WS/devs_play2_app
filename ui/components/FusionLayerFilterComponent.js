import React from "react";
import FusionLayerFilterCriteriaController from '../containers/FusionLayerFilterCriteriaController';
import DynamicFilterCriteriaController from '../containers/DynamicFilterCriteriaController';
import {changeCriteriaCode, initializeDynamicFilter} from "../actions/NormalLayersAction";

class FusionLayerFilterComponent extends React.Component {

    constructor(props) {
        super(props);
        this.onSelectOptionChanged = this.onSelectOptionChanged.bind(this);
        this.lookupCriteriaCodeNameById =this.lookupCriteriaCodeNameById.bind(this);
    }

    lookupCriteriaCodeNameById (id){
        let { criteriaCodeLookupMap } = this.props;
        return criteriaCodeLookupMap.get(id).criteria_name;
    }

    onSelectOptionChanged(event) {
        let {dispatch, layer, criteriaCodeLookupMap} = this.props;
        let criteriaCodeIdInt = parseInt(event.target.value);
        let criteriaCode = criteriaCodeLookupMap.get(criteriaCodeIdInt);
        if (criteriaCode && criteriaCode.criteria_type.toLowerCase() === "dynamic") {
            dispatch(initializeDynamicFilter(criteriaCodeIdInt, layer.id));
        }
        dispatch(changeCriteriaCode(criteriaCodeIdInt, layer.id));
    }

    render(){
        let { criteriaArray, selectedCriteriaCodeArray, criteriaCodeLookupMap, layer } = this.props;
        if (criteriaArray) {
            return (
                <div className="fusion-layer-filter-div">
                    <div className='fusion-layer-filter-header-div'>
                        <label>Add Filter(s): </label>
                            <select onChange={(event) => this.onSelectOptionChanged(event)} value={-1}>
                                <option value={-1}>-- Select a filter criteria --</option>
                                {criteriaArray.length === 1 ?
                                    <option key={criteriaArray[0]} value={criteriaArray[0]}>{this.lookupCriteriaCodeNameById(criteriaArray[0])}</option>
                                    :
                                    criteriaArray.map(criteriaCode => (
                                        <option key={criteriaCode} value={criteriaCode}>
                                            {this.lookupCriteriaCodeNameById(criteriaCode)}
                                        </option>
                                        )
                                    )
                                }
                            </select>
                    </div>
                    {selectedCriteriaCodeArray.map((criteriaCodeId) => {
                        let criteria = criteriaCodeLookupMap.get(criteriaCodeId);
                        switch(criteria.criteria_type.toLowerCase()) {
                            case "dynamic":
                                return (<DynamicFilterCriteriaController key={criteriaCodeId} layer={layer} criteria={criteria} />);
                            default:
                                return (<FusionLayerFilterCriteriaController key={criteriaCodeId} layer={layer} criteria={criteria} />);
                        }
                    })}
                </div>
            );
        } else {
            return null;
        }
    }
}

export default FusionLayerFilterComponent;