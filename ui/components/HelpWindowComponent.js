import React from "react";
import HelpTopicController from '../containers/HelpTopicController';
import {SearchIcon} from "../constant/SvgIcons";
import { toggleVisibilityForHelpPanel} from "../actions/SidebarOptionsAction";
import $ from "jquery";

class HelpWindow extends React.Component {
    constructor(props) {
            super(props);
            this.handleCancel = this.handleCancel.bind(this);

    }
    handleCancel(event) {
        let { dispatch } = this.props;
        event.preventDefault();
        //this.handleReset(event);
        dispatch(toggleVisibilityForHelpPanel());
        $("#help-btn").attr('class', "tabRightClose");
    }
    render() {
        const {showHelpPanel, helpList} = this.props;

        if (showHelpPanel) {
            return (
                <section id="help-container">
                    <div id="help-panel">
                        <div id="help-header">
                            <h3>Help</h3>
                            <div>
                                <button id="helpclose"
                                        onClick={this.handleCancel}/>
                            </div>
                        </div>
                        <h4 id="help-title">How can we help?</h4>
                        <div id="searchbar">
                            <input type="text" size="42" placeholder="Enter your search" autoComplete="off" id="help-search"/>
                            <button id="search-icon"><SearchIcon/></button>
                        </div>
                        <h5 id="help-text"> Help Topics </h5>
                        <ul>
                            {helpList.map(item => (
                                <HelpTopicController className="helpTopicItem" key={item.devsHelpTopicId} helpTopic={item}/>
                            ))}
                        </ul>
                    </div>
                </section>
            );

        } else {
            return null;
        }
    }

}

export default HelpWindow;
