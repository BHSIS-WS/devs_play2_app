"use strict";

import React, { Component } from 'react'
import IdleTimer from 'react-idle-timer'

class TimerModelComponent extends React.Component {
    constructor(props) {
        super(props);
        this.idleTimer = null;
        this.onIdle = this.onIdle.bind(this);
        this.onLogout = this.onLogout.bind(this);
        this.onContinue = this.onContinue.bind(this);
        this.onAction = this.onAction.bind(this);
    }

    componentDidMount() {
        localStorage.setItem("lastActive", 0);
        localStorage.setItem("remaining", 0);
        localStorage.setItem("elapsed", 0);
        localStorage.setItem("isIdle", false);
        localStorage.setItem("loggedOut", false);
    }

    render() {
        let {idleTimer, warnTimeout, finalTimeout, isIdle} = this.props;
        var greyScreenStyle = {
            display: 'block',
            fadeIn: 3000,
            width: window.innerWidth,
            height: window.innerHeight
        }
        if(isDevEnv === false) {
            return (
                <div>
                    <IdleTimer
                        ref={ref => { this.idleTimer = ref; }}
                        element={document}
                        onIdle={this.onIdle}
                        timeout={isIdle ? finalTimeout : warnTimeout}
                        startOnMount={true}
                        onAction={this.onAction}
                        debounce={1000} />

                    <div id="idle-holder">
                        {isIdle ?
                            <div>
                                <div id="greyScreen" style={greyScreenStyle}></div>
                                <div id="timeout-module">
                                    <div id="timeout-header">
                                        <h1>DEVS Session Timeout</h1>
                                    </div>
                                <div id="timeout-body">
                                    <p> Your DEVS session has timed out. If you would like to continue using DEVS, click the Continue
                                    button to close this message. <br/> If you do nothing, your session will end and any unsaved
                                    work will be lost. </p>
                                    </div>
                                <div id="timeout-footer">
                                    <button className="timeout-btn" id="timeout-logout-btn" onClick={this.onLogout}>Log out</button>
                                    <button className="timeout-btn" id="timeout-continue-btn" onClick={() => this.onContinue(this.idleTimer)}>Continue</button>
                                </div>
                                </div>
                            </div>
                        : null }
                    </div>
                </div>
            );
        } else {
            // Return null if in Development environment (set in application.conf)
            return null;
        }
    }


    onLogout() {
        var { processIdleLogout } = this.props;
        localStorage.clear();
        localStorage.setItem("loggedOut", true);
        processIdleLogout();
        window.location = "/devs2/logout";
    }

    onContinue(timer) {
        var { idleTimer, processContinueSession } = this.props;
        localStorage.setItem("isIdle", false);
        processContinueSession();
        timer.reset();
    }

    onIdle() {
        var { isIdle, warnTimeout, processIdleOn } = this.props;
        var active = Date.now() - localStorage.getItem("lastActive") < warnTimeout;
        var sessionIdle = (localStorage.getItem("isIdle") === "true"),
            sessionLoggedOut = (localStorage.getItem("loggedOut") === "true");
        if(isIdle || sessionLoggedOut) {
            if(sessionIdle && !active) {
                this.onLogout();
            } else if(!isIdle) {
                processIdleOn();
                this.idleTimer.reset();
            }
        } else {
            if(active) {
                this.onContinue(this.idleTimer);
            } else {
                localStorage.setItem("isIdle", true);
                processIdleOn();
                this.idleTimer.reset();
            }
        }
    }

    onAction(e) {
        var { processUserAction } = this.props;

        localStorage.setItem("lastActive", this.idleTimer.getLastActiveTime());
        localStorage.setItem("remaining", this.idleTimer.getRemainingTime());
        localStorage.setItem("elapsed", this.idleTimer.getElapsedTime());

        // this will update state with these values
        processUserAction(this.idleTimer.getLastActiveTime(), this.idleTimer.getRemainingTime(), this.idleTimer.getElapsedTime());

        // if another tab/window has set "loggedOut" to true since it timed out
        if(localStorage.getItem("loggedOut") === "true") {
            this.onLogout();
        }
    }

}

export default TimerModelComponent;