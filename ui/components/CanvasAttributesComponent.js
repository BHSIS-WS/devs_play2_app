import React from "react";
import {removeAllPrintLayerInfoBox} from "../actions/PrintInfoBoxAction";

class CanvasAttributesComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            width: this.props.mapCanvasD3.width,
            height: this.props.mapCanvasD3.height,
            zoom: this.props.mapCanvasD3.zoom,
            projection: this.props.mapCanvasD3.projection,
            center_lat: this.props.mapCanvasD3.center_lat,
            center_lng: this.props.mapCanvasD3.center_lng
        };

        this.onChangeProjection = this.onChangeProjection.bind(this);
        this.applyAttributes = this.applyAttributes.bind(this);
        this.applyAttributesAndClose = this.applyAttributesAndClose.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let savedMapCanvasAttributes = this.state;
        if(nextProps.mapCanvasD3 !== savedMapCanvasAttributes) {
            this.updateMapCanvasAttributes(nextProps.mapCanvasD3);
        }
    }

    updateMapCanvasAttributes(newCanvasAttributes) {
        let { width, height, zoom, projection } = newCanvasAttributes;
        this.setState({
            width: width,
            height: height,
            zoom: zoom,
            projection: projection
        });
    }

    onChangeProjection(evt) {
        this.setState({projection: parseInt(evt.target.value)});
    }

    handleWidthChange(evt) {
        this.setState({width: evt.target.value});
    }

    handleHeightChange(evt) {
        this.setState({height: evt.target.value});
    }

    handleZoomChange(evt) {
        this.setState({zoom: evt.target.value});
    }

    // This function only applies the updated attributes, it does not close the dialog box.
    applyAttributes() {
        let { mapCanvasD3, dispatch } = this.props;
        let width = parseFloat(this.state.width),
            height = parseFloat(this.state.height),
            zoom = parseFloat(this.state.zoom),
            projection = parseInt(this.state.projection);

        if (projection !== mapCanvasD3.projection) {
            dispatch(removeAllPrintLayerInfoBox());
        }

        dispatch({type: "WIDTH_D3_CHANGE", value: width });
        dispatch({type: "HEIGHT_D3_CHANGE", value: height});
        dispatch({type: "ZOOM_D3_CHANGE", value: zoom});
        dispatch({type: "PROJECTION_D3_CHANGE", value: projection});
    }

    // This function is called when the 'OK' button is clicked. Closes the dialog box after applying the attributes.
    applyAttributesAndClose() {
        this.applyAttributes();
        this.props.hideCanvasAttributesDialog();
    }

    render() {
        const { selectedMapTab, mapCanvasD3, dataInitialized, layerArray,
            selectedLayerIdSet, gradientLayerArray, selectedGradLayerIdSet,
            showCanvasAttributesDialog, hideCanvasAttributesDialog } = this.props;

        if (!dataInitialized || selectedMapTab !== "Print") {
            return null;
        }

        return (
            <div id="canvas-attributes-dialog" className="toolbar-popup-div" hidden={!showCanvasAttributesDialog}>
                <h4>Print Configurations:</h4>
                <div className="toolbar-popup-content-div">
                    <div className='toolbar-popup-input-container'>
                        <div>
                        <label htmlFor="width" id="canvas-attr-setting">Width (px)</label>
                        <input id="width" className="canvas-attr-input" type="text" value={this.state.width} onChange={this.handleWidthChange.bind(this)}/>
                        </div>
                        <div>
                        <label htmlFor="height" id="canvas-attr-setting">Height (px)</label>
                        <input id="height" className="canvas-attr-input" type="text" value={this.state.height} onChange={this.handleHeightChange.bind(this)}/>
                        </div>
                        <div>
                        <label htmlFor="zoom" id="canvas-attr-setting">Zoom</label>
                        <input id="zoom" ref="zoomInput" className="canvas-attr-input" type="text" value={this.state.zoom} onChange={this.handleZoomChange.bind(this)}/>
                        </div>
                    </div>
                    <legend><strong>Map Projection:</strong></legend>
                    <div className='toolbar-popup-input-container'>
                            <input type="radio" name="projection" value="1" id="albersUsaTerritoriesRadio" onChange={(e)=>this.onChangeProjection(e)} checked={this.state.projection === 1}/>
                            <label htmlFor="albersUsaTerritoriesRadio" id="canvas-attr-map-projection" className="radioLabel">
                            AlbersUsaTerritories
                            </label>
                            <input type="radio" name="projection" value="2" id="albersUsaRadio" onChange={(e)=>this.onChangeProjection(e)} checked={this.state.projection === 2}/>
                            <label htmlFor="albersUsaRadio" id="canvas-attr-map-projection" className="radioLabel">
                             AlbersUsa
                            </label>
                            <input type="radio" name="projection" value="3" id="mercatorRadio" onChange={(e)=>this.onChangeProjection(e)} checked={this.state.projection === 3}/>
                            <label htmlFor="mercatorRadio" id="canvas-attr-map-projection" className="radioLabel">
                             Mercator
                            </label>
                    </div>
                    <div className="toolbar-popup-button-container">
                        <button onClick={hideCanvasAttributesDialog}>Cancel</button>
                        <button onClick={this.applyAttributes}>Apply</button>
                        <button onClick={this.applyAttributesAndClose}>Ok</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default CanvasAttributesComponent;