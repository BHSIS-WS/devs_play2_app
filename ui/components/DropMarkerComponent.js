"use strict";

import React from 'react';

class DropMarkerComponent extends React.Component {

    render() {
        let {dropMarkerDialogVisible, closeDialog, dropMarker, placeholderText, labelText, inputType, buttonText} = this.props;
        let inputValue = "";
        return (
            <div hidden={!dropMarkerDialogVisible} id="drop-marker-dialog" className="dialog-over-map">
                <label htmlFor='drop-marker-input'>{labelText}</label>
                <input placeholder={placeholderText} onChange={event => inputValue = event.target.value}
                       id="drop-marker-input" type="text"/>
                <div>
                    <button onClick={() => {
                        dropMarker(inputValue, inputType);
                        document.getElementById("drop-marker-input").value = ""
                    }}>{buttonText}
                    </button>
                    <button onClick={closeDialog}>Close</button>
                </div>
            </div>
        );
    }
}

export default DropMarkerComponent;