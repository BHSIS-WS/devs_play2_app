import React from "react";
import GradientLayer1 from '../containers/GradientLayer1';

class GradientLayerList extends React.Component {

  getGradientLayers(gradientLayerArray, dataSetId) {
    return gradientLayerArray.filter(layer => layer.data_set_id == dataSetId);
  }

//  renderGradientLayers(gradientLayerArray, dataSet) {
//  }

  getExpandCollapseClassName(startingName, expand){
    return expand ? startingName+" open" : startingName+" close";
  }

  toggleGrandientCategoryExpand(e){
    this.props.dispatch({type:"TOGGLE_GRADIENT_CATEGORY"});
  }

  toggleGrandientDataSetExpand(e, dataSetId) {
    this.props.dispatch({type:"TOGGLE_GRADIENT_DATASET", dataSetId: dataSetId});
  }

  render() {
    const {gradientDataSetArray, gradientLayerArray, expanded, expandedDataSetIdSet} = this.props;
    //let expand = true;
    let name = "Gradient Layers";
    return (
      <div className="RtNav1">
          <div className="RtNavTitle">
            <a href="#" className={this.getExpandCollapseClassName('',expanded)}
                onClick={e=>this.toggleGrandientCategoryExpand(e)}>{name}
            </a>
          </div>
          <div className="GradientLayerMenu">
            <ul className="RtNavGdrLr">
              {expanded ? gradientDataSetArray.map(dataSet => (
                  <li key={dataSet.id}>
                    <a href="javascript:void(0)" className="RtNavGdrLrHeader RtNavGdrLrDpdnNormal"
                        onClick={e=>this.toggleGrandientDataSetExpand(e,dataSet.id)}>{dataSet.name}</a>
                    {expandedDataSetIdSet.has(dataSet.id)?
                        <ul className="RtNavGdrLrGroup">
                          {this.getGradientLayers(gradientLayerArray, dataSet.id).map(layer => (
                            <GradientLayer1 key={layer.id} gradLayerId={layer.id}>
                            </GradientLayer1>
                          ))}
                        </ul>
                    :""}
                  </li>
              )) : ''}
            </ul>
          </div>
      </div>
    );
  }
}

export default GradientLayerList;
