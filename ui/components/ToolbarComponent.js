import React from "react";
import printContentModel from "../others/PrintContentModel";
import {toggleD3MapRectTool} from "../actions/D3MapToolOptionsAction";
import { TextIcon, RectIcon, JpgIcon, PdfIcon, PlusIcon, MinusIcon, RefreshIcon,
         SaveHistoryIcon, MapSettingIcon, StatusIcon, LegendIcon, MinimapIcon, MoreOption } from "../constant/SvgIcons";

class ToolbarComponent extends React.Component {

    constructor(props) {
        super(props);
        this.setZoomLevel = this.setZoomLevel.bind(this);

        this.state = {
            zoom: this.props.zoom,
        };
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.zoom !== this.state.zoom) {
            let zoomSelect = document.getElementById("zoomSelect");
            let newZoom = parseFloat(nextProps.zoom * 100).toFixed(0);
            zoomSelect.options[0].innerHTML = newZoom + "%";
            zoomSelect.options[0].value = newZoom;
            zoomSelect.value = newZoom;
        }
    }

    setZoomLevel(event) {
        let { dispatch } = this.props;
        let zoom = parseFloat(event.target.value) / 100;
        dispatch({type: "ZOOM_D3_CHANGE", value: zoom});
    }

    textToolClicked() {
        printContentModel.addMapTextD3();
    }

    rectToolClicked() {
        printContentModel.showRectDrawingCanvasD3();
    }

    render() {
        let { toolbarOptions, showTextToolDialog, drawingCanvasOn, toggleVisibilityForCanvasAttributesDialog,
            toggleVisibilityForPrintMapLegend, toggleVisibilityForRenderStatusPanel, toggleVisibilityForSaveLayerConfigDialog, toggleVisibilityForMinimapPanel,
            performD3MapZoomIn, performD3MapZoomOut, downloadToImage, downloadToPdf, refreshMapLayers, toggleMouseZoomForPrintMap, zoom} = this.props;

        return (
            <ul className="toolbarList">
                <li>
                    <a role="button" className={showTextToolDialog ? "activeToolbarIcon" : "notActiveToolbarIcon"} href="javascript:void(0);" onClick={(e) => this.textToolClicked(e)} title="Text tool" id="pull1">
                       <TextIcon/>
                    </a>
                </li>
                <li className={drawingCanvasOn ? "active" : ""}>
                 <a role="button" className={drawingCanvasOn ? "activeToolbarIcon" : "notActiveToolbarIcon"} href="javascript:void(0);" onClick={(e) => this.rectToolClicked(e)} title="Rectangle draw" id="pull2">
                   <RectIcon/>
                 </a>
                </li>
                <li className="divline"/>
                <li>
                    <a role="button" className="notActiveToolbarIcon" href="javascript:void(0);" onClick={() => downloadToImage()} title="Map download JPEG" id="pull3">
                       <JpgIcon/>
                    </a>
                </li>
                <li className={toolbarOptions.showSavePdfDialog ? "active" : ""}>
                    <a role="button" className={toolbarOptions.showSavePdfDialog ? "activeToolbarIcon" : "notActiveToolbarIcon"} href="javascript:void(0);" onClick={() => downloadToPdf()} title="Map download PDF" id="pull4">
                       <PdfIcon/>
                    </a>
                </li>
                <li className="divline"/>
                <li>
                    <a role="button" className="notActiveToolbarIcon" href="javascript:void(0);" onClick={() => performD3MapZoomIn()} title="Zoom in" id="zoomin">
                       <PlusIcon/>
                    </a>
                </li>
                <li>
                    <a role="button" className="notActiveToolbarIcon" href="javascript:void(0);" onClick={() => performD3MapZoomOut()} title="Zoom out" id="zoomout">
                        <MinusIcon/>
                    </a>
                </li>
                <li>
                    <label htmlFor="zoomSelect" className="visibility-hidden">Select Zoom</label>
                    <select id="zoomSelect" defaultValue={zoom * 100} onChange={this.setZoomLevel}>
                        <option value={500}>500%</option>
                        <option value={400}>400%</option>
                        <option value={300}>300%</option>
                        <option value={200}>200%</option>
                        <option value={170}>170%</option>
                        <option value={150}>150%</option>
                        <option value={120}>120%</option>
                        <option value={100}>100%</option>
                        <option value={90}>90%</option>
                        <option value={80}>80%</option>
                        <option value={70}>70%</option>
                        <option value={50}>50%</option>
                    </select>
                </li>
                <li className="divline"/>
                <li>
                    <a role="button" className="notActiveToolbarIcon" href="javascript:void(0);" onClick={(evt) => refreshMapLayers(evt)} title="Refresh Map" id="pull7">
                        <RefreshIcon/>
                    </a>
                </li>
                <li className="divline"/>
                <li className={toolbarOptions.showSaveLayerConfigDialog ? "active" : ""}>
                    <a role="button" className={toolbarOptions.showSaveLayerConfigDialog ? "activeToolbarIcon" : "notActiveToolbarIcon"} href="javascript:void(0);" onClick={() => toggleVisibilityForSaveLayerConfigDialog()} title="Take map snapshot" id="pull8">
                       <SaveHistoryIcon/>
                    </a>
                </li>
                <li className="divline"/>
                <li className={toolbarOptions.showCanvasAttributesDialog ? "active" : ""}>
                    <a role="button" className={toolbarOptions.showCanvasAttributesDialog ? "activeToolbarIcon" : "notActiveToolbarIcon"} href="javascript:void(0);" onClick={() => toggleVisibilityForCanvasAttributesDialog()} title="Map Controls Settings" id="pull9">
                       <MapSettingIcon/>
                    </a>
                </li>
                <li className="divline"/>
                <li className={toolbarOptions.showRenderStatusPanel ? "active" : ""}>
                    <a role="button" className={toolbarOptions.showRenderStatusPanel ? "activeToolbarIcon" : "notActiveToolbarIcon"} href="javascript:void(0);" onClick={() => toggleVisibilityForRenderStatusPanel()} title="Map progress status bar" id="pull10">
                       <StatusIcon/>
                    </a>
                </li>
                <li className="divline"/>
                <li className={toolbarOptions.showPrintMapLegend ? "active" : ""}>
                    <a role="button" className={toolbarOptions.showPrintMapLegend ? "activeToolbarIcon" : "notActiveToolbarIcon"} href="javascript:void(0);" onClick={() => toggleVisibilityForPrintMapLegend()} title="Show Legend" id="pull11">
                       <LegendIcon/>
                    </a>
                </li>
                <li className="divline"/>
                <li className={toolbarOptions.showMinimapPanel ? "active" : ""}>
                    <a role="button" className={toolbarOptions.showMinimapPanel ? "activeToolbarIcon" : "notActiveToolbarIcon"} href="javascript:void(0);" onClick={() => toggleVisibilityForMinimapPanel()} title="Show Minimap" id="pull12">
                       <MinimapIcon/>
                    </a>
                </li>
                <li className="divline" />
                <li className={toolbarOptions.disableMouseZoom ? "active" : ""}>
                    <div className={toolbarOptions.disableMouseZoom ? "activeToolbarIcon toolbarInputGroup" : "notActiveToolbarIcon toolbarInputGroup"}>
                        <input type="checkbox" id="disableMouseZoom" checked={toolbarOptions.disableMouseZoom} onChange={() => toggleMouseZoomForPrintMap(!toolbarOptions.disableMouseZoom)}/>
                        <label htmlFor="disableMouseZoom">Disable mouse zoom</label>
                    </div>
                </li>
                {/*<li className="divline">
                    <div className={toolbarOptions.showSelectedLayer ? "active" : ""}>
                        <a role="button" className={toolbarOptions.showSelectedLayer ? "activeToolbarIcon" : "notActiveToolbarIcon"} href="javascript:void(0);" onClick={() => toggleVisibilityForLayerStatusPanel()} title="Show Select Layer" id="pull11">
                            <MoreOption/>
                        </a>
                    </div>
                </li>*/}
                <li className="divline" />
            </ul>
        );
    }
}

export default ToolbarComponent;

