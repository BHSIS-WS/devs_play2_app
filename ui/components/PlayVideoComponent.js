import React, { Component } from "react";

class Modal extends Component {
  handleRightClick(e) {
                e.preventDefault();
                return false;
            }
  render() {
    return this.props.open ? (
      <div>
        <div className="modal-background" />
        <div role="dialog" className="modal-dialog">
                  <header>
            <span>{this.props.header}</span>
            <button
              onClick={() => this.props.onClose()}
              type="button"
              aria-label="close"
            >
              CLOSE
            </button>
          </header>
          <div className="modal-content">
                <br/>
                <video controls className="videoplaydiv" controlsList="nodownload" onContextMenu={(e) => this.handleRightClick(e)} preload="none">
                    <source src={this.props.src} type="video/mp4" />
                </video>
          </div>
        </div>
      </div>
    ) : null;
  }
}
export default Modal;