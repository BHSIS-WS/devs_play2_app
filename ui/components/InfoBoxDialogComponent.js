import React from "react";
import ReactDOM from "react-dom";

class InfoBoxDialogComponent extends React.Component {

    constructor(props) {
        super(props);
        this.hideInfoBoxDialog = this.hideInfoBoxDialog.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
    }

    hideInfoBoxDialog() {
        this.props.toggleInfoBoxMaxLimitDialog(false);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleOutsideClick);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleOutsideClick);
    }

    handleOutsideClick(evt) {
        let { maxLimitDialogBoxOpen } = this.props;
        const dialogBoxNode = ReactDOM.findDOMNode(this.refs.infoBoxDialog);

        if (dialogBoxNode && !dialogBoxNode.contains(evt.target) && maxLimitDialogBoxOpen) {
            this.hideInfoBoxDialog();
        }
    }

    render() {
        const { maxLimitDialogBoxOpen, maxInfoBox } = this.props;

        return (
            <div ref='infoBoxDialog' id="info-box-dialog" className="toolbar-popup-div" hidden={!maxLimitDialogBoxOpen}>
                <h4>Info Box Maximum Limit<span className="pullRight" onClick={this.hideInfoBoxDialog}>X</span></h4>
                <div className='toolbar-popup-content-div'>
                    <p>You have reached your maximum limit of total {maxInfoBox} info box allowed.</p>
                </div>
                <div className='toolbar-popup-button-container'>
                    <button onClick={this.hideInfoBoxDialog}>OK</button>
                </div>
            </div>
        );
    }
}

export default InfoBoxDialogComponent;