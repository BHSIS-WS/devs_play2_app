"use strict";

import React from 'react';
import ContextMenuListItemController from '../containers/ContextMenuListItemController';

class ContextMenuComponent extends React.Component {

    render() {
        let {rightClickContextMenuPosition, listItems} = this.props;

        return (
            <ul id="contextMenu" style={rightClickContextMenuPosition === null ? {display: 'none'} : {} }>
                {listItems.map(listItem =>
                    <ContextMenuListItemController
                        key={listItem.id}
                        id={listItem.id}
                        message={listItem.message}
                        cssClass={listItem.cssClass}
                        actionMethod={listItem.actionMethod}
                    />
                )}
            </ul>
        );
    }
}

export default ContextMenuComponent;