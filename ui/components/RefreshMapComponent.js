import React, { Component } from "react";
import MapContentModel from '../others/MapContentModel';

class RefreshMapComponent extends Component {

    constructor(props) {
        super(props);
        this.refreshMap = this.refreshMap.bind(this);
    }

    refreshMap() {
        MapContentModel.refreshGoogleMapLayers();
    }

    render() {
        return this.props.selectedMapTab === "Print" ?
            null :
            (
                <button title="Refresh Data" onClick={this.refreshMap}>Refresh Data</button>
            );
    }
}
export default RefreshMapComponent;