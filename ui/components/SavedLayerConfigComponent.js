import React from "react";
import {TimeIcon} from "../constant/SvgIcons";


class SavedLayerConfigComponent extends React.Component {

    constructor(props) {
        super(props);
        this.getDate = this.getDate.bind(this);
        this.toggleExpanded = this.toggleExpanded.bind(this);
        this.loadSavedLayerConfig = this.loadSavedLayerConfig.bind(this);
        this.getSelectedLayers = this.getSelectedLayers.bind(this);
        this.getProjection = this.getProjection.bind(this);
        this.showRemoveConfigDialog = this.showRemoveConfigDialog.bind(this);
        this.getExpandCollapseClassName = this.getExpandCollapseClassName.bind(this);
        this.state = {
            expanded: true
        };
    }

    getSelectedLayers() {
        let { config, staticState } = this.props;
        let selectedLayers = [];

        config.normalLayers.selectedLayerIdSet.forEach(layerId => {
            staticState.layerArray.forEach(refLayer => {
                if (refLayer.id === layerId) {
                    selectedLayers.push({
                        layerName: refLayer.layer_name,
                        layerId : "normal_" + refLayer.id
                    });
                }
            })
        });

        config.gradientLayers.selectedGradLayerIdSet.forEach(layerId => {
            staticState.gradientLayerArray.forEach(refLayer => {
                if (refLayer.id === layerId) {
                    let fusionLegendJson = JSON.parse(refLayer.fusion_legend_json_string);
                    selectedLayers.push({
                        layerName: fusionLegendJson.title,
                        layerId: "gradient_" + refLayer.id
                    });
                }
            })
        });
        return selectedLayers;
    }

    getProjection() {
        let { config } = this.props;
        let projection = config.mapCanvasD3.projection;
        if (projection === 1) {
            return "AlbersUsaTerritories";
        } else if (projection === 2) {
            return "AlbersUsa";
        } else {
            return "Mercator";
        }
    }

    getDate(lastModified) {
        return lastModified.toLocaleDateString() + " " + lastModified.toLocaleTimeString();
    }

    toggleExpanded() {
        this.setState({
            expanded: !this.state.expanded
        });
    }

    getExpandCollapseClassName(startingName, expand){
        return expand ? startingName+" open" : startingName+" close";
    }

    loadSavedLayerConfig() {
        let {loadSavedLayerConfig, config} = this.props;
        loadSavedLayerConfig(config);
    }

    showRemoveConfigDialog() {
        let { config, showRemoveConfigDialog } = this.props;
        showRemoveConfigDialog(config);
    }

    render() {
        let {config} = this.props;
        let selectedLayers = this.getSelectedLayers();
        return (
            <li className="saved-layer-config-list-item">
                <div className='saved-layer-config-header-div' onClick={this.toggleExpanded}>
                    <h4 className={this.getExpandCollapseClassName('',this.state.expanded)}>{config.configName}</h4>
                    <label>{this.getDate(config.lastModified)}</label>
                    <div id="calendaricon"><TimeIcon/></div>
                </div>
                <div className='saved-layer-config-content-div' hidden={!this.state.expanded}>
                    <div className="config-content-row">
                        <div className="config-content-column1"><label><strong>Description</strong></label></div>
                        <div className="config-content-column2"><p>{config.configDescription}</p></div>
                    </div>
                    <hr/>
                    <div className="config-content-row">
                        <div className="config-content-column1">
                            <label><strong>Selected Layers</strong></label>
                        </div>
                        <div className="config-content-column2">
                            {selectedLayers.map(layer =>
                                <p key={layer.layerId}>{layer.layerName}</p>
                            )}
                        </div>
                    </div>
                    <hr/>
                    <div className="config-content-row">
                        <div className="config-content-column1"><label><strong>Canvas Attributes</strong></label></div>
                        <div className="config-content-column2">
                            <p>Projection: {this.getProjection()}</p>
                            <p>Height: {config.mapCanvasD3.height} px</p>
                            <p>Width: {config.mapCanvasD3.width} px</p>
                        </div>
                    </div>
                    <hr/>
                    <div className='saved-layer-config-button-container'>
                        <button onClick={this.loadSavedLayerConfig}>Load</button>
                        <button onClick={this.showRemoveConfigDialog}>Remove</button>
                    </div>
                </div>
            </li>
        );
    }

}

export default SavedLayerConfigComponent;