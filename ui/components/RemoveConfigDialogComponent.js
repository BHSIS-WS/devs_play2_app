import React from "react";

class RemoveConfigDialogComponent extends React.Component {

    constructor(props) {
        super(props);
        this.hideRemoveConfigDialog = this.hideRemoveConfigDialog.bind(this);
        this.removeSavedLayerConfig = this.removeSavedLayerConfig.bind(this);
    }

    hideRemoveConfigDialog() {
        let { hideRemoveConfigDialog } = this.props;
        hideRemoveConfigDialog();
    }

    removeSavedLayerConfig() {
        let { userSavedLayerConfig, removeSavedLayerConfig } = this.props;
        removeSavedLayerConfig(userSavedLayerConfig);
    }

    render() {
        const {showRemoveConfigDialog, userSavedLayerConfig} = this.props;
            return (
                <div id="remove-config-dialog" className="toolbar-popup-div" hidden={!showRemoveConfigDialog}>
                    <h4>Remove Configuration</h4>
                    <div className='toolbar-popup-content-div'>
                        <p>Are you sure you want to permanently delete
                            the layer configuration with the name: <strong>{userSavedLayerConfig? userSavedLayerConfig.configName : ""}</strong>?</p>
                    </div>
                    <div className='toolbar-popup-button-container remove-config-dialog-button-container'>
                        <button onClick={this.removeSavedLayerConfig}>Yes</button>
                        <button onClick={this.hideRemoveConfigDialog}>No</button>
                    </div>
                </div>
            )
    }
}

export default RemoveConfigDialogComponent;
