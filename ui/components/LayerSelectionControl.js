import React from 'react';
import FusionLayerGroups from '../containers/FusionLayerGroups'
import FusionLayerSelectedItems1 from '../containers/FusionLayerSelectedItems1'
import FusionLayerSelectedCriteria1 from '../containers/FusionLayerSelectedCriteria1'
import GradientLayers from '../containers/GradientLayers'
import { SmallDoubleArrowIcon } from '../../ui/constant/SvgIcons';
import {
    toggleVisibilityForLayerPrintControlPanel,
    toggleVisibilityForSelectLayersPanel
} from '../actions/SidebarOptionsAction'

class LayerSelectionControl extends React.Component {

    render() {
        let {dataInitialized, selectedTab, showSelectLayersPanel, showLayerPrintControlPanel} = this.props;

        if (dataInitialized) {
            return (
                <div className="layerControlPanel" hidden={!showSelectLayersPanel}>
                    <div className="print-config-header" hidden={!(selectedTab==='Print') || showLayerPrintControlPanel}>
                        <a href="javascript:void(0)" onClick={() => this.props.dispatch(toggleVisibilityForSelectLayersPanel())}>Layer Print Control</a>
                        <button onClick={ () => this.props.dispatch(toggleVisibilityForLayerPrintControlPanel()) }  className='show-d3-map-control-button'><SmallDoubleArrowIcon/></button>
                    </div>
                    <div id="layerSelectionControl" hidden={!showSelectLayersPanel && !showSelectLayersPanel}>
                        <FusionLayerGroups name="Layers" layerType="L"/>
                        <FusionLayerGroups name="Boundary Layers" layerType="B"/>
                        <GradientLayers/>
                    </div>
                    <div id="selectedItemsDiv">
                        <FusionLayerSelectedItems1/>
                        <FusionLayerSelectedCriteria1/>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }
}

export default LayerSelectionControl;