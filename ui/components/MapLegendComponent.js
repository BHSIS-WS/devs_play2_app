import React from "react";

class MapLegendComponent extends React.Component {

    setSelectedMapLegendLayerId(layerId) {
        this.props.dispatch({type:"SET_SELECTED_MAP_LEGEND_LAYER_ID", selectedMapLegendLayerId: layerId});
    }

    render() {
        let {selectedMapLegendLayerId, selectedNormalLayers, selectedGradLayers, selectedMapLegendJson} = this.props;
        let legendColorAndValueArray = [];
        if (selectedMapLegendLayerId !== '-1') {
            for (let i = 0; i < selectedMapLegendJson.color.length; i++) {
                legendColorAndValueArray.push({"color": selectedMapLegendJson.color[i], "value": selectedMapLegendJson.value[i]})
            }
        }
        return (
            <div id="map-legend">
                <select value={selectedMapLegendLayerId} onChange={(event) => this.setSelectedMapLegendLayerId(event.target.value)} id="legend-select">
                    <option value={'-1'}>-- Select an option below --</option>
                    {selectedNormalLayers.map(layer =>
                        layer.fusion_legend_json_string !== null ?
                            <option value={'normal-' + layer.id} key={'normal-' + layer.id}>{layer.layer_name == null? layer.name : layer.layer_name}</option>
                        :null
                    )}
                    {selectedGradLayers.map(layer =>
                        layer.fusion_legend_json_string !== null ?
                            <option value={'grad-' + layer.id} key={'grad-' + layer.id}>{layer.layer_name == null? layer.name : layer.layer_name}</option>
                        : null
                    )}
                </select>
                {selectedMapLegendLayerId === '-1' ?
                    <p>Please make a selection using above field.</p>
                    :
                    <p><b>{selectedMapLegendJson.title}</b></p>}

                    <ul id="map-legend-key-list">
                        {legendColorAndValueArray.map(colorAndValue =>
                            <li key={colorAndValue.color}>
                                <span id="mapLegendColorSpan" style={{backgroundColor: colorAndValue.color}}/>
                                <span>{colorAndValue.value}</span>
                            </li>
                        )}
                    </ul>
            </div>
    );
}

}

export default MapLegendComponent;

