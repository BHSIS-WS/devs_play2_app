import React from 'react';

class RenderStatusLayerComponent extends React.Component {

    render() {
        let {layerObject, renderStatus} = this.props;
        let layer = layerObject.layer;

        let circleClass;
        switch(renderStatus) {
            case "Loading":
                circleClass = "yellow-circle";
                break;
            case "Complete":
                circleClass = "green-circle";
                break;
            default:
                circleClass = "red-circle";
        }

        return (
            <div className='render-status-layer'>
                <p className='render-status-layer-name'>{layerObject.layerType === "gradient" ? layer.name : layer.layer_name}</p>
                <div className='render-status-status-div'>
                    <div className={"render-status-circle-div " + circleClass} />
                    <p className="render-status-layer-status">{renderStatus}</p>
                </div>
            </div>
        );
    }
}

export default RenderStatusLayerComponent;