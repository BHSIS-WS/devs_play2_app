import React from 'react';
import LayerSelectionControl1 from '../containers/LayerSelectionControl1'
import D3MapControl1 from '../containers/mapd3control/D3MapControl1'
import ColorPalette1 from '../containers/ColorPalette1';
import SavedLayerConfigPanelController from '../containers/SavedLayerConfigPanelController';
import FeedbackController from '../containers/FeedbackController';
import HelpWindowController from '../containers/HelpWindowController';

class SideContainerComponent extends React.Component {
    render() {
        let {dataInitialized, selectedMapTab} = this.props;

        if (dataInitialized) {
            return (
                <div id="sideContainer" className={selectedMapTab === "Print" ? "printContainer" : ""} >
                    <LayerSelectionControl1/>
                    <D3MapControl1/>
                    <ColorPalette1/>
                    <SavedLayerConfigPanelController />
                    <FeedbackController />
                    <HelpWindowController />
                </div>
            );
        } else {
            return null;
        }
    }
}

export default SideContainerComponent;