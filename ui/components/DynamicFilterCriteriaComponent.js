import React from "react";
import {
    removeDynamicCriteria,
    removeCriteria,
    removeDynamicCriteriaCondition,
    toggleCriteriaExpanded,
    addDynamicCriteriaCondition,
    updateDynamicConditionBound,
    toggleDynamicCondition
} from "../actions/NormalLayersAction";
import {fetchLayerDataList} from "../actions/LayerDatatListingAction";
import mapContentModel from "../others/MapContentModel";
import printContentModel from "../others/PrintContentModel";

class DynamicFilterCriteriaComponent extends React.Component {

    constructor(props) {
        super(props);
        this.maxId = 1;
        this.removeFilter = this.removeFilter.bind(this);
        this.toggleCriteriaExpanded = this.toggleCriteriaExpanded.bind(this);
        this.addDynamicCriteriaCondition = this.addDynamicCriteriaCondition.bind(this);
        this.removeDynamicCriteriaCondition = this.removeDynamicCriteriaCondition.bind(this);
        this.refreshData = this.refreshData.bind(this);
    }

    addDynamicCriteriaCondition() {
        let {dispatch, criteria}  = this.props;
        dispatch(addDynamicCriteriaCondition(criteria.id, ++this.maxId));
    }

    removeDynamicCriteriaCondition(conditionId) {
        let {dispatch, criteria} = this.props;
        dispatch(removeDynamicCriteriaCondition(criteria.id, conditionId));
        this.refreshData();
    }

    removeFilter() {
        let {dispatch, criteria, layer} = this.props;

        dispatch(removeDynamicCriteria(criteria.id));
        dispatch(removeCriteria(layer.id, criteria.id));
        this.refreshData();
    }

    toggleCriteriaExpanded() {
        let {dispatch, criteria} = this.props;
        dispatch(toggleCriteriaExpanded(criteria.id));
    }

    updateDynamicConditionBound(bound, conditionId, event) {
        let { dispatch, criteria, layer } = this.props;
        if(event.target.value !== "" && !isNaN(event.target.value)) {
            let intValue = parseInt(event.target.value);
            dispatch(updateDynamicConditionBound(bound, conditionId, criteria.id, intValue));
            dispatch(toggleDynamicCondition(conditionId, false, criteria.id, layer.id));
        }
    }

    toggleDynamicCondition(condition) {
        let { dispatch, criteria, layer } = this.props;
        dispatch(toggleDynamicCondition(condition.conditionId, !condition.applied, criteria.id, layer.id));
        this.refreshData();
    }

    refreshData() {
        let { dispatch, dataTableAutoRefresh } = this.props;
        if (dataTableAutoRefresh) {
            dispatch(fetchLayerDataList());
        }
        mapContentModel.refreshGoogleMapLayers();
        printContentModel.refreshD3MapLayersFromState();
    }

    render(){
        let { criteriaConditionsArray, criteria, expanded } = this.props;
        return (
            <div className="fusion-layer-filter-criteria-div">
                <div className='fusion-layer-filter-criteria-header-div'>
                    <b>{criteria.criteria_name}</b>
                    <button className='remove-filter-button' title='Remove filter' onClick={this.removeFilter} />
                    <button className={expanded? 'collapse-filter-button' : 'expand-filter-button'} title='Toggle Filter Visibility' onClick={this.toggleCriteriaExpanded} />
                </div>
                <div className='fusion-layer-filter-criteria-body-div' hidden={!expanded}>
                    <div className='fusion-layer-filter-criteria-info-div'>
                        <button onClick={() => this.addDynamicCriteriaCondition()} className="add_btn"><img src="/devs2/assets/images/add_btn.png" className="img_add"/></button>
                        <span>Type of search: OR</span>
                    </div>
                    <ul className='dynamic-filter-conditions-ul'>
                        {criteriaConditionsArray.map((condition) => {
                            return (
                                <li key={condition.conditionId}>
                                    <input onClick={() => this.toggleDynamicCondition(condition)} checked={condition.applied} className='checkbox-input' type='checkbox'/>
                                    <input onChange={(e) => this.updateDynamicConditionBound('lowerBound', condition.conditionId, e)} className='range-input' type='number' defaultValue={condition.lowerBound} />
                                    <span>To</span>
                                    <input onChange={(e) => this.updateDynamicConditionBound('upperBound', condition.conditionId, e)} className='range-input' type='number' defaultValue={condition.upperBound} />
                                    <button onClick={() => this.removeDynamicCriteriaCondition(condition.conditionId)} className="remove_btn"><img src="/devs2/assets/images/remove_btn.png" className="img_remove"/></button>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}

export default DynamicFilterCriteriaComponent;