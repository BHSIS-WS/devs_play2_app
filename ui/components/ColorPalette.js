import React from "react";
import ReactDOM from 'react-dom';

class ColorPalette extends React.Component {

	getColorList(paletteOptions, colorType) {
		let outerList = [];
		
		for(let k=0; k<paletteOptions.totalRows; k++) {
			let innerList = [];
				for(let i=0; i<paletteOptions.totalCells; i++) {
					const currentClassName = 'colorpale' + (k+1) + '-'+ i;
					innerList.push(<li key={i} data-type={colorType} className={currentClassName} onClick={(evt) => this.selectColor(evt)}/>)
				}
			outerList.push(<ul key={k}>{innerList}</ul>)
	    }
	    return outerList
	}

	closeColorPalette() {
		this.props.dispatch({type: 'TOGGLE_PALETTE', value: false})
	}

	saveColorPalette(shapeValue, colorPalette) {
        if (!colorPalette.visibleSection || colorPalette.visibleSection === "FILL") {
            shapeValue.shapeId = this.refs.shapeSelection ? this.refs.shapeSelection.value : "";
            shapeValue.fillColor = this.isValidHexValue(this.refs.fillColor.value) ?'#' + this.refs.fillColor.value : this.refs.fillColor.value;
		} else if (!colorPalette.visibleSection || colorPalette.visibleSection === "STROKE") {
            shapeValue.strokeColor = this.isValidHexValue(this.refs.borderColor.value) ? '#' + this.refs.borderColor.value : this.refs.borderColor.value;
        }
		this.props.dispatch({
			type: colorPalette.actionType,
			value: shapeValue,
			layerId: colorPalette.layerId,
            criteriaKey: colorPalette.criteriaKey,
            categoryCodeKey: colorPalette.categoryCodeKey,
			hasSameShapeStyle: colorPalette.hasSameShapeStyle,
		});
        this.closeColorPalette();
	}

	isValidHexValue(val) {
        return !isNaN(parseInt(val, 16));
	}

	selectColor(evt) {
		const target = evt.target,
			dispatchType = target.dataset.type === 'B' ? 'CHANGE_BORDER_COLOR' : 'CHANGE_FILL_COLOR',
			css = window.getComputedStyle(target),
			rgbColor = css.backgroundColor,
			hexColor = this.rgbToHex(rgbColor);

		this.props.dispatch({type: dispatchType, value: '#' + hexColor})
	}

	rgbToHex(color) {
		const cSplit = color.replace(/[^\d,]/g,"").split(",");
		return ([this.valueToHex(cSplit[0]), this.valueToHex(cSplit[1]), this.valueToHex(cSplit[2])]).join('')
	}

	valueToHex(val) {
		const hex = Math.round(val).toString(16);
    	return hex.length === 1 ? '0' + hex : hex;
	}

	onChangeFillColor(evt) {
		const fillColor = evt.target.value ?'#' + evt.target.value : evt.target.value;
		this.props.dispatch({type: 'CHANGE_FILL_COLOR', value: fillColor})
	}

	onChangeBorderColor(evt) {
        const borderColor = evt.target.value ?'#' + evt.target.value : evt.target.value;
		this.props.dispatch({type: 'CHANGE_BORDER_COLOR', value: borderColor})
	}

    onChangeShapeStyle(evt) {
        this.props.dispatch({type: 'CHANGE_SHAPE_STYLE', value: evt.target.value})
    }

    onChangeStrokeSize(evt) {
        this.props.dispatch({type: 'CHANGE_STROKE_SIZE', value: evt.target.value})
    }

    onChangeStrokeOpacity(evt) {
        this.props.dispatch({type: 'CHANGE_STROKE_OPACITY', value: evt.target.value})
	}

    onChangeFillOpacity(evt) {
        this.props.dispatch({type: 'CHANGE_FILL_OPACITY', value: evt.target.value})
    }

	displaySelectedColor(displayColorValue, opacityValue) {
		return <span className='colorDisplay' style={{background: displayColorValue, opacity: opacityValue}}/>
	}

    componentDidMount() {
        document.addEventListener('mousedown', this.handleOutsideClick);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleOutsideClick);
    }

    handleOutsideClick = (evt) => {
        const paletteNode = ReactDOM.findDOMNode(this.refs.colorPalette);

        if (paletteNode && !paletteNode.contains(evt.target)) {
            this.closeColorPalette();
        }
    };

	render() {
		const { colorPalette, svgShapes } = this.props;

		if (!colorPalette.show) {
			return null
		}

		let selectedShapeConf = colorPalette.selectedShapeConfiguration,
			displayFillValue = selectedShapeConf.fillColor || colorPalette.defaultFillColor,
			displayBorderValue = selectedShapeConf.strokeColor || colorPalette.defaultBorderColor,
			shapeId = selectedShapeConf.shapeId,
			strokeSize = selectedShapeConf.strokeSize,
			paletteOptions = {
				totalRows: 8,
				totalCells: 8
			};

		return (
			<div ref='colorPalette' className="colorPalette" style={{top: colorPalette.topPosition, left: colorPalette.leftPosition}}>
				<div className="title">
	                <h3>Shape Configuration</h3>
	            </div>
	            <div>
                    {!colorPalette.visibleSection || colorPalette.visibleSection === "STROKE" ?
						<div className={!colorPalette.visibleSection ? "alignLeft" : "alignSingle"}>
							<div className="colorParams">
								<div>
									<label htmlFor="strokeSelection">Stroke Size:</label>
									<input type="number" id="strokeSelection" min="0" max="4" value={strokeSize} onChange={(evt) => this.onChangeStrokeSize(evt)}/>
								</div>
								<div>
									<label htmlFor="borderColor">Stroke Color #:</label>
									<input ref="borderColor" type="text" id="borderColor" size="5" maxLength="6" value={displayBorderValue && displayBorderValue.startsWith("#") ? displayBorderValue.slice(1) : displayBorderValue}
										   onChange={(evt)=>this.onChangeBorderColor(evt)}/>
								</div>
								<div>
									<label htmlFor="strokeOpacity">Stroke Opacity:</label>
									<input type="text" id="strokeOpacity" size="4" value={selectedShapeConf.strokeOpacity} onChange={(evt) => this.onChangeStrokeOpacity(evt)}/>
								</div>
							</div>
							<div className="selectedDisplay">
								{this.displaySelectedColor(displayBorderValue, selectedShapeConf.strokeOpacity)}
							</div>
							<div className="colorSelection">
								{this.getColorList(paletteOptions, 'B', displayBorderValue)}
							</div>
						</div>
					: null }
                    {!colorPalette.visibleSection || colorPalette.visibleSection === "FILL" ?
						<div className={!colorPalette.visibleSection ? "alignRight" : "alignSingle" }>
                            <div className="colorParams">
								{colorPalette.layerGroupType ?
									<div>
										<label htmlFor="shapeSelection">Shape:</label>
										<select ref="shapeSelection" id="shapeSelection" value={shapeId} onChange={(evt) => this.onChangeShapeStyle(evt)}>
											{svgShapes.map(shape =>
												(shape.groupType === colorPalette.layerGroupType) ?
													<option key={shape.id} value={shape.id}>{shape.name}</option>
													: null
											)}
										</select>
									</div>
								: null }
								<div>
									<label htmlFor="fillColor">Fill Color #:</label>
									<input ref="fillColor" type="text" id="fillColor" size="6" maxLength="6" value={displayFillValue && displayFillValue.startsWith("#") ? displayFillValue.slice(1) : displayFillValue}
										   onChange={(evt)=>this.onChangeFillColor(evt)}/>
								</div>
								<div>
									<label htmlFor="fillOpacity">Fill Opacity:</label>
									<input type="text" id="fillOpacity" size="4" value={selectedShapeConf.fillOpacity} onChange={(evt) => this.onChangeFillOpacity(evt)}/>
								</div>
							</div>
							<div className="selectedDisplay">
								{this.displaySelectedColor(displayFillValue, selectedShapeConf.fillOpacity)}
							</div>
							<div className="colorSelection">
								{this.getColorList(paletteOptions, 'F', displayFillValue)}
							</div>
						</div>
					: null }
	            </div>
	            <div className="pullRight">
	                <button className="defaultBtn" onClick={() => this.saveColorPalette(selectedShapeConf, colorPalette)}>OK</button>
	                <button className="defaultBtn close" onClick={() => this.closeColorPalette()}>Cancel</button>
	            </div>
            </div>
		)
	}
}

export default ColorPalette