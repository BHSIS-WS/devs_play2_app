import React from "react";

class SaveLayerConfigComponent extends React.Component {

    constructor(props) {
        super(props);
        this.saveLayerConfigForUser = this.saveLayerConfigForUser.bind(this);
        this.closeLayerConfigPanel = this.closeLayerConfigPanel.bind(this);
        this.initLayerConfigPanelValue = this.initLayerConfigPanelValue.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let { showSaveLayerConfigDialog } = this.props;
        if(nextProps.showSaveConfigDuplicateNameError === true) {
            document.getElementById("layer-config-name-input").select();
        } else if(!showSaveLayerConfigDialog) {
            this.initLayerConfigPanelValue();
        }
    }

    saveLayerConfigForUser() {
        let { saveLayerConfigForUser } = this.props;
        let name = document.getElementById("layer-config-name-input");
        let description = document.getElementById("layer-config-description-textarea");
        if (name.value === "" || description.value === "") {
            alert("Name and description are required to save your layer configuration!");
        } else {
            saveLayerConfigForUser(name.value, description.value);
            name.focus();
        }
    }

    initLayerConfigPanelValue() {
        document.getElementById("layer-config-name-input").value = "";
        document.getElementById("layer-config-description-textarea").value = "";
    }

    closeLayerConfigPanel() {
        let { hideSaveLayerConfigDialog, showSaveConfigDuplicateNameErrorAction } = this.props;
        this.initLayerConfigPanelValue();
        showSaveConfigDuplicateNameErrorAction(false);
        hideSaveLayerConfigDialog();
    }

    render() {
        let { showSaveLayerConfigDialog, showSaveConfigDuplicateNameError } = this.props;
        return (
            <div id="save-layer-config-dialog" className='toolbar-popup-div' hidden={!showSaveLayerConfigDialog}>
                <h4>Save Current Configuration</h4>
                <div className='toolbar-popup-content-div'>
                    <div className='toolbar-popup-input-container'>
                        <label htmlFor='layer-config-name-input' id="canvas-attr-setting">Name</label>
                        <input id='layer-config-name-input' type='text' placeholder='name'/>
                        <label htmlFor='layer-config-description-input' id="canvas-attr-setting">Description</label>
                        <textarea id='layer-config-description-textarea' placeholder='description'/>
                    </div>
                    <div className='toolbar-popup-button-container'>
                        { showSaveConfigDuplicateNameError ? <p id="save-config-duplicate-name-err" style={{display: "inline-block"}}>Configuration name must be unique.</p> : null}
                        <button onClick={this.closeLayerConfigPanel}>Cancel</button>
                        <button onClick={this.saveLayerConfigForUser}>Save</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default SaveLayerConfigComponent;