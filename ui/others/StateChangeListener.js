import ReduxStateChangeListenerModel from './ReduxStateChangeListenerModel';
import store from '../redux/store';
import $ from "jquery";
import mapContentModel from './MapContentModel';
import printContentModel from './PrintContentModel';
import {setRightClickValues} from "../actions/MapVariablesAction";
import {
    toggleVisibilityForFeedbackPanel, toggleVisibilityForHelpPanel,
    toggleVisibilityForSelectLayersPanel
} from "../actions/SidebarOptionsAction";
import {fetchLayerDataList} from "../actions/LayerDatatListingAction";

let modelThis;

const onMapTabChanged = (selectedMapTab) => {
    $('.tabLeft').removeClass("selected");
    let tabSelector = "#map"+selectedMapTab;
    $(tabSelector).addClass("selected");

    $(".mapContent").css("display", "none");
    switch(selectedMapTab) {
        case 'Print':
            $("#mapLegend, #googleMenuTab").css('display', 'none');
            $('#mainContainer').addClass('printContainer');
            $('body').addClass('printBody');
            $('#mapContentPrint').css("display", "block");
            break;
        case 'Carto':
            $("#mapLegend, #googleMenuTab").css('display', 'block');
            $('#mainContainer').removeClass('printContainer');
            $('body').removeClass('printBody');
            $('#mapContentGoogle').css("display", "block");
            $("#mapContentGoogle").width($("#mainContainer").width());
            mapContentModel.triggerMapResize();
            mapContentModel.refreshGoogleMapLayers();
            break;
    }
};

const onDataInitializedChanged = (dataInitialized, state) => {
    if (dataInitialized) {
        mapContentModel.initializeGoogleMap(store);
        printContentModel.initializePrintableMap(store.getState(), store.dispatch);
        printContentModel.updateMapCanvasD3FromState(store.getState(), store.dispatch);
        printContentModel.observeD3CanvasResize(store.getState(), store.dispatch);
    }
};

const onSavedFillColorChanged = (savedFillColor, state) => {
    modelThis.updateMapLayerD3BorderColors(store.getState(), store.dispatch);
};

const onSavedBorderColorChanged = (savedBorderColor, state) => {
    modelThis.updateMapLayerD3FillColors(store.getState(), store.dispatch);
};

const onBorderWidthMapChanged = (borderWidthMap, state) => {
    modelThis.updateMapLayerD3BorderWidth(store.getState(), store.dispatch);
};

const onRightClickContextMenuPositionChanged = () => {
    if (store.getState().mapVariables.rightClickContextMenuPosition !== null) {
        mapContentModel.setMenuXY(store.getState());
    }
};

const onMapZoomLevelChanged = () => {
    if (store.getState().mapVariables.shouldZoomMap) {
        mapContentModel.zoomMapToZoomLevel(store.getState(), store.dispatch);
    }
};

const showOrHideMapLegend = () => {
    if(store.getState().toolbarOptions.showPrintMapLegend) {
        printContentModel.addLegend();
    } else {
        printContentModel.removeLegend();
    }
};

const updateMapLegend = () => {
    if(store.getState().toolbarOptions.showPrintMapLegend) {
        printContentModel.addLegend();
    }
};

const onD3TranslateChange = () => {
    if (store.getState().mapCanvasD3.fromMinimap) {
        printContentModel.moveViewFromMinimap();
    }
};

const onInfoWindowChanged = () => {
    if (store.getState().mapVariables.infoWindow && store.getState().mapVariables.infoWindow.htmlToDisplay) {
        mapContentModel.showInfoWindow(store.getState());
    }
};

const onMapShapesChanged = () => {
    mapContentModel.refreshGoogleMapLayers();
};

const onCartoWhereStringUpdated = (state, prev) => {
    if (store.getState().layerDataListing.autoRefresh) {
        fetchLayerDataList();
    }
    mapContentModel.refreshGoogleMapLayers();
    printContentModel.refreshD3MapLayersFromState();
}

const registerStateChangeListener = (store)=>{
    const stateCallbackManager = new ReduxStateChangeListenerModel(store);
    stateCallbackManager.register(state => state.selectedMapTab, onMapTabChanged,false);
    stateCallbackManager.register(state => state.ref.dataInitialized, onDataInitializedChanged,false);
    stateCallbackManager.register(state => state.colorPalette.savedFillColor, onSavedFillColorChanged, false);
    stateCallbackManager.register(state => state.colorPalette.savedBorderColor, onSavedBorderColorChanged, false);
    stateCallbackManager.register(state => state.layers.borderWidthMap, onBorderWidthMapChanged, false);
    stateCallbackManager.register(state => state.mapVariables.rightClickContextMenuPosition, onRightClickContextMenuPositionChanged, false);
    stateCallbackManager.register(state => state.mapVariables.zoomLevel, onMapZoomLevelChanged, false);
    stateCallbackManager.register(state => state.toolbarOptions.showPrintMapLegend, showOrHideMapLegend, false);
    stateCallbackManager.register(state => state.printMapLegend, updateMapLegend, false);
    stateCallbackManager.register(state => state.d3LayerConfig.layerConfigMap, updateMapLegend, false);
    stateCallbackManager.register(state => state.d3LayerConfig.gradientLayerConfigMap, updateMapLegend, false);
    stateCallbackManager.register(state => state.d3LayerConfig.disabledLayerIdSet, updateMapLegend, false);
    stateCallbackManager.register(state => state.d3LayerConfig.disabledGradientLayerIdSet, updateMapLegend, false);
    stateCallbackManager.register(state => state.normalLayers.selectedLayerIdSet, updateMapLegend, false);
    stateCallbackManager.register(state => state.gradientLayers.selectedGradLayerIdSet, updateMapLegend, false);
    stateCallbackManager.register(state => [state.mapCanvasD3.translateX, state.mapCanvasD3.translateY], onD3TranslateChange, false);
    stateCallbackManager.register(state => state.mapVariables.mapShapes, onMapShapesChanged, false);
    stateCallbackManager.register(state => state.mapVariables.infoWindow, onInfoWindowChanged, false);
    stateCallbackManager.register(state => state.normalLayers.cartoInclusiveWhereString, onCartoWhereStringUpdated, false);

    stateCallbackManager.start();
};


class StateChangeListener {

    constructor(store) {
        modelThis = this;
        this.store=store;
        this.init(store);
        registerStateChangeListener(store);


        $(".tabLeft").on('click', (e) => {
            e.preventDefault();
            store.dispatch({type: "CHANGE_MAP_TAB", value: $(e.currentTarget).text()});
            store.dispatch(setRightClickValues(null, null, null, null));
            mapContentModel.refreshMapLayers(store.getState(), store.dispatch);
            printContentModel.refreshMapLayers(store.getState(), store.dispatch);
        });

        $("#select-layers-btn1").on("click", () => {
            store.dispatch(toggleVisibilityForSelectLayersPanel());
            let classSelectLayer = store.getState().sidebarOptions.showLayerPrintControlPanel ? "tabRightOpen" : "tabRightClose";
            $("#select-layers-btn1").attr('class', classSelectLayer);
        });

        $("#feedback-btn1").on("click", () => {
            store.dispatch(toggleVisibilityForFeedbackPanel());
            let classFeedback = store.getState().sidebarOptions.showFeedbackPanel ? "tabRightOpen" : "tabRightClose";
            $("#feedback-btn1").attr('class', classFeedback);
        });

        $("#help-btn").on("click", () => {
            store.dispatch(toggleVisibilityForHelpPanel());
            let classHelp = store.getState().sidebarOptions.showHelpPanel ? "tabRightOpen" : "tabRightClose";
            $("#help-btn").attr('class', classHelp);
        });

        printContentModel.registerLayerSelectionChangeListener(store.getState(), store.dispatch);
    }

    init(store) {
        let selectedMapTab = store.getState().selectedMapTab,
            tabSelector = "#map"+selectedMapTab,
            tabContentSelector = "#mapContent"+selectedMapTab;

        if (selectedMapTab === "Carto") {
            tabContentSelector = "#mapContentGoogle";
        }

        $('.tabLeft').removeClass("selected");
        $(tabSelector).addClass("selected");
        $(".mapContent").css("display", "none");
        $(tabContentSelector).css("display", "block");
        if (selectedMapTab === 'Print') {
            $('#mainContainer').addClass('printContainer');
            $('body').addClass('printBody');
        }
    }
}

const stateChangeListener = new StateChangeListener(store);

export default stateChangeListener;