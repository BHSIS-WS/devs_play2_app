import $ from 'jquery';
import {saveAs} from 'file-saver';
import jsPDF from 'jspdf';
import store from '../redux/store';
import layerSelectionSubject from '../observables/layerSelectionSubject';
import D3MapRenderingManager from './d3map/D3MapRenderingManager';
import SVGtoPDF from 'svg-to-pdfkit';
import {resizeCanvas} from '../actions/MapCanvasD3Action';

class PrintContentModel {

    registerLayerSelectionChangeListener(state, dispatch) {
        const subscription = layerSelectionSubject
            .filter(event => event === "D3_CANVAS_CHANGE")
            .debounceTime(1000)
            .subscribe(
                (data) => {
                    console.log("layerSelectionSubject.subscription on : data ");
                    this.d3MapRenderingManager.updateMapCanvasD3FromState(state, dispatch);
                },
                (err) => {
                    console.log("layerSelectionSubject.subscription.err: " + err)
                },
                () => {
                    console.log("layerSelectionSubject.subscription: " + 'Completed')
                }
            );
    }

    observeD3CanvasResize = (state, dispatch) => {
        let canvas = document.querySelector('#mapContentPrint #mapCanvas');
        let observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                if (mutation.target === canvas && mutation.attributeName === 'style') {
                    let prevWidth = state.mapCanvasD3.width;
                    let prevHeight = state.mapCanvasD3.height;
                    let width = parseInt(canvas.style.width, 10);
                    let height = parseInt(canvas.style.height, 10);
                    if (width !== prevWidth || height !== prevHeight) {
                        dispatch(resizeCanvas(height, width));
                    }
                }
            });
        });
        observer.observe(canvas, {attributes: true});
    };

    performZoomIn() {
        this.d3MapRenderingManager.zoomIn();
    }

    performZoomOut() {
        this.d3MapRenderingManager.zoomOut();
    }

    addLegend() {
        this.d3MapRenderingManager.addLegend();
    }

    removeLegend() {
        this.d3MapRenderingManager.removeLegend();
    }

    toggleMouseZoom(disableMouseZoom) {
        this.d3MapRenderingManager.toggleMouseZoom(disableMouseZoom);
    }

    downloadToImage(state) {
        let self = this,
            canvasAttributes = state.mapCanvasD3,
            width = canvasAttributes.width,
            height = canvasAttributes.height,
            backgroundColor = "#FFFFFF";

        self.format = 'jpeg';
        let svgImageData = self.generateSvgImageData(state);
        self.svgToImage(svgImageData, width, height, backgroundColor, self.format, save);

        function save(dataBlob, fileSize) {
            saveAs(dataBlob, 'devs_printable_map.' + self.format);
        }
    }

    downloadToPdf(state) {
        let currentWidth = state.mapCanvasD3.width;
        let currentHeight = state.mapCanvasD3.height;
        $("#pdf-width-input").val((currentWidth / 72.0).toFixed(2));
        $("#pdf-height-input").val((currentHeight / 72.0).toFixed(2));
    }

    refreshMapLayers(e, state, dispatch) {
        e.preventDefault();
        this.refreshD3MapLayersFromState(state, dispatch);
        layerSelectionSubject.next("test button clicked!");
    }

    performPDFGeneration(width, height) {
        let self = this;
        let svgString = self.getSVGString(self.d3MapRenderingManager.svg.node());
        svgString = svgString.replace(new RegExp('stroke-width="\d*\.?\d*(px)?"', 'g'), 'stroke-width="0.25"');
        let widthInPoints = width * 72.0;
        let heightInPoints = height * 72.0;
        let options = {
            width: widthInPoints,
            height: heightInPoints
        };

        let doc = new PDFDocument({
            compress: false,
            size: [widthInPoints, heightInPoints]
        });
        let stream = doc.pipe(blobStream());

        let p = new Promise(function (resolve, reject) {
            stream.on('finish', function () {
                resolve(stream.toBlob('application/pdf'));
            });

            setTimeout(() => {
                SVGtoPDF(doc, svgString, 0, 0, options);
                doc.end();
            }, 0);

        });

        p.then((blob) => {
            saveAs(blob, 'devs_printable_map.pdf');
        });
    }

    generateSvgImageData(state) {
        let canvasAttributes = state.mapCanvasD3,
            width = canvasAttributes.width,
            height = canvasAttributes.height,
            backgroundColor = $("#mapCanvas").css("background-color");
        let svg = this.d3MapRenderingManager.svg;
        let widthBak = svg.attr("width"),
            heightBak = svg.attr("height"),
            backgroundColorBak = svg.style("background-color");
        svg.attr("width", width);
        svg.attr("height", height);
        svg.style("background-color", backgroundColor);
        let svgString = this.getSVGString(svg.node());
        let svgImageData = btoa(unescape(encodeURIComponent(svgString)));
        svg.attr("width", widthBak);
        svg.attr("height", heightBak);
        svg.style("background-color", backgroundColorBak);
        return svgImageData;
    }

    getSVGString(svgNode) {
        svgNode.setAttribute("xlink", "http://www.w3.org/1999/xlink");
        let cssStyleText = getCSSStyles(svgNode);
        appendCSS(cssStyleText, svgNode);

        let serializer = new XMLSerializer();
        let svgString = serializer.serializeToString(svgNode);
        svgString = svgString.replace(/(\w+)?:?xlink=/g, 'xmlns:xlink='); // Fix root xlink without namespace
        svgString = svgString.replace(/NS\d+:href/g, 'xlink:href'); // Safari NS namespace fix
        return svgString;

        function getCSSStyles(parentElement) {
            let selectorTextArr = [];
            selectorTextArr.push('#' + parentElement.id);
            for (let c = 0; c < parentElement.classList.length; c++) {
                if (!contains('.' + parentElement.classList[c], selectorTextArr)) {
                    selectorTextArr.push('.' + parentElement.classList[c]);
                }
            }

            let nodes = parentElement.getElementsByTagName("*");
            for (let i = 0; i < nodes.length; i++) {
                let id = nodes[i].id;
                if (!contains('#' + id, selectorTextArr)) {
                    selectorTextArr.push('#' + id);
                }
                let classes = nodes[i].classList;
                for (let c = 0; c < classes.length; c++) {
                    if (!contains('.' + classes[c], selectorTextArr)) {
                        selectorTextArr.push('.' + classes[c]);
                    }
                }
            }

            let extractedCSSText = "";
            for (let i = 0; i < document.styleSheets.length; i++) {
                let s = document.styleSheets[i];
                try {
                    if (!s.cssRules) {
                        continue;
                    }
                } catch (e) {
                    if (e.name !== 'SecurityError') {
                        throw e;
                    } // for Firefox
                    continue;
                }
                let cssRules = s.cssRules;
                for (let r = 0; r < cssRules.length; r++) {
                    if (contains(cssRules[r].selectorText, selectorTextArr)) {
                        extractedCSSText += cssRules[r].cssText;
                    }
                }
            }

            return extractedCSSText;

            function contains(str, arr) {
                return arr.indexOf(str) !== -1;
            }
        }

        function appendCSS(cssText, element) {
            let styleElement = document.createElement("style");
            styleElement.setAttribute("type", "text/css");
            styleElement.innerHTML = cssText;
            let refNode = element.hasChildNodes() ? element.children[0] : null;
            element.insertBefore(styleElement, refNode);
        }
    }

    svgToImage(dataUrl, width, height, bgColor, format, callback) {
        let imageSrc = 'data:image/svg+xml;base64,' + dataUrl;
        let canvas = document.createElement('canvas'),
            context = canvas.getContext('2d'),
            image = new Image();
        canvas.width = width;
        canvas.height = height;
        context.fillStyle = bgColor;
        image.onload = function () {
            context.fillRect(0, 0, width, height);
            context.drawImage(image, 0, 0, width, height);
            canvas.toBlob(function (blob) {
                let fileSize = Math.round(blob.size / 1024) + ' KB';
                if (callback) {
                    callback(blob, fileSize);
                }
            }, 'image/' + format);
        };
        image.src = imageSrc;
    }

    generatePdf(state, dispatch) {

        function getBase64Image(img) {

            let canvas = document.createElement("canvas");

            canvas.width = img.width;
            canvas.height = img.height;
            let ctx = canvas.getContext("2d");

            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(img, 0, 0, canvas.width / 4, canvas.height / 4);

            let dataURL = canvas.toDataURL("image/jpeg");

            return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

        }

        let img = new Image();
        let self = this;

        img.onload = function () {
            let dataURI = getBase64Image(img);
            let doc = new jsPDF();
            let canvasAttributes = state.mapCanvasD3,
                width = canvasAttributes.width,
                height = canvasAttributes.height;
            doc.addImage(dataURI, 'JPEG', 0, 0, img.width, img.height);
            doc.save('D3_SVG_map.pdf');
        };
        img.src = "/devs2/assets/images/D3_SVG_map.jpeg";
    }


    //================= Printable Map =====================
    initializePrintableMap(state, dispatch) {
        console.log(" ... in initializePrintableMap...(empty)");
        this.mapCanvasD3 = $("#mapContentPrint #mapCanvas");
        this.d3MapRenderingManager = new D3MapRenderingManager(store, this.mapCanvasD3);
        // this.minScaleD3 = 1;
        // this.maxScaleD3 = 10;
        // this.maxWidthD3 = 1020 * 2;
        // this.maxHeightD3 = 750 * 2;
        this.refreshD3MapLayersFromState();
        // this.refreshGradientMapLayersD3FromState(state, dispatch);
    }

    refreshD3MapLayersFromState() {
        if(store.getState().selectedMapTab === 'Print') {
            console.log("\t |->ENTER * refreshD3MapLayersFromState...");
            this.d3MapRenderingManager.refresh();
            console.log("\t |<-EXIT  * refreshD3MapLayersFromState...");
        }
    }

    moveViewFromMinimap() {
        this.d3MapRenderingManager.moveViewFromMinimap();
    }


    addMapTextD3() {
        this.d3MapRenderingManager.addText();
    }

    getTextToolTextFields() {
        return this.d3MapRenderingManager.getTextToolTextFields();
    }

    getNewSvgTextId() {
        return this.d3MapRenderingManager.getNewSvgTextId();
    }

    hideTextTypingTextArea() {
        this.d3MapRenderingManager.hideTextTypingTextArea();
    }

    createSvgText(textId, fontSize, rotate, fillColor, textContent, fontFamily, isBold, isItalic,
                  isUnderlined) {
        this.d3MapRenderingManager.createText(textId, fontSize, rotate, fillColor, textContent, fontFamily,
            isBold, isItalic, isUnderlined, null, null);
    }

    toggleRectDrawingCanvasD3() {
        this.d3MapRenderingManager.toggleRectDrawingCanvas();
    }

    showRectDrawingCanvasD3() {
        this.d3MapRenderingManager.showRectDrawingCanvas();
    }

    saveToolElementCoordinates() {
        this.d3MapRenderingManager.saveToolElementCoordinates();
    }

    updateMapCanvasD3FromState(state, dispatch) {
        //let mapCanvasD3 = $("#mapContentPrint #mapCanvas");
        let canvasAttributes = state.mapCanvasD3;
        if (!canvasAttributes) return;
        this.mapCanvasD3.width(canvasAttributes.width);
        this.mapCanvasD3.height(canvasAttributes.height);
        this.updateMapCanvasD3Scale(state, dispatch, canvasAttributes.zoom);
    }

    updateMapCanvasD3Scale(state, dispatch, scale) {
        let self = this,
            width = state.mapCanvasD3.width,
            height = state.mapCanvasD3.height;
        if (scale >= self.minScaleD3 && scale <= self.maxScaleD3) {
            if (self.d3GMap) {
                let transX = width * (1 - scale) / 2,
                    transY = height * (1 - scale) / 2;
                self.d3GMap.forEach((g, layerKey, map) => {
                    g.attr("transform", "translate(" + (transX) + ", " + (transY) + ")scale(" + scale + ")");
                });
                self.baseRect.attr("transform", "translate(" + (transX) + ", " + (transY) + ")scale(" + scale + ")");
            }
        }
    }

    updateMapLayerD3BorderColors(state, dispatch) {
        let self = this,
            savedBorderColorMap = state.colorPalette.savedBorderColor;
        savedBorderColorMap.forEach((color, layerKey, map) => {
            let keyStrings = layerKey.split("_");
            if (keyStrings[0] === "normal") {
                self.d3GMap.get(layerKey).selectAll(".layer_d3_" + keyStrings[2]).attr("stroke", color);
            } else if (keyStrings[0] === "grad") {
                self.d3GMap.get(layerKey).selectAll(".layer_grad_d3_" + keyStrings[2]).attr("stroke", color);
            }
        });
    }

    updateMapLayerD3FillColors(state, dispatch) {
        let self = this,
            savedFillColorMap = state.colorPalette.savedFillColor;
        savedFillColorMap.forEach((color, layerKey, map) => {
            let keyStrings = layerKey.split("_");
            if (keyStrings[0] === "normal") {
                self.d3GMap.get(layerKey).selectAll(".layer_d3_" + keyStrings[2]).attr("fill", color);
            } else if (keyStrings[0] === "grad") {
                self.d3GMap.get(layerKey).selectAll(".layer_grad_d3_" + keyStrings[2]).attr("fill", color);
            }
        });
    }

    updateMapLayerD3BorderWidth(state, dispatch) {
        let self = this,
            borderWidthMap = state.layers.borderWidthMap;
        borderWidthMap.forEach((borderWidth, layerKey, map) => {
            let keyStrings = layerKey.split("_");
            if (keyStrings[0] === "normal") {
                self.d3GMap.get(layerKey).selectAll(".layer_d3_" + keyStrings[2]).attr("stroke-width", borderWidth + "px");
            } else if (keyStrings[0] === "grad") {
                self.d3GMap.get(layerKey).selectAll(".layer_grad_d3_" + keyStrings[2]).attr("stroke-width", borderWidth + "px");
            }
        });
    }


    quoteJSONKeys(jString) {
        if (jString && jString.length >= 2 && !jString.includes("\"")) {
            let jStr = jString.trim();
            jStr = jStr.substr(1, jStr.length - 2);
            let jFieldStrs = jStr.split(",");
            let jFieldPairStrs;
            let result = "{";
            jFieldPairStrs = jFieldStrs[0].trim().split(":");
            result += "\"" + jFieldPairStrs[0].trim() + "\":" + jFieldPairStrs[1].trim();
            for (let i = 1; i < jFieldStrs.length; i++) {
                jFieldPairStrs = jFieldStrs[i].trim().split(":");
                result += ",\"" + jFieldPairStrs[0].trim() + "\":" + jFieldPairStrs[1].trim();
            }
            result += "}";
            return result;
        } else {
            return "{}";
        }
    }

    getShareFacilityUrl() {
        let url = window.location.protocol + "//" + window.location.host;
        let pathName = window.location.pathname;
        url = url + pathName;

        return url;
    }

    refreshMapLayers(state, dispatch) {
        this.refreshD3MapLayersFromState(state, dispatch);
    }


}

const printContentModel = new PrintContentModel();

export default printContentModel;