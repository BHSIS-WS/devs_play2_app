import AbstractCartoMapLayerWrapper from './AbstractCartoMapLayerWrapper';
import {isLayerCriteriaMapDataDifferent} from '../../state/stateDataHelpers'
import {buildNormalLayerConditionsQuery} from "../../state/layerCriteriaHelper";
import {setupInfoWindow} from "../../actions/MapVariablesAction";
import D3MapNormalLayer from "../d3map/D3MapNormalLayer";
import mapContentModel from "../MapContentModel";
import carto from '@carto/carto.js';
import store from "../../redux/store";

class CartoMapLayerWrapper extends AbstractCartoMapLayerWrapper {

    constructor(layerId, store, map, cartoClient){
        super(layerId, store, map, cartoClient);
    }

    getSelectedCriteriaCodeAndIdMapFromState() {
        if (!this.store.getState().normalLayers.selectedCriteriaIdMap) return null;
        return this.store.getState().normalLayers.selectedCriteriaIdMap.get(this.layerId);
    }

    getLayerConfig(){
        let layerConfigArray = this.store.getState().ref.layerArray;
        return layerConfigArray.find(layerConfig => layerConfig.id === this.layerId);
    }

    create(){
        let self = this;
        let layerConfig=this.layerConfig;
        let shapeSQL = "";
        let mapShapes = this.store.getState().mapVariables.mapShapes;
        for (let i = 0; i < mapShapes.length; i++) {
            let shape = mapShapes[i];
            let vertices = this.getPolygonVertices(shape.overlay);
            if (i > 0) {
                shapeSQL += " AND ";
            }
            shapeSQL += " ST_Contains(ST_MakePolygon(ST_GeomFromText('LINESTRING" + vertices + "', 4326)), this_layer.the_geom)";
        }

        let whereString = this.createCartoQueryWhereString();

        if (whereString === "") {
            whereString = " WHERE 1 = 1 ";
        } else {
            whereString = " WHERE " + whereString;
        }

        let SQL = "SELECT this_layer.* FROM " + layerConfig.db_table_name + " as this_layer " + whereString;

        console.log("CARTO SQL+WHERE_STRING FOR LAYER " + this.layerId + ": " + SQL);

        if (shapeSQL !== "") {
            SQL += " AND " + shapeSQL;
        }

        const cartoSQL = new carto.source.SQL(SQL);

        let styleString = '';

        if (layerConfig.geom_type === "POLYGON") {
            styleString =`
                #layer {
                polygon-fill: #826dba;
                polygon-opacity: 0;
                }
                #layer::outline {
                line-width: 1;
                line-color: #000000;
                line-opacity: 1;
                }`;
        } else if (layerConfig.geom_type === "POINT") {
            styleString =`
                #layer {
                marker-width: 8;
                marker-fill: #7f7fff;
                marker-fill-opacity: 1;
                marker-allow-overlap: true;
                marker-line-width: 0.8;
                marker-line-color: #000000;
                marker-line-opacity: 1;
                }`;
        }

        if (layerConfig.carto_style_string) {
            styleString =layerConfig.carto_style_string;
        }

        let style =  new carto.style.CartoCSS(styleString);

        let columnsAndHtml = mapContentModel.getColumnsAndHtmlFromInfoWindowTemplate(layerConfig.layer_info_window_template);

        let cartoLayer = new carto.layer.Layer(cartoSQL, style, {
            featureClickColumns: columnsAndHtml.columnList
        });

        cartoLayer.on('featureClicked', featureEvent => {
            this.store.dispatch(setupInfoWindow(layerConfig.layer_name, columnsAndHtml.htmlList, columnsAndHtml.columnList, featureEvent.data, featureEvent.latLng, featureEvent.position));
        });

        this.layer = cartoLayer;
    }


    createCartoQueryWhereString() {

        let whereString = "";

        // check if this layer is in the selected criteria code id map
        if (this.store.getState().normalLayers.selectedCriteriaCodeIdMap.has(this.layerId)) {
            this.selectedCriteriaCodeAndIdMap = this.getSelectedCriteriaCodeAndIdMapFromState();
            let criteriaCodeArray = this.store.getState().ref.criteriaCodeArray;
            let layerCriteriaArray = this.store.getState().ref.layerFilteringCriteriaArray;
            let selectedCriteriaIdToCriteriaCodeMap = this.store.getState().normalLayers.selectedCriteriaIdMap.get(this.layerId);

            if (selectedCriteriaIdToCriteriaCodeMap != null && selectedCriteriaIdToCriteriaCodeMap.size > 0) {

                selectedCriteriaIdToCriteriaCodeMap.forEach((criteriaIdSet, criteriaCodeId, map) => {

                    let selectedCriteriaValueArray = [];
                    criteriaIdSet.forEach(criteriaId => {
                        selectedCriteriaValueArray.push(layerCriteriaArray.find(
                            criteria => {return criteria.id === criteriaId}).criteria_value);
                    });

                    let criteriaColumn = criteriaCodeArray.find(criteriaCode => {
                        return criteriaCode.id === criteriaCodeId
                    });

                    if (criteriaColumn.criteria_name == null) {
                        return "";
                    }
                    if (selectedCriteriaValueArray.length > 0) {
                        whereString += " this_layer." + criteriaColumn.criteria_name + " in ( ";
                        for(let i = 0; i < selectedCriteriaValueArray.length; i++) {
                            let criteriaValue = selectedCriteriaValueArray[i];
                            whereString += ("'" + criteriaValue + "'");
                            if (i !== selectedCriteriaValueArray.length - 1) {
                                whereString += ", ";
                            }
                        }
                        whereString += ")";
                    }
                });
            }

            let selectedCriteriaCodeIdSet = this.store.getState().normalLayers.selectedCriteriaCodeIdMap.get(this.layerId);
            let criteriaConditionsMap = this.store.getState().normalLayers.criteriaConditionsMap;

            if (criteriaConditionsMap != null && criteriaConditionsMap.size > 0 && selectedCriteriaCodeIdSet != null) {
                let conditionsWhereString = buildNormalLayerConditionsQuery(criteriaConditionsMap, selectedCriteriaCodeIdSet, criteriaCodeArray);
                if (whereString !== "" && conditionsWhereString !== "") {
                    conditionsWhereString = " AND " + conditionsWhereString;
                }
                whereString += conditionsWhereString;
            }
        }

        // handle whether or not to use the inclusive 'Show Common' string
        let selectedLayerIdSet = this.store.getState().normalLayers.selectedLayerIdSet;
        let layerGeomType = this.store.getState().ref.layerArray.find(lay => ((lay['id'] === this.layerId) ? lay['geom_type'] : '')).geom_type;
        let useInclusiveWhereString = false;
        if(selectedLayerIdSet.size >= 1) {
            let selectedGeomType = this.store.getState().ref.layerArray.find(lay => ((lay['id'] === this.layerId) ? lay['geom_type'] : '')).geom_type;
            let inclusiveWhereString = this.store.getState().normalLayers.cartoInclusiveWhereString.get(this.layerId);
            if(this.store.getState().normalLayers.selectedCriteriaIdMap.has(this.layerId) && inclusiveWhereString != null) {
                useInclusiveWhereString = true;
            }
            if(useInclusiveWhereString) {
                whereString = (whereString === "") ? inclusiveWhereString : whereString + " AND " + inclusiveWhereString;
                inclusiveWhereString = false;
            }
        }

        return whereString;
    }

    dataFetchRequired(){
        //get selectedCriteriaIdMap using layerId
        let oldSelectedCriteriaData = this.selectedCriteriaCodeAndIdMap;
        let newSelectedCriteriaData = this.getSelectedCriteriaCodeAndIdMapFromState() ;

        //has selected Criterias changed?
        return isLayerCriteriaMapDataDifferent(oldSelectedCriteriaData,newSelectedCriteriaData);

    }

}
export default CartoMapLayerWrapper;