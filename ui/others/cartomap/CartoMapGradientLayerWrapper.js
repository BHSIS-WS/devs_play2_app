import AbstractCartoMapLayerWrapper from './AbstractCartoMapLayerWrapper';
import {isLayerCriteriaMapDataDifferent} from '../../state/stateDataHelpers';
import {setupInfoWindow} from "../../actions/MapVariablesAction";
import mapContentModel from "../MapContentModel";
import carto from '@carto/carto.js';
import store from "../../redux/store";

class CartoMapGradientLayerWrapper extends AbstractCartoMapLayerWrapper {

    constructor(layerId, store, map, cartoClient){
        super(layerId, store, map, cartoClient);
    }

    getSelectedCriteriaCodeAndIdMapFromState() {
        if (!this.store.getState().normalLayers.selectedCriteriaIdMap) return null;
        return this.store.getState().normalLayers.selectedCriteriaIdMap.get(this.layerId);
    }

    getLayerConfig(){
        let gradientLayerConfigArray = this.store.getState().ref.gradientLayerArray;
        return gradientLayerConfigArray.find(gradientLayerConfig => gradientLayerConfig.id === this.layerId);
    }

    create(){
        let self = this;
        let layerConfig=this.layerConfig;
        let shapeSQL = "";
        let mapShapes = this.store.getState().mapVariables.mapShapes;
        for (let i = 0; i < mapShapes.length; i++) {
            let shape = mapShapes[i];
            let vertices = this.getPolygonVertices(shape.overlay);
            if (i > 0) {
                shapeSQL += " AND ";
            }
            shapeSQL += " ST_Contains(ST_MakePolygon(ST_GeomFromText('LINESTRING" + vertices + "', 4326)), this_layer.the_geom)";
        }

        let whereString = this.createCartoQueryWhereString();

        if (whereString === "") {
            whereString = " WHERE 1 = 1 ";
        } else {
            whereString = " WHERE " + whereString;
        }

        let SQL = "SELECT this_layer.* FROM " + layerConfig.db_table_name + " as this_layer " + whereString;

        if (shapeSQL !== "") {
            SQL += " AND " + shapeSQL;
        }

        const cartoSQL = new carto.source.SQL(SQL);

        let styleString =`
                #layer {
                polygon-fill: #826dba;
                polygon-opacity: 0;
                }
                #layer::outline {
                line-width: 1;
                line-color: #000000;
                line-opacity: 1;
                }`;

        if (layerConfig.carto_style_string) {
            styleString = layerConfig.carto_style_string;
        }

        let style = new carto.style.CartoCSS(styleString);

        let columnsAndHtml = mapContentModel.getColumnsAndHtmlFromInfoWindowTemplate(layerConfig.layer_info_window_template);

        let cartoLayer = new carto.layer.Layer(cartoSQL, style, {
            featureClickColumns: columnsAndHtml.columnList
        });

        cartoLayer.on('featureClicked', featureEvent => {
            this.store.dispatch(setupInfoWindow(layerConfig.name, columnsAndHtml.htmlList, columnsAndHtml.columnList, featureEvent.data, featureEvent.latLng, featureEvent.position));
        });
        this.layer = cartoLayer;
    }

    createCartoQueryWhereString() {
        let self = this,
            gradientLayerArray = self.store.getState().ref.gradientLayerArray,
            gradFilterMap = self.store.getState().gradientLayers.gradFilterMap;

        let gradientLayer = gradientLayerArray.find(gradLayer => {return gradLayer.id === self.layerId}),
            columnName = gradientLayer.field_column_name,
            gradFilterEntryArray = gradFilterMap.get(self.layerId);

        let whereString = '';
        if (gradFilterEntryArray && gradFilterEntryArray.length > 0) {
            gradFilterEntryArray.forEach((filterEntry, index) => {
                let filterSign = filterEntry.filterSign,
                    filterVal = filterEntry.filterValue;
                if (typeof filterVal !== 'undefined' && filterVal !== null) {
                    filterVal = filterVal.replace(/[^0-9.]/g, '');
                }
                if (filterVal !== '') {
                    if (index !== 0) {
                        whereString += ' AND ';
                    }
                    whereString += columnName + ' ' + filterSign + ' ' + filterVal;
                }
            });
        }
        return whereString;
    }
    
    dataFetchRequired(){
        //get selectedCriteriaIdMap using layerId
        let oldSelectedCriteriaData = this.selectedCriteriaCodeAndIdMap;
        let newSelectedCriteriaData = this.getSelectedCriteriaCodeAndIdMapFromState() ;

        //has selected Criterias changed?
        return isLayerCriteriaMapDataDifferent(oldSelectedCriteriaData,newSelectedCriteriaData);

    }

}
export default CartoMapGradientLayerWrapper;