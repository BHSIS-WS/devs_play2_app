import CartoMapLayerWrapper from './CartoMapLayerWrapper';
import CartoMapGradientLayerWrapper from './CartoMapGradientLayerWrapper';
import carto from '@carto/carto.js';

class CartoLayerRenderingManager{

    constructor(store, map, cartoClient) {
        this.store = store;
        this.map = map;
        this.cartoClient = cartoClient;
        this.renderedNormalLayers = [];
        this.renderedGradLayers = [];
    }

    hideAllExistingLayer(){
        this.hideAllExistingNormalLayers();
        this.hideAllExistingGradLayers();
    }

    hideAllExistingNormalLayers() {
        if (this.renderedNormalLayers) {
            for (let normalLayerWrapper of this.renderedNormalLayers) {
                this.cartoClient.removeLayer(normalLayerWrapper.layer);
            }
            this.renderedNormalLayers = [];
        }
    }

    hideAllExistingGradLayers() {
        if (this.renderedGradLayers) {
            for (let gradLayerWrapper of this.renderedGradLayers) {
                this.cartoClient.removeLayer(gradLayerWrapper.layer);
            }
            this.renderedGradLayers = [];
        }
    }

    refresh(){
        this.refreshGradLayers();
        this.refreshNormalLayers();

        this.renderedGradLayers.sort((layer1,layer2) => {
            return layer1.layerConfig.display_order - layer2.layerConfig.display_order;
        });

        let gradientCartoLayers = this.renderedGradLayers.map(layer => layer.layer);
        this.cartoClient.addLayers(gradientCartoLayers);

        this.renderedNormalLayers.sort((layer1,layer2) => {
            return -(layer1.layerConfig.display_order - layer2.layerConfig.display_order);
        });

        let normalCartoLayers = this.renderedNormalLayers.map(layer => layer.layer);
        this.cartoClient.addLayers(normalCartoLayers);

    }

    refreshNormalLayers() {
        this.hideAllExistingNormalLayers();

        let selectedNormalLayerIdSet = this.store.getState().normalLayers.selectedLayerIdSet;
        if (selectedNormalLayerIdSet) {
            let selectedNormalLayerIdArray = Array.from(selectedNormalLayerIdSet);
            // selectedNormalLayerIdArray.sort((a,b) => this.sortNormalLayers(a, b));
            this.renderedNormalLayers = selectedNormalLayerIdArray.map(normalLayerId => {
                return this.createLayer(normalLayerId);
            });
        }
    }

    refreshGradLayers() {
        this.hideAllExistingGradLayers();

        let selectedGradLayerIdSet = this.store.getState().gradientLayers.selectedGradLayerIdSet;
        if (selectedGradLayerIdSet) {
            let selectedGradLayerIdArray = Array.from(selectedGradLayerIdSet);
            this.renderedGradLayers = selectedGradLayerIdArray.map(gradLayerId => {
                return this.createGradientLayer(gradLayerId)
            });
        }
    }

    sortNormalLayers(a, b) {
        let layerA = this.store.getState().ref.layerArray.find((l) => (l.id === a));
        let layerB = this.store.getState().ref.layerArray.find((l) => (l.id === b));
        if(layerA.geom_type === "POLYGON" && layerB.geom_type !== "POLYGON") {
            return -1;
        }
        if(layerA.geom_type !== "POLYGON" && layerB.geom_type === "POLYGON") {
            return 1;
        }
        return 0;
    }

    //private
    createLayer(layerId){
        return new CartoMapLayerWrapper(layerId, this.store, this.map, this.cartoClient);
    }

    //private
    createGradientLayer(gradLayerId){
        return new CartoMapGradientLayerWrapper(gradLayerId, this.store, this.map, this.cartoClient);
    }

}


export default CartoLayerRenderingManager;