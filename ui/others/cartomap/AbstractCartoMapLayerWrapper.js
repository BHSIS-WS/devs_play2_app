class AbstractCartoMapLayerWrapper {

    constructor(layerId, store, map, cartoClient){
        this.layerId = layerId;
        this.store = store;
        this.map = map;
        this.cartoClient = cartoClient;
        this.layer = null;
        this.layerConfig = this.getLayerConfig();
        this.create();
    }

    hide(){
        if (this.layer && this.cartoClient){
            this.cartoClient.removeLayer(this.layer);
        }
    }

    show(){
        if (this.layer && this.cartoClient){
            this.cartoClient.addLayer(this.layer);
        }
    }

    refresh(){
        if (this.dataFetchRequired()) {
            this.create();
            return;
        }
        this.hide();
    }

    getPolygonVertices(polygon) {
        let verticesString = '';
        let paths = polygon.getPaths();

        for (let i = 0; i < paths.getLength(); i++) {
            let path = paths.getAt(i);

            verticesString += "(";
            for (let j = 0; j < path.getLength(); j++) {
                verticesString += path.getAt(j).lng().toString() + " " + path.getAt(j).lat().toString() + ",";
            }

            verticesString += path.getAt(0).lng().toString() + " " + path.getAt(0).lat().toString() + "),";
        }

        verticesString = verticesString.substring(0, verticesString.length - 1);
        return verticesString;
    }


    dataFetchRequired(){
        throw new Error("Abstract method dataFetchRequired() needs to be implemented.");
    }

    create(){
        throw new Error("Abstract method create() needs to be implemented.");
    }
}

export default AbstractCartoMapLayerWrapper;