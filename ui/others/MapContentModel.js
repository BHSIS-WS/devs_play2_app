import $ from 'jquery';
import GoogleMapsLoader from 'google-maps';
import store from '../redux/store';
import carto from '@carto/carto.js';
import {
    addMarkerValuesToMarkerList, addShapeToShapeList, closeInfoWindow, setMapCenter, setMapZoomLevel,
    setRightClickValues
} from "../actions/MapVariablesAction";
import CartoLayerRenderingManager from "./cartomap/CartoLayerRenderingManager";

class MapContentModel {

    //================ Google Map ==================

    initializeGoogleMap(store) {
        let self = this;

        if (typeof google === 'undefined') {
            let googleApiKey = appConfig.googleMapApiKey,
                googleApiLibraries = ['drawing', 'places'];
            GoogleMapsLoader.KEY = googleApiKey;
            GoogleMapsLoader.LIBRARIES = googleApiLibraries;
            GoogleMapsLoader.load(function (google) {
                self.initializeGoogleMapApiComponents(store);
                self.refreshGoogleMapLayers();
                if (!appConfig.printShowTab && appConfig.cartoShowTab && store.getState().selectedMapTab === "Print") {
                    store.dispatch({type: "CHANGE_MAP_TAB", value: "Carto"});
                }
            });
        } else {
            self.initializeGoogleMapApiComponents(store);
            self.refreshGoogleMapLayers();
            if (!appConfig.printShowTab && appConfig.cartoShowTab && store.getState().selectedMapTab === "Print") {
                store.dispatch({type: "CHANGE_MAP_TAB", value: "Carto"});
            }
        }
    }

    initializeGoogleMapApiComponents(store) {
        let self = this,
            mapCanvasElementId = "mapContentGoogle",
            center = store.getState().mapVariables.center,
            minZoomLevel = store.getState().mapVariables.minZoomLevel,
            zoomLevel = store.getState().mapVariables.zoomLevel;

        let mapOptions = {
            center: center,
            minZoomLevel: minZoomLevel,
            zoom: zoomLevel,
            zoonControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            disableDoubleClickZoom: true,
            scrollwheel: false,
            scaleControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        self.geocoder = new google.maps.Geocoder();

        // Add controls to map
        self.map = new google.maps.Map(document.getElementById(mapCanvasElementId), mapOptions);
        self.map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(document.getElementById("mapLegend"));

        self.drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: null,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['polygon']
            },
            polygonOptions: {
                fillColor: '#FF00FF',
                fillOpacity: 0.1,
                strokeColor: '#FF00FF',
                strokeWeight: 1.5
            }
        });
        self.drawingManager.setMap(self.map);

        // Add events to map
        google.maps.event.addListener(self.map, "click", function (event) {
            self.updateRightClickValuesToNull(store.getState(), store.dispatch)
        });
        google.maps.event.addListener(self.map, "rightclick", function (event) {
            self.calculateRightClickLatLng(store.dispatch, event.latLng);
        });

        google.maps.event.addListener(self.map, "center_changed", function(event) {
            if (store.getState().mapVariables.infoWindow) {
                store.dispatch(closeInfoWindow());
            }
        });

        // NOT SURE IF NEEDED - Updates state when map zooms or changes center
        // google.maps.event.addListener(self.map, "center_changed", function(event) {
        //     let center = self.map.getCenter();
        //     if (center !== null){
        //         let oldCenter = self.getCanvasXY(store.getState().mapVariables.center);
        //         let newCenter = self.getCanvasXY(center);
        //         console.log("OLD CENTER: " + oldCenter);
        //         console.log("NEW CENTER: " + newCenter);
        //         store.dispatch(setMapCenter({lat: center.lat(), lng: center.lng()}))
        //     }
        // });
        google.maps.event.addListener(self.map, "zoom_changed", function () {
            if (store.getState().mapVariables.infoWindow) {
                store.dispatch(closeInfoWindow());
            }
            store.dispatch(setMapZoomLevel(self.map.getZoom(), false));
        });
        google.maps.event.addListener(self.drawingManager, 'overlaycomplete', function (shape) {
            let shapeId = 1;
            let dragging = false;
            let existingShapes = store.getState().mapVariables.mapShapes;
            for (let i = 0 ; i < existingShapes.length; i++) {
                if (existingShapes[i].id >= shapeId) {
                    shapeId = existingShapes[i].id +1;
                }
            }
            shape.id = shapeId;
            store.dispatch(addShapeToShapeList(shape));

            self.drawingManager.setDrawingMode(null);
            google.maps.event.addListener(shape.overlay, 'rightclick', function (event) {
                let currentLatLngOffset = self.getCanvasXY(event.latLng);
                store.dispatch(setRightClickValues(event.latLng, currentLatLngOffset, null, shape));
                return false;
            });

            google.maps.event.addListener(shape.overlay, 'click', function (event) {
                self.updateRightClickValuesToNull(store.getState(), store.dispatch);
            });

            google.maps.event.addListener(shape.overlay, 'dragstart', function () {
                dragging = true;
            });

            google.maps.event.addListener(shape.overlay, 'dragend', function () {
                self.refreshGoogleMapLayers();
                dragging = false;
            });

            google.maps.event.addListener(shape.overlay.getPath(), "insert_at", function () {
                self.refreshGoogleMapLayers();
            });
            google.maps.event.addListener(shape.overlay.getPath(), "set_at", function () {
                if (!dragging) {
                    self.refreshGoogleMapLayers();
                }
            });

            google.maps.event.addListener(shape.overlay.getPath(), "remove_at", function () {
                self.refreshGoogleMapLayers();
            });
        });


        // Set width
        $("#mapContentGoogle").width($("#mainContainer").width());

        // self.refreshGoogleMapLayers();
        self.registerWindowResizeListener(store.getState(), store.dispatch);

        self.cartoClient = new carto.Client({
            apiKey: appConfig.cartoApiKey,
            username: appConfig.cartoUsername,
            serverUrl: 'https://' + appConfig.cartoHostname + '/user/' + appConfig.cartoUsername
        });

        self.cartoLayerRenderingManager = new CartoLayerRenderingManager(store, self.map, self.cartoClient);
        self.map.overlayMapTypes.push(self.cartoClient.getGoogleMapsMapType(self.map));
    }

    registerWindowResizeListener(state, dispatch) {
        let self = this;
        window.addEventListener("resize", () => {
            // const topPosition = document.getElementById('headerContainer').getBoundingClientRect().height;
            // document.getElementById('sideContainer').style.top = topPosition + "px";
            $("#mapContentGoogle").width($("#mainContainer").width());
            google.maps.event.trigger(self.map, 'resize');
        });
    }

    initializeAutocomplete(dispatch) {
        let self = this;
        let placesInputElementId = "drop-marker-input";
        if (typeof google !== "undefined" && document.getElementById(placesInputElementId) !== null) {
            self.autocomplete = new google.maps.places.Autocomplete(document.getElementById(placesInputElementId));

            self.autocompleteListener = self.autocomplete.addListener('place_changed', function () {
                self.updateSearchAddress(null, dispatch, placesInputElementId);
            });
        }
    }

    removeAutocomplete() {
        let placesInputElementId = "drop-marker-input";
        if (typeof google !== "undefined" && self.autocomplete !== null && self.autocompleteListener !== null) {
            google.maps.event.removeListener(self.autocompleteListener);
            google.maps.event.clearInstanceListeners(document.getElementById(placesInputElementId));
        }
    }

    getPolygonVertices(polygon) {
        let verticesString = '';
        let paths = polygon.getPaths();

        for (let i = 0; i < paths.getLength(); i++) {
            let path = paths.getAt(i);

            verticesString += "(";
            for (let j = 0; j < path.getLength(); j++) {
                verticesString += path.getAt(j).lng().toString() + " " + path.getAt(j).lat().toString() + ",";
            }

            verticesString += path.getAt(0).lng().toString() + " " + path.getAt(0).lat().toString() + "),";
        }

        verticesString = verticesString.substring(0, verticesString.length - 1);
        return verticesString;
    }


    triggerMapResize() {
        if (typeof google !== "undefined" && self.map !== null) {
            google.maps.event.trigger(self.map, 'resize');
        }
    }

    updateSearchAddress(address, dispatch, placesInputElementId) {
        let self = this;
        let inputAddress = document.getElementById(placesInputElementId).value;
        self.geocodePlaceWithName(inputAddress, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK && results[0] !== null) {
                let searchAddress = results[0].formatted_address;
                let sAddr = results[0].geometry.location.lat() + "," + results[0].geometry.location.lng();
                document.getElementById(placesInputElementId).value = results[0].formatted_address;
                // dispatch(changeSearchAddress(searchAddress, sAddr));
            } else {
                window.alert("No details available for input: '" + inputAddress + "'");
            }
        });
    }

    geocodePlaceWithName(name, callback) {
        let self = this;
        self.geocoder.geocode({
            'address': name
        }, function (results, status) {
            callback(results, status);
        });
    }

    refreshGoogleMapLayers() {
        let selectedMapTab = store.getState().selectedMapTab;
        if (selectedMapTab === 'Carto') {
            this.cartoLayerRenderingManager.refresh();
        }

        if(this.infowindow != null) {
            this.infowindow.close();
        }
    }

    refreshGoogleGradientMapLayers() {
        let selectedMapTab = store.getState().selectedMapTab;
        if (selectedMapTab === 'Carto') {
            this.cartoLayerRenderingManager.refreshGradLayers();
            this.cartoLayerRenderingManager.renderedGradLayers.sort((layer1,layer2) => {
                return layer1.layerConfig.display_order - layer2.layerConfig.display_order;
            });

            let gradientCartoLayers = this.cartoLayerRenderingManager.renderedGradLayers.map(layer => layer.layer);
            this.cartoLayerRenderingManager.cartoClient.addLayers(gradientCartoLayers);
        }

        if(this.infowindow != null) {
            this.infowindow.close();
        }
    }


    quoteJSONKeys(jString) {
        if (jString && jString.length >= 2 && !jString.includes("\"")) {
            let jStr = jString.trim();
            jStr = jStr.substr(1, jStr.length - 2);
            let jFieldStrs = jStr.split(",");
            let jFieldPairStrs;
            let result = "{";
            jFieldPairStrs = jFieldStrs[0].trim().split(":");
            result += "\"" + jFieldPairStrs[0].trim() + "\":" + jFieldPairStrs[1].trim();
            for (let i = 1; i < jFieldStrs.length; i++) {
                jFieldPairStrs = jFieldStrs[i].trim().split(":");
                result += ",\"" + jFieldPairStrs[0].trim() + "\":" + jFieldPairStrs[1].trim();
            }
            result += "}";
            return result;
        } else {
            return "{}";
        }
    }

    calculateRightClickLatLng(dispatch, currentLatLng) {
        let currentLatLngOffset = this.getCanvasXY(currentLatLng);
        dispatch(setRightClickValues(currentLatLng, currentLatLngOffset, null, null));
    }

    getCanvasXY(currentLatLng) {
        let self = this;
        let scale = Math.pow(2, self.map.getZoom());
        let nw = new google.maps.LatLng(
            self.map.getBounds().getNorthEast().lat(),
            self.map.getBounds().getSouthWest().lng()
        );
        let worldCoordinateNW = self.map.getProjection().fromLatLngToPoint(nw);
        // if (typeof currentLatLng.lat === "number") {
        //     currentLatLng = new google.maps.LatLng(currentLatLng.x, currentLatLng.y);
        // }
        let worldCoordinate = self.map.getProjection().fromLatLngToPoint(currentLatLng);
        let currentLatLngOffset = new google.maps.Point(
            Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale),
            Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale)
        );
        return currentLatLngOffset;
    }

    setMenuXY(state) {
        let contextMenu = document.getElementById("contextMenu");
        let mapWidth = document.getElementById('mapContentGoogle').offsetWidth;
        let mapHeight = document.getElementById('mapContentGoogle').offsetHeight;
        let menuWidth = contextMenu.offsetWidth;
        let menuHeight = contextMenu.offsetHeight;
        let x = state.mapVariables.rightClickContextMenuPosition.x;
        let y = state.mapVariables.rightClickContextMenuPosition.y;

        if ((mapWidth - x) < menuWidth)
            x = x - menuWidth;
        if ((mapHeight - y) < menuHeight)
            y = y - menuHeight;

        contextMenu.style.left = x.toString() + "px";
        contextMenu.style.top = y.toString() + "px";
    };


    calculateRightClickLatLngForMarkerOnMap(dispatch, currentLatLng, marker) {
        let currentLatLngOffset = this.getCanvasXY(currentLatLng);
        dispatch(setRightClickValues(currentLatLng, currentLatLngOffset, marker, null));
    }

    dropMarkerFromRightClickCoordinates(state, dispatch) {
        let self = this;
        let coordinates = state.mapVariables.rightClickCoordinates;
        self.dropMarker(state, dispatch, coordinates.lat(), coordinates.lng());
        self.updateRightClickValuesToNull(state, dispatch);
    }

    dropMarker(state, dispatch, latitude, longitude) {
        let self = this;
        let markerId = 1;
        let existingMarkers = store.getState().mapVariables.mapMarkers;
        for (let i = 0 ; i < existingMarkers.length; i++) {
            if (existingMarkers[i].id >= markerId) {
                markerId = existingMarkers[i].id +1;
            }
        }

        let coordinate = new google.maps.LatLng(latitude, longitude);
        let marker = new google.maps.Marker({
            position: coordinate,
            map: self.map,
            id: markerId
        });

        self.geocoder.geocode({'location': coordinate}, function (results, status) {
            let markerLocation = null;
            if (status === 'OK') {
                if (results[0]) {
                    markerLocation = results[0];
                }
            }

            marker.addListener('rightclick', function () {
                self.calculateRightClickLatLngForMarkerOnMap(dispatch, coordinate, marker);
            });

            dispatch(addMarkerValuesToMarkerList(marker, markerLocation));
        });
    }

    dropMarkerFromLocation(state, dispatch, locationString) {
        let self = this;
        self.geocoder.geocode({'address': locationString}, function (results, status) {
            let markerLocation = null;
            if (status === 'OK') {
                if (results[0]) {
                    markerLocation = results[0];
                }
            }
            self.dropMarker(state, dispatch, markerLocation.geometry.location.lat(), markerLocation.geometry.location.lng());
        });
    }

    createGoogleMapsShapeFromCoordinateString(coordinateString, index) {
        let self = this;
        let shapeCoordsArray = [];
        let coordinateArray = coordinateString.slice(1, -1).split(",");
        coordinateArray.forEach(coordinate => {
            let shapeCoords = {
                lat: parseFloat(coordinate.split(" ")[1]),
                lng: parseFloat(coordinate.split(" ")[0])
            };
            shapeCoordsArray.push(shapeCoords);
        });

        let overlay = new google.maps.Polygon({
            paths: shapeCoordsArray,
            fillColor: '#FF00FF',
            fillOpacity: 0.1,
            strokeColor: '#FF00FF',
            strokeWeight: 1.5
        });
        overlay.setMap(self.map);
        return {
            overlay: overlay,
            type: "polygon",
            id: index
        };
    }

    showInfoWindow(state) {
        let self = this;
        let infoWindowState = state.mapVariables.infoWindow;
        let latLng = new google.maps.LatLng(infoWindowState.latLng.lat, infoWindowState.latLng.lng);

        if (self.infowindow != null) {
            self.infowindow.close();
        }

        self.infowindow = new google.maps.InfoWindow({
            content: infoWindowState.htmlToDisplay,
            position: latLng
        });
        self.infowindow.open(self.map);
    }

    removeInfoWindow() {
        if (typeof self.infowindow !== "undefined") {
            self.infowindow.close();
        }
    }

    getColumnsAndHtmlFromInfoWindowTemplate(template) {
        let cartoHtmlList = [];
        if (template) {
            cartoHtmlList = template.split("+");
        }
        let columnList = [];
        let htmlList = [];
        for (let i =0; i < cartoHtmlList.length; i++) {
            let matches = cartoHtmlList[i].match("{{(.*)}}");
            if (matches && matches.length === 2) {
                columnList.push(matches[1]);
                htmlList.push(cartoHtmlList[i]);
            }
        }
        return {
            columnList,
            htmlList
        };
    }

    updateRightClickValuesToNull(state, dispatch) {
        if (state.mapVariables.rightClickContextMenuPosition !== null) {
            dispatch(setRightClickValues(null, null, null, null));
        }
    }

    zoomMapToZoomLevel(state, dispatch) {
        let self = this,
            zoomLevel = state.mapVariables.zoomLevel,
            rightClickCoordinates = state.mapVariables.rightClickCoordinates;

        self.map.setZoom(zoomLevel);
        self.map.setCenter(rightClickCoordinates);

        self.updateRightClickValuesToNull(state, dispatch);
    }

    centerMapOnRightClickLatLng(state, dispatch) {
        let self = this;
        self.map.setCenter(state.mapVariables.rightClickCoordinates);
        self.updateRightClickValuesToNull(state, dispatch);
    }

    centerMapOnCoordinate(state, coordinate) {
        let self = this;
        self.map.setCenter(coordinate);
    }

    removeMarker(marker) {
        if (marker !== null) {
            marker.setMap(null);
        }
    }

    removeRightClickedMarker(state, dispatch) {
        let self = this;
        let rightClickedMarker = state.mapVariables.rightClickedMarker;
        self.removeMarker(rightClickedMarker);
    }

    removeRightClickedShape(state, dispatch) {
        let self = this;
        let rightClickedShape = state.mapVariables.rightClickedShape;
        self.removeShape(rightClickedShape);
    }

    removeShape(shape) {
        if (shape !== null) {
            shape.overlay.setMap(null);
        }
    }

    toggleEditableRightClickedShape(state, dispatch) {
        let rightClickedShape = state.mapVariables.rightClickedShape;
        if (rightClickedShape != null) {
            rightClickedShape.overlay.setEditable(!rightClickedShape.overlay.editable);
        }
    }

    toggleDraggableRightClickedShape(state, dispatch) {
        let rightClickedShape = state.mapVariables.rightClickedShape;
        if (rightClickedShape != null) {
            rightClickedShape.overlay.setDraggable(!rightClickedShape.overlay.draggable);
        }
    }

    removeNonRightClickedMarkers(state, dispatch) {
        let self = this;
        let markers = state.mapVariables.mapMarkers;
        let rightClickedMarker = state.mapVariables.rightClickedMarker;
        markers.map(marker => {
            if (marker !== rightClickedMarker) {
                self.removeMarker(marker);
            }
        });
        self.updateRightClickValuesToNull(state, dispatch);
    }

    removeAllMarkers(state, dispatch) {
        let self = this;
        let markers = state.mapVariables.mapMarkers;
        markers.map(marker => {
            self.removeMarker(marker);
        });
        self.updateRightClickValuesToNull(state, dispatch);
    }

    removeAllShapes(state, dispatch) {
        let self = this;
        let shapes = state.mapVariables.mapShapes;
        shapes.map(shape => {
            self.removeShape(shape);
        });
        self.updateRightClickValuesToNull(state, dispatch);
    }

    getShareFacilityUrl() {
        let url = window.location.protocol + "//" + window.location.host;
        let pathName = window.location.pathname;
        url = url + pathName;

        return url;
    }

    refreshMapLayers(state, dispatch) {
        this.refreshGoogleMapLayers(state, dispatch);
    }

}

const mapContentModel = new MapContentModel();

export default mapContentModel;