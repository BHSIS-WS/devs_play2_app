import * as d3 from 'd3';
import {isGradFilterArrayDataDifferent} from '../../state/stateDataHelpers';
import {updateRenderStatusForLayer} from "../../actions/ToolbarOptionsAction";
import {
    addPrintLayerInfoBox, removePrintLayerInfoBox,
    toggleInfoBoxMaxLimitDialog
} from "../../actions/PrintInfoBoxAction";

class D3MapGradientLayer {

    constructor(gradLayerId, store, drawingManager) {
        this.gradLayerId = gradLayerId;
        this.store = store;
        this.defaultFill = "none";

        this.drawingManager = drawingManager;
        this.layerConfig = this.getLayerConfig();
        //this.layerFillColorBoundaryArray = this.getLayerFillColorBoundaryArray();
        // this.create();
    }

    create() {
        let layerG = this.drawingManager.svgg.append("g")
            .attr("class", "layer_grad_d3")
            .attr("id", "layer_grad_d3_"+this.gradLayerId);

        this.gradFilterArray = this.getGradFileterArrayFromState();
        let filterJsonStr = this.getFilterJsonString();
        this.store.dispatch(updateRenderStatusForLayer(this.gradLayerId, "gradient", "Loading"));
        d3.json("/devs2/gradGeoJson/" + this.gradLayerId+"?filterJson="+filterJsonStr, jsonData => this.drawSvg(jsonData, layerG));
    }

    refresh() {
        if (this.dataFetchRequired()) {
            this.create();
        } else {
            let existingNode = this.drawingManager.svgg.select("layer_grad_d3_"+this.gradLayerId);
            if (existingNode) {
                existingNode.remove();
            }
            let layerG = this.drawingManager.svgg.append("g")
                .attr("class", "layer_grad_d3")
                .attr("id", "layer_grad_d3_"+this.gradLayerId);
            this.drawSvg(this.geoJson, layerG);
        }
    }

    drawSvg(geoJson, layerG) {
        this.store.dispatch(updateRenderStatusForLayer(this.gradLayerId, "gradient", "Rendering"));
        if (typeof geoJson.features !== "undefined") {
            let self = this;
            this.geoJson = geoJson;
            let dm = this.drawingManager;
            let gradientConfig = this.getD3LayerConfig();
            let colorBoundaries = this.getD3LayerColorConfigArray(gradientConfig);
            let colorRange = colorBoundaries.map(c => this.hexToRgba(c.color, (c.opacity || "")));
            let colorScale = d3.scaleLinear()
                .domain([0, colorBoundaries[0].value, colorBoundaries[colorBoundaries.length - 1].value])
                .range(colorRange);
            let layerPolygonGs = layerG.selectAll("g").data(geoJson.features).enter().append("g");
            let layerPolygonPs = layerPolygonGs.append("path").attr("d", dm.path).attr("class", "layer_grad_d3_"+this.gradLayerId)
                .attr("fill", function(d) {return colorScale(d.field);})
                .attr("vector-effect","non-scaling-stroke")
                .on("click", function (d) {
                    self.onLayerShapeClick(d);
                });

            this.setBorderAttributesFromConfig(layerPolygonPs, gradientConfig.borderStyle);
            this.store.dispatch(updateRenderStatusForLayer(this.gradLayerId, "gradient", "Complete"));
        } else {
            this.store.dispatch(updateRenderStatusForLayer(this.gradLayerId, "gradient", "Nothing to Render"));
        }
    }

    setBorderAttributesFromConfig(layerPolygonPs, borderStyle) {
        if(!borderStyle) {
            layerPolygonPs.attr("stroke-width", "0.5")
                .attr("stroke", "grey");
            return;
        }

        layerPolygonPs.attr("stroke-width", borderStyle.size ? borderStyle.size : "0.5")
            .attr("stroke", borderStyle.color ? borderStyle.color : "grey")
            .attr("stroke-opacity", borderStyle.opacity ? borderStyle.opacity : "");
    }

    dataFetchRequired() {
        if (this.hasGradFilterChange()) {
            return true;
        }
        return (!this.geoJson);
    }

    hasGradFilterChange() {
        let oldGradFilterArray = this.gradFilterArray;
        let newGradFilterArray = this.getGradFileterArrayFromState();
        return isGradFilterArrayDataDifferent(oldGradFilterArray, newGradFilterArray);
    }

    getGradFileterArrayFromState() {
        let gradFilterMap = this.store.getState().gradientLayers.gradFilterMap;
        if (!gradFilterMap) {return null;}
        return gradFilterMap.get(this.gradLayerId);
    }

    getLayerConfig() {
        let layerConfigArray = this.store.getState().ref.gradientLayerArray;
        return layerConfigArray.find(layerConfig => layerConfig.id === this.gradLayerId);
    }

    getD3LayerConfig(){
        return this.store.getState().d3LayerConfig.gradientLayerConfigMap.get(this.gradLayerId);
    }

    getD3LayerColorConfigArray(gradientConfig) {
        let fillColorBoundaryArray = [...gradientConfig.gradientColorConfigArray];
        fillColorBoundaryArray.sort((a, b) => {
            return a.value - b.value;
        });
        return fillColorBoundaryArray;
    }

    hexToRgba(color, opacity) {
        const r = parseInt(color.length === 4  ? color.slice(1, 2).repeat(2) : color.slice(1, 3), 16),
            g = parseInt(color.length === 4  ? color.slice(2, 3).repeat(2) : color.slice(3, 5), 16),
            b = parseInt(color.length === 4  ? color.slice(3, 4).repeat(2) : color.slice(5, 7), 16);

        if (opacity) {
            return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + opacity + ')';
        } else {
            return 'rgb(' + r + ', ' + g + ', ' + b + ')';
        }
    }

    // getLayerFillColorBoundaryArray() {
    //     let colorBoundaryJson = JSON.parse(this.layerConfig.color_boundaries_json_string);
    //     let fillColorBoundaryArray = colorBoundaryJson.boundaries;
    //     fillColorBoundaryArray.sort((a, b) => {
    //         return a.value - b.value;
    //     });
    //     return fillColorBoundaryArray;
    // }
    //
    // getLayerFillColor(value, colorBoundaries) {
    //     if (typeof value !== 'undefined' && value !== null) {
    //         let steps = 5;
    //         if (value <= colorBoundaries[0].value) {
    //             return colorBoundaries[0].color;
    //         } else if (value > colorBoundaries[0].value && value < colorBoundaries[colorBoundaries.length - 1].value) {
    //             for (let i = 1; i < colorBoundaries.length; i++) {
    //                 if (value > colorBoundaries[i-1].value && value < colorBoundaries[i].value) {
    //                     let r = (value - colorBoundaries[i-1].value) / (colorBoundaries[i].value - colorBoundaries[i-1].value);
    //                     let colorIndex = Math.round(steps * r) - 1;
    //                     let gradientColors = this.gradientColor(colorBoundaries[i-1].color, colorBoundaries[i].color, steps);
    //                     return gradientColors[colorIndex];
    //                 }
    //             }
    //         } else if (value >= colorBoundaries[colorBoundaries.length - 1].value) {
    //             return colorBoundaries[colorBoundaries.length - 1].color;
    //         }
    //     } else {
    //         return this.defaultFill;
    //     }
    // }

    // gradientColor(startColor, endColor, steps) {
    //      let start = {
    //              'Hex'   : startColor,
    //              'R'     : parseInt(startColor.slice(1,3), 16),
    //              'G'     : parseInt(startColor.slice(3,5), 16),
    //              'B'     : parseInt(startColor.slice(5,7), 16)
    //      };
    //      let end = {
    //              'Hex'   : endColor,
    //              'R'     : parseInt(endColor.slice(1,3), 16),
    //              'G'     : parseInt(endColor.slice(3,5), 16),
    //              'B'     : parseInt(endColor.slice(5,7), 16)
    //      };
    //      let diffR = end['R'] - start['R'];
    //      let diffG = end['G'] - start['G'];
    //      let diffB = end['B'] - start['B'];
    //
    //      let stepsHex  = [];
    //      let stepsR    = [];
    //      let stepsG    = [];
    //      let stepsB    = [];
    //
    //      for(let i = 0; i <= steps; i++) {
    //              stepsR[i] = start['R'] + ((diffR / steps) * i);
    //              stepsG[i] = start['G'] + ((diffG / steps) * i);
    //              stepsB[i] = start['B'] + ((diffB / steps) * i);
    //              stepsHex[i] = '#' + Math.round(stepsR[i]).toString(16) + '' + Math.round(stepsG[i]).toString(16) + '' + Math.round(stepsB[i]).toString(16);
    //      }
    //      return stepsHex;
    // }

    getFilterJsonString() {
        let state = this.store.getState(),
            gradFilterMap = state.gradientLayers.gradFilterMap;

        let fieldColumnName = this.layerConfig.field_column_name,
            gradFilterEntryArray = gradFilterMap.get(this.gradLayerId);

        let filter = {};
        if (typeof gradFilterEntryArray !== "undefined" && gradFilterEntryArray.length > 0) {
            filter.fieldColumnName = fieldColumnName;
            filter.filterEntries = gradFilterEntryArray;
        }
        return JSON.stringify(filter);
    }

    onLayerShapeClick(d) {
        let self = this,
            layerInfoBoxId = "gradient" + self.gradLayerId + "-" + d.id,
            printInfoBoxState = self.store.getState().printInfoBox;
        if (!printInfoBoxState.printInfoBoxSet.has(layerInfoBoxId) && printInfoBoxState.printInfoBoxSet.size < printInfoBoxState.maxInfoBox) {
            self.addLayerInfoBox(d, layerInfoBoxId);
            self.store.dispatch(addPrintLayerInfoBox(layerInfoBoxId));
        } else if (printInfoBoxState.printInfoBoxSet.size >= printInfoBoxState.maxInfoBox) {
            self.store.dispatch(toggleInfoBoxMaxLimitDialog(true));
        }
    }

    addLayerInfoBox(d, idx) {
        let self = this,
            currentPosition = d3.mouse(self.drawingManager.svgg.node()),
            addressList = ["countyname", "statename", "name", "state_abbreviation"],
            infoIndex = 0,
            scale = 1/self.drawingManager.zoomValue,
            h = 31 * scale, hy = 30 * scale, positionOffset = 2 * scale, arrowScale = 15 * scale,
            xyOffset = 10 * scale,
            fontSize = 16 * scale + "px", headingFontSize = 18 * scale + "px",
            headerFillColor = "#426288", fillColor = "#ebf7f7",
            xPosition, yPosition, infoBoxDrag, dragStartPos = {};

        infoBoxDrag = d3.drag()
            .on("start", function() {
                dragStartPos.x = d3.event.x;
                dragStartPos.y = d3.event.y;
                console.log("drag start");
            })
            .on("drag", function() {
                dragStartPos.dX = Math.abs(dragStartPos.x - d3.event.x);
                dragStartPos.dY = Math.abs(dragStartPos.y - d3.event.y);
                if(Math.max(dragStartPos.dX, dragStartPos.dY) >= 1) {
                    console.log("dragged");
                    d3.select(this).attr("transform", function () {
                        return "translate(" + d3.event.x + "," + d3.event.y + ")";
                    });
                    console.log("start creating line");
                    if(d3.select("#print-info-box-line-" +idx).empty()) {
                        self.drawingManager.svgg
                            .append("line")
                            .attr("class", "print-info-g")
                            .attr("id", "print-info-box-line-" +idx)
                            .attr("x1", d3.event.x + (self.currentInfoBoxWidth/2))
                            .attr("y1", d3.event.y + (d3.select("#print-info-group-" +idx).node().getBBox().height))
                            .attr("x2", currentPosition[0] + positionOffset)
                            .attr("y2", currentPosition[1] + positionOffset)
                            .attr("vector-effect", "non-scaling-stroke")
                            .style("stroke-dasharray", "4")
                            .style("stroke", headerFillColor)
                            .style("stroke-width", "3");
                    } else {
                        d3.select("#print-info-box-line-" +idx)
                            .attr("x1", d3.event.x + (self.currentInfoBoxWidth/2))
                            .attr("y1", d3.event.y + (d3.select("#print-info-group-" +idx).node().getBBox().height));
                    }
                }
            });

        self.drawingManager.svgg
            .append("g")
            .attr("class", "print-info-g")
            .attr("id", "print-info-group-" +idx)
            .style("cursor", "pointer")
            .call(infoBoxDrag);

        d3.select("#print-info-group-" +idx)
            .append("rect")
            .attr("class", "print-info-rect-box-" +idx)
            .attr("x", 0)
            .attr("y", 0)
            .style("stroke", "#a2a6af")
            .style("stroke-width", scale)
            .attr("height", h)
            .style("fill", headerFillColor);

        d3.select("#print-info-group-" +idx)
            .append("text")
            .text(self.layerConfig.name)
            .attr("x", xyOffset)
            .attr("y", -xyOffset)
            .attr("dx", 0.1)
            .attr("dy", hy - 0.1 / 2)
            .style("fill", "#FFFFFF")
            .style("font-size", headingFontSize)
            .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');

        //infobox details.
        d3.select("#print-info-group-" +idx)
            .append("g")
            .attr("id", "print-info-box-" +idx);

        if (d.detail !== null && typeof d.detail === "object") {
            Object.keys(d.detail).forEach((key, i) => {
                const info = d.detail[key];
                if(addressList.includes(key) && !d3.select(".print-info-address-text-" +idx).empty() && info.displayName === "null") {
                    const addressText = d3.select(".print-info-address-text-" +idx).text() + ", " + info.value;
                    d3.select(".print-info-address-text-" +idx).text(addressText);
                } else {
                    infoIndex++;
                    d3.select("#print-info-box-" +idx)
                        .append("rect")
                        .attr("class", "print-info-rect-box-" +idx)
                        .attr("x", 0)
                        .attr("y", infoIndex * hy)
                        .attr("height", h)
                        .style("fill", fillColor);

                    d3.select("#print-info-box-" +idx)
                        .append("text")
                        .text(info.displayName !== "null" ? info.displayName + ":    " + info.value : info.value)
                        .attr("class", addressList.includes(key) ? "print-info-address-text-" +idx : "")
                        .attr("x", xyOffset)
                        .attr("y", (infoIndex * hy) - xyOffset)
                        .attr("dx", 0.1)
                        .attr("dy", hy - 0.1 / 2)
                        .style("font-size", fontSize)
                        .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');
                }
            });
        } else {
            infoIndex++;
            d3.select("#print-info-box-" +idx)
                .append("rect")
                .attr("class", "print-info-rect-box-" +idx)
                .attr("x", 0)
                .attr("y", hy)
                .attr("height", h)
                .style("fill", fillColor);

            d3.select("#print-info-box-" +idx)
                .append("text")
                .text("No Data Found")
                .attr("x", xyOffset)
                .attr("y", hy - xyOffset)
                .attr("dx", 0.1)
                .attr("dy", hy - 0.1 / 2)
                .style("font-size", fontSize)
                .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');
        }

        //hiv footer exception. 41 is the dataset id for the HIV layers
        if(self.layerConfig.data_set_id === 41) {
            self.footerDetailInfo(idx, scale);
        }

        //width and close button
        let infoBox = d3.select("#print-info-group-" +idx).node().getBBox();
        self.currentInfoBoxWidth = self.currentInfoBoxWidth !== infoBox.width ? infoBox.width + (50 * scale) : self.currentInfoBoxWidth;
        d3.selectAll(".print-info-rect-box-" +idx).attr("width", self.currentInfoBoxWidth);

        d3.select("#print-info-group-" +idx)
            .append("text")
            .text("X")
            .attr("x", infoBox.width + (30 * scale))
            .attr("y", -xyOffset)
            .attr("dx", 0.1)
            .attr("dy", hy - 0.1 / 2)
            .style("fill", "#FFFFFF")
            .style("font-size", fontSize)
            .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif')
            .on("click", function () {
                self.removeInfoBox(idx);
            });

        d3.select("#print-info-group-" +idx)
            .append("path")
            .attr("d", "m0.28932,0.39558l1.41135,-0.00138l-0.69829,1.30647l-0.71306,-1.30509l0,0z")
            .attr("id", "print-info-arrow")
            .attr("vector-effect","non-scaling-stroke")
            .attr("fill", fillColor)
            .attr("transform", "translate(" + (self.currentInfoBoxWidth/2 - arrowScale) + "," + infoBox.height + ") scale(" + arrowScale + ")");

        xPosition = currentPosition[0] - (self.currentInfoBoxWidth/2) + positionOffset;
        yPosition = currentPosition[1] - (d3.select("#print-info-group-" +idx).node().getBBox().height) + positionOffset;
        d3.select("#print-info-group-" +idx).attr("transform", "translate(" + xPosition + "," + yPosition + ")");
    }

    footerDetailInfo(idx, scale) {
        let h = 21 * scale, hy = 20 * scale,
            topMargin = 20 * scale,
            xyOffset = 10 * scale,
            fontSize = 14 * scale + "px",
            fillColor = "#ebf7f7",
            currentBoxHeight = d3.select("#print-info-group-" +idx).node().getBBox().height,
            footerData = ["* Data Description", "-1 = Data Suppressed", "-2 = Data not available"];

        footerData.map(function (data, i) {
            d3.select("#print-info-box-" +idx)
                .append("rect")
                .attr("class", "print-info-rect-box-" +idx)
                .attr("x", 0)
                .attr("y", i === 0 ? currentBoxHeight + (i * hy) : currentBoxHeight + (i * hy) + topMargin)
                .attr("height", i === 0 ? (h + topMargin) : h)
                .style("fill", fillColor);

            d3.select("#print-info-box-" +idx)
                .append("text")
                .text(data)
                .attr("x", xyOffset)
                .attr("y", currentBoxHeight + ((i * hy) - xyOffset + topMargin))
                .attr("dx", 0.1)
                .attr("dy", hy - 0.1 / 2)
                .style("font-size", fontSize)
                .style("font-style", "italic")
                .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');
        });
    }

    removeInfoBox(layerInfoBoxId) {
        d3.event.preventDefault();
        d3.select("#print-info-group-" +layerInfoBoxId).remove();
        d3.select("#print-info-box-line-" +layerInfoBoxId).remove();
        this.store.dispatch(removePrintLayerInfoBox(layerInfoBoxId));
    }

}

export default D3MapGradientLayer;