import * as d3 from 'd3';
import {isLayerCriteriaMapDataDifferent} from '../../state/stateDataHelpers'
import { buildNormalLayerQuery, buildNormalLayerQueryCarto, buildGradientLayerQuery } from '../../state/layerCriteriaHelper';
import { updateRenderStatusForLayer } from "../../actions/ToolbarOptionsAction";
import {addPrintLayerInfoBox, removePrintLayerInfoBox, toggleInfoBoxMaxLimitDialog} from "../../actions/PrintInfoBoxAction";
import {fetchLayerDataList, infoWindowLinkClicked} from "../../actions/LayerDatatListingAction";

class D3MapNormalLayer{

    constructor(layerId, store, drawingManager){
        this.layerId = layerId;
        this.store = store;
        this.drawingManager=drawingManager;
        this.layerConfig = this.getLayerConfig();
    }

    create() {
        this.selectedCriteriaCodeAndIdMap = this.getSelectedCriteriaCodeAndIdMapFromState();
        let parameterJsonStr = this.createRequestParameterJsonString();
        this.store.dispatch(updateRenderStatusForLayer(this.layerId, "normal", "Loading"));
        let layerG = this.drawingManager.svgg.append("g").attr("class", "layer_normal_d3").attr("id", "layer_d3_"+this.layerId);
        d3.request("/devs2/geoJson/"+this.layerId)
            .header("Content-Type", "application/json")
            .post(
                JSON.stringify({parameterJson: parameterJsonStr}),
                xhr => {
                    this.drawSvg(JSON.parse(xhr.response), layerG);
                }
            );
    }


    refresh(){
        if (this.dataFetchRequired()){
            this.create();
        }else {
            let existingNode= this.drawingManager.svgg.select("#layer_d3_"+this.layerId);
            if (existingNode) {
                existingNode.remove();
            }
            let layerG = this.drawingManager.svgg.append("g")
                .attr("class", "layer_normal_d3")
                .attr("id", "layer_d3_"+this.layerId);
            this.drawSvg(this.geoJson, layerG);
        }
    }


    getShapeStyle (shapeConfigMap, d, selectedVariable) {
         if (!shapeConfigMap ||!selectedVariable||!d){
             return { shapeId:1, fillColor: "none", strokeColor:'black', strokeSize: '1px' };
         }
         let value = d[selectedVariable];
         if (!value) {
             return { shapeId:1, fillColor: "none", strokeColor:'black', strokeSize: '1px' };
         }
         let style = shapeConfigMap.get(value);
         if (!style){
             return { shapeId:1, fillColor: "none", strokeColor:'black', strokeSize: '1px' };
         }
         return style;
    }

    appendShapeOrShapeArrayByShapeStyle(parentNode , shapeStyle){
        // let svgTag = "circle";
        // let shapeId = shapeStyle.shapeId;
        // switch (shapeId) {
        //     case 1:
        //         break;
        //     case 2:
        //         svgTag="rect";
        //         break;
        //     default:
        //         svgTag="path";
        // }
        let svgShapeDefinition = this.getSvgShapeDefinitionById(shapeStyle.shapeId);

        if (!svgShapeDefinition){
            console.log ("SvgShapeDefinition not found for shapeId="+shapeStyle.shapeId);
            return parentNode.append("path")
        }

        let svgTag = svgShapeDefinition.svgTag;
        if (!svgTag){
            console.log ("SvgShapeDefinition.svgTag not found for shapeId="+shapeStyle.shapeId);
            return parentNode.append("path")
        }

        return parentNode.append(svgTag);
    }

    setNodeAllAttributesFromShapeStyle(appendedShape, shapeStyle, coordFunc, width, height, calculatedScale){
       this.setShapeBasicAttributesFromShapeStyle(appendedShape, shapeStyle, coordFunc, width, height, calculatedScale);
       this.setStyleAttributesFromShapeStyle(appendedShape, shapeStyle);
   }

   setupSvgDefinitionMap(){
        let svgShapeDefinitionMap = new Map();
        let svgShapes=this.store.getState().ref.svgShapes;
        svgShapes.forEach(e => svgShapeDefinitionMap.set(e.id,e));
        this.svgShapeDefinitionMap=svgShapeDefinitionMap;
   }

   getSvgShapeDefinitionById(shapeId){
        if (!this.svgShapeDefinitionMap) this.setupSvgDefinitionMap();
        return this.svgShapeDefinitionMap.get(shapeId);

   }

   setShapeBasicAttributesFromShapeStyle(appendedShape, shapeStyle, coordFunc, width, height, calculatedScale){
       let shapeId = shapeStyle.shapeId;
       let svgShapeDefinition = this.getSvgShapeDefinitionById(shapeId);
       if (svgShapeDefinition==null){console.log ("svgShapeDefinition not found for shapeId: "+shapeId +", default circle is used");
            return appendedShape;
       }

       svgShapeDefinition.svgAttributes.forEach(attr =>{
           appendedShape.attr(attr.name,attr.value)
       });

       //apply x, y for point shape
       if (svgShapeDefinition.groupType==="POINT") {
           appendedShape.attr("transform", d => {
               let coordD = coordFunc(d);
               if (coordD == null || coordD.xd == null || coordD.yd == null) {
                   return "translate(0,0) scale(" + calculatedScale + ")";
               }

               if (width == null || height == null) {
                   return "translate(" + coordD.xd + "," + coordD.yd + ") scale(" + calculatedScale + ")";
               }

               const centerX = coordD.xd + width / 2;
               const centerY = coordD.yd + height / 2;
               return "translate(" + centerX + "," + centerY + ") scale(" + calculatedScale + ")";

           });
           appendedShape.attr("class", this.drawingManager.pointLayerElementClassName);
       }
   }


    setStyleAttributesFromShapeStyle(appendedShape, shapeStyle) {
        let self = this;

        if (!shapeStyle){
           appendedShape.attr("vector-effect","non-scaling-stroke")
               .attr("stroke", "black")
               .attr("fill", "transparent")
               .on("click", function(d) {
                   self.onLayerShapeClick(d);
               });
           return;
        }

        appendedShape.attr("vector-effect","non-scaling-stroke")
            .attr("stroke", shapeStyle.strokeColor? shapeStyle.strokeColor : "black")
            .attr("fill", shapeStyle.fillColor && shapeStyle.fillColor !== "none" ? shapeStyle.fillColor : "transparent")
            .attr("fill-opacity", shapeStyle.fillOpacity? shapeStyle.fillOpacity : "")
            .attr("stroke-opacity", shapeStyle.strokeOpacity? shapeStyle.strokeOpacity : "")
            .attr("stroke-width", shapeStyle.strokeSize ? shapeStyle.strokeSize : "")
            .on("click", function(d) {
                self.onLayerShapeClick(d);
            });

        //For debug: overlay a circle over the shape to see if the center of non-circle shape is set correctly
        // layerElems.append("circle")
        //     .attr("cx", d => x(d))
        //     .attr("cy", d => y(d))
        //     .attr("r", "1px")
        //     .attr("stroke", "yellow")
        //     .attr("fill", "none")
        //     .attr("vector-effect","non-scaling-stroke")
    }

    onLayerShapeClick(d) {
        let self = this,
            layerInfoBoxId = "normal" + self.layerConfig.id + "-" + d.id,
            printInfoBoxState = self.store.getState().printInfoBox;
        if (!printInfoBoxState.printInfoBoxSet.has(layerInfoBoxId) && printInfoBoxState.printInfoBoxSet.size < printInfoBoxState.maxInfoBox) {
            self.addLayerInfoBox(d, layerInfoBoxId);
            self.store.dispatch(addPrintLayerInfoBox(layerInfoBoxId));
        } else if (printInfoBoxState.printInfoBoxSet.size >= printInfoBoxState.maxInfoBox) {
            self.store.dispatch(toggleInfoBoxMaxLimitDialog(true));
        }
    }

    addLayerInfoBox(d, idx) {
        let self = this,
            store = self.store.getState(),
            currentPosition = d3.mouse(self.drawingManager.svgg.node()),
            addressList = ['lcity', 'lst', 'lzip5', 'city', 'state', 'zip', 'st', 'zip5', 'mloccity', 'mstate', 'mloczip'],
            phoneJoinList = ['area', 'telno'],
            infoIndex = 0,
            scale = 1/self.drawingManager.zoomValue,
            h = 31 * scale, hy = 30 * scale, positionOffset = 2 * scale, arrowScale = 15 * scale,
            xyOffset = 10 * scale,
            fontSize = 16 * scale + "px", headingFontSize = 18 * scale + "px",
            headerFillColor = "#426288", fillColor = "#ebf7f7",
            totalSelectedLayers = store.gradientLayers.selectedGradLayerIdSet.size + store.normalLayers.selectedLayerIdSet.size,
            xPosition, yPosition, infoBoxDrag, dragStartPos = {};

        infoBoxDrag = d3.drag()
            .on("start", function() {
                dragStartPos.x = d3.event.x;
                dragStartPos.y = d3.event.y;
                console.log("drag start");
            })
            .on("drag", function() {
                dragStartPos.dX = Math.abs(dragStartPos.x - d3.event.x);
                dragStartPos.dY = Math.abs(dragStartPos.y - d3.event.y);
                if(Math.max(dragStartPos.dX, dragStartPos.dY) >= 1) {
                    console.log("dragged");
                    d3.select(this).attr("transform", function () {
                        return "translate(" + d3.event.x + "," + d3.event.y + ")";
                    });
                    console.log("start creating line");
                    if(d3.select("#print-info-box-line-" +idx).empty()) {
                        self.drawingManager.svgg
                            .append("line")
                            .attr("class", "print-info-g")
                            .attr("id", "print-info-box-line-" +idx)
                            .attr("x1", d3.event.x + (self.currentInfoBoxWidth/2))
                            .attr("y1", d3.event.y + (d3.select("#print-info-group-" +idx).node().getBBox().height))
                            .attr("x2", currentPosition[0] + positionOffset)
                            .attr("y2", currentPosition[1] + positionOffset)
                            .attr("vector-effect", "non-scaling-stroke")
                            .style("stroke-dasharray", "4")
                            .style("stroke", headerFillColor)
                            .style("stroke-width", "3");
                    } else {
                        d3.select("#print-info-box-line-" +idx)
                            .attr("x1", d3.event.x + (self.currentInfoBoxWidth/2))
                            .attr("y1", d3.event.y + (d3.select("#print-info-group-" +idx).node().getBBox().height));
                    }
                }
            });

        self.drawingManager.svgg
            .append("g")
            .attr("class", "print-info-g")
            .attr("id", "print-info-group-" + idx)
            .style("cursor", "pointer")
            .call(infoBoxDrag);

        d3.select("#print-info-group-" + idx)
            .append("rect")
            .attr("class", "print-info-rect-box-" +idx)
            .attr("x", 0)
            .attr("y", 0)
            .style("stroke", "#a2a6af")
            .style("stroke-width", scale)
            .attr("height", h)
            .style("fill", headerFillColor);

        d3.select("#print-info-group-" +idx)
            .append("text")
            .text(self.layerConfig.layer_name)
            .attr("x", xyOffset)
            .attr("y", -xyOffset)
            .attr("dx", 0.1)
            .attr("dy", hy - 0.1 / 2)
            .style("fill", "#FFFFFF")
            .style("font-size", headingFontSize)
            .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');

        //infobox details.
        d3.select("#print-info-group-" +idx)
            .append("g")
            .attr("id", "print-info-box-" +idx);

        if (d.detail !== null && typeof d.detail === "object") {
            Object.keys(d.detail).forEach((key) => {
                let info = d.detail[key],
                    infoBoxClassName = "";
                if((info.value !== null) && (info.displayName.indexOf("?") === -1 || info.value === "1")) {
                    if(addressList.includes(key) && !d3.select(".print-info-address-text-" +idx).empty()) {
                        const newValue = info.displayName !== "null" ? info.displayName + ":    " + info.value : info.value,
                            addressText = d3.select(".print-info-address-text-" +idx).text() + ", " + newValue;
                        d3.select(".print-info-address-text-" +idx).text(addressText);
                    } else if(phoneJoinList.includes(key) && !d3.select(".print-info-phone-text-" +idx).empty() && info.displayName === "null") {
                        d3.select(".print-info-phone-text-" +idx).text(d3.select(".print-info-phone-text-" +idx).text() + info.value);
                    } else {
                        infoIndex++;
                        d3.select("#print-info-box-" +idx)
                            .append("rect")
                            .attr("class", "print-info-rect-box-" +idx)
                            .attr("x", 0)
                            .attr("y", infoIndex * hy)
                            .attr("height", h)
                            .style("fill", fillColor);

                        if(addressList.includes(key)) {
                            infoBoxClassName = "print-info-address-text-" + idx;
                        } else if (phoneJoinList.includes(key)) {
                            infoBoxClassName = "print-info-phone-text-" + idx;
                        }
                        //includes special case for the medication assisted therapy layer for 0 and 1 value
                        d3.select("#print-info-box-" +idx)
                            .append("text")
                            .text(function() {
                                if (info.displayName.indexOf("?") !== -1) {
                                    return info.displayName.replace(/\?/g, "");
                                } else {
                                    return info.displayName !== "null" ? info.displayName + ":    " + info.value : info.value;
                                }
                            })
                            .attr("class", infoBoxClassName)
                            .attr("x", xyOffset)
                            .attr("y", (infoIndex * hy) - xyOffset)
                            .attr("dx", 0.1)
                            .attr("dy", hy - 0.1 / 2)
                            .style("font-size", fontSize)
                            .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');
                    }
                }
            });
        } else {
            infoIndex++;
            d3.select("#print-info-box-" +idx)
                .append("rect")
                .attr("class", "print-info-rect-box-" +idx)
                .attr("x", 0)
                .attr("y", hy)
                .attr("height", h)
                .style("fill", fillColor);

            d3.select("#print-info-box-" +idx)
                .append("text")
                .text("No Data Found")
                .attr("x", xyOffset)
                .attr("y", hy - xyOffset)
                .attr("dx", 0.1)
                .attr("dy", hy - 0.1 / 2)
                .style("font-size", fontSize)
                .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');
        }

        //add show in the list link
        if (!(self.layerConfig.geom_type === "POLYGON" && totalSelectedLayers > 1)) {
            self.infoBoxListLink(idx, infoIndex, h, hy, xyOffset, fillColor, fontSize, d);
        }

        //width and close button
        let infoBox = d3.select("#print-info-group-" +idx).node().getBBox();
        self.currentInfoBoxWidth = self.currentInfoBoxWidth !== infoBox.width ? infoBox.width + (50 * scale) : self.currentInfoBoxWidth;
        d3.selectAll(".print-info-rect-box-" +idx).attr("width", self.currentInfoBoxWidth);

        d3.select("#print-info-group-" +idx)
            .append("text")
            .text("X")
            .attr("x", infoBox.width + (30 * scale))
            .attr("y", -xyOffset)
            .attr("dx", 0.1)
            .attr("dy", hy - 0.1 / 2)
            .style("fill", "#FFFFFF")
            .style("font-size", fontSize)
            .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif')
            .on("click", function () {
                self.removeInfoBox(idx);
            });

        d3.select("#print-info-group-" +idx)
            .append("path")
            .attr("d", "m0.28932,0.39558l1.41135,-0.00138l-0.69829,1.30647l-0.71306,-1.30509l0,0z")
            .attr("id", "print-info-arrow")
            .attr("vector-effect","non-scaling-stroke")
            .attr("fill", fillColor)
            .attr("transform", "translate(" + (self.currentInfoBoxWidth/2 - arrowScale) + "," + infoBox.height + ") scale(" + arrowScale + ")");

        xPosition = currentPosition[0] - (self.currentInfoBoxWidth/2) + positionOffset;
        yPosition = currentPosition[1] - (d3.select("#print-info-group-" +idx).node().getBBox().height) + positionOffset;
        d3.select("#print-info-group-" +idx).attr("transform", "translate(" + xPosition + "," + yPosition + ")");
    }

    infoBoxListLink(idx, infoIndex, h, hy, xyOffset, fillColor, fontSize, d) {
        let self = this;

        d3.select("#print-info-box-" +idx)
            .append("rect")
            .attr("class", "print-info-rect-box-" +idx)
            .attr("x", 0)
            .attr("y", (infoIndex + 1) * hy)
            .attr("height", h)
            .style("fill", fillColor);

        d3.select("#print-info-box-" +idx)
            .append("text")
            .text("Show in the List")
            .attr("x", xyOffset)
            .attr("y", (hy * (infoIndex + 1)) - xyOffset)
            .attr("dx", 0.1)
            .attr("dy", hy - 0.1 / 2)
            .style("fill", "#00008b")
            .style("text-decoration", "underline")
            .style("font-size", fontSize)
            .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif')
            .on("click", function () {
                self.store.dispatch(infoWindowLinkClicked(String(d.id)));
                self.store.dispatch(fetchLayerDataList());
            });
    }

    removeInfoBox(layerInfoBoxId) {
        d3.event.preventDefault();
        d3.select("#print-info-group-" +layerInfoBoxId).remove();
        d3.select("#print-info-box-line-" +layerInfoBoxId).remove();
        this.store.dispatch(removePrintLayerInfoBox(layerInfoBoxId));
    }

    getSelectedShapeConfigMap(){
        let selectedVariable = this.getSelectedVariableForStyle();
        if (!selectedVariable) return null;
        let d3LayerConfig = this.getD3LayerConfig();
        let shapeConfigMap = d3LayerConfig.shapeConfigMap;
        if (!shapeConfigMap) return null;
        return shapeConfigMap.get(selectedVariable);
    }

    renderAllPointShapesByCategoricalVariable(layerG, coordFunc) {
        let self = this;
        let selectedVariable = this.getSelectedVariableForStyle();
        let theShapeConfigMap = this.getSelectedShapeConfigMap();
        let width, height;
        let index = 0;
        let calculatedScale = this.getCalculatedScale();
        this.geoJson.features.forEach(function(d) {
            let shapeStyle = self.getShapeStyle(theShapeConfigMap, d, selectedVariable);
            let appendedShape = self.appendShapeOrShapeArrayByShapeStyle(layerG,shapeStyle);
            appendedShape.datum(d);

            if (index === 0) {
                const bbox = appendedShape.node().getBBox();
                width = bbox.width;
                height = bbox.height;
            }
            index++;
            self.setNodeAllAttributesFromShapeStyle(appendedShape, shapeStyle,coordFunc, width, height, calculatedScale);
        });
    }

    renderAllBoundaryShapesByCategoricalVariable(layerG) {
        let self = this;
        let selectedVariable = this.getSelectedVariableForStyle();
        let theShapeConfigMap = this.getSelectedShapeConfigMap();
        this.geoJson.features.forEach(function(d) {
            let shapeStyle = self.getShapeStyle(theShapeConfigMap, d,selectedVariable);
            let appendedPathNode = layerG.append('path').datum(d)
                .attr("d", self.drawingManager.path);
            self.setStyleAttributesFromShapeStyle(appendedPathNode, shapeStyle);
        });
    }


    renderAllPointShapesBySameStyle(layerG, shapeStyleForAll,coordFunc){
        let layerElems = layerG.selectAll("path")
            .data(this.geoJson.features).enter();
        this.getCalculatedScale();
        let appendedShapeArray=this.appendShapeOrShapeArrayByShapeStyle(layerElems ,shapeStyleForAll);
        this.setNodeAllAttributesFromShapeStyle(appendedShapeArray,shapeStyleForAll,coordFunc);
    }

    renderAllBoundaryShapesBySameStyle(layerG, shapeStyleForAll){
        let layerElems = layerG.selectAll("path")
            .data(this.geoJson.features).enter();
        let appendedPathArray = layerElems.append("path").attr("d", this.drawingManager.path);
        this.setStyleAttributesFromShapeStyle(appendedPathArray,shapeStyleForAll);
    }

    renderPointLayer(layerG){
        let dm = this.drawingManager;

        let d3LayerConfig = this.getD3LayerConfig();

        let coordFunc = function (d) {
            if (!d.geometry) return null;
            let newCoordinates = dm.projection(d.geometry.coordinates);
            return newCoordinates ? {xd: newCoordinates[0], yd: newCoordinates[1]} : null
        };

        let useSameShapeStyle = !this.isStyledByVariable();
        if (useSameShapeStyle) {
            let shapeStyleforAll = d3LayerConfig.shapeForAllStyle;
            this.renderAllPointShapesBySameStyle(layerG, shapeStyleforAll, coordFunc);
            this.store.dispatch(updateRenderStatusForLayer(this.layerId, "normal", "Complete"));
            return;
        }

        //shape by variable
        // let shapeConfigMap = d3LayerConfig.shapeConfigMap;
        this.renderAllPointShapesByCategoricalVariable(layerG, coordFunc);
        this.store.dispatch(updateRenderStatusForLayer(this.layerId, "normal", "Complete"));


    }

    renderBoundaryLayer(layerG){

        let d3LayerConfig = this.getD3LayerConfig();

        if (!d3LayerConfig){
            this.renderAllBoundaryShapesBySameStyle(layerG, null);
            return;
        }

        let useSameShapeStyle = !this.isStyledByVariable();
        if (useSameShapeStyle) {
            let shapeStyleforAll = d3LayerConfig.shapeForAllStyle;
            this.renderAllBoundaryShapesBySameStyle(layerG, shapeStyleforAll);
            this.store.dispatch(updateRenderStatusForLayer(this.layerId, "normal", "Complete"));
            return;
        }

        //shape by variable
        // let shapeConfigMap = d3LayerConfig.shapeConfigMap;
        this.renderAllBoundaryShapesByCategoricalVariable(layerG);
        this.store.dispatch(updateRenderStatusForLayer(this.layerId, "normal", "Complete"));

    }

    getD3LayerConfig(){
        return this.store.getState().d3LayerConfig.layerConfigMap.get(this.layerId);
    }

    getSelectedVariableForStyle() {
        let d3LayerConfig = this.getD3LayerConfig();
        if (d3LayerConfig) return d3LayerConfig.selectedVariableForStyle;
        return null;
    }

    isStyledByVariable(){
        let d3LayerConfig = this.getD3LayerConfig();
        if (d3LayerConfig) return d3LayerConfig && d3LayerConfig.configShapeStyleByVariable;
        return null;
    }

    isShowOnlyCommon(){
        let d3LayerConfig = this.getD3LayerConfig();
        if (d3LayerConfig) return d3LayerConfig && d3LayerConfig.commonShapeForAllLayers;
        return null;
    }

    drawSvg(geoJson, layerG){

        this.store.dispatch(updateRenderStatusForLayer(this.layerId, "normal", "Rendering"));

        this.geoJson=geoJson;

        if (!geoJson || !geoJson.features){
            this.store.dispatch(updateRenderStatusForLayer(this.layerId, "normal", "Nothing to Render"));
            console.log ("drawSvg() empty geoJson, nothing to render!");
            return;
        }

        let layerGeoType=this.getGeomType();

        if (layerGeoType === "POINT") {
            this.renderPointLayer(layerG);
        } else if (layerGeoType === "POLYGON") {
            this.renderBoundaryLayer(layerG);
        }

        this.drawingManager.setViewBox();
    }


    dataFetchRequired() {

        if (this.isShowOnlyCommon()){
            return true;
        }

        if (this.hasFilterCriteriaChanged()){
            console.log ("D3MapNormalLayer.dataFetchRequired() criteria changed!");
            return true;
        }
        console.log ("D3MapNormalLayer.dataFetchRequired() criteria NOT changed!");
        if (!this.geoJson) return true;//not yet has a geoJson

        return !this.geoJsonHasFieldNeededToStyle();
        // return false;
    }

    geoJsonHasFieldNeededToStyle() {
        if (!this.isStyledByVariable())return true;
        let selectedVariable = this.getSelectedVariableForStyle();
        if (!selectedVariable) return true;
        if (!this.geoJson) return false;
        let fieldArrayInGeoJson = this.geoJson.additionalFields;
        if (!fieldArrayInGeoJson || fieldArrayInGeoJson.length === 0) {
            return false;
        }
        return fieldArrayInGeoJson.includes(selectedVariable);
    }

    hasFilterCriteriaChanged() {
        let oldSelectedCriteriaData=this.selectedCriteriaCodeAndIdMap;
        let newSelectedCriteriaData=this.getSelectedCriteriaCodeAndIdMapFromState();

        console.log ("!D3 layer hasFilterCriteriaChanged() "
            +isLayerCriteriaMapDataDifferent(oldSelectedCriteriaData,newSelectedCriteriaData))//debug

        //has selected Criterias changed?
        return isLayerCriteriaMapDataDifferent(oldSelectedCriteriaData,newSelectedCriteriaData);
    }

    getSelectedCriteriaCodeAndIdMapFromState() {
        let selectedCriteriaIdMap =  this.store.getState().normalLayers.selectedCriteriaIdMap;
        if (!selectedCriteriaIdMap) return null;
        return selectedCriteriaIdMap.get(this.layerId);
    }


    getLayerConfig(){
        let layerConfigArray = this.store.getState().ref.layerArray;
        return layerConfigArray.find(layerConfig => layerConfig.id === this.layerId);
    }

    getGeomType(){
        return this.layerConfig.geom_type;
    }


    createRequestParameterJsonString() {
        let criteria = this.getCriteriaJson();
        let dynamicConditions = this.getDynamicConditionsJson();
        let additionalFields = this.getAdditionalFieldsJson();
        let ret = {"layerCriteria": criteria};
        if (additionalFields){
            ret.additionalFields=additionalFields;
        }
        if (dynamicConditions) {
            ret.dynamicConditions = dynamicConditions;
        }
        if (this.isShowOnlyCommon()){
            console.log ("isShowOnlyCommon() true for layer="+this.layerId);
            let allLayersCriteriaJson = this.createAllLayersCriteriaJson();
            if (allLayersCriteriaJson){
                ret.allLayersCriteria = allLayersCriteriaJson;
            }
            let gradientLayersJson = this.createGradientLayersJson();
            if (gradientLayersJson) {
                ret.gradientLayers = gradientLayersJson;
            }
        }

        return JSON.stringify(ret);
    }

    createRequestParameterJsonStringForCarto() {
        let criteria = this.getCriteriaJson();
        let dynamicConditions = this.getDynamicConditionsJson();
        let additionalFields = this.getAdditionalFieldsJson();
        let ret = {"layerCriteria": criteria};
        if (additionalFields){
            ret.additionalFields=additionalFields;
        }
        if (dynamicConditions) {
            ret.dynamicConditions = dynamicConditions;
        }
        console.log ("isShowOnlyCommon() true for layer="+this.layerId);
        let allLayersCriteriaJson = this.createAllLayersCriteriaJson();
        if (allLayersCriteriaJson){
            ret.allLayersCriteria = allLayersCriteriaJson;
        }
        let gradientLayersJson = this.createGradientLayersJson();
        if (gradientLayersJson) {
            ret.gradientLayers = gradientLayersJson;
        }
        return JSON.stringify(ret);
    }

    createRequestParameterJsonStringForCarto2(selectedLayersFilteredSet) {
        let criteria = this.getCriteriaJson();
        let dynamicConditions = this.getDynamicConditionsJson();
        let additionalFields = this.getAdditionalFieldsJson();
        let ret = {"layerCriteria": criteria};
        if (additionalFields){
            ret.additionalFields=additionalFields;
        }
        if (dynamicConditions) {
            ret.dynamicConditions = dynamicConditions;
        }
        console.log ("isShowOnlyCommon() true for layer="+this.layerId);
        let allLayersCriteriaJson = this.createAllLayersCriteriaJsonCarto(selectedLayersFilteredSet);
        if (allLayersCriteriaJson){
            ret.allLayersCriteria = allLayersCriteriaJson;
        }
        let gradientLayersJson = this.createGradientLayersJson();
        if (gradientLayersJson) {
            ret.gradientLayers = gradientLayersJson;
        }
        return JSON.stringify(ret);
    }

    createAllLayersCriteriaJson(){
        let state = this.store.getState();
        if (state.normalLayers != null) {
            return buildNormalLayerQuery(state.normalLayers.selectedLayerIdSet, state.normalLayers.selectedCriteriaIdMap,
                state.normalLayers.selectedCriteriaCodeIdMap, state.ref.criteriaCodeArray, state.ref.layerArray, state.ref.layerFilteringCriteriaArray,
                state.ref.criteriaCodeLookupMap, state.normalLayers.criteriaConditionsMap);
        }
        return null;
    }

    createAllLayersCriteriaJsonCarto(selectedLayersFilteredSet){
        let state = this.store.getState();
        if (state.normalLayers != null) {
            return buildNormalLayerQueryCarto(selectedLayersFilteredSet, state.normalLayers.selectedCriteriaIdMap,
                state.normalLayers.selectedCriteriaCodeIdMap, state.ref.criteriaCodeArray, state.ref.layerArray, state.ref.layerFilteringCriteriaArray,
                state.ref.criteriaCodeLookupMap, state.normalLayers.criteriaConditionsMap);
        }
        return null;
    }

    createGradientLayersJson() {
        let state = this.store.getState();
        if (state.gradientLayers != null) {
            return buildGradientLayerQuery(state.gradientLayers.selectedGradLayerIdSet, state.ref.gradientLayerArray,
                state.gradientLayers.gradFilterMap);
        }
        return null;
    }


    getAdditionalFieldsJson(){
        let d3layerConfig = this.getD3LayerConfig();
        if (!d3layerConfig) return null;
        let columnSelected = d3layerConfig.selectedVariableForStyle;
        if (!columnSelected) return null;
        return [columnSelected];
    }

    getCriteriaJson() {
        let state = this.store.getState(),
            criteriaCodeArray = state.ref.criteriaCodeArray,
            layerCriteriaArray = state.ref.layerFilteringCriteriaArray,
            fusionLayer = state.ref.layerArray.find(layer => {return layer.id == this.layerId}),
            selectedCriteriaIdToCriteriaCodeMap = state.normalLayers.selectedCriteriaIdMap.get(this.layerId);

        let criteria = {};
        if (typeof selectedCriteriaIdToCriteriaCodeMap != 'undefined'
            && selectedCriteriaIdToCriteriaCodeMap.size > 0) {
            criteria.queryType = fusionLayer.query_type;
            criteria.criteria = [];
            let criteriaCode;
            selectedCriteriaIdToCriteriaCodeMap.forEach((criteriaIdSet, criteriaCodeId, map) => {
                criteriaCode = {};
                criteriaCode.criteriaCodeId = criteriaCodeId;
                criteriaCode.criteriaValues = [];

                if (criteria.queryType === "in") {
                    let criteriaColumnName = criteriaCodeArray.find(criteriaCode =>
                        {return criteriaCode.id == criteriaCodeId}).criteria_name;
                    if (typeof criteriaColumnName == 'undefined') {
                        return "{}";
                    }
                    criteriaCode.criteriaName = criteriaColumnName;
                } else if (criteria.queryType === "and") {
                }

                criteriaIdSet.forEach(criteriaId => {
                    criteriaCode.criteriaValues.push(layerCriteriaArray.find(
                        criteria => {return criteria.id == criteriaId}).criteria_value);
                });
                if (criteriaCode.criteriaValues.length > 0) {
                    criteria.criteria.push(criteriaCode);
                }
            });
        }

        return criteria;
    }

    getDynamicConditionsJson() {
        let state = this.store.getState();
        let criteriaConditionsMap = state.normalLayers.criteriaConditionsMap;
        let layerCriteriaArray = state.ref.layerFilteringCriteriaArray.filter((layerCriteria) => {
            return layerCriteria.fusion_map_layer_id === this.layerId;
        });
        let dynamicConditions = [];

        criteriaConditionsMap.forEach((criteriaArray, criteriaId, map) => {
            let layerCriteria = layerCriteriaArray.find((criteria) => {
                return criteria.criteria_code_id === criteriaId;
            });
            if (layerCriteria) {
                criteriaArray.map((criteria) => {
                    if (criteria.applied) {
                        let newCriteria = Object.assign({} , criteria);
                        state.ref.criteriaCodeArray.find((critCode) => {
                            if (critCode.id === criteriaId) {
                                newCriteria.displayColumnName = critCode.display_column_name;
                            }
                        });
                        dynamicConditions.push(newCriteria);
                    }
                });
            }
        });

        return dynamicConditions;
    }

    getCalculatedScale() {
        let zV = this.drawingManager.zoomValue;

        if(this.drawingManager.projectionSelected === 3 && zV < 5) {
            return 5.0/zV;
        }

        return 6.0/zV;
    }
}

export default D3MapNormalLayer
