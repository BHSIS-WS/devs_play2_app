import D3MapNormalLayer from './D3MapNormalLayer';
import D3MapGradientLayer from './D3MapGradientLayer';
import D3MapShapeLayer from './D3MapShapeLayer';
import * as d3 from 'd3';
import $ from 'jquery';
import * as d3_composite from "d3-composite-projections";
import * as d3_geo from "d3-geo";
import {
    toggleD3MapTextToolOptionDialog,
    removeD3MapText,
    toggleD3MapRectToolOptionDialog, addD3MapRect, removeD3MapRect,
    saveMapToolElementCoordinates
} from "../../actions/D3MapToolOptionsAction";
import { toggleNormalLayerLegendDisplay, toggleGradientLayerLegendDisplay } from "../../actions/PrintMapLegendAction";


class D3MapRenderingManager{

    constructor(store,d3MapCanvas) {
        this.store = store;
        this.d3MapCanvas = d3MapCanvas;
        this.renderedLayers=[];
        this.renderedGradLayers = [];
        this.renderedShapeLayers = [];
        this.projectionChanged = false;
        this.init();
    }

    init(){
        this.updateCanvasParametersFromState();
        this.svg = d3.select('#mapCanvas').append('svg').attr("id", "mapCanvasSvg").attr("viewBox", "0 0 1000 1000");
        this.svgg = this.svg.append('g').attr("id", "mapCanvasSvgG").attr("transform", "translate(-80,20)scale(1.7)");
        this.currentScale = 1.7;
        this.currentZoom= d3.zoomIdentity.translate("-80","20").scale(1.7);
        this.setupProjection();
        this.setupZoom();
        this.pointLayerElementClassName = "point_layer_elem";
        this.mapTextMap = new Map();
        this.textCount = 0;
        this.mapCanvasJs = document.getElementById("mapCanvas");
        this.rectCount = 0;
        this.mapRectMap = new Map();
    }

    updateCanvasParametersFromState() {
        let canvasAttributes = this.store.getState().mapCanvasD3;
        this.projectionSelected = canvasAttributes.projection;

        this.centerLat = canvasAttributes.center_lat;
        this.centerLong = canvasAttributes.center_lng;
        this.width = canvasAttributes.width;
        this.height = canvasAttributes.height;
        this.zoomValue = canvasAttributes.zoom;
        this.translateX = canvasAttributes.translateX;
        this.translateY = canvasAttributes.translateY;
    }

    refresh(){
        let printInfoNodes;
        //0. update Canvas setting
        this.updateCanvasParametersFromState();

        //refresh projection
        this.setupProjection();

        //copy printInfoBoxNode before clearing svgg
        if (this.store.getState().printInfoBox.printInfoBoxSet.size > 0) {
            printInfoNodes = this.svgg.selectAll('.print-info-g');
        }

        //clear first
        this.svgg.html("");

        // Set zoom & scale to appropriate level (4.68) in Mercator projection, otherwise reset upon refresh
        if(this.projectionChanged && this.projectionSelected === 3) {
            this.setZoomScale(4.5);
            this.zoomValue = 4.5;
        } else if (this.projectionChanged) {
            this.setZoomScale(1.7);
            this.zoomValue = 1.7;
        }

        this.refreshNormalMapLayers();
        this.refreshGradientMapLayers();
        this.refreshShapeMapLayers();
        this.refreshMapToolElements();

        this.renderedGradLayers.sort((layer1, layer2) => {
            return -(layer1.layerConfig.display_order - layer2.layerConfig.display_order);

        });

        this.renderedGradLayers.forEach(layerWrapper => {
            layerWrapper.refresh();
        });

        this.renderedLayers.sort((layer1,layer2) => {
            return -(layer1.layerConfig.display_order - layer2.layerConfig.display_order);
        });

        this.renderedLayers.forEach(layerWrapper => {
            layerWrapper.refresh();
        });

        this.renderedShapeLayers.forEach(layerWrapper => {
            layerWrapper.refresh();
        });

        //set scale from the state
        d3.select('#mapCanvas>svg>g')
            .attr("transform",
                "translate(" + this.currentZoom.x + "," + this.currentZoom.y + ")" +
                "scale(" + this.zoomValue + ") ");

        if (printInfoNodes !== undefined && printInfoNodes.nodes().length > 0) {
            printInfoNodes.nodes().forEach(function (printInfoNode) {
                d3.select('#mapCanvas>svg>g').node().appendChild(printInfoNode);
            });
        }
    }

    refreshNormalMapLayers() {
        let selectedLayerIdSet = this.store.getState().normalLayers.selectedLayerIdSet;
        let disabledLayerIdSet = this.store.getState().d3LayerConfig.disabledLayerIdSet;
        let oldRenderedLayers = this.renderedLayers;

        if (selectedLayerIdSet) {
            let selectedLayerIdArray = Array.from(selectedLayerIdSet);
            this.renderedLayers = selectedLayerIdArray.map(layerId=>{
                if (!disabledLayerIdSet.has(layerId)) {
                    return this.createOrReuseRenderedLayer(oldRenderedLayers, layerId);
                }
            });
            this.renderedLayers = this.renderedLayers.filter(item => item !== undefined);
        }
    }

    refreshGradientMapLayers() {
        let selectedGradLayerIdSet = this.store.getState().gradientLayers.selectedGradLayerIdSet;
        let oldRenderedGradLayers = this.renderedGradLayers;
        if (selectedGradLayerIdSet) {
            let selectedGradLayerIdArray = Array.from(selectedGradLayerIdSet);
            this.renderedGradLayers = selectedGradLayerIdArray.map(gradLayerId => {
                return this.createOrReuseRenderedGradLayer(oldRenderedGradLayers, gradLayerId);
            });
            this.renderedGradLayers = this.renderedGradLayers.filter(item => item !== undefined);
        }
    }

    refreshShapeMapLayers() {
        let shapeArray = this.store.getState().mapVariables.mapShapes;
        if (shapeArray) {
            this.renderedShapeLayers = shapeArray.map(shape => {
                return this.createOrReuseRenderedShapeLayer(this.renderedShapeLayers, shape);
            });
            this.renderedShapeLayers = this.renderedShapeLayers.filter(item => item !== undefined);
        }
    }

    //private
    createOrReuseRenderedLayer(oldRenderedLayers, layerId){
        let oldLayer;
        if (oldRenderedLayers){
            oldLayer = oldRenderedLayers.find(layer => typeof layer !== 'undefined' && layer.layerId === layerId);
        }
        if (oldLayer){
            return oldLayer;
        } else{
            return new D3MapNormalLayer(layerId, this.store, this);
        }
    }

    //private
    createOrReuseRenderedGradLayer(oldRenderedGradLayers, gradLayerId) {
        let oldGradLayer;
        if (oldRenderedGradLayers) {
            oldGradLayer = oldRenderedGradLayers.find(gradLayer => gradLayer.gradLayerId === gradLayerId);
        }
        if (oldGradLayer) {
            return oldGradLayer;
        } else {
            return new D3MapGradientLayer(gradLayerId, this.store, this);
        }
    }

    //private
    createOrReuseRenderedShapeLayer(oldRenderedShapeLayers, newShape) {
        let oldShapeLayer;
        if (oldRenderedShapeLayers) {
            oldShapeLayer = oldRenderedShapeLayers.find(d3MapShapeLayer => d3MapShapeLayer.shape.id === newShape.id);
        }
        if (oldShapeLayer) {
            return oldShapeLayer;
        } else {
            return new D3MapShapeLayer(newShape, this.store, this);
        }
    }

    //private
    setupProjection() {
        switch (this.projectionSelected){
            case 3:
                this.projection = d3.geoMercator();
                this.path = d3.geoPath(this.projection);
                break;
            case 2:
                this.projection = d3_composite.geoAlbersUsaTerritories();
                this.path = d3_geo.geoPath().projection(this.projection);
                break;
            case 1:
            default:
                this.projection = d3_composite.geoAlbersUsa();
                this.path = d3_geo.geoPath().projection(this.projection);
        }
    }

    //private
    setupZoom() {
        let transform;
        let self = this;

        let zoomed = function() {
            if (self.currentScale !== d3.event.transform.k) {
                // This handles zooming only
                transform = d3.event.transform;
                lazyZoom();

                self.currentScale = d3.event.transform.k;
                self.store.dispatch({type: "ZOOM_D3_CHANGE", value: self.currentScale});
                self.currentZoom = d3.event.transform;
                self.toggleMouseZoom(self.store.getState().toolbarOptions.disableMouseZoom);
            } else {
                // This takes care of click-and-drag shifting around the map
                let transformValue = self.getTransformation(self.svgg.attr("transform"));
                let newX = d3.event.transform.x;
                let newY = d3.event.transform.y;

                self.currentZoom = d3.zoomIdentity.translate(newX, newY).scale(transformValue.scaleX.toFixed(2));
                d3.select('#mapCanvas>svg>g').attr("transform", "translate(" + newX + "," + newY + ")scale(" + transformValue.scaleX.toFixed(2) + ")");
            }
        };

        let zoomEnd = function() {
            let mapCanvasD3 = self.store.getState().mapCanvasD3;
            let newX = d3.event.transform.x;
            let newY = d3.event.transform.y;
            if (newX !== mapCanvasD3.translateX || newY !== mapCanvasD3.translateY) {
                self.store.dispatch({type: "TRANSLATE_D3_CHANGE", translateX: newX, translateY: newY, fromMinimap: false});
            }
        };

        // let initialTranslateExtent = [[],[]];
        // initialTranslateExtent[0][0] = -700;
        // initialTranslateExtent[0][1] = -469;
        // initialTranslateExtent[1][0] = 1450;
        // initialTranslateExtent[1][1] = 819;

        let lazyZoom = this.debounce(function () {
            d3.select('#mapCanvas>svg>g').attr("transform", transform);
        }, 500);

        this.zoomD3 = d3.zoom()
            .scaleExtent([.1, 100])
            //.translateExtent(initialTranslateExtent)
            .on("zoom", zoomed)
            .on("end", zoomEnd);

        self.toggleMouseZoom(self.store.getState().toolbarOptions.disableMouseZoom);
        this.setZoomScale(1.7);
    }

    toggleMouseZoom(disableMouseZoom) {
        let self = this;

        if (disableMouseZoom) {
            self.svg.call(self.zoomD3).on("dblclick.zoom", null).on("wheel.zoom", null);
        } else {
            self.svg.call(self.zoomD3).on("dblclick.zoom", null);
        }
    }

    debounce(func, wait, immediate) {
        let timeout;
        return function execFunc() {
            let context = this,
                args = arguments;
            let later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            let callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        }
    }

    getTranslateExtent(self) {
        let bbox = self.svgg.node().getBBox();
        let mapCanvas = document.getElementById("mapCanvas");
        let transformValue = self.getTransformation(self.svgg.attr("transform"));

        let offsetScale = self.getOffsetScale(self);
        let leftBound = -(((offsetScale * mapCanvas.offsetWidth -bbox.width) / 2) + bbox.width) / transformValue.scaleX;
        let topBound = -(((offsetScale * mapCanvas.offsetHeight -bbox.height) / 2) + bbox.height) / transformValue.scaleX;
        let rightBound = (((offsetScale * mapCanvas.offsetWidth -bbox.width) / 2) + (2*bbox.width)) / transformValue.scaleX;
        let bottomBound = (((offsetScale * mapCanvas.offsetHeight -bbox.height) / 2) + (2*bbox.height)) / transformValue.scaleX;
        let translateExtent = [[leftBound, topBound], [rightBound, bottomBound]];
        return translateExtent;
    }

    getOffsetScale(self) {
        let mapCanvas = document.getElementById("mapCanvas");
        let viewBox = self.svg.attr('viewBox').split(" ");
        return Math.max((viewBox[2] / mapCanvas.offsetWidth), (viewBox[3] / mapCanvas.offsetHeight));
    }

    getTransformation(transform) {
        let g = document.createElementNS("http://www.w3.org/2000/svg", "g");
        g.setAttributeNS(null, "transform", transform);
        let matrix = g.transform.baseVal.consolidate().matrix;
        let {a, b, c, d, e, f} = matrix;
        let scaleX, scaleY, skewX;
        if (scaleX = Math.sqrt(a * a + b * b)) { a /= scaleX; b /= scaleX; }
        if (skewX = a * c + b * d) { c -= a * skewX; d -= b * skewX; }
        if (scaleY = Math.sqrt(c * c + d * d)) { c /= scaleY; d /= scaleY; }
        if (a * d < b * c) scaleX = -scaleX;

        return { translateX: e, translateY: f, scaleX: scaleX, scaleY: scaleY };
    }

    //private
    // createTranslateString(transform, eventTransform, renderingManager) {
    //     let transStr = "";
    //     if (transform !== null) {
    //         let transStrs = transform.split(")");
    //         let translate = [];
    //         if (transStrs[0].includes("translate")) {
    //               transStr = renderingManager.generateTranslateString(transStrs[0], eventTransform);
    //         } else if (transStrs.length > 1 && transStrs[1].includes("translate")) {
    //               transStr = renderingManager.generateTranslateString(transStrs[1], eventTransform);
    //         }
    //     }
    //     return transStr;
    // }

    //private
    // generateTranslateString(translateRawString, eventTransform) {
    //     let translate = translateRawString.substring(translateRawString.indexOf("(")+1).split(",");
    //     let translateX = parseFloat(translate[0]);
    //     let translateY = parseFloat(translate[1]);
    //     return "translate("+translateX+","+translateY+")";
    // }

    setViewBox() {
        if (this.viewBoxSet) return;
        let defaultView = "0 0 1000 1000";

        this.svg.attr("viewBox", defaultView).attr("preserveAspectRatio", "xMinYMax meet");
        this.viewBoxSet= true;
    }

    resetView() {
        this.viewBoxSet=false;
        this.setViewBox();
        this.svg.call(this.zoomD3.transform, d3.zoomIdentity.translate(-80,20).scale(1.7));
    }

    moveViewFromMinimap() {
        let mapCanvasD3 = this.store.getState().mapCanvasD3;
        this.svg.call(this.zoomD3.transform,
            d3.zoomIdentity.translate(mapCanvasD3.translateX, mapCanvasD3.translateY).scale(this.currentScale));
    }

    updateMapCanvasZoomD3FromState() {
        this.updateCanvasParametersFromState();
        this.dragOrigX = 0;
        this.dragOrigY = 0;
        // this.updateMapCanvasD3Scale(this.zoomValue);
        this.refreshZoomD3ControlInput();
    }

    refreshZoomD3ControlInput() {
        $("#mapControlD3 #zoom").val(this.store.getState().mapCanvasD3.zoom);
    }

    updateMapCanvasZoomD3FromInput() {
        this.setZoomScale(this.zoomValue);
    }

    checkProjectionChange() {
        let canvasAttributes = this.store.getState().mapCanvasD3;
        this.projectionChanged = this.projectionSelected !== canvasAttributes.projection;
    }

    updateMapCanvasD3FromState() {
        console.log ("!!! updateMapCanvasD3FromState()");
        this.checkProjectionChange();
        this.updateCanvasParametersFromState();
        this.d3MapCanvas.width(this.width);
        this.d3MapCanvas.height(this.height);
        // this.updateMapCanvasD3Scale();
        this.updateMapCanvasZoomD3FromInput();
        this.updateMapTextTitleFromState();
        this.updateMapRectangleFromState();
        this.updateMapLegendScale();
        if(this.projectionChanged) {
            this.refresh();
            this.projectionChanged = false;
        }
    }

    refreshD3ControlInputFromState() {
        console.log ("!!! refreshD3ControlInputFromState()");
        $("#mapControlD3 #width").val(this.width);
        $("#mapControlD3 #height").val(this.height);
        $("#mapControlD3 #zoom").val(this.zoomValue);
        $("#mapControlD3 #center").val(this.centerLat+", "+this.centerLong);
    }

    refeshMapTextsFromState() {
        let self = this,
            d3MapTextTool = self.store.getState().d3MapToolOptions.d3MapTextTool;
        let lastId = 0;
        self.mapTextMap.forEach((textObj, textId, map) => {
            textObj.remove();
        });
        self.mapTextMap.clear();
        d3MapTextTool.textToolConfigMap.forEach((config, id, map) => {
            self.createText(id, config.fontSize, config.rotate, config.fillColor, config.textContent,
                config.fontFamily, config.bold, config.italic, config.underline, config.x, config.y);
            lastId = id;
        });
        self.textCount = lastId;
    }

    updateMapTextTitleFromState() {
        let self = this,
            d3MapTextTool = this.store.getState().d3MapToolOptions.d3MapTextTool;
        let activeTextConfig = d3MapTextTool.textToolConfigMap.get(d3MapTextTool.activeTextId);
        if (typeof activeTextConfig !== "undefined") {
            let text = this.mapTextMap.get(d3MapTextTool.activeTextId),
                svgViewBoxTotal = self.svg.node().viewBox.baseVal.width + self.svg.node().viewBox.baseVal.height,
                canvasWidth = self.mapCanvasJs.clientWidth,
                canvasHeight = self.mapCanvasJs.clientHeight,
                svgScale = canvasHeight < canvasWidth ? svgViewBoxTotal/(2*canvasHeight) : svgViewBoxTotal/(2*canvasWidth);
            if (typeof text !== "undefined") {
                text.style("font-size", (svgScale * activeTextConfig.fontSize) + "px");
                text.style("font-family", activeTextConfig.fontFamily);
                text.style("font-weight", activeTextConfig.bold ? "bold" : "");
                text.style("font-style", activeTextConfig.italic ? "italic" : "");
                text.attr("transform", "rotate("+activeTextConfig.rotate+","+text.node().getAttribute("x")+","+text.node().getAttribute("y")+")");
                text.attr("fill", activeTextConfig.fillColor);
                text.selectAll(".d3-text-" + d3MapTextTool.activeTextId).remove();
                text.selectAll(".d3-text-" + d3MapTextTool.activeTextId)
                    .data(activeTextConfig.textContent).enter()
                    .append("tspan")
                    .attr("class", "d3-text-" + d3MapTextTool.activeTextId)
                    .attr("x", text.node().getAttribute("x"))
                    .attr("text-anchor", "start")
                    .attr("dominant-baseline", "text-before-edge")
                    .attr("dy", function(d, i) {
                        return i !== 0 ? (svgScale * activeTextConfig.fontSize) + "px" : 0 + "px";
                    }).style("text-decoration", function (d) {
                        return d.length !== 0 && activeTextConfig.underline ? "underline" : "";
                    }).text(function (d) {
                        return d.length !== 0 ? d : "\n";
                    });
                if (typeof self.textTypingTextArea === "undefined" || self.textTypingTextArea.style.display === "none") {
                    text.style("display", "inline");
                }
            }
        }

    }

    refreshMapRectanglesFromState() {
        let self = this,
            d3MapRectTool = self.store.getState().d3MapToolOptions.d3MapRectangleTool;
        let lastId = 0;
        self.mapRectMap.forEach((rectObj, rectId, map) => {
            rectObj.remove();
        });
        self.mapRectMap.clear();
        d3MapRectTool.rectToolConfigMap.forEach((config, id, map) => {
            self.createRectInSvg(id, config.x, config.y, config.width, config.height, config.stroke, config.strokeWidth);
            lastId = id;
        });
        self.rectCount = lastId;
    }

    updateMapRectangleFromState() {
        let d3MapRectTool = this.store.getState().d3MapToolOptions.d3MapRectangleTool;
        let activeRectConfig = d3MapRectTool.rectToolConfigMap.get(d3MapRectTool.activeRectId);
        if (typeof activeRectConfig !== "undefined") {
            let rect = this.mapRectMap.get(d3MapRectTool.activeRectId);
            if (typeof rect !== "undefined") {
                rect.attr("width", activeRectConfig.width);
                rect.attr("height", activeRectConfig.height);
                rect.style("stroke", activeRectConfig.stroke);
                rect.style("stroke-width", activeRectConfig.strokeWidth);
            }
        }
    }

    refreshMapToolElements() {
        this.refeshMapTextsFromState();
        this.refreshMapRectanglesFromState();
    }

    zoomIn(){
        this.setZoomScale( this.currentZoom.k+.05 );
    }

    zoomOut(){
        this.setZoomScale( this.currentZoom.k-.05 );
    }

    setZoomScale(newScale) {
        this.svg.call(this.zoomD3.transform, d3.zoomIdentity.translate(this.currentZoom.x, this.currentZoom.y)
            .scale(newScale));
    }

    showTextTypingTextArea(x, y, width, height, defaultValue, fontConfig) {
        let self = this;
        if (typeof self.textTypingTextArea === "undefined") {
            self.textTypingTextArea = document.createElement("textarea");
            self.textTypingTextArea.style.position = "absolute";
            self.textTypingTextArea.style.resize = "none";
            self.textTypingTextArea.style.border = "1px solid #000";
            document.body.appendChild(self.textTypingTextArea);
        }
        self.textTypingTextArea.style.left = x + "px";
        self.textTypingTextArea.style.top = y + "px";
        self.textTypingTextArea.style.width = width + "px";
        self.textTypingTextArea.style.height = height + "px";
        self.textTypingTextArea.value = (defaultValue !== null && typeof defaultValue !== "undefined") ? defaultValue : "";
        self.textTypingTextArea.style.display = "inline";
        self.textTypingTextArea.style.fontSize = fontConfig.fontSize + "px";
        self.textTypingTextArea.style.fontFamily = fontConfig.fontFamily;
        self.textTypingTextArea.style.color = fontConfig.fillColor;
        self.textTypingTextArea.style.fontWeight = fontConfig.bold ? "bold" : "";
        self.textTypingTextArea.style.fontStyle = fontConfig.italic ? "italic" : "";
        self.textTypingTextArea.style.textDecoration = fontConfig.underline ? "underline" : "";
        self.textTypingTextArea.focus();
    }

    createTextTypingCanvas() {
        let self = this;
        self.textTypingCanvas = document.createElement("canvas");
        let canvas = self.textTypingCanvas,
            svgPoint = self.svg.node().createSVGPoint(),
            defaultTextToolConfig = self.store.getState().d3MapToolOptions.d3MapTextTool.defaultTextToolConfig,
            lastMouseX = 0, lastMouseY = 0, mouseDown = false, mouseX = 0, mouseY = 0, lastCanvasX = 0, lastCanvasY = 0,
            rectWidth = 0, rectHeight = 0, canvasClientRect, canvasContext;

        self.svgTextCoords = svgPoint.matrixTransform(self.svg.node().getScreenCTM().inverse()),

        canvas.id = "text-typing-canvas";
        canvas.style.top = self.svg.style("top");
        canvas.style.left = self.svg.style("left");
        self.mapCanvasJs.appendChild(canvas);
        canvasContext = canvas.getContext("2d");

        canvas.addEventListener("mousedown", function(e) {
            canvasClientRect = canvas.getBoundingClientRect();
            lastMouseX = e.clientX;
            lastMouseY = e.clientY;
            lastCanvasX = e.clientX - canvasClientRect.left;
            lastCanvasY = e.clientY - canvasClientRect.top;
            mouseDown = true;
        }, false);

        canvas.addEventListener("mouseup", function() {
            mouseDown = false;
            canvasContext.clearRect(0, 0, canvas.width, canvas.height);
            if(rectWidth < 0 && rectHeight < 0) {
                lastMouseX = lastMouseX + rectWidth;
                lastMouseY = lastMouseY + rectHeight;
            } else if (rectWidth < 0) {
                lastMouseX = lastMouseX + rectWidth;
            } else if (rectHeight < 0) {
                lastMouseY = lastMouseY + rectHeight
            }
            svgPoint.x = lastMouseX;
            svgPoint.y = lastMouseY;
            self.svgTextCoords = svgPoint.matrixTransform(self.svg.node().getScreenCTM().inverse());
            //if no rect is set default to 100 by 100
            rectWidth = Math.abs(rectWidth) > 1 ? rectWidth : 100;
            rectHeight = Math.abs(rectHeight) > 1 ? rectHeight : 100;
            self.showTextTypingTextArea(lastMouseX, lastMouseY, Math.abs(rectWidth), Math.abs(rectHeight), null, defaultTextToolConfig);
            canvas.style.cursor = "text";
            canvas.style.display = "none";
        });

        canvas.addEventListener("mousemove", function(e) {
            canvasClientRect = canvas.getBoundingClientRect();
            mouseX = e.clientX - canvasClientRect.left;
            mouseY = e.clientY - canvasClientRect.top;
            if (mouseDown) {
                canvas.style.cursor = "crosshair";
                canvasContext.clearRect(0, 0, canvas.width, canvas.height);
                canvasContext.beginPath();
                rectWidth = mouseX - lastCanvasX;
                rectHeight = mouseY - lastCanvasY;
                canvasContext.rect(lastCanvasX, lastCanvasY, rectWidth, rectHeight);
                canvasContext.strokeStyle = "#000";
                canvasContext.lineWidth = 1;
                canvasContext.stroke();
            }
        });
    }

    renderTextTypingCanvas() {
        let self = this;
        if (typeof self.textTypingCanvas === "undefined") {
            self.createTextTypingCanvas();
        }
        if (typeof self.rectDrawingCanvas !== "undefined") {
            self.rectDrawingCanvas.style.display = "none";
        }
        self.textTypingCanvas.width = parseInt(self.mapCanvasJs.style.width);
        self.textTypingCanvas.height = parseInt(self.mapCanvasJs.style.height);
        self.textTypingCanvas.style.display = "inline";
    }

    hideTextTypingTextArea() {
        let self = this,
            activeTextId = self.store.getState().d3MapToolOptions.d3MapTextTool.activeTextId;
        if (typeof self.textTypingTextArea !== "undefined") {
            self.textTypingTextArea.style.display = "none";
        }
        if (typeof self.textTypingCanvas !== "undefined") {
            self.textTypingCanvas.style.display = "none";
        }
        if (activeTextId > 0) {
            self.mapTextMap.get(activeTextId).style("display", "inline");
        }
    }

    getTextToolTextFields() {
        let self = this,
            textAreaFields = {
                textContent: self.textTypingTextArea.value.split("\n"),
                width: parseFloat(self.textTypingTextArea.style.width),
                height: parseFloat(self.textTypingTextArea.style.height)
            };
        self.textTypingTextArea.style.display = "none";
        self.textTypingCanvas.style.display = "none";
        return textAreaFields;
    }

    getNewSvgTextId() {
        return ++this.textCount;
    }

    addText() {
        let self = this;
        self.store.dispatch(toggleD3MapTextToolOptionDialog(0));
        self.renderTextTypingCanvas();
    }

    createText(textId, fontSize, rotate, fillColor, textContent, fontFamily, isBold, isItalic, isUnderlined,
               x, y) {
        if (typeof this.textTypingCanvas === "undefined") {
            this.createTextTypingCanvas();
        }
        let self = this,
            textIdV = (textId !== null && typeof textId !== "undefined") ? textId : ++self.textCount,
            defaultTextToolConfig = self.store.getState().d3MapToolOptions.d3MapTextTool.defaultTextToolConfig,
            fontSizeV = (fontSize !== null && typeof fontSize !== "undefined") ? fontSize :
                defaultTextToolConfig.fontSize,
            rotateV = (rotate !== null && typeof rotate !== "undefined") ? rotate : defaultTextToolConfig.rotate,
            fillColorV = (fillColor !== null && typeof fillColor !== "undefined") ? fillColor :
                defaultTextToolConfig.fillColor,
            textContentV = (textContent !== null && typeof textContent !== "undefined" && textContent.length > 0) ?
                textContent : defaultTextToolConfig.textContent,
            fontFamilyV = (fontFamily !== null && typeof fontFamily !== "undefined") ? fontFamily :
                defaultTextToolConfig.fontFamily,
            isBoldV = (isBold !== null && typeof  isBold !== "undefined") ? isBold : defaultTextToolConfig.bold,
            isItalicV = (isItalic !== null && typeof isItalic !== "undefined") ? isItalic : defaultTextToolConfig.italic,
            isUnderlinedV = (isUnderlined !== null && typeof isUnderlined !== "undefined") ? isUnderlined :
                defaultTextToolConfig.underline,
            xV = (x !== null && typeof x !== "undefined") ? x : self.svgTextCoords.x,
            yV = (y !== null && typeof y !== "undefined") ? y : self.svgTextCoords.y,
            dragStartPos = {},
            svgViewBoxTotal = self.svg.node().viewBox.baseVal.width + self.svg.node().viewBox.baseVal.height,
            canvasWidth = self.mapCanvasJs.clientWidth,
            canvasHeight = self.mapCanvasJs.clientHeight,
            svgScale = canvasHeight < canvasWidth ? svgViewBoxTotal/(2*canvasHeight) : svgViewBoxTotal/(2*canvasWidth);

        let dragStarted = function () {
            dragStartPos.x = d3.event.x;
            dragStartPos.y = d3.event.y;
        };
        let dragged = function () {
            dragStartPos.dX = Math.abs(dragStartPos.x - d3.event.x);
            dragStartPos.dY = Math.abs(dragStartPos.y - d3.event.y);
            if(Math.max(dragStartPos.dX, dragStartPos.dY) >= 2) {
                d3.select(this)
                    .attr("x", d3.event.x)
                    .attr("y", d3.event.y);
                d3.selectAll("." + this.getAttribute("id"))
                    .attr("x", d3.event.x);
            }
        };
        let drag = d3.drag()
            .on("start", dragStarted)
            .on("drag", dragged);

        let dblclicked = function() {
            let text = d3.select(this),
                screenText = this.getScreenCTM(),
                point = self.svg.node().createSVGPoint(),
                textToolState = self.store.getState().d3MapToolOptions.d3MapTextTool.textToolConfigMap.get(textIdV),
                textValue = text.selectAll(".d3-text-" + textIdV).data().join('\n'),
                clientPos;
            point.x = this.getAttribute("x");
            point.y = this.getAttribute("y");
            clientPos = point.matrixTransform(screenText);
            self.store.dispatch(toggleD3MapTextToolOptionDialog(textIdV, true));
            d3.selectAll(".d3-text-value").style("display", "inline");
            text.style("display", "none");
            self.showTextTypingTextArea(clientPos.x, clientPos.y, textToolState.width, textToolState.height, textValue, textToolState);
        };
        let newText = self.svg.append("text")
            .datum({textId: textIdV})
            .attr("id", function(d) { return "d3-text-"+d.textId;})
            .attr("class", "d3-text-value")
            .attr("x", xV)
            .attr("y", yV)
            .attr("text-anchor", "start")
            .attr("dominant-baseline", "text-before-edge")
            .attr("xml:space","preserve")
            .attr("transform", "rotate("+rotateV+","+xV+","+yV+")")
            .attr("fill", fillColorV)
            .style("font-size", (svgScale * fontSizeV) + "px")
            .style("font-family", fontFamilyV)
            .style("font-weight", isBoldV ? "bold" : "")
            .style("font-style", isItalicV ? "italic" : "")
            .on("dblclick", dblclicked)
            .on("contextmenu", function(d) {
                d3.event.preventDefault();
                self.textContextMenu(d3.mouse(this)[0], d3.mouse(this)[1], d.textId);
            })
            .call(drag);

        newText.selectAll(".d3-text-" + textIdV)
            .data(textContentV).enter()
            .append("tspan")
            .attr("class", "d3-text-" + textIdV)
            .attr("x", xV)
            .attr("dy", function(d, i) {
                return i !== 0 ? (svgScale * fontSizeV) + "px" : 0 + "px";
            }).attr("text-anchor", "start")
            .attr("dominant-baseline", "text-before-edge")
            .style("text-decoration", function (d) {
                return d.length !== 0 && isUnderlinedV ? "underline" : "";
            })
            .text(function (d) {
                return d.length !== 0 ? d : "\n";
            });
        self.mapTextMap.set(textIdV, newText);
    }

    textContextMenu(x, y, textId) {
        let self = this,
            menuItemHeight = 35,
            menuItemWidth = 190,
            menuItemMargin = 9,
            menuEntries = ["Edit", "Remove"],
            style = {
                rect: {
                    mouseout: {
                        fill: "white",
                        stroke: "grey",
                        strokeWidth: "1px"
                    },
                    mouseover: {
                        cursor: "pointer",
                        fill: "#e6e6e6"
                    }
                },
                text: {
                    fill: "#222",
                    fontSize: "20px"
                }
            };

        d3.select(".d3-text-context-menu").remove();
        this.svg.append("g").attr("class", "d3-text-context-menu")
            .selectAll(".d3-text-context-menu-entry")
            .data(menuEntries).enter()
            .append("g").attr("class", "d3-text-context-menu-entry")
            .style("cursor", "pointer")
            .on("mouseover", function() {
                d3.select(this).select("rect")
                    .style("fill", style.rect.mouseover.fill);
            })
            .on("mouseout", function() {
                d3.select(this).select("rect")
                    .style("fill", style.rect.mouseout.fill)
                    .style("stroke", style.rect.mouseout.stroke)
                    .style("stroke-width", style.rect.mouseout.strokeWidth);
            })
            .on("click", function(d) {
                if (d === "Edit") {
                    let text = self.mapTextMap.get(textId),
                        screenText = text.node().getScreenCTM(),
                        point = self.svg.node().createSVGPoint(),
                        textToolState = self.store.getState().d3MapToolOptions.d3MapTextTool.textToolConfigMap.get(textId),
                        textValue = text.selectAll(".d3-text-" + textId).data().join('\n'),
                        clientPos;
                    point.x = text.node().getAttribute("x");
                    point.y = text.node().getAttribute("y");
                    clientPos = point.matrixTransform(screenText);
                    self.store.dispatch(toggleD3MapTextToolOptionDialog(textId, true));
                    d3.selectAll(".d3-text-value").style("display", "inline");
                    text.style("display", "none");
                    self.showTextTypingTextArea(clientPos.x, clientPos.y, textToolState.width, textToolState.height, textValue, textToolState);
                } else if (d === "Remove") {
                    self.store.dispatch(removeD3MapText());
                    self.mapTextMap.delete(textId);
                    d3.select("#d3-text-"+textId).remove();
                }
            });

        d3.selectAll(".d3-text-context-menu-entry")
            .append("rect")
            .attr("x", x)
            .attr("y", function(d, i) {return y + (i * menuItemHeight);})
            .attr("width", menuItemWidth)
            .attr("height", menuItemHeight)
            .style("fill", style.rect.mouseout.fill)
            .style("stroke", style.rect.mouseout.stroke)
            .style("stroke-width", style.rect.mouseout.strokeWidth);

        d3.selectAll(".d3-text-context-menu-entry")
            .append("text")
            .text(function(d) {return d;})
            .attr("x", x + 5)
            .attr("y", function(d, i) {return y + (i * menuItemHeight) - 5;})
            .attr("dx", menuItemMargin + 2)
            .attr("dy", menuItemHeight - menuItemMargin / 2)
            .style("fill", style.text.fill)
            .style("font-size", style.text.fontSize);

        d3.select("body").on("click", function() {
            d3.select(".d3-text-context-menu").remove();
            d3.select(".d3-rect-context-menu").remove();
        });
    }

    rectContextMenu(x, y, rectId) {
        let self = this,
            menuItemHeight = 35,
            menuItemWidth = 190,
            menuItemMargin = 9,
            menuEntries = ["Edit", "Remove"],
            style = {
                rect: {
                    mouseout: {
                        fill: "white",
                        stroke: "grey",
                        strokeWidth: "1px"
                    },
                    mouseover: {
                        cursor: "pointer",
                        fill: "#e6e6e6"
                    }
                },
                text: {
                    fill: "#222",
                    fontSize: "20px"
                }
            };

        d3.select(".d3-rect-context-menu").remove();
        this.svg.append("g").attr("class", "d3-rect-context-menu")
            .selectAll(".d3-rect-context-menu-entry")
            .data(menuEntries).enter()
            .append("g").attr("class", "d3-rect-context-menu-entry")
            .style("cursor", "pointer")
            .on("mouseover", function() {
                d3.select(this).select("rect")
                    .style("fill", style.rect.mouseover.fill);
            })
            .on("mouseout", function() {
                d3.select(this).select("rect")
                    .style("fill", style.rect.mouseout.fill)
                    .style("stroke", style.rect.mouseout.stroke)
                    .style("stroke-width", style.rect.mouseout.strokeWidth);
            })
            .on("click", function(d) {
                if (d === "Edit") {
                    self.store.dispatch(toggleD3MapRectToolOptionDialog(rectId));
                } else if (d === "Remove") {
                    self.store.dispatch(removeD3MapRect());
                    self.mapRectMap.delete(rectId);
                    d3.select("#d3-rect-"+rectId).remove();
                }
            });

        d3.selectAll(".d3-rect-context-menu-entry")
            .append("rect")
            .attr("x", x)
            .attr("y", function(d, i) {return y + (i * menuItemHeight);})
            .attr("width", menuItemWidth)
            .attr("height", menuItemHeight)
            .style("fill", style.rect.mouseout.fill)
            .style("stroke", style.rect.mouseout.stroke)
            .style("stroke-width", style.rect.mouseout.strokeWidth);

        d3.selectAll(".d3-rect-context-menu-entry")
            .append("text")
            .text(function(d) {return d;})
            .attr("x", x + 5)
            .attr("y", function(d, i) {return y + (i * menuItemHeight) - 5;})
            .attr("dx", menuItemMargin + 2)
            .attr("dy", menuItemHeight - menuItemMargin / 2)
            .style("fill", style.text.fill)
            .style("font-size", style.text.fontSize);

        d3.select("body").on("click", function() {
            d3.select(".d3-rect-context-menu").remove();
            d3.select(".d3-text-context-menu").remove();
        });
    }

    addRectToSvg(x, y, width, height) {
        let self = this;
        self.createRectInSvg(null, x, y, width, height, null, null);
        self.store.dispatch(addD3MapRect(width, height, self.rectCount));
    }

    createRectInSvg(rectId, x, y, width, height, stroke, strokeWidth) {
        let self = this,
            rectIdV = (rectId !== null && typeof rectId !== "undefined") ? rectId : ++self.rectCount,
            d3MapRectTool = self.store.getState().d3MapToolOptions.d3MapRectangleTool,
            xV = (x !== null && typeof x !== "undefined") ? x : 50,
            yV = (y !== null && typeof y !== "undefined") ? y : 50,
            strokeV = (stroke !== null && typeof stroke !== "undefined") ? stroke : d3MapRectTool.defaultStroke,
            strokeWidthV = (strokeWidth !== null && typeof strokeWidth !== "undefined") ? strokeWidth :
                d3MapRectTool.defaultStrokeWidth;

        let dragStarted = function() {};
        let dragged = function() {
            let thisRect = d3.select(this),
                width = thisRect.attr("width"),
                height = thisRect.attr("height");
            thisRect.attr("x", d3.event.x - width / 2)
                .attr("y", d3.event.y - height / 2)
                .style("cursor", "grabbing");
        };
        let dragEnded = function() {
            d3.select(this)
                .style("cursor", "grab");
        };
        let drag = d3.drag()
            .on("start", dragStarted)
            .on("drag", dragged)
            .on("end", dragEnded);
        let doubleClicked = function() {
            self.store.dispatch(toggleD3MapRectToolOptionDialog(rectIdV));
        };
        let newRect = self.svg.append("rect")
            .datum({rectId: rectIdV})
            .attr("id", function(d) {return "d3-rect-"+d.rectId;})
            .attr("x", xV)
            .attr("y", yV)
            .attr("width", width)
            .attr("height", height)
            // .attr("transform", "scale("+self.zoomValue+")")
            .style("stroke", strokeV)
            .style("stroke-width", strokeWidthV)
            .style("fill-opacity", 0)
            .style("cursor", "grab")
            .on("dblclick", doubleClicked)
            .on("contextmenu", function(d) {
                d3.event.preventDefault();
                self.rectContextMenu(d3.mouse(this)[0], d3.mouse(this)[1], d.rectId);
            })
            .call(drag);
        self.mapRectMap.set(rectIdV, newRect);
    }

    createRectDrawingCanvas() {
        let self = this;
        let d3MapRectangleTool = self.store.getState().d3MapToolOptions.d3MapRectangleTool;
        self.rectDrawingCanvas = document.createElement("canvas");
        let canvas = self.rectDrawingCanvas;
        canvas.id = "rect-drawing-canvas";
        canvas.style.top = self.svg.style("top");
        canvas.style.left = self.svg.style("left");
        self.mapCanvasJs.appendChild(canvas);

        let canvasContext = canvas.getContext("2d"),
            svgPoint = self.svg.node().createSVGPoint(),
            svgViewBoxTotal = self.svg.node().viewBox.baseVal.width + self.svg.node().viewBox.baseVal.height,
            lastCanvasX = 0,
            lastCanvasY = 0,
            lastMouseX = 0,
            lastMouseY = 0,
            mouseDown = false,
            mouseX = 0,
            mouseY = 0,
            rectWidth = 0,
            rectHeight = 0,
            canvasClientRect,
            svgCursorPoint;

        canvas.addEventListener("mousedown", function(e) {
            //for canvas starting position
            canvasClientRect = canvas.getBoundingClientRect();
            lastCanvasX = e.clientX - canvasClientRect.left;
            lastCanvasY = e.clientY - canvasClientRect.top;
            //for svg starting position
            svgPoint.x = e.clientX;
            svgPoint.y = e.clientY;
            svgCursorPoint = svgPoint.matrixTransform(self.svg.node().getScreenCTM().inverse());
            lastMouseX = svgCursorPoint.x;
            lastMouseY = svgCursorPoint.y;
            mouseDown = true;
        }, false);

        canvas.addEventListener("mouseup", function(e) {
            let canvasScale = canvas.height < canvas.width ? svgViewBoxTotal/(2*canvas.height) : svgViewBoxTotal/(2*canvas.width);
            mouseDown = false;
            canvasContext.clearRect(0, 0, canvas.width, canvas.height);
            rectWidth = rectWidth * canvasScale;
            rectHeight = rectHeight * canvasScale;
            if(rectWidth < 0 && rectHeight < 0) {
                lastMouseX = lastMouseX + rectWidth;
                lastMouseY = lastMouseY + rectHeight;
            } else if (rectWidth < 0) {
                lastMouseX = lastMouseX + rectWidth;
            } else if (rectHeight < 0) {
                lastMouseY = lastMouseY + rectHeight
            }
            self.addRectToSvg(lastMouseX, lastMouseY, Math.abs(rectWidth), Math.abs(rectHeight));
            canvas.style.display = "none";
        });

        canvas.addEventListener("mousemove", function(e) {
            canvasClientRect = canvas.getBoundingClientRect();
            mouseX = e.clientX - canvasClientRect.left;
            mouseY = e.clientY - canvasClientRect.top;
            if (mouseDown) {
                canvasContext.clearRect(0, 0, canvas.width, canvas.height);
                canvasContext.beginPath();
                rectWidth = mouseX - lastCanvasX;
                rectHeight = mouseY - lastCanvasY;
                canvasContext.rect(lastCanvasX, lastCanvasY, rectWidth, rectHeight);
                canvasContext.strokeStyle = d3MapRectangleTool.defaultStroke;
                canvasContext.lineWidth = d3MapRectangleTool.defaultStrokeWidth;
                canvasContext.stroke();
            }
        });

    }

    //not used
    toggleRectDrawingCanvas() {
        let self = this,
            d3MapRectangleTool = this.store.getState().d3MapToolOptions.d3MapRectangleTool,
            drawingCanvasOn = d3MapRectangleTool.drawingCanvasOn;
        if (typeof self.rectDrawingCanvas === "undefined") {
            self.createRectDrawingCanvas();
        }
        if (drawingCanvasOn) {
            self.rectDrawingCanvas.width = parseInt(self.mapCanvasJs.style.width);
            self.rectDrawingCanvas.height = parseInt(self.mapCanvasJs.style.height);
            self.rectDrawingCanvas.style.display = "inline";
        } else {
            self.rectDrawingCanvas.style.display = "none";
        }
    }

    showRectDrawingCanvas() {
        let self = this;
        if (typeof self.rectDrawingCanvas === "undefined") {
            self.createRectDrawingCanvas();
        }
        self.rectDrawingCanvas.width = parseInt(self.mapCanvasJs.style.width);
        self.rectDrawingCanvas.height = parseInt(self.mapCanvasJs.style.height);
        self.rectDrawingCanvas.style.display = "inline";
    }

    removeRect(rectId) {
        this.mapRectMap.delete(rectId);
        d3.select("#d3-rect-"+rectId).remove();
    }

    saveToolElementCoordinates() {
        let self = this;
        let textCoordsMap = new Map(),
            rectCoordsMap = new Map();
        let coord;
        self.mapTextMap.forEach((textObj, textId, map) => {
            coord = {};
            coord.x = parseInt(textObj.attr("x"));
            coord.y = parseInt(textObj.attr("y"));
            textCoordsMap.set(textId, coord);
        });
        self.mapRectMap.forEach((rectObj, rectId, map) => {
            coord = {};
            coord.x = parseFloat(rectObj.attr("x"));
            coord.y = parseFloat(rectObj.attr("y"));
            rectCoordsMap.set(rectId, coord);
        });
        let data = {textCoordsMap: textCoordsMap, rectCoordsMap: rectCoordsMap};
        self.store.dispatch(saveMapToolElementCoordinates(data));
    }

    updateMapLegendScale() {
        if (!d3.select("#print-map-legend").empty()) {
            let self = this,
                svgViewBoxTotal = self.svg.node().viewBox.baseVal.width + self.svg.node().viewBox.baseVal.height,
                canvasWidth = self.mapCanvasJs.clientWidth,
                canvasHeight = self.mapCanvasJs.clientHeight;

            self.legendSvgScale = canvasHeight < canvasWidth ? (svgViewBoxTotal/(2*canvasHeight)) * 0.7 : (svgViewBoxTotal/(2*canvasWidth)) * 0.7;
            if(self.legendPosition) {
                d3.select("#print-map-legend").attr("transform", d3.select("#print-map-legend").attr("transform").split(" ")[0] + " scale(" + self.legendSvgScale + ")");
            } else {
                let legendHeight = d3.select("#print-map-legend").node().getBBox().height * self.legendSvgScale,
                    ly = this.svg.attr('viewBox').split(" ")[3] - legendHeight;
                d3.select("#print-map-legend").attr("transform", "translate(0," + ly +  ") scale(" + self.legendSvgScale + ")");
            }
        }
    }

    addLegend() {
        let self = this,
            state = self.store.getState(),
            selectedLayerArray = state.ref.layerArray.filter(layer => {
                return state.normalLayers.selectedLayerIdSet.has(layer.id)
                    && !state.d3LayerConfig.disabledLayerIdSet.has(layer.id)
                    && (state.d3LayerConfig.layerConfigMap.get(layer.id) !== undefined && state.d3LayerConfig.layerConfigMap.get(layer.id).legendDisplayIncluded);
        }),
            selectedGradLayerArray = state.ref.gradientLayerArray.filter(layer => {
                return state.gradientLayers.selectedGradLayerIdSet.has(layer.id)
                    && !state.d3LayerConfig.disabledGradientLayerIdSet.has(layer.id)
                    && (state.d3LayerConfig.gradientLayerConfigMap.get(layer.id) !== undefined && state.d3LayerConfig.gradientLayerConfigMap.get(layer.id).legendDisplayIncluded);
        }),
        allSelectedArray = [...selectedLayerArray].concat(...selectedGradLayerArray),
        svgShapes = state.ref.svgShapes,
        printMapLegend = state.printMapLegend,
        svgViewBoxTotal = self.svg.node().viewBox.baseVal.width + self.svg.node().viewBox.baseVal.height,
        canvasWidth = self.mapCanvasJs.clientWidth,
        canvasHeight = self.mapCanvasJs.clientHeight,
        dragStartPos = {};

        self.removeLegend();
        self.legendSvgScale = canvasHeight < canvasWidth ? (svgViewBoxTotal/(2*canvasHeight)) * 0.7 : (svgViewBoxTotal/(2*canvasWidth)) * 0.7;
        let dragBehavior = d3.drag()
            .on("start", function() {
                dragStartPos.x = d3.event.x;
                dragStartPos.y = d3.event.y;
            })
            .on("drag", function() {
                dragStartPos.dX = Math.abs(dragStartPos.x - d3.event.x);
                dragStartPos.dY = Math.abs(dragStartPos.y - d3.event.y);
                if(Math.max(dragStartPos.dX, dragStartPos.dY) >= 5) {
                    d3.select(this).attr("transform", function () {
                        self.legendPosition = "translate(" + d3.event.x + "," + d3.event.y + ") scale(" + self.legendSvgScale + ")";
                        return self.legendPosition;
                    });
                }
            });

        self.mapLegend =  self.svg.append("g")
            .attr("id", "print-map-legend")
            .call(dragBehavior);

        self.mapLegend.selectAll(".print-legend-list")
            .data(allSelectedArray).enter()
            .append("g").attr("class", "print-legend-list")
            .style("cursor", "pointer")
            .on("click", function(i) {
                if (i.layer_name !== undefined) {
                    self.store.dispatch(toggleNormalLayerLegendDisplay(i.id));
                } else {
                    self.store.dispatch(toggleGradientLayerLegendDisplay(i.id));
                }
            });

        d3.selectAll(".print-legend-list").each(function (d, i) {
            let initialY = i > 0 ? d3.select("#print-map-legend").node().getBBox().height : 0;
            d3.select(this)
                .append("rect")
                .attr("x", 0)
                .attr("y", initialY)
                .attr("height", 40)
                .attr("class", "print-legend-list-rect")
                .style("fill", "#426288")
                .style("stroke", "#1E3859")
                .style("stroke-width", "1");

            d3.select(this)
                .append("rect")
                .attr("x", 0)
                .attr("y", initialY)
                .attr("height", 40)
                .attr("width", 35)
                .style("fill", "#1E3859");

            d3.select(this)
                .append("text")
                .text(function() {return "+"})
                .attr("x", 8)
                .attr("y", initialY - 10)
                .attr("dx", 0.1)
                .attr("dy", 40 - 0.1 / 2)
                .style("fill", "#FFFFFF")
                .style("font-size", "30px")
                .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');

            d3.select(this)
                .append("text")
                .text(function(d) {return d.layer_name || d.name;})
                .attr("x", 45)
                .attr("y", initialY - 10)
                .attr("dx", 0.1)
                .attr("dy", 40 - 0.1 / 2)
                .style("fill", "#FFFFFF")
                .style("font-size", "18px")
                .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');

            if (printMapLegend.expandedLayerIdSet.has(d.id) && d.layer_name !== undefined) {
                this.getElementsByTagName("text")[0].innerHTML = "-";
                self.addLegendLayerConfig(this, d, state.d3LayerConfig.layerConfigMap.get(d.id), svgShapes);
            } else if (printMapLegend.expandedGradLayerIdSet.has(d.id) && d.name !== undefined) {
                this.getElementsByTagName("text")[0].innerHTML = "-";
                self.addLegendGradientLayerConfig(this, d, state.d3LayerConfig.gradientLayerConfigMap.get(d.id));
            }
        });

        let mapLegendWidth = d3.select("#print-map-legend").node().getBBox().width;
        self.currentLegendWidth = self.currentLegendWidth !== mapLegendWidth ? mapLegendWidth + 20 : self.currentLegendWidth;
        d3.selectAll(".print-legend-list-rect").attr("width", self.currentLegendWidth);
        d3.selectAll(".print-legend-layer-list-rect").attr("width", this.currentLegendWidth);
        d3.selectAll(".print-legend-gLayer-list-rect").attr("width", this.currentLegendWidth);

        if(self.legendPosition) {
            d3.select("#print-map-legend").attr("transform", self.legendPosition.split(" ")[0] + " scale(" + self.legendSvgScale + ")");
        } else {
            let legendHeight = d3.select("#print-map-legend").node().getBBox().height * self.legendSvgScale,
                ly = this.svg.attr('viewBox').split(" ")[3] - legendHeight;
            d3.select("#print-map-legend").attr("transform", "translate(0," + ly +  ") scale(" + self.legendSvgScale + ")");
        }

    }

    addLegendLayerConfig(target, layer, layerConfig, svgShapes) {
        if(typeof layerConfig === "undefined" || layerConfig.configShapeStyleByVariable === undefined || !layerConfig.configShapeStyleByVariable) {
            let xt = target.getBBox().y + target.getBBox().height,
                listTag = d3.select(target).append("g").attr("class", "print-legend-layer"),
                svgShape = svgShapes[0],
                shapeStyle = typeof layerConfig !== "undefined" ? layerConfig.shapeForAllStyle : "";

            listTag.append("rect")
                .attr("class", "print-legend-layer-list-rect")
                .attr("x", 0)
                .attr("y", function(d, i) {return (xt + (i * 30));})
                .attr("height", 30)
                .style("fill", "#ebf7f7");

            this.svgTag = listTag.append("svg")
                .attr("width", "15")
                .attr("height", "15")
                .attr("viewBox", "0 0 2 2")
                .attr("x", 10)
                .attr("y", xt + 6);

            this.svgTag.append(svgShape.svgTag)
                .attr(svgShape.svgAttributes[0].name, svgShape.svgAttributes[0].value)
                .attr("vector-effect", "non-scaling-stroke")
                .attr("stroke", shapeStyle && shapeStyle.strokeColor ? shapeStyle.strokeColor : "#000")
                .attr("stroke-width", shapeStyle && shapeStyle.strokeSize ? shapeStyle.strokeSize : "1")
                .attr("fill", shapeStyle && shapeStyle.fillColor ? shapeStyle.fillColor : "#fff");

            listTag.append("text")
                .text(layer.layer_name)
                .attr("x", 40)
                .attr("y", function(d, i) {return (xt + (i * 30) - 10);})
                .attr("dx", 0.1)
                .attr("dy", 30 - 0.1 / 2)
                .style("font-size", "16px")
                .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');
        } else {
            let shapeConfigMap = typeof layerConfig !== "undefined" && layerConfig.shapeConfigMap !== undefined ? layerConfig.shapeConfigMap : new Map(),
                selectedVariableForStyle = typeof layerConfig !== "undefined" ? layerConfig.selectedVariableForStyle : null,
                hideLegendConfigSet = layerConfig.hideLegendConfigSet !== undefined ? layerConfig.hideLegendConfigSet : new Set(),
                categorialListArray = this.populateCategoryCodeAndCriteria(layer, shapeConfigMap, selectedVariableForStyle, hideLegendConfigSet),
                xt = target.getBBox().y + target.getBBox().height,
                listTag = d3.select(target).append("g").attr("class", "print-legend-layer");

            listTag.selectAll(".print-legend-layer-list-" + layer.id)
                .data(categorialListArray).enter()
                .append("g").attr("class", "print-legend-layer-list-" + layer.id);

            d3.selectAll(".print-legend-layer-list-" + layer.id)
                .append("rect")
                .attr("class", "print-legend-layer-list-rect")
                .attr("x", 0)
                .attr("y", function(d, i) {return (xt + (i * 30));})
                .attr("height", 30)
                .style("fill", "#ebf7f7");

            this.svgTag = d3.selectAll(".print-legend-layer-list-" + layer.id)
                .append("svg")
                .attr("width", "15")
                .attr("height", "15")
                .attr("viewBox", "0 0 2 2")
                .attr("x", 10)
                .attr("y", function(d, i) {return (xt + (i * 30) + 6);});

            this.svgTag.each(function (d) {
                const shapeId = d.shapeConfigStyle && d.shapeConfigStyle.shapeId ? (d.shapeConfigStyle.shapeId - 1) : 0;
                d3.select(this).append(svgShapes[shapeId].svgTag)
                    .attr(svgShapes[shapeId].svgAttributes[0].name, svgShapes[shapeId].svgAttributes[0].value)
                    .attr("vector-effect", "non-scaling-stroke")
                    .attr("stroke", d.shapeConfigStyle && d.shapeConfigStyle.strokeColor ? d.shapeConfigStyle.strokeColor : "#000")
                    .attr("stroke-width", d.shapeConfigStyle && d.shapeConfigStyle.strokeSize ? d.shapeConfigStyle.strokeSize : "1")
                    .attr("fill", d.shapeConfigStyle && d.shapeConfigStyle.fillColor ? d.shapeConfigStyle.fillColor : "#fff");
            });

            d3.selectAll(".print-legend-layer-list-" + layer.id)
                .append("text")
                .text(function(d) {return d.name;})
                .attr("x", 40)
                .attr("y", function(d, i) {return (xt + (i * 30) - 10);})
                .attr("dx", 0.1)
                .attr("dy", 30 - 0.1 / 2)
                .style("font-size", "16px")
                .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');
        }
    }

    addLegendGradientLayerConfig(target, layer, layerConfig) {
        let fillColorBoundaryArray = this.getGradientColorConfigArray(layerConfig);

        if(layerConfig !== undefined && fillColorBoundaryArray.length > 0) {
            let xt = target.getBBox().y + target.getBBox().height;

            d3.select(target).selectAll(".print-legend-gLayer-list-" + layer.id)
                .data(fillColorBoundaryArray).enter()
                .append("g").attr("class", "print-legend-gLayer-list-" + layer.id);

            d3.selectAll(".print-legend-gLayer-list-" + layer.id)
                .append("rect")
                .attr("class", "print-legend-gLayer-list-rect")
                .attr("x", 0)
                .attr("y", function(d, i) {return (xt + (i * 30));})
                .attr("height", 30)
                .attr("width", this.currentLegendWidth)
                .style("fill", "#ebf7f7");

            this.svgTag = d3.selectAll(".print-legend-gLayer-list-" + layer.id)
                .append("svg")
                .attr("width", "50")
                .attr("height", "15")
                .attr("viewBox", "0 0 2 2")
                .attr("x", 0)
                .attr("y", function(d, i) {return (xt + (i * 30) + 6);});

            this.svgTag.append("rect")
                .attr("width", "4")
                .attr("height", "2")
                .attr("y", "0")
                .attr("x", "0")
                .attr("vector-effect", "non-scaling-stroke")
                .attr("stroke", "#000")
                .attr("stroke-width", "1")
                .attr("fill", function(d) {return d.color;});

            d3.selectAll(".print-legend-gLayer-list-" + layer.id)
                .append("text")
                .text(function(d, i, a) {
                    let operator = "";
                    if(a.length - 1 === i && a.length > 1) {
                        operator = ">=";
                    } else if (i === 0) {
                        operator = "<=";
                    }
                    return operator + d.value;
                }).attr("x", 60)
                .attr("y", function(d, i) {return (xt + (i * 30) - 10);})
                .attr("dx", 0.1)
                .attr("dy", 30 - 0.1 / 2)
                .style("font-size", "16px")
                .style("font-family", 'Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif');
        }
    }

    getGradientColorConfigArray(layerConfig) {
        let hideLegendConfigSet = layerConfig.hideLegendConfigSet !== undefined ? layerConfig.hideLegendConfigSet : new Set();
        return [...layerConfig.gradientColorConfigArray].filter((value, index) => {
            return !hideLegendConfigSet.has(index);
        }).sort((a, b) => {
            return a.value - b.value;
        });
    }

    populateCategoryCodeAndCriteria(currentLayer, shapeConfigMap, selectedVariableForStyle, hideLegendConfigSet) {
        let categorialListString, criteriaShapeConfigMap, categorialCodes, categorialListArray;

        if (!currentLayer.style_column_json || currentLayer.style_column_json === null) return null;

        categorialCodes = [...new Set(currentLayer.style_column_json.map(layerStyle => JSON.parse(layerStyle).name))];
        if (!selectedVariableForStyle){
            selectedVariableForStyle = categorialCodes[0];
        }

        categorialListString = currentLayer.style_column_json.filter(layerStyle => JSON.parse(layerStyle).name === selectedVariableForStyle);
        criteriaShapeConfigMap = shapeConfigMap.get(selectedVariableForStyle);
        categorialListArray = categorialListString ? (JSON.parse(categorialListString).value).filter((criteria) => {
            return criteria !== null && !hideLegendConfigSet.has(selectedVariableForStyle  + "-"  + criteria);
        }).map((criteria, index) => {
            return ({
                id: index,
                name: criteria,
                shapeConfigStyle: criteriaShapeConfigMap ? criteriaShapeConfigMap.get(criteria) : null
            });
        }) : null;

        return categorialListArray;
    }

    removeLegend() {
        d3.select("#print-map-legend").remove();
    }

    removeLegendLayerConfig(target) {
        d3.select(target.getElementsByTagName("g")[0]).remove();
    }

}


export default D3MapRenderingManager
