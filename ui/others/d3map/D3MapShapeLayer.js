class D3MapShapeLayer{

    constructor(shape, store, drawingManager){
        this.shape = shape;
        this.store = store;
        this.drawingManager=drawingManager;
        // this.create();
    }

    create() {
        let layerG = this.drawingManager.svgg.append("g")
            .attr("class", "shape_layer_d3")
            .attr("id", "shape_layer_d3_" + this.shape.id);
        this.drawSvg(layerG);
    }


    refresh(){
        this.startTime = new Date().getTime();
        let existingNode= this.drawingManager.svgg.select("#shape_layer_d3_"+this.shape.id);
        if (existingNode) {
            existingNode.remove();
        }
        let layerG = this.drawingManager.svgg.append("g")
            .attr("class", "shape_layer_d3")
            .attr("id", "shape_layer_d3_" + this.shape.id);
        this.drawSvg(layerG);
    }

    drawSvg(layerG){
        this.renderShapeLayer(layerG);
        this.drawingManager.setViewBox();
    }

    renderShapeLayer(layerG) {
        this.startTime = new Date().getTime();

        this.renderShape(layerG);
        this.drawingManager.setViewBox();
        let deltaTime = new Date().getTime() - this.startTime;
        console.log("SHAPE_LAYERID: ", this.shape.id, "\nRENDER DELAY: ", deltaTime);
    }

    renderShape(layer){
        let self = this;
        let paths = [];
        this.shape.overlay.getPaths().forEach(function(path)  {
            paths.push(path.getArray());
        });

        let svgProps = this.poly_gm2svg(paths, function (latLng) {
            return {
                lat: latLng.lat(),
                lng: latLng.lng()
            }
        });

        if (self.drawingManager.projectionSelected === 3) {
            layer.attr("transform", "translate(-0.21,-230.17)");
        } else {
            let translateX = 1704;
            let translateY = -1712.9;
            if (svgProps.minX < -1150) {
                translateY = -1710.9
            }
            if (svgProps.minY > -2050) {
                translateX = 1705;
            }
            layer.attr("transform", "translate(" + translateX + "," + translateY + ") rotate(-116)");
        }

        let layerElems = layer.selectAll("path").data([svgProps.path]).enter();

        let appendedPathArray = layerElems.append("path").attr("d", svgProps.path).attr("class", "shape_layer_d3_"+this.shape.id);
        appendedPathArray.attr("stroke-width", "0.5")
            .attr("stroke", this.shape.overlay.strokeColor)
            .attr("stroke-opacity", "")
            .attr("fill", this.shape.overlay.fillColor)
            .attr("fill-opacity", this.shape.overlay.fillOpacity)
            .attr("vector-effect","non-scaling-stroke");
    }


    setStyleAttributesFromShapeStyle(appendedShape, shapeStyle) {

        if (!shapeStyle){
            appendedShape.attr("vector-effect","non-scaling-stroke")
                .attr("stroke", "black")
                .attr("fill", "none");
            return;
        }

        appendedShape.attr("vector-effect","non-scaling-stroke")
            .attr("stroke", shapeStyle.strokeColor? shapeStyle.strokeColor : "black")
            .attr("fill", shapeStyle.fillColor? shapeStyle.fillColor : "none");

        if (shapeStyle.strokeSize){
            appendedShape.attr("stroke-width",shapeStyle.strokeSize)
        }
    }


    latLngToMercatorPoint(latLng){

        return {
            x:(latLng.lng+180)*(256/360),
            y:(256/2)-(256*Math.log(Math.tan((Math.PI/4)
                +((latLng.lat*Math.PI/180)/2)))/(2*Math.PI))
        };
    }

     latLngToAlbersPoint(latLng) {

        let lat0 = 39 * (Math.PI/180),   // Latitude_Of_Origin
            lng0 = 96 * (Math.PI/180),  // Central_Meridian
            phi1 = 45.5 * (Math.PI/180),   // Standard_Parallel_1
            phi2 = 29.5 * (Math.PI/180),  // Standard_Parallel_2


            n = 0.5 * (Math.sin(phi1) + Math.sin(phi2)),
            c = Math.cos(phi1),
            C = c*c + 2*n*Math.sin(phi1),
            p0 = Math.sqrt(C - 2*n*Math.sin(lat0)) / n,
            theta = n * (latLng.lng * Math.PI/180 - lng0),
            p = Math.sqrt(C - 2*n*Math.sin(latLng.lat* Math.PI/180)) / n;

        return {
            x: p * Math.sin(theta),
            y: p0 - p * Math.cos(theta)
        }
    }

    poly_gm2svg(gmPaths, fx) {

        let point,
            gmPath,
            svgPath,
            svgPaths = [];

        let minX = 0;
        let minY = 0;
        for (let pp = 0; pp < gmPaths.length; ++pp) {
            gmPath = gmPaths[pp];
            svgPath = [];
            for (let p = 0; p < gmPath.length; ++p) {
                if (this.drawingManager.projectionSelected === 3) {
                    point = this.latLngToMercatorPoint(fx(gmPath[p]));
                    point = {
                        x: point.x * 3.75,
                        y: point.y * 3.75
                    }
                } else {
                    point = this.latLngToAlbersPoint(fx(gmPath[p]));
                    point = {
                        x: point.x * 1065,
                        y: point.y* -1065
                    };
                    minX = Math.min(minX, point.x);
                    minY = Math.min(minY, point.y);
                }
                svgPath.push([point.x, point.y].join(','));
            }

            svgPaths.push(svgPath.join(' '))
        }
        return {
            path: 'M' + svgPaths.join('z M') + 'z',
            minX: minX,
            minY: minY
        }

    }

}

export default D3MapShapeLayer
