import { Subject } from 'rxjs/Subject'
import 'rxjs/add/operator/delay';

const layerSelectionSubject = new Subject();

export default layerSelectionSubject;
