"use strict";

import { ajax } from 'rxjs/observable/dom/ajax';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';

import {fetchSavedLayerConfigList} from '../actions/LayerConfigHistoryAction';
import { OBSERVABLE_DEBOUNCE_DUE_TIME } from '../constant/constants';

const RemoveSavedLayerConfigEpic = (action$, store) =>
    action$.ofType('REMOVE_SAVED_LAYER_CONFIG')
        .debounceTime(OBSERVABLE_DEBOUNCE_DUE_TIME)
        .mergeMap((action) => {
            let req = {
                userSavedLayerConfigId: action.userSavedLayerConfig.userSavedLayerConfigId
            };

            let ajaxRequest = ajax.post('/devs2/removeLayerConfigForUser', req, {'Content-Type': 'application/json'});
            return ajaxRequest.map(ajaxObject => (fetchSavedLayerConfigList(ajaxObject)))
        });


export default RemoveSavedLayerConfigEpic;