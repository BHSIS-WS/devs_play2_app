"use strict";

import { ajax } from 'rxjs/observable/dom/ajax';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import {Observable} from "rxjs/Observable";

import { updateLayerDataListing, openDataTableExcelFile } from '../actions/LayerDatatListingAction';
import { OBSERVABLE_DEBOUNCE_DUE_TIME } from '../constant/constants';
import { buildNormalLayerQuery, buildGradientLayerQuery } from '../state/layerCriteriaHelper';
import mapContentModel from "../others/MapContentModel";

const getQueryRequest = function(state) {
    let req = {
        "layers": [],
        "gradientLayers": [],
        "stateLevelOpts": [],
        "countyLevelOpts": [],
        "invertQuery": false,
        "tableColumns": [],
        "page": 1,
        "pageSize": 10,
        "shapeArray": [],
        "search": null,
        "order": {
            "name": "ogc_fid",
            "dir": "asc"
        }
    };
    req.page = state.layerDataListing.page;
    req.pageSize = state.layerDataListing.pageSize;
    req.search = state.layerDataListing.search;
    req.order = state.layerDataListing.order;
    req.invertQuery = state.layerDataListing.invertQuery;
    req.tableColumns = getTableColumnsParam(state);
    if (state.normalLayers !== null && state.normalLayers !== undefined) {
        req.layers = buildNormalLayerQuery(state.normalLayers.selectedLayerIdSet, state.normalLayers.selectedCriteriaIdMap, state.normalLayers.selectedCriteriaCodeIdMap, state.ref.criteriaCodeArray, state.ref.layerArray, state.ref.layerFilteringCriteriaArray, state.ref.criteriaCodeLookupMap, state.normalLayers.criteriaConditionsMap);
        req.gradientLayers = buildGradientLayerQuery(state.gradientLayers.selectedGradLayerIdSet, state.ref.gradientLayerArray, state.gradientLayers.gradFilterMap);
    }
    let shapeArray = [];
    for(let i = 0; i < state.mapVariables.mapShapes.length; i++) {
        let shape = {
            type: state.mapVariables.mapShapes[i].type,
            vertices: mapContentModel.getPolygonVertices(state.mapVariables.mapShapes[i].overlay)
        };
        shapeArray.push(shape);
    }
    req.shapeArray = shapeArray;
    req.stateLevelOpts = getStateLevelOptsParam(state);
    req.countyLevelOpts = getCountyLevelOptsParam(state);
    if (state.layerDataListing.sendMarker && state.layerDataListing.markerLat && state.layerDataListing.markerLng) {
        req.lat = state.layerDataListing.markerLat;
        req.lng = state.layerDataListing.markerLng;
    }
    if (state.layerDataListing.infoWindowSelectedId !== null) {
        req.infoWindowSelectedId = state.layerDataListing.infoWindowSelectedId;
    }
    return JSON.stringify(req);
};

const getTableColumnsParam = function(state) {
    let tableColumns = [];
    let tblColumn = null;
    let selectedColumnIds = null;
    let selectedFPColumnIdMap = state.layerDataListing.selectedFPColumnIdMap,
        selectedFPLayerIdSet = state.layerDataListing.selectedFPLayerIdSet;
    selectedFPColumnIdMap.forEach((columnIdSet, layerId, map) => {
        if (selectedFPLayerIdSet.has(layerId)) {
            selectedColumnIds = [];
            columnIdSet.forEach((columnId) => {
                selectedColumnIds.push(columnId);
            });
            tblColumn = {fusionMapLayerId: layerId, selectedTableColumnIds: selectedColumnIds};
            tableColumns.push(tblColumn);
        }
    });
    return tableColumns;
};

const getStateLevelOptsParam = function(state) {
    let stateLevelOpts = [];
    let selectedAddtnlStateGradLayerIdSet = state.layerDataListing.selectedAddtnlStateGradLayerIdSet;
    selectedAddtnlStateGradLayerIdSet.forEach((value, key, set) => {
        stateLevelOpts.push(value);
    });
    return stateLevelOpts;
};

const getCountyLevelOptsParam = function(state) {
    let countyLevelOpts = [];
    let selectedAddtnlCountyGradLayerIdSet = state.layerDataListing.selectedAddtnlCountyGradLayerIdSet;
    selectedAddtnlCountyGradLayerIdSet.forEach((value, key, set) => {
        countyLevelOpts.push(value);
    });
    return countyLevelOpts;
};

const fetchLayerDataListEpic = (action$, store) =>
    action$.ofType('FETCH_LAYER_DATA_LIST')
        .debounceTime(OBSERVABLE_DEBOUNCE_DUE_TIME)
        .mergeMap(() => {
            let req = getQueryRequest(store.getState());
            //let facilityListingJson = ajax.post('/devs/fetchdata', req, { 'Content-Type': 'application/json' }, {'Csrf-Token': 'nocheck'});
            let facilityListingJson = ajax.post('/devs2/fetchdata', req, { 'Content-Type': 'application/json' });
            // additional data from the state
            return facilityListingJson
                .map(data => (updateLayerDataListing(data)));
        });

const openExcelFile = (reqJson) => {
    let form = document.createElement('form');
    form.action = '/devs2/downloadExcel';
    form.method = 'POST';
    form.target = '_self';
    form.enctype = 'text/plain';
    let input = document.createElement('input');
    input.name = 'json';
    input.value = reqJson;
    form.appendChild(input);
    form.style.display = 'none';
    document.body.appendChild(form);
    form.submit();
};

const downloadDataTableExcelEpic = (action$, store) =>
    action$.ofType('DOWNLOAD_DATA_TABLE_EXCEL')
        .debounceTime(OBSERVABLE_DEBOUNCE_DUE_TIME)
        .mergeMap(() => {
            let req = getQueryRequest(store.getState());
            return Observable.of(openDataTableExcelFile(openExcelFile, req));
        });

export {
    fetchLayerDataListEpic,
    downloadDataTableExcelEpic
};