"use strict";

import { ajax } from 'rxjs/observable/dom/ajax';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import {processForm, validateError} from "../actions/FeedbackAction";
import {Observable} from "rxjs/Observable";

const submitFeedbackEpic = action$ =>
    action$.ofType("SUBMIT_FEEDBACK_FORM")
        .mergeMap(action => {
            const reqParams = action.data;  //formDataObj and gRecaptchaResponse
            const feedbackSubmit = ajax.post('/devs2/saveFeedback', reqParams, {'Content-Type': 'application/json'});
            const feedbackSubmitResponse = feedbackSubmit.map(response => (response));

            return feedbackSubmitResponse
                .map(data => (processForm(data.response)) )
                .catch(() => Observable.of(
                    validateError("Your form was not submitted successfully, please try again...")
                ));
        });

export default submitFeedbackEpic;