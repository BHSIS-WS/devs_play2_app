"use strict";

import { ajax } from 'rxjs/observable/dom/ajax';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';

import {fetchSavedLayerConfigList, updateSavedLayerConfigList} from '../actions/LayerConfigHistoryAction';
import { OBSERVABLE_DEBOUNCE_DUE_TIME } from '../constant/constants';
import mapContentModel from "../others/MapContentModel";
import printContentModel from '../others/PrintContentModel';
import {hideSaveLayerConfigDialog, showSaveConfigDuplicateNameError} from "../actions/ToolbarOptionsAction";

function mapToObject(map) {
    const out = Object.create(null);
    map.forEach((value, key) => {
        if (value instanceof Map) {
            out[key] = mapToObject(new Map(value));
        }
        else if (value instanceof Set) {
            out[key] = Array.from(value);
        }
        else if (typeof value === "object") {
            let newValue = Object.assign({}, value);
            Object.keys(newValue).forEach((objKey) => {
                if (newValue[objKey] instanceof Map) {
                    newValue[objKey] = mapToObject(newValue[objKey]);
                } else if (newValue[objKey] instanceof Set) {
                    newValue[objKey] = Array.from(newValue[objKey]);
                }
            });
            out[key] = newValue;
        } else {
            out[key] = value;
        }
    });
    return out;
}

function getCoordinateArraysFromShapes(mapShapes) {
    let shapeArray = [];
    mapShapes.forEach(shape => {
        shapeArray.push(mapContentModel.getPolygonVertices(shape.overlay));
    });
    return shapeArray;
}

const saveLayerConfigForUserEpic = (action$, store) =>
    action$.ofType('SAVE_LAYER_CONFIG_FOR_USER')
        .debounceTime(OBSERVABLE_DEBOUNCE_DUE_TIME)
        .mergeMap((action) => {printContentModel.saveToolElementCoordinates();
            let currentState = Object.assign({}, store.getState());
            let req = {
                layerConfigurationName: action.name,
                layerConfigurationDescription: action.description,
                state: {
                    d3LayerConfig: {
                        disabledLayerIdSet: Array.from(currentState.d3LayerConfig.disabledLayerIdSet),
                        layerConfigMap: mapToObject(currentState.d3LayerConfig.layerConfigMap),
                        disabledGradientLayerIdSet: Array.from(currentState.d3LayerConfig.disabledGradientLayerIdSet),
                        gradientLayerConfigMap: mapToObject(currentState.d3LayerConfig.gradientLayerConfigMap)
                    },
                    gradientLayers: {
                        expanded: currentState.gradientLayers.expanded,
                        expandedDataSetIdSet: Array.from(currentState.gradientLayers.expandedDataSetIdSet),
                        filterExpandedLayerIdSet: Array.from(currentState.gradientLayers.filterExpandedLayerIdSet),
                        selectedGradLayerIdSet: Array.from(currentState.gradientLayers.selectedGradLayerIdSet),
                        gradFilterMap: mapToObject(currentState.gradientLayers.gradFilterMap)
                    },
                    layers: {
                        visibleLayerKeySet: Array.from(currentState.layers.visibleLayerKeySet),
                        borderWidthMap: mapToObject(currentState.layers.borderWidthMap)
                    },
                    mapVariables: {
                        center: currentState.mapVariables.center,
                        minZoomLevel: currentState.mapVariables.minZoomLevel,
                        zoomLevel: currentState.mapVariables.zoomLevel,
                        mapMarkers: [],
                        mapMarkerLocations: [],
                        mapShapes: getCoordinateArraysFromShapes(currentState.mapVariables.mapShapes)
                    },
                    mapCanvasD3: currentState.mapCanvasD3,
                    normalLayers: {
                        expandedLayerIdSet: Array.from(currentState.normalLayers.expandedLayerIdSet),
                        selectedLayerIdSet: Array.from(currentState.normalLayers.selectedLayerIdSet),
                        selectedCriteriaIdSet: Array.from(currentState.normalLayers.selectedCriteriaIdSet),
                        selectedCriteriaCodeIdMap: mapToObject(currentState.normalLayers.selectedCriteriaCodeIdMap),
                        selectedCriteriaIdMap: mapToObject(currentState.normalLayers.selectedCriteriaIdMap),
                        expandSelectedLayerId: currentState.normalLayers.expandSelectedLayerId,
                        expandedCriteriaIdSet: Array.from(currentState.normalLayers.expandedCriteriaIdSet),
                        criteriaConditionsMap: mapToObject(currentState.normalLayers.criteriaConditionsMap)
                    },
                    d3MapToolOptions: {
                        d3MapTextTool: {
                            optionDialogOpen: currentState.d3MapToolOptions.d3MapTextTool.optionDialogOpen,
                            activeTextId: currentState.d3MapToolOptions.d3MapTextTool.activeTextId,
                            textToolConfigMap: mapToObject(currentState.d3MapToolOptions.d3MapTextTool.textToolConfigMap)
                        },
                        d3MapRectangleTool: {
                            drawingCanvasOn: currentState.d3MapToolOptions.d3MapRectangleTool.drawingCanvasOn,
                            rectOptionDialogOpen: currentState.d3MapToolOptions.d3MapRectangleTool.rectOptionDialogOpen,
                            activeRectId: currentState.d3MapToolOptions.d3MapRectangleTool.activeRectId,
                            rectToolConfigMap: mapToObject(
                                currentState.d3MapToolOptions.d3MapRectangleTool.rectToolConfigMap)
                        }
                    },
                    toolbarOptions: currentState.toolbarOptions
                }
            };

            let ajaxRequest = ajax.post('/devs2/saveLayerConfigForUser', req, {'Content-Type': 'application/json'});
            hideSaveLayerConfigDialog();
            return ajaxRequest.map(ajaxObject => fetchSavedLayerConfigList(ajaxObject));
        });


export default saveLayerConfigForUserEpic;