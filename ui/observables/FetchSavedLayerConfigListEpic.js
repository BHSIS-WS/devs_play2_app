"use strict";

import { ajax } from 'rxjs/observable/dom/ajax';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';

import { updateSavedLayerConfigList } from '../actions/LayerConfigHistoryAction';
import { OBSERVABLE_DEBOUNCE_DUE_TIME } from '../constant/constants';

const fetchSavedLayerConfigListEpic = (action$, store) =>
    action$.ofType('FETCH_SAVED_LAYER_CONFIG_LIST')
        .debounceTime(OBSERVABLE_DEBOUNCE_DUE_TIME)
        .mergeMap(() => {
            let userSavedLayerConfigListJson = ajax.getJSON('/devs2/fetchSavedLayerConfigList');
            return userSavedLayerConfigListJson
                .map(data => {
                    let savedLayerConfigList = generateStateObjectsFromData(data, store);
                    return updateSavedLayerConfigList(savedLayerConfigList);
                });
        });


const generateStateObjectsFromData = (data, store) => {

    let savedLayerConfigList = [];
    let layerConfigList = data.layerConfigList;

    for(let i = 0; i < layerConfigList.length; i++) {
        let layerConfig = {};
        let currentLayerConfig = layerConfigList[i];
        let jsonConfig = JSON.parse(currentLayerConfig.configJsonString);
        layerConfig = {
            userSavedLayerConfigId: currentLayerConfig.userSavedLayerConfigId,
            configDescription: currentLayerConfig.configDescription,
            configName: currentLayerConfig.configName,
            lastModified: new Date(currentLayerConfig.lastModified),
            normalLayers: {
                expandSelectedLayerId: jsonConfig.normalLayers.expandSelectedLayerId,
                expandedLayerIdSet: new Set(jsonConfig.normalLayers.expandedLayerIdSet),
                selectedCriteriaCodeIdMap: generateMapFromObject(jsonConfig.normalLayers.selectedCriteriaCodeIdMap),
                selectedCriteriaIdMap: generateSelectedCriteriaIdMapFromObject(jsonConfig.normalLayers.selectedCriteriaIdMap),
                selectedCriteriaIdSet: new Set(jsonConfig.normalLayers.selectedCriteriaIdSet),
                selectedLayerIdSet: new Set(jsonConfig.normalLayers.selectedLayerIdSet),
                expandedCriteriaIdSet: new Set(jsonConfig.normalLayers.expandedCriteriaIdSet),
                criteriaConditionsMap: generateMapFromObject(jsonConfig.normalLayers.criteriaConditionsMap)
            },
            gradientLayers: {
                expanded: jsonConfig.gradientLayers.expanded,
                expandedDataSetIdSet: new Set(jsonConfig.gradientLayers.expandedDataSetIdSet),
                filterExpandedLayerIdSet: new Set(jsonConfig.gradientLayers.filterExpandedLayerIdSet),
                gradFilterMap: generateMapFromObject(jsonConfig.gradientLayers.gradFilterMap),
                selectedGradLayerIdSet: new Set(jsonConfig.gradientLayers.selectedGradLayerIdSet)
            },
            layers: {
                visibleLayerKeySet: new Set(jsonConfig.layers.visibleLayerKeySet),
                borderWidthMap: generateMapFromObject(jsonConfig.layers.borderWidthMap)
            },
            d3LayerConfig: {
                disabledGradientLayerIdSet: new Set(jsonConfig.d3LayerConfig.disabledGradientLayerIdSet),
                disabledLayerIdSet: new Set(jsonConfig.d3LayerConfig.disabledLayerIdSet),
                gradientLayerConfigMap: generateMapFromObject(jsonConfig.d3LayerConfig.gradientLayerConfigMap),
                layerConfigMap: generateD3LayerConfigMapFromObject(jsonConfig.d3LayerConfig.layerConfigMap)
            },
            mapCanvasD3 : {
                ...jsonConfig.mapCanvasD3
            },
            mapVariables : {
                ...jsonConfig.mapVariables,
                dropMarkerDialogVisible: false,
                dropMarkerType: null,
                enteredLat: null,
                enteredLng: null,
                rightClickContextMenuPosition: null,
                rightClickCoordinates: null,
                rightClickedMarker: null,
                rightClickedShape: null,
                shouldZoomMap: true
            },
            toolbarOptions : {
                ...store.getState().toolbarOptions,
                renderStatusLayerList: jsonConfig.toolbarOptions.renderStatusLayerList
            }
        };
        if (typeof jsonConfig.d3MapToolOptions !== "undefined") {
            layerConfig = {
                ...layerConfig,
                d3MapToolOptions: {
                    d3MapTextTool: {
                        optionDialogOpen: jsonConfig.d3MapToolOptions.d3MapTextTool.optionDialogOpen,
                        activeTextId: jsonConfig.d3MapToolOptions.d3MapTextTool.activeTextId,
                        textToolConfigMap: generateMapFromObject(
                            jsonConfig.d3MapToolOptions.d3MapTextTool.textToolConfigMap)
                    },
                    d3MapRectangleTool: {
                        drawingCanvasOn: jsonConfig.d3MapToolOptions.d3MapRectangleTool.drawingCanvasOn,
                        rectOptionDialogOpen: jsonConfig.d3MapToolOptions.d3MapRectangleTool.rectOptionDialogOpen,
                        activeRectId: jsonConfig.d3MapToolOptions.d3MapRectangleTool.activeRectId,
                        rectToolConfigMap: generateMapFromObject(
                            jsonConfig.d3MapToolOptions.d3MapRectangleTool.rectToolConfigMap)
                    }
                }
            };
        }
        savedLayerConfigList.push(layerConfig);
    }
    return savedLayerConfigList;
};

const generateMapFromObject = function(gradientFilterMap) {
    let newGradientFilterMap = new Map();
    if (gradientFilterMap !== undefined) {
        Object.keys(gradientFilterMap).map(function(layerId) {
            newGradientFilterMap.set(parseInt(layerId), gradientFilterMap[layerId]);
        });
    }
    return newGradientFilterMap;
};


const generateD3LayerConfigMapFromObject = (layerConfigMap) => {
    let newLayerConfigMap = new Map();
    Object.keys(layerConfigMap).map(function(layerId) {
        let layerConfig = layerConfigMap[layerId];
        let newShapeConfigMap = new Map();
        Object.keys(layerConfig.shapeConfigMap).map(function(shapeConfigKey) {
            if (!isNaN(shapeConfigKey)) {
                newShapeConfigMap.set(parseInt(shapeConfigKey), layerConfig.shapeConfigMap[shapeConfigKey]);
            } else {
                let configMap = new Map();
                Object.keys(layerConfig.shapeConfigMap[shapeConfigKey]).map(function(shapeConfig) {
                    configMap.set(shapeConfig, layerConfig.shapeConfigMap[shapeConfigKey][shapeConfig]);
                });
                newShapeConfigMap.set(shapeConfigKey, configMap);
            }
        });
        layerConfig = {
            ...layerConfig,
            shapeConfigMap: newShapeConfigMap,
            hideLegendConfigSet: new Set(layerConfig.hideLegendConfigSet)
        };
        newLayerConfigMap.set(parseInt(layerId), layerConfig);
    });
    return newLayerConfigMap;
};

const generateSelectedCriteriaIdMapFromObject = (selectedCriteriaIdMap) => {
    let newSelectedCriteriaIdMap = new Map();
    Object.keys(selectedCriteriaIdMap).map(function(layerId) {
        let criteriaIdMap = new Map();
        let criteriaIdObject = selectedCriteriaIdMap[layerId];
        Object.keys(criteriaIdObject).map(function(criteriaId) {
            let criteriaObject = criteriaIdObject[criteriaId];
            let criteriaSet = new Set();
            criteriaObject.forEach(criteria => {
                criteriaSet.add(criteria);
            });
            criteriaIdMap.set(parseInt(criteriaId), criteriaSet);
        });
        newSelectedCriteriaIdMap.set(parseInt(layerId), criteriaIdMap);
    });
    return newSelectedCriteriaIdMap;
};

export default fetchSavedLayerConfigListEpic;