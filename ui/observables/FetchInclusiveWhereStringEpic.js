"use strict";

import { ajax } from 'rxjs/observable/dom/ajax';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/map';

import FusionLayerFilterCriteriaComponent from '../components/FusionLayerFilterCriteriaComponent';
import {setInclusiveWhereString, refreshMapLayers } from '../actions/NormalLayersAction';
import {Observable} from "rxjs";

const fetchInclusiveWhereStringEpic = (action$, store) =>
    action$.ofType('FETCH_INCLUSIVE_WHERE_STRING_FOR_CARTO')
        .mergeMap((action) => {
            let ajaxRequest = ajax.post('/devs2/geoJsonCarto/'+action.layerId, JSON.stringify(action.layerParams), {'Content-Type': 'application/json'})
            return ajaxRequest.map(ajaxObject => {
                console.log(ajaxObject);
                return(setInclusiveWhereString(action.layerId, ajaxObject));
            });
        });



export default fetchInclusiveWhereStringEpic;