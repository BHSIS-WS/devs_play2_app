import {combineEpics} from 'redux-observable';
import { ajax } from 'rxjs/observable/dom/ajax';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/concat';
import 'rxjs/add/observable/zip';
import 'rxjs/add/observable/of';
import  { fetchLayerDataListEpic, downloadDataTableExcelEpic }from './FetchLayerDataListEpic';
import fetchSavedLayerConfigListEpic from './FetchSavedLayerConfigListEpic';
import saveLayerConfigForUserEpic from './SaveLayerConfigForUserEpic';
import submitFeedbackEpic from './SubmitFeedbackEpic';
import removeSavedLayerConfigEpic from './RemoveSavedLayerConfigEpic';
import fetchInclusiveWhereStringEpic from './FetchInclusiveWhereStringEpic';
import {initializeD3LayerConfigs} from "../actions/D3LayerConfigAction";
import {fetchLayerDataList} from "../actions/LayerDatatListingAction";
import MapContentModel from '../others/MapContentModel';


const initStateEpic = action$ =>
    action$.ofType('INIT_STATE_REF')
        .mergeMap(action => {
            const refs = ajax.getJSON('/devs2/ref');
            return refs.switchMap(data => [({type: 'ASSIGN_STATE_REF', params: data}), initializeD3LayerConfigs(data)]);
        });

const initStateEpicHelp = action$ =>
    action$.ofType('INIT_STATE_HELP')
        .mergeMap(action => {
            const refs = ajax.getJSON('/devs2/fetchHelpTopicList');
            return refs
                .map(data => ({type: 'ASSIGN_STATE_HELP', params: data}));
        });

const clearLayerSelectEpic = action$ =>
    action$.ofType('CLEAR_LAYER_SELECT')
        .mergeMap(action => {
            return Observable.of({type:"CLEAR_GRADIENT_LAYER_SELECT"});
        });

const clearGradientLayerSelectEpic = action$ =>
    action$.ofType('CLEAR_GRADIENT_LAYER_SELECT')
        .mergeMap(action => {
            MapContentModel.refreshGoogleMapLayers();
            return Observable.of(fetchLayerDataList());
        });


const detectCanvasChangesEpic = action$ =>
    action$.filter(action => {
            switch (action.type)
            {
                case 'RESIZE_CANVAS':
                case "HEIGHT_D3_CHANGE":
                case "WIDTH_D3_CHANGE":
                case 'ZOOM_D3_CHANGE':
                case 'PROJECTION_D3_CHANGE':
                case 'CENTER_D3_CHANGE':

                case 'CHANGE_D3_MAP_TEXT_TOOL_FONT_SIZE':
                case 'CHANGE_D3_MAP_TEXT_TOOL_ROTATE':
                case 'CHANGE_D3_MAP_TEXT_TOOL_FILL_COLOR':
                case 'CHANGE_D3_MAP_TEXT_TOOL_TEXT_CONTENT':
                case 'CHANGE_D3_MAP_TEXT_TOOL_FONT_FAMILY':
                case 'TOGGLE_D3_MAP_TEXT_TOOL_BOLD':
                case 'TOGGLE_D3_MAP_TEXT_TOOL_ITALIC':
                case 'TOGGLE_D3_MAP_TEXT_TOOL_UNDERLINE':
                case 'APPLY_D3_MAP_TEXT_TOOL_OPTIONS':
                case 'ADD_D3_MAP_TEXT':
                case 'REMOVE_D3_MAP_TEXT':

                case 'APPLY_D3_MAP_RECT_TOOL_OPTIONS':
                case 'ADD_D3_MAP_RECT':
                case 'REMOVE_D3_MAP_RECT':
                case 'CHANGE_D3_MAP_RECT_TOOL_WIDTH':
                case 'CHANGE_D3_MAP_RECT_TOOL_HEIGHT':
                case 'CHANGE_D3_MAP_RECT_TOOL_STROKE':
                case 'CHANGE_D3_MAP_RECT_TOOL_STROKE_WIDTH':
                    return true;
                default:
                    return false;
            }
        })
        .debounceTime(2000)
        .map(action => ({type: 'D3_CANVAS_CHANGE'}) );


const allEpics = combineEpics(
    initStateEpic,
    initStateEpicHelp,
    detectCanvasChangesEpic,
    fetchLayerDataListEpic,
    downloadDataTableExcelEpic,
    fetchSavedLayerConfigListEpic,
    saveLayerConfigForUserEpic,
    submitFeedbackEpic,
    removeSavedLayerConfigEpic,
    fetchInclusiveWhereStringEpic,
    clearLayerSelectEpic,
    clearGradientLayerSelectEpic
);

export default allEpics;