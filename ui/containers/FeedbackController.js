import React from "react";
import {connect} from 'react-redux';
import FeedbackComponent from '../components/FeedbackComponent';

const mapStateToProps = (state) => {
    return {
        successMessage: state.feedbackForm.successMessage,
        result: state.feedbackForm.result,
        gRecaptchaResponse: state.captcha.gRecaptchaResponse,
        showFeedbackPanel: state.sidebarOptions.showFeedbackPanel
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch : dispatch
    }
};

let FeedbackController = connect(mapStateToProps,mapDispatchToProps)(FeedbackComponent);

export default FeedbackController;