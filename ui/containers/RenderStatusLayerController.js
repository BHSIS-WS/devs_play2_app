import React from "react";
import {connect} from 'react-redux';
import RenderStatusLayerComponent from '../components/RenderStatusLayerComponent.js';

const mapStateToProps = (state, ownProps) => {
    if (ownProps.layerObject.disabled) {
        return {
            renderStatus: "Disabled"
        }
    }

    let renderStatusLayer = state.toolbarOptions.renderStatusLayerList.find((renderStatusLayer) => {
        if (renderStatusLayer.layerId === ownProps.layerObject.layer.id
        && renderStatusLayer.layerType === ownProps.layerObject.layerType) {
            return renderStatusLayer;
        }
    });

    return {
        renderStatus: renderStatusLayer.status
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch : dispatch
    }
};

let RenderStatusLayerController = connect(mapStateToProps,mapDispatchToProps)(RenderStatusLayerComponent);

export default RenderStatusLayerController;