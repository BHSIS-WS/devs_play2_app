import React from 'react';
import {connect} from 'react-redux';
import DynamicFilterCriteriaComponent from '../components/DynamicFilterCriteriaComponent';


const mapStateToProps = (state, ownProps) => {

    let criteriaConditionsArray = state.normalLayers.criteriaConditionsMap.get(ownProps.criteria.id);

    if (!criteriaConditionsArray) {
        criteriaConditionsArray = []
    }
    let expanded = state.normalLayers.expandedCriteriaIdSet.has(ownProps.criteria.id);

    return {
        selectedCriteriaIdSet: state.normalLayers.selectedCriteriaIdSet,
        criteriaConditionsArray: criteriaConditionsArray,
        criteria: ownProps.criteria,
        layer: ownProps.layer,
        expanded: expanded
    };
};


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch : dispatch
    };
};

let DynamicFilterCriteriaController = connect(mapStateToProps,mapDispatchToProps)( DynamicFilterCriteriaComponent );

export default DynamicFilterCriteriaController;