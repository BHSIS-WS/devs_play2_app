import React from 'react';
import {connect} from 'react-redux';
import HelpWindowComponent from '../components/HelpWindowComponent';

const mapStateToProps = (state, ownProps) => {
  return {
      showHelpPanel: state.sidebarOptions.showHelpPanel,
      helpList: state.sidebarOptions.helpList
  }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch : dispatch
  }
};

let HelpWindowController = connect(mapStateToProps, mapDispatchToProps)(HelpWindowComponent);

export default HelpWindowController;