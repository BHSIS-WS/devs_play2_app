"use strict";

import React from "react";
import {connect} from 'react-redux';
import CaptchaComponent from '../components/CaptchaComponent';
import { captchaChange } from "../actions/CaptchaAction";

const mapStateToProps = (state) => {
    return {
        gRecaptchaResponse : state.captcha.gRecaptchaResponse
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        captchaChange: (gRecaptchaResponse) => {
            dispatch(captchaChange(gRecaptchaResponse));
        }
    };
};

let CaptchaController = connect(mapStateToProps,mapDispatchToProps)(CaptchaComponent);

export default CaptchaController;