import React from 'react';
import {connect} from 'react-redux';
import D3MapRectToolOptionDialogComponent from '../../components/mapd3tools/D3MapRectToolOptionDialogComponent';

const mapStateToProps = (state, ownProps) => {
    return {
        rectOptionDialogOpen: state.d3MapToolOptions.d3MapRectangleTool.rectOptionDialogOpen,
        activeRectId: state.d3MapToolOptions.d3MapRectangleTool.activeRectId,
        rectToolConfigMap: state.d3MapToolOptions.d3MapRectangleTool.rectToolConfigMap
    }

};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch : dispatch
    }
};

let D3MapRectToolOptionDialogController = connect(mapStateToProps,mapDispatchToProps)(D3MapRectToolOptionDialogComponent);

export default D3MapRectToolOptionDialogController;
