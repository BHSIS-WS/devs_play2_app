import React from "react";
import {connect} from "react-redux";
import D3MapToolOptionPanel from "../../components/mapd3tools/D3MapToolOptionPanel";

const mapStateToProps = (state, ownProps) => {
  return {
    d3MapToolOptions: state.d3MapToolOptions
  }

};


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch : dispatch
  }
};

let D3MapToolOptionPanel1 = connect(mapStateToProps,mapDispatchToProps)(D3MapToolOptionPanel);

export default D3MapToolOptionPanel1;