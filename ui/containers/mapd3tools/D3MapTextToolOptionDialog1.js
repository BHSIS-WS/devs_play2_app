import React from 'react';
import {connect} from 'react-redux';
import D3MapTextToolOptionDialog from '../../components/mapd3tools/D3MapTextToolOptionDialog';

const mapStateToProps = (state, ownProps) => {
  return {
    optionDialogOpen: state.d3MapToolOptions.d3MapTextTool.optionDialogOpen,
    defaultTextToolConfig: state.d3MapToolOptions.d3MapTextTool.defaultTextToolConfig,
    activeTextId: state.d3MapToolOptions.d3MapTextTool.activeTextId,
    textToolConfigMap: state.d3MapToolOptions.d3MapTextTool.textToolConfigMap
  }

};


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch : dispatch
  }
};

let D3MapTextToolOptionDialog1 = connect(mapStateToProps,mapDispatchToProps)(D3MapTextToolOptionDialog);

export default D3MapTextToolOptionDialog1;