"use strict";

import React from "react";
import {connect} from 'react-redux';
import DropMarkerComponent from '../components/DropMarkerComponent';
import {
    dropMarkerByLatLng,
    dropMarkerByLocation,
    hideDropMarkerDialog
} from '../actions/MapVariablesAction';


const mapStateToProps = (state, ownProps) => {
    let labelText = "",
        placeholderText = "",
        buttonText = "";

    if (state.mapVariables.dropMarkerType === "latlng") {
        labelText = "Please enter Latitude and Longitude to drop the pin";
        placeholderText = "latitude, longitude";
        buttonText = "Drop Pin";
    } else if (state.mapVariables.dropMarkerType === "location") {
        labelText = "Enter location to mark on the map";
        placeholderText = "address, city, or zip code";
        buttonText = "Go";
    }
    return {
        dropMarkerDialogVisible: state.mapVariables.dropMarkerDialogVisible,
        labelText: labelText,
        placeholderText: placeholderText,
        buttonText: buttonText,
        inputType: state.mapVariables.dropMarkerType
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        closeDialog: () => {
            dispatch(hideDropMarkerDialog());
        },
        dropMarker: (inputString, inputType) => {
            if (inputType === "latlng") {
                dispatch(dropMarkerByLatLng(inputString));
            } else if (inputType === "location") {
                dispatch(dropMarkerByLocation(inputString));
            }
        }
    };
};

let DropMarkerController = connect(mapStateToProps,mapDispatchToProps)(DropMarkerComponent);

export default DropMarkerController;