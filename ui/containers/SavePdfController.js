import React from 'react'
import {connect} from 'react-redux'
import SavePdfComponent from '../components/SavePdfComponent'
import {hideSavePdfDialog, savePdf} from '../actions/ToolbarOptionsAction';

const mapStateToProps = (state) => {
    return {
        showSavePdfDialog: state.toolbarOptions.showSavePdfDialog,
        height: state.mapCanvasD3.height,
        width: state.mapCanvasD3.width
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideSavePdfDialog: () => {
            dispatch(hideSavePdfDialog());
        },
        savePdf: (width, height) => {
            dispatch(savePdf(width, height));
        }
    }
};

let SavePdfController = connect(mapStateToProps,mapDispatchToProps)( SavePdfComponent );

export default SavePdfController;