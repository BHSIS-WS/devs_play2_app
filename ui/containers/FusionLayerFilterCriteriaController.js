import React from 'react';
import {connect} from 'react-redux';
import FusionLayerFilterCriteriaComponent from '../components/FusionLayerFilterCriteriaComponent';


const mapStateToProps = (state, ownProps) => {

    let criteriaCodeArray = state.ref.layerFilteringCriteriaArray.filter((criteria) => {
        return (criteria.criteria_code_id === ownProps.criteria.id);
    });

    let expanded = state.normalLayers.expandedCriteriaIdSet.has(ownProps.criteria.id);

    return {
        selectedMapTab: state.selectedMapTab,
        selectedCriteriaIdSet: state.normalLayers.selectedCriteriaIdSet,
        selectedLayerIdSet: state.normalLayers.selectedLayerIdSet,
        criteriaCodeArray: criteriaCodeArray,
        criteria: ownProps.criteria,
        layer: ownProps.layer,
        expanded: expanded,
        layerArray: state.ref.layerArray,
        dataTableAutoRefresh: state.layerDataListing.autoRefresh
    };
};


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch : dispatch
    };
};

let FusionLayerFilterCriteriaController = connect(mapStateToProps,mapDispatchToProps)( FusionLayerFilterCriteriaComponent );

export default FusionLayerFilterCriteriaController;