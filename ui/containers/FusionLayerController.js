import React from 'react';
import {connect} from 'react-redux';
import FusionLayerComponent from '../components/FusionLayerComponent';


const mapStateToProps = (state, ownProps) => {
    let selected = state.normalLayers.selectedLayerIdSet.has(ownProps.layer.id);
    let expanded = state.normalLayers.expandedLayerIdSet.has(ownProps.layer.id);
    let layerCriteriaArray = state.ref.layerFilteringCriteriaArray.filter((criteria) => {
        return criteria.fusion_map_layer_id === ownProps.layer.id;
    });
    let criteriaArray = [...new Set(layerCriteriaArray.map(item => item.criteria_code_id))];

    return {
        layerCriteriaArray: layerCriteriaArray,
        criteriaArray: criteriaArray,
        selected: selected,
        expanded: expanded,
        dataTableAutoRefresh: state.layerDataListing.autoRefresh,
        selectedLayerIdSet: state.normalLayers.selectedLayerIdSet,
        selectedMapTab: state.selectedMapTab,
        layerArray: state.ref.layerArray
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch: dispatch
    };
};

let FusionLayerController = connect(mapStateToProps, mapDispatchToProps)(FusionLayerComponent);

export default FusionLayerController;