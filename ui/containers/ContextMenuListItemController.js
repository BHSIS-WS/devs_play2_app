"use strict";

import React from "react";
import {connect} from 'react-redux';
import ContextMenuListItemComponent from '../components/ContextMenuListItemComponent';

const mapStateToProps = (state, ownProps) => {
    return {
        id: ownProps.id,
        message: ownProps.message,
        cssClass: ownProps.cssClass
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    if (typeof ownProps.actionMethod !== "undefined") {
        return {
            actionMethod: () => {
                dispatch(ownProps.actionMethod());
            }
        };
    }

    return {
        actionMethod: () => {
            console.log("click")
        }
    };
};

let ContextMenuListItemController = connect(mapStateToProps,mapDispatchToProps)(ContextMenuListItemComponent);

export default ContextMenuListItemController;