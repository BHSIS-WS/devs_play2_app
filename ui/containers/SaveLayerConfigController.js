import React from 'react'
import {connect} from 'react-redux'
import SaveLayerConfigComponent from '../components/SaveLayerConfigComponent'
import {
    hideSaveLayerConfigDialog,
    saveLayerConfigForUser,
    showSaveConfigDuplicateNameError
} from "../actions/ToolbarOptionsAction";

const mapStateToProps = (state) => {
    return {
        showSaveLayerConfigDialog: state.toolbarOptions.showSaveLayerConfigDialog,
        showSaveConfigDuplicateNameError: state.toolbarOptions.showSaveConfigDuplicateNameError
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideSaveLayerConfigDialog: () => {
            dispatch(hideSaveLayerConfigDialog());
        },
        saveLayerConfigForUser: (name, description) => {
            dispatch(saveLayerConfigForUser(name, description));
        },
        showSaveConfigDuplicateNameErrorAction: (show) => {
            dispatch(showSaveConfigDuplicateNameError(show));
        }
    }
};

let SaveLayerConfigController = connect(mapStateToProps,mapDispatchToProps)( SaveLayerConfigComponent );

export default SaveLayerConfigController;