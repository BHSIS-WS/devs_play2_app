import React from "react";
import {connect} from 'react-redux';
import RenderStatusPanelComponent from '../components/RenderStatusPanelComponent';

const mapStateToProps = (state) => {
    let selectedLayers = [];

    if (state.ref.dataInitialized) {
        let gradientLayers = state.ref.gradientLayerArray;
        let normalLayers = state.ref.layerArray;
        let disabledGradientLayers = state.d3LayerConfig.disabledGradientLayerIdSet;
        let disabledLayers = state.d3LayerConfig.disabledLayerIdSet;

        state.toolbarOptions.renderStatusLayerList.forEach((layer) => {
            for(let i = 0; i < normalLayers.length; i++) {
                if (layer.layerType === "normal" && layer.layerId === normalLayers[i].id) {
                    let disabled = false;
                    if (disabledLayers.has(layer.layerId)) {
                        disabled = true;
                    }
                    selectedLayers.push({layer: normalLayers[i], layerType: "normal", disabled: disabled});
                    break;
                }
            }
        });

        state.toolbarOptions.renderStatusLayerList.forEach((layer) => {
            for(let i = 0; i < gradientLayers.length; i++) {
                if (layer.layerType === "gradient" && layer.layerId === gradientLayers[i].id) {
                    let disabled = false;
                    if (disabledGradientLayers.has(layer.layerId)) {
                        disabled = true;
                    }
                    selectedLayers.push({layer: gradientLayers[i], layerType: "gradient", disabled: disabled});
                    break;
                }
            }
        });
    }

    return {
        showRenderStatusPanel: state.toolbarOptions.showRenderStatusPanel,
        selectedLayers: selectedLayers
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch : dispatch
    }
};

let RenderStatusPanelController = connect(mapStateToProps,mapDispatchToProps)(RenderStatusPanelComponent);

export default RenderStatusPanelController;