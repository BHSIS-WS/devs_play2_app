import React from 'react'
import {connect} from 'react-redux'
import GradientLayerList from '../components/GradientLayerList'


const mapStateToProps = (state, ownProps) => {
  return {
    gradientDataSetArray: state.ref.gradientDataSetArray,
    gradientLayerArray: state.ref.gradientLayerArray,
    expandedDataSetIdSet: state.gradientLayers.expandedDataSetIdSet,
    expanded: state.gradientLayers.expanded
  }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch : dispatch
  }
};

let GradientLayers = connect(mapStateToProps,mapDispatchToProps)(GradientLayerList);

export default GradientLayers;