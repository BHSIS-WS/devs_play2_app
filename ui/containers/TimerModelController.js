import React from 'react'
import {connect} from 'react-redux'
import 'rxjs/add/operator/map';
import TimerModelComponent from '../components/TimerModelComponent';
import { processIdleOn, processIdleLogout,
    processContinueSession, processUserAction } from '../actions/IdleTimerAction';

// Use this function to convert max session times to milliseconds for IdleTimer purposes
const convertToMilliseconds = (hrs,min) => {
    return( ((hrs*60*60) + (min*60)) * 1000);
};

const mapStateToProps = (state, ownProps) => {
    let maxAge = convertToMilliseconds(0,state.idleTimeout.finalTimeout),
        warnAge = convertToMilliseconds(0,state.idleTimeout.warnTimeout);
    return {
        idleTimer: state.idleTimeout.idleTimer,
        warnTimeout: warnAge,
        finalTimeout: maxAge,
        isIdle: state.idleTimeout.isIdle
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
       processIdleOn: () => {
            dispatch(processIdleOn());
        },
        processIdleLogout: () => {
           dispatch(processIdleLogout());
        },
        processContinueSession: () => {
           dispatch(processContinueSession());
        },
        processUserAction: (lastActiveTime, remainingTime, elapsedTime) => {
           dispatch(processUserAction(lastActiveTime, remainingTime, elapsedTime));
        },
        dispatch : dispatch
    }
};

let TimerModelController = connect(mapStateToProps,mapDispatchToProps)(TimerModelComponent);

export default TimerModelController;

