import React from 'react'
import {connect} from 'react-redux'
import SavedLayerConfigComponent from '../components/SavedLayerConfigComponent'
import { setD3LayerConfigStateFromConfig } from '../actions/D3LayerConfigAction';
import { setGradientLayersStateFromConfig } from '../actions/GradientLayersAction';
import { setLayersStateFromConfig } from "../actions/LayersAction";
import {removeAllShapes, setMapVariablesStateFromConfig} from "../actions/MapVariablesAction";
import { setMapCanvasD3StateFromConfig } from "../actions/MapCanvasD3Action";
import { setNormalLayersStateFromConfig } from "../actions/NormalLayersAction";
import {setD3MapToolOptionsStateFromConfig} from "../actions/D3MapToolOptionsAction";
import { setToolbarOptionsStateFromConfig, showRemoveConfigDialog } from '../actions/ToolbarOptionsAction';
import { toggleVisibilityForRenderStatusPanel } from "../actions/ToolbarOptionsAction";
import mapContentModel from '../others/MapContentModel';
import printContentModel from '../others/PrintContentModel';

const mapStateToProps = (state) => {
    return {
        staticState: state.ref
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadSavedLayerConfig: (config) => {
            dispatch(setD3LayerConfigStateFromConfig(config.d3LayerConfig));
            dispatch(setGradientLayersStateFromConfig(config.gradientLayers));
            dispatch(setLayersStateFromConfig(config.layers));
            dispatch(removeAllShapes());
            dispatch(setMapVariablesStateFromConfig(config.mapVariables));
            dispatch(setMapCanvasD3StateFromConfig(config.mapCanvasD3));
            dispatch(setNormalLayersStateFromConfig(config.normalLayers));
            dispatch(setD3MapToolOptionsStateFromConfig(config.d3MapToolOptions));
            dispatch(setToolbarOptionsStateFromConfig(config.toolbarOptions));
            dispatch(toggleVisibilityForRenderStatusPanel(true));
            mapContentModel.refreshGoogleMapLayers();
            printContentModel.refreshD3MapLayersFromState();
        },
        showRemoveConfigDialog: (config) => {
            dispatch(showRemoveConfigDialog(config));
        }
    }
};

let SavedLayerConfigController = connect(mapStateToProps,mapDispatchToProps)( SavedLayerConfigComponent );

export default SavedLayerConfigController;