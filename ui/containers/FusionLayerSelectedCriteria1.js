import React from 'react'
import {connect} from 'react-redux'
import FusionLayerSelectedCriteria from '../components/FusionLayerSelectedCriteria'


const mapStateToProps = (state) => {
  return {
    layerCriteriaArray: state.ref.layerFilteringCriteriaArray,
    selectedCriteriaIdSet: state.normalLayers.selectedCriteriaIdSet,
    expandSelectedLayerId: state.normalLayers.expandSelectedLayerId,
    selectedMapTab: state.selectedMapTab
  }
};


const mapDispatchToProps = (dispatch) => {
  return {
    dispatch : dispatch
  }
};

let FusionLayerSelectedCriteria1 = connect(mapStateToProps,mapDispatchToProps)( FusionLayerSelectedCriteria );

export default FusionLayerSelectedCriteria1;