import React from 'react'
import {connect} from 'react-redux'
import SavedLayerConfigPanelComponent from '../components/SavedLayerConfigPanelComponent'

const mapStateToProps = (state) => {
    return {
        configList: state.layerConfigHistory.savedLayerConfigList,
        showSavedLayerConfigPanel: state.sidebarOptions.showSavedLayerConfigPanel,
        selectedMapTab: state.selectedMapTab,
        showSidebarButtons: state.sidebarOptions.showSidebarButtons
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch : dispatch
    }
};

let SavedLayerConfigPanelController = connect(mapStateToProps,mapDispatchToProps)( SavedLayerConfigPanelComponent );

export default SavedLayerConfigPanelController;