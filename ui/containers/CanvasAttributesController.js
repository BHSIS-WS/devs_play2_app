import React from 'react';
import {connect} from 'react-redux';
import CanvasAttributesComponent from '../components/CanvasAttributesComponent';
import { hideCanvasAttributesDialog } from "../actions/ToolbarOptionsAction";

const mapStateToProps = (state) => {
    return {
        mapCanvasD3: state.mapCanvasD3,
        dataInitialized: state.ref.dataInitialized,
        layerArray: state.ref.layerArray,
        selectedLayerIdSet: state.normalLayers.selectedLayerIdSet,
        gradientLayerArray: state.ref.gradientLayerArray,
        selectedGradLayerIdSet: state.gradientLayers.selectedGradLayerIdSet,
        visibleLayerKeySet: state.layers.visibleLayerKeySet,
        borderWidthMap: state.layers.borderWidthMap,
        selectedMapTab: state.selectedMapTab,
        showCanvasAttributesDialog: state.toolbarOptions.showCanvasAttributesDialog
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideCanvasAttributesDialog: () => {
            dispatch(hideCanvasAttributesDialog());
        },
        dispatch: dispatch
    }
};

let CanvasAttributesController = connect(mapStateToProps,mapDispatchToProps)( CanvasAttributesComponent );

export default CanvasAttributesController;