import React from "react";
import {connect} from 'react-redux';
import RefreshMapComponent from '../components/RefreshMapComponent.js';

const mapStateToProps = (state) => {
    return {
        selectedMapTab: state.selectedMapTab
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    }
};

let RefreshMapController = connect(mapStateToProps,mapDispatchToProps)(RefreshMapComponent);

export default RefreshMapController;