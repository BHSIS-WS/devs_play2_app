import React from "react";
import {connect} from 'react-redux';
import MinimapPanelComponent from '../components/MinimapPanelComponent.js';

const mapStateToProps = (state, ownProps) => {
    return {
        showMinimapPanel: state.toolbarOptions.showMinimapPanel,
        projection: state.mapCanvasD3.projection,
        zoom: state.mapCanvasD3.zoom,
        width: state.mapCanvasD3.width,
        height: state.mapCanvasD3.height,
        translateX: state.mapCanvasD3.translateX,
        translateY: state.mapCanvasD3.translateY
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    }
};

let MinimapPanelController = connect(mapStateToProps,mapDispatchToProps)(MinimapPanelComponent);

export default MinimapPanelController;