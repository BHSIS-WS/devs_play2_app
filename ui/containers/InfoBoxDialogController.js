import React from "react";
import {connect} from "react-redux";
import InfoBoxDialogComponent from '../components/InfoBoxDialogComponent';
import { toggleInfoBoxMaxLimitDialog } from "../actions/PrintInfoBoxAction";

const mapStateToProps = (state) => {
    return {
        maxLimitDialogBoxOpen: state.printInfoBox.maxLimitDialogBoxOpen,
        maxInfoBox: state.printInfoBox.maxInfoBox
    }
};

const mapDispatchToProps = (dispatch) => {
  return {
      toggleInfoBoxMaxLimitDialog: (value) => {
          dispatch(toggleInfoBoxMaxLimitDialog(value));
      }
  }
};

let InfoBoxDialogController = connect(mapStateToProps, mapDispatchToProps)( InfoBoxDialogComponent );

export default InfoBoxDialogController;