import React from 'react'
import {connect} from 'react-redux'
import ContextMenuComponent from '../components/ContextMenuComponent'
import * as MapAction from '../actions/MapVariablesAction';

const mapStateToProps = (state, ownProps) => {
    let listItems = [];
    let contextMenuItemCssClass = "contextMenuItem";
    let contextMenuItemInfoCssClass = "contextMenuItemInfo";
    if (state.mapVariables.rightClickedMarker === null && state.mapVariables.rightClickedShape === null) {
        listItems.push({id: "1", cssClass: contextMenuItemCssClass, message: "Go to a location", actionMethod: () => MapAction.showDropMarkerDialog("location")});
        listItems.push({id: "2", cssClass: contextMenuItemCssClass, message: "Drop a pin here", actionMethod: MapAction.dropMarkerForRightClickCoordinates});
        listItems.push({id: "3", cssClass: contextMenuItemCssClass, message: "Drop pin by latitude and longitude", actionMethod: () => MapAction.showDropMarkerDialog("latlng")});
        listItems.push({id: "4", cssClass: contextMenuItemCssClass, message: "Zoom in", actionMethod: MapAction.zoomMapIn});
        listItems.push({id: "5", cssClass: contextMenuItemCssClass, message: "Zoom out", actionMethod: MapAction.zoomMapOut});
        listItems.push({id: "6", cssClass: contextMenuItemCssClass, message: "Center map here", actionMethod: MapAction.centerMapOnRightClickCoordinates});
        // listItems.push({id: "7", cssClass: contextMenuItemCssClass, message: "Refresh Layers According to Pre-Defined Order", actionMethod: MapAction.refreshMap});
    } else if (state.mapVariables.rightClickedMarker !== null) {
        let markers = state.mapVariables.mapMarkers;
        let markerLocations = state.mapVariables.mapMarkerLocations;
        let marker = state.mapVariables.rightClickedMarker;
        let location = null;

        let markerLocationString = "Unidentified Place";
        let countyString = "Unknown County";
        let index = markers.indexOf(marker);
        if (index > -1) {
            location = markerLocations[index];
            if (location != null && location.address_components != null) {
                for (let i = 0; i < location.address_components.length; i++) {
                    if (location.address_components[i].types != null) {
                        if (location.address_components[i].types.includes("administrative_area_level_2")) {
                            countyString = location.address_components[i].long_name;
                            break;
                        }
                    }
                }

                for (let i =0; i < location.address_components.length; i++) {
                    if (location.address_components[i].types != null) {
                        if (location.address_components[i].types.includes("locality")) {
                            markerLocationString = location.address_components[i].long_name;
                            break;
                        }
                    }
                }
            }
        }

        listItems.push({id: "1", cssClass: contextMenuItemCssClass, message: "Remove this pin", actionMethod: MapAction.removeRightClickedMarker});
        listItems.push({id: "2", cssClass: contextMenuItemCssClass, message: "Remove all pin(s)", actionMethod: MapAction.removeAllMarkers});
        listItems.push({id: "3", cssClass: contextMenuItemCssClass, message: "Remove other pin(s)", actionMethod: MapAction.removeNonRightClickedMarkers});
        listItems.push({id: "4", cssClass: contextMenuItemInfoCssClass, message: "Latitude & Longitude: " + marker.position.lat().toFixed(6) + ", " + marker.position.lng().toFixed(6)});

    } else if (state.mapVariables.rightClickedShape !== null) {
        listItems.push({id: "1", cssClass: contextMenuItemCssClass, message: "Remove this shape", actionMethod: MapAction.removeRightClickedShape});
        listItems.push({id: "2", cssClass: contextMenuItemCssClass, message: "Toggle shape editable", actionMethod: MapAction.toggleEditableRightClickedShape});
        listItems.push({id: "3", cssClass: contextMenuItemCssClass, message: "Toggle shape movable", actionMethod: MapAction.toggleDraggableRightClickedShape});
    }
    return {
        rightClickContextMenuPosition: state.mapVariables.rightClickContextMenuPosition,
        listItems: listItems
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch : dispatch
    }
};

let ContextMenuController = connect(mapStateToProps,mapDispatchToProps)(ContextMenuComponent);

export default ContextMenuController;


