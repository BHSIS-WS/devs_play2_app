import React from 'react';
import {connect} from 'react-redux';
import D3PointLayerConfig from '../../components/mapd3control/D3PointLayerConfig';

const mapStateToProps = (state, ownProps) => {
    let layerConfig = state.d3LayerConfig.layerConfigMap.get(ownProps.layer.id);
    let layer = ownProps.layer;
    let categoricalCodes = [];
    let categoricalListArray = [];
    let selectedVariableForStyle = layerConfig.selectedVariableForStyle;

    if (layer.style_column_json !== null) {
        layer.style_column_json.map(layerStyle => {
            let name = JSON.parse(layerStyle).name;
            if (name !== "custom") {
                categoricalCodes.push(name);
            }
        });

        if (!selectedVariableForStyle){
            selectedVariableForStyle = categoricalCodes[0];
        }

        let categoricalListString = layer.style_column_json.filter((layerStyle) => {
            return JSON.parse(layerStyle).name === selectedVariableForStyle;
        });

        let criteriaShapeConfigMap = layerConfig.shapeConfigMap.get(selectedVariableForStyle);
        categoricalListArray = categoricalListString ? (JSON.parse(categoricalListString).value).map((criteria, index) => {
            return ({
                id: index,
                name: criteria,
                shapeConfigStyle: criteriaShapeConfigMap ? criteriaShapeConfigMap.get(criteria) : null
            });
        }) : null;
    }


    return {
        layerConfig: layerConfig,
        colorPalette: state.colorPalette,
        svgShapes: state.ref.svgShapes,
        disabledLayerIdSet: state.d3LayerConfig.disabledLayerIdSet,
        expanded : layerConfig.expanded,
        selectedTab : layerConfig.selectedTab !== undefined ? layerConfig.selectedTab : "shape",
        isShapeByVariable : layerConfig.configShapeStyleByVariable !== undefined ? layerConfig.configShapeStyleByVariable : false,
        coordinateAccuracyTolerance : layerConfig.coordinateAccuracyTolerance !== undefined ? layerConfig.coordinateAccuracyTolerance : "",
        commonShapeForAllLayers : layerConfig.commonShapeForAllLayers !== undefined ? layerConfig.commonShapeForAllLayers : false,
        legendDisplayIncluded : layerConfig.legendDisplayIncluded !== undefined ? layerConfig.legendDisplayIncluded : false,
        shapeForAllStyle : layerConfig.shapeForAllStyle,
        shapeConfigMap : layerConfig.shapeConfigMap !== undefined ? layerConfig.shapeConfigMap : new Map(),
        freezeShapeSizeWhenZoom : layerConfig.freezeShapeSizeWhenZoom !== undefined ? layerConfig.freezeShapeSizeWhenZoom : false,
        selectedVariableForStyle : selectedVariableForStyle,
        categoricalCodes: categoricalCodes,
        categoricalListArray: categoricalListArray
    };

};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: dispatch
    };
};

let D3PointLayerConfig1 = connect(mapStateToProps,mapDispatchToProps)(D3PointLayerConfig);

export default D3PointLayerConfig1;