import React from 'react'
import {connect} from 'react-redux'
import D3MapControl from '../../components/mapd3control/D3MapControl'


const mapStateToProps = (state, ownProps) => {
    return {
        mapCanvasD3: state.mapCanvasD3,
        dataInitialized: state.ref.dataInitialized,
        colorPalette: state.colorPalette,
        layerArray: state.ref.layerArray,
        selectedLayerIdSet: state.normalLayers.selectedLayerIdSet,
        gradientLayerArray: state.ref.gradientLayerArray,
        selectedGradLayerIdSet: state.gradientLayers.selectedGradLayerIdSet,
        visibleLayerKeySet: state.layers.visibleLayerKeySet,
        borderWidthMap: state.layers.borderWidthMap,
        selectedMapTab: state.selectedMapTab,
        showSelectLayersPanel: state.sidebarOptions.showSelectLayersPanel,
        showLayerPrintControlPanel: state.sidebarOptions.showLayerPrintControlPanel
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch : dispatch
    }
};

let D3MapControl1 = connect(mapStateToProps,mapDispatchToProps)( D3MapControl );

export default D3MapControl1