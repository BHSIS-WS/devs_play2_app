import React from 'react'
import {connect} from 'react-redux'
import D3LayerConfigList from '../../components/mapd3control/D3LayerConfigList'
import {
    toggleD3LayerConfigPanel, toggleD3LayerDisable, updateConfigTab,
    updateGeneralType, updateShapeByVariable, initializeLayerConfigMap,
    updateCoordinateTolerance, updateCodeConfigId, updateShapeForAll, setSelectedPrintControlLayerId,
    initializeGradientConfigMap, toggleDisplayLegendCategory
} from "../../actions/D3LayerConfigAction";
import {
    toggleColorPalette,
    updateSelectedShapeConfiguration
} from "../../actions/ColorPaletteAction";


const mapStateToProps = (state) => {
    let selectedNormalLayers = state.ref.layerArray.filter(layer => state.normalLayers.selectedLayerIdSet.has(layer.id)),
        selectedGradLayers = state.ref.gradientLayerArray.filter(layer => state.gradientLayers.selectedGradLayerIdSet.has(layer.id)),
        selectedPrintControlLayerId = state.selectedPrintControlLayerId,
        selectedPrintControlLayerArray = [],
        selectedLayers,
        selectedPrintControlJson;

    if(selectedPrintControlLayerId !== '-1') {
        selectedLayers = selectedPrintControlLayerId.split('-')[0] === 'normal' ? selectedNormalLayers : selectedGradLayers;
        selectedPrintControlLayerArray = selectedLayers.filter(layer => layer.id === parseInt(selectedPrintControlLayerId.split('-')[1]));
    }
    if (selectedPrintControlLayerArray.length > 0) {
        selectedPrintControlJson = selectedPrintControlLayerArray[0];
    } else {
        selectedPrintControlLayerId = '-1';
        selectedPrintControlJson = null;
    }

    return {
        selectedLayerArray: selectedNormalLayers,
        selectedGradLayerArray: selectedGradLayers,
        selectedPrintControlLayerId: state.selectedPrintControlLayerId,
        selectedPrintControlJson: selectedPrintControlJson,
        gradientLayerArray: state.ref.gradientLayerArray,
        layerArray: state.ref.layerArray
    }

};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleD3LayerConfigPanel: (layerId, isGradientLayer) => {
            dispatch(toggleD3LayerConfigPanel(layerId, isGradientLayer))
        },
        initializeLayerConfigMap: (value, layerId) => {
            dispatch(initializeLayerConfigMap(value, layerId));
        },
        initializeGradientConfigMap: (value, layerId) => {
            dispatch(initializeGradientConfigMap(value, layerId));
        },
        toggleD3LayerDisable: (layerId, isGradientLayer) => {
            dispatch(toggleD3LayerDisable(layerId, isGradientLayer));
        },
        updateConfigTab: (layerId, value, isGradientLayer) => {
            dispatch(updateConfigTab(layerId, value, isGradientLayer));
        },
        updateGeneralType: (layerId, variableName, value, isGradientLayer) => {
            dispatch(updateGeneralType(layerId, variableName, value, isGradientLayer));
        },
        updateCoordinateTolerance: (layerId, value, isGradientLayer) => {
            dispatch(updateCoordinateTolerance(layerId, value, isGradientLayer));
        },
        updateCodeConfigId: (layerId, value) => {
            dispatch(updateCodeConfigId(layerId, value));
        },
        updateSelectedShapeConfiguration: (layerConfig, hasSameShapeStyle) => {
            dispatch(updateSelectedShapeConfiguration(layerConfig, hasSameShapeStyle));
        },
        updateShapeForAll: (layerId, value) => {
            dispatch(updateShapeForAll(layerId, value));
        },
        updateShapeByVariable: (layerId, value) => {
            dispatch(updateShapeByVariable(layerId, value));
        },
        toggleColorPalette: (value, criteriaKey, criteriaKeyCode, layerId, layerGroupType, topPosition, leftPosition, visibleSection, actionType) => {
            dispatch(toggleColorPalette(value, criteriaKey, criteriaKeyCode, layerId, layerGroupType, topPosition, leftPosition, visibleSection, actionType));
        },
        toggleDisplayLegendCategory: (criteriaKey, layerId, isGradientLayer) => {
            dispatch(toggleDisplayLegendCategory(criteriaKey, layerId, isGradientLayer));
        },
        dispatch: dispatch
    }
};

let D3LayerConfigList1 = connect(mapStateToProps,mapDispatchToProps)( D3LayerConfigList );

export default D3LayerConfigList1;