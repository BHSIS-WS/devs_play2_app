import React from 'react';
import {connect} from 'react-redux';
import D3GradientLayerConfig from '../../components/mapd3control/D3GradientLayerConfig';
import {
    addNewGradientConfigValue,
    initializeGradientConfigMap, removeGradientConfigValue,
    updateGradientConfigValue
} from "../../actions/D3LayerConfigAction";
import {updateSelectedGradientShapeConfiguration} from "../../actions/ColorPaletteAction";

const mapStateToProps = (state) => {

    return {
        disabledGradientLayerIdSet: state.d3LayerConfig.disabledGradientLayerIdSet,
        gradientLayerConfigMap: state.d3LayerConfig.gradientLayerConfigMap,
        gradientLayerArray: state.ref.gradientLayerArray
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateGradientConfigValue: (index, value, layerId) => {
            dispatch(updateGradientConfigValue(index, value, layerId));
        },
        addNewGradientConfigValue: (layerId) => {
            dispatch(addNewGradientConfigValue(layerId));
        },
        removeGradientConfigValue: (index, layerId) => {
            dispatch(removeGradientConfigValue(index, layerId));
        },
        updateSelectedGradientShapeConfiguration: (layerConfig) => {
            dispatch(updateSelectedGradientShapeConfiguration(layerConfig));
        },
        dispatch: dispatch
    };
};

let D3GradientLayerConfig1 = connect(mapStateToProps,mapDispatchToProps)(D3GradientLayerConfig);

export default D3GradientLayerConfig1;