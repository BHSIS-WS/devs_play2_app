import React from 'react';
import {connect} from 'react-redux';
import HeaderButtonsComponent from "../components/HeaderButtonsComponent";
import {
    toggleVisibilitySidebarButtons,
    toggleVisibilityForSavedLayerConfigPanel,
    toggleVisibilityForSelectLayersPanel,
    toggleVisibilityForFeedbackPanel,
    toggleVisibilityForHelpPanel
} from '../actions/SidebarOptionsAction';

const mapStateToProps = (state) => {
    return {
        sidebarOptions: state.sidebarOptions,
        selectedMapTab: state.selectedMapTab
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleVisibilitySidebarButtons: () => {
            dispatch(toggleVisibilitySidebarButtons())
        },
        toggleVisibilityForSelectLayersPanel: () => {
            dispatch(toggleVisibilityForSelectLayersPanel())
        },
        toggleVisibilityForSavedLayerConfigPanel: () => {
            dispatch(toggleVisibilityForSavedLayerConfigPanel())
        },
        toggleVisibilityForFeedbackPanel: () => {
            dispatch(toggleVisibilityForFeedbackPanel())
        },
        toggleVisibilityForHelpPanel: () => {
            dispatch(toggleVisibilityForHelpPanel())
        },
        changeMapTab: (targetTab) => {
            dispatch({type: "CHANGE_MAP_TAB", value: targetTab})
        }
    }
};

let HeaderButtonsController = connect(mapStateToProps,mapDispatchToProps) ( HeaderButtonsComponent );

export default HeaderButtonsController;