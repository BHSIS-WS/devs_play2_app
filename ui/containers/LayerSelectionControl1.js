import React from "react";
import {connect} from 'react-redux';
import LayerSelectionControl from '../components/LayerSelectionControl';

const mapStateToProps = (state) => {
    return {
        dataInitialized: state.ref.dataInitialized,
        selectedTab: state.selectedMapTab,
        showSelectLayersPanel: state.sidebarOptions.showSelectLayersPanel,
        showLayerPrintControlPanel: state.sidebarOptions.showLayerPrintControlPanel
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch : dispatch
    }
};

let LayerSelectionControl1 = connect(mapStateToProps,mapDispatchToProps)(LayerSelectionControl);

export default LayerSelectionControl1;