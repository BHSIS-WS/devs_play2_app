import React from "react";
import {connect} from 'react-redux';
import RemoveConfigDialogComponent from '../components/RemoveConfigDialogComponent.js';
import { hideRemoveConfigDialog, removeSavedLayerConfig } from "../actions/ToolbarOptionsAction";

const mapStateToProps = (state, ownProps) => {
    return {
        showRemoveConfigDialog: state.toolbarOptions.showRemoveConfigDialog,
        userSavedLayerConfig: state.toolbarOptions.userSavedLayerConfig
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideRemoveConfigDialog: () => {
            dispatch(hideRemoveConfigDialog());
        },
        removeSavedLayerConfig: (userSavedLayerConfig) => {
            dispatch(removeSavedLayerConfig(userSavedLayerConfig));
        }
    }
};

let RemoveConfigDialogController = connect(mapStateToProps,mapDispatchToProps)(RemoveConfigDialogComponent);

export default RemoveConfigDialogController;