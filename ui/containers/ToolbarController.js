import React from 'react';
import {connect} from 'react-redux';
import ToolbarComponent from "../components/ToolbarComponent";
import {
    toggleVisibilityForCanvasAttributesDialog,
    toggleVisibilityForSaveLayerConfigDialog,
    toggleVisibilityForRenderStatusPanel,
    toggleVisibilityForPrintMapLegend,
    toggleVisibilityForMinimapPanel,
    performD3MapZoomIn,
    performD3MapZoomOut,
    downloadToImage,
    downloadToPdf,
    toggleVisibilityForSavePdfDialog,
    refreshMapLayers,
    toggleMouseZoomForPrintMap,
} from "../actions/ToolbarOptionsAction";

const mapStateToProps = (state) => {
    return {
        zoom : state.mapCanvasD3.zoom,
        toolbarOptions: state.toolbarOptions,
        showTextToolDialog: state.d3MapToolOptions.d3MapTextTool.optionDialogOpen,
        drawingCanvasOn: state.d3MapToolOptions.d3MapRectangleTool.drawingCanvasOn
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleVisibilityForCanvasAttributesDialog: () => {
            dispatch(toggleVisibilityForCanvasAttributesDialog());
        },
        toggleVisibilityForSaveLayerConfigDialog: () => {
            dispatch(toggleVisibilityForSaveLayerConfigDialog());
        },
        toggleVisibilityForRenderStatusPanel: ()=> {
            dispatch(toggleVisibilityForRenderStatusPanel());
        },
        toggleVisibilityForPrintMapLegend: () => {
            dispatch(toggleVisibilityForPrintMapLegend());
        },
        toggleVisibilityForMinimapPanel: () => {
            dispatch(toggleVisibilityForMinimapPanel());
        },
        performD3MapZoomIn: () => {
            performD3MapZoomIn();
        },
        performD3MapZoomOut: () => {
            performD3MapZoomOut();
        },
        downloadToImage: () => {
            downloadToImage();
        },
        downloadToPdf: () => {
            downloadToPdf();
            dispatch(toggleVisibilityForSavePdfDialog());
        },
        refreshMapLayers: (evt) => {
            refreshMapLayers(evt);
        },
        toggleMouseZoomForPrintMap: (disableMouseZoom) => {
            dispatch(toggleMouseZoomForPrintMap(disableMouseZoom));
        },
        dispatch: dispatch
    }
};

let ToolbarController = connect(mapStateToProps,mapDispatchToProps) ( ToolbarComponent );

export default ToolbarController;