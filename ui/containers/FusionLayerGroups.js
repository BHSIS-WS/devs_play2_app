import React from 'react';
import {connect} from 'react-redux';
import FusionLayerGroupList from '../components/FusionLayerGroupList';


const mapStateToProps = (state, ownProps) => {
  return {
    expandMap: state.groups.expandMap,
    groupArray: state.ref.layerGroupArray
  };
};


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch : dispatch
  };
};

let FusionLayerGroups = connect(mapStateToProps,mapDispatchToProps)( FusionLayerGroupList );

export default FusionLayerGroups;