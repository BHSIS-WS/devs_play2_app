import React from 'react';
import {connect} from 'react-redux';
import FusionLayerGroup from '../components/FusionLayerGroup';


const mapStateToProps = (state, ownProps) => {

  let layerArray = state.ref.layerArray.filter(layer=>layer.fusion_map_layer_group_id===ownProps.group.id);

  return {
    layerArray: layerArray
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch : dispatch
  };
};

let FusionLayerGroup1 = connect(mapStateToProps,mapDispatchToProps)( FusionLayerGroup );

export default FusionLayerGroup1;