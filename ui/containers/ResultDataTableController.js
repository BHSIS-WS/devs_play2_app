import React from 'react';
import {connect} from 'react-redux';
import ResultDataTableComponent from '../components/ResultDataTableComponent';
import {fetchLayerDataList,
    downloadDataTableExcel,
    changeDataTablePageNumber,
    changeDataTablePageSize,
    toggleDtStateCountyDataPicker,
    toggleDtAdditionalDataLevel,
    toggleDtDataSet,
    toggleDtGradLayer,
    toggleDataTableAutoRefresh,
    toggleDataTableInvertQuery}
    from '../actions/LayerDatatListingAction';
// import {dataTableRowClicked} from '../actions/MapVariablesAction'


const mapStateToProps = (state) => {
    return {
        layerDataListing: state.layerDataListing,
        gradientDataSetArray: state.ref.gradientDataSetArray,
        gradientLayerArray: state.ref.gradientLayerArray,
        layerArray: state.ref.layerArray,
        selectedMapTab: state.selectedMapTab
    }
};

const mapDispatchToProps = (dispatch) => {
  return {
      fetchLayerDataList: () => {
        dispatch(fetchLayerDataList());
      },
      downloadDataTableExcel: () => {
          dispatch(downloadDataTableExcel());
      },
      changeDataTablePageNumber: (pageNum, shouldUpdate) => {
          dispatch(changeDataTablePageNumber(pageNum, shouldUpdate));
      },
      changeDataTablePageSize: (pageSize) => {
          dispatch(changeDataTablePageSize(pageSize));
      },
      toggleDtStateCountyDataPicker: () => {
          dispatch(toggleDtStateCountyDataPicker());
      },
      toggleDtAdditionalDataLevel: (dataLevel) => {
          dispatch(toggleDtAdditionalDataLevel(dataLevel));
      },
      toggleDtDataSet: (dataSetId) => {
          dispatch(toggleDtDataSet(dataSetId));
      },
      toggleDtGradLayer: (gradLayerId, dataLevel) => {
          dispatch(toggleDtGradLayer(gradLayerId, dataLevel));
      },
      toggleDataTableAutoRefresh: () => {
          dispatch(toggleDataTableAutoRefresh());
      },
      toggleDataTableInvertQuery: () => {
          dispatch(toggleDataTableInvertQuery());
      },
      // dataTableRowClicked: (item) => {
      //     dispatch(dataTableRowClicked(item));
      // },
      dispatch : dispatch
  }
};

let ResultDataTableController = connect(mapStateToProps, mapDispatchToProps)(ResultDataTableComponent);

export default ResultDataTableController;