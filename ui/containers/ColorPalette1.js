import React from 'react'
import {connect} from 'react-redux'
import ColorPalette from '../components/ColorPalette'


const mapStateToProps = (state) => {
  return {
  	colorPalette: state.colorPalette,
    svgShapes: state.ref.svgShapes
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    dispatch : dispatch
  }
}

let ColorPalette1 = connect(mapStateToProps,mapDispatchToProps)( ColorPalette )

export default ColorPalette1