import React from 'react';
import {connect} from 'react-redux';
import FusionLayerFilterComponent from '../components/FusionLayerFilterComponent';


const mapStateToProps = (state, ownProps) => {

    let layerCriteriaArray = state.ref.layerFilteringCriteriaArray.filter((criteria) => {
        return (criteria.fusion_map_layer_id === ownProps.layer.id);
    });
    let criteriaArray = [...new Set(layerCriteriaArray.map(item => item.criteria_code_id))];

    let selectedCriteriaCodeSet = state.normalLayers.selectedCriteriaCodeIdMap.get(ownProps.layer.id);
    let selectedCriteriaCodeArray = [];
    if (selectedCriteriaCodeSet) {
        selectedCriteriaCodeArray = Array.from(selectedCriteriaCodeSet);
    }

    return {
        criteriaArray: criteriaArray,
        criteriaCodeLookupMap: state.ref.criteriaCodeLookupMap,
        selectedCriteriaCodeArray: selectedCriteriaCodeArray,
        dataTableAutoRefresh: state.layerDataListing.autoRefresh
    };
};


const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch : dispatch
    };
};

let FusionLayerFilterController = connect(mapStateToProps,mapDispatchToProps)( FusionLayerFilterComponent );

export default FusionLayerFilterController;