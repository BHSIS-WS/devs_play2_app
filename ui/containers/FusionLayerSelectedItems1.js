import React from 'react'
import {connect} from 'react-redux'
import FusionLayerSelectedItems from '../components/FusionLayerSelectedItems'


const mapStateToProps = (state, ownProps) => {
  return {
  	//layerGroupArray: state.ref.layerGroupArray,
  	layerArray: state.ref.layerArray,
    selectedLayerIdSet: state.normalLayers.selectedLayerIdSet,
    gradientLayerArray: state.ref.gradientLayerArray,
    selectedGradLayerIdSet: state.gradientLayers.selectedGradLayerIdSet,
    selectedCriteriaIdMap: state.normalLayers.selectedCriteriaIdMap,
    expandSelectedLayerId: state.normalLayers.expandSelectedLayerId,
    selectedMapTab: state.selectedMapTab
  }
};


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch : dispatch
  }
};

let FusionLayerSelectedItems1 = connect(mapStateToProps,mapDispatchToProps)( FusionLayerSelectedItems );

export default FusionLayerSelectedItems1;