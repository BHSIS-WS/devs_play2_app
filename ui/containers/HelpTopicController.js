import React from 'react';
import {connect} from 'react-redux';
import HelpTopicComponent from '../components/HelpTopicComponent';

const mapStateToProps = (state, ownProps) => {
    return {
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatch : dispatch
    }
};

let HelpTopicController = connect(mapStateToProps, mapDispatchToProps)(HelpTopicComponent);

export default HelpTopicController;