import React from 'react'
import {connect} from 'react-redux'
import MapLegendComponent from '../components/MapLegendComponent'

const mapStateToProps = (state) => {

    let selectedNormalLayers = state.ref.layerArray.filter(layer => state.normalLayers.selectedLayerIdSet.has(layer.id)),
        selectedGradLayers = state.ref.gradientLayerArray.filter(layer => state.gradientLayers.selectedGradLayerIdSet.has(layer.id)),
        selectedMapLegendLayerId = state.selectedMapLegendLayerId,
        selectedMapLegendLayerArray = [],
        selectedLayers,
        selectedMapLegendJson;

    if(selectedMapLegendLayerId !== '-1') {
        selectedLayers = selectedMapLegendLayerId.split('-')[0] === 'normal' ? selectedNormalLayers : selectedGradLayers;
        selectedMapLegendLayerArray = selectedLayers.filter(layer => layer.id === parseInt(selectedMapLegendLayerId.split('-')[1]));
    }
    if (selectedMapLegendLayerArray.length > 0) {
        selectedMapLegendJson = JSON.parse(selectedMapLegendLayerArray[0].fusion_legend_json_string);
    } else {
        selectedMapLegendLayerId = '-1';
    }
    return {
        selectedNormalLayers: selectedNormalLayers,
        selectedGradLayers: selectedGradLayers,
        selectedMapLegendLayerId : selectedMapLegendLayerId,
        selectedMapLegendJson: selectedMapLegendJson
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch : dispatch
    }
};

let MapLegendController = connect(mapStateToProps,mapDispatchToProps)( MapLegendComponent );

export default MapLegendController;