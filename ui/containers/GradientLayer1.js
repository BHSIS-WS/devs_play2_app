import React from 'react'
import {connect} from 'react-redux'
import GradientLayer from '../components/GradientLayer'


const mapStateToProps = (state, ownProps) => {
  return {
//    layerArray: state.ref.layerArray,
//    layerCriteriaArray: state.ref.layerFilteringCriteriaArray,
//    criteriaCodeLookupMap: state.ref.criteriaCodeLookupMap,
//    expandedLayerIdSet: state.layers.expandedLayerIdSet,
//    selectedCriteriaIdSet: state.layers.selectedCriteriaIdSet,
//    selectedCriteriaCodeIdMap: state.layers.selectedCriteriaCodeIdMap
    gradientLayerArray: state.ref.gradientLayerArray,
    filterExpandedLayerIdSet: state.gradientLayers.filterExpandedLayerIdSet,
    selectedGradLayerIdSet: state.gradientLayers.selectedGradLayerIdSet,
    gradFilterMap: state.gradientLayers.gradFilterMap
  }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    dispatch : dispatch
  }
};

let GradientLayer1 = connect(mapStateToProps,mapDispatchToProps)(GradientLayer);

export default GradientLayer1;

