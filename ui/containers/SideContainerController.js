import React from "react";
import {connect} from 'react-redux';
import SideContainerComponent from '../components/SideContainerComponent';

const mapStateToProps = (state) => {
    return {
        dataInitialized: state.ref.dataInitialized,
        selectedMapTab: state.selectedMapTab
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch : dispatch
    }
};

let SideContainerController = connect(mapStateToProps,mapDispatchToProps)(SideContainerComponent);

export default SideContainerController;