#!/bin/sh
## Created By Pavan Kumar
## Purpose of this script is to use few paramenter dynamically wtih the environment. 
##Also we need the application start log. It helps to find the initial error of application during initail start.
##Going to add email and app web status also
#Later we need to find better solution.
#set -x

APP_NAME=@@APP_NAME@@
PROJECT_NAME=@@PROJECT_NAME@@
#Keeping for the reference
#JVM_MEMORY_ARGS="-J-server -J-XX:+UseG1GC -J-Xms4G -J-Xmx4G"
JVM_MEMORY_ARGS="-J-server -J-XX:+Use@@GARBAGE_COLLECTION@@GC -J-Xms@@JVM_MIN@@ -J-Xmx@@JVM_MAX@@"

ENV_RUN_CONFIG_FILE=@@ENV_RUN_CONFIG_FILE@@
RUN_FUNCTION=$1

DATE=$(date +%F_%T)
LOG_FILE=./log/${PROJECT_NAME}_start_initial.log

if [ ! -d ./log ]
then
  mkdir ./log
fi

if [ -f ${LOG_FILE} ]
then
  mv ${LOG_FILE} ${LOG_FILE}_${DATE}
else
  touch ${LOG_FILE}
fi
 
usage(){
  cat << EOF
  usage: $0 options
  This script run with these arguments
  OPTIONS:
        Pass Argument 1 as STATUS START STOP or RESTART only
EOF
}

if [[ $# -eq "" ]]
then
  usage
  exit 1
else
  :
fi


if [[ ${RUN_FUNCTION} == "STATUS" ]] ||  [[ ${RUN_FUNCTION} == "START" ]]  ||  [[ ${RUN_FUNCTION} == "STOP" ]] ||   [[ ${RUN_FUNCTION} == "RESTART" ]]
then
  :
else
  echo "Argument number 1 must be STATUS, START, STOP, or RESTART. Please run the script again"
  echo
  usage
  exit 1
fi

function app_status {
  echo 
  echo "Checking the status of ${PROJECT_NAME} application"
  PROCESS_STATUS=$(ps -ef | grep "${APP_NAME}"|grep java |grep -v grep | head -c 500)
  PID=$(ps -ef | grep "${APP_NAME}" |grep java|grep -v grep | awk ' { print $2 } ')
  echo 
  echo "Status of PLAY Process: ${PROCESS_STATUS}"
  echo 
  #echo "Please check the Process ID, config file and java memory"
  if [[ "${PROCESS_STATUS}" != "" ]];
  then
    echo
    echo "Application ${PROJECT_NAME} is Running with PID: ${PID}"
    return 0
  else
    echo "Application ${PROJECT_NAME} is Not Running"
    return 1
  fi
}

function stop_app {
  echo ""
  echo "Stopping the Application: ${PROJECT_NAME}"
  PID=$(ps -ef | grep "${APP_NAME}" |grep java|grep -v grep | awk ' { print $2 } ')
  if [ "${PID}" != "" ]
  then
    /bin/kill ${PID}
    sleep 3
  fi
  PROCESS_STATUS=$(ps -ef | grep "${APP_NAME}" |grep java |grep -v grep| head -c 500 )
  echo 
  echo "Status of PLAY Process should be empty: ${PROCESS_STATUS}"
  if [[ "${PROCESS_STATUS}" == "" ]];
  then
    echo
    echo "Application ${PROJECT_NAME} is Successfully Down Now"
    return 0
  else
    echo "Application has Not been down. Please manually bring it down. Running PID is ${PID} "
    return 1
  fi
}

function start_app {
  if ! app_status; then
    echo
    echo "Startingg the play application ${PROJECT_NAME}"
    echo 
#Below comment line is for reference only: Do not delete it. 
#nohup ../bin/${PROJECT_NAME} ${JVM_MEMORY_ARGS} -Dconfig.resource=${ENV_RUN_CONFIG_FILE} >/dev/null 2>&1 &
    nohup ../bin/${APP_NAME} ${JVM_MEMORY_ARGS} -Dconfig.resource=${ENV_RUN_CONFIG_FILE} > ${LOG_FILE}  &
    echo 
    echo "Application ${PROJECT_NAME} has started, running the status function"
    return 0
  else
    echo
    echo "Application is already running on the server, please bring it down"
    return 1
  fi
}

if [[ ${RUN_FUNCTION} == "STATUS" ]]
then
  echo "Calling the status function"
  app_status
fi

if  [[ ${RUN_FUNCTION} == "START" ]]
then
  echo "Calling the start function"
  start_app
  echo 
  echo "Running the Application Status check"
  sleep 4
  app_status
fi

if [[ ${RUN_FUNCTION} == "STOP" ]]
then
  echo "Calling the start function"
  stop_app
fi

if [[ ${RUN_FUNCTION} == "RESTART" ]]
then
  echo "Calling the restart function"
  stop_app
  sleep 1
  start_app
  echo 
  echo "Running the Application Status check"
  sleep 4
  app_status
fi

