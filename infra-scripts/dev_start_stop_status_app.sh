#!/bin/bash
## Created By Pavan Kumar
## Purpose of this script is to copy the created release package to web servers
#set -x

project_name="dss-play2-app"
JVM_MEMORY_ARGS="-J-server -J-XX:+UseG1GC -J-Xms2G -J-Xmx2G"

usage() {
  cat <<EOF
usage: $0 options

This script run with these arguments

OPTIONS:
        Argument 1 = STATUS, START, STOP, or RESTART only
        OPTIONAL - Argument 2 - the play config file name (e.g. application.conf)
EOF

}

if [[ $# = "1" ]]; then
  env_file_name=$(cat appConfigFileName)
elif [[ $# = "2" ]]; then
  env_file_name=$2
else
  usage
  exit 1
fi

RUN_FUNCTION=$1

if [[ ${RUN_FUNCTION} == "STATUS" ]] || [[ ${RUN_FUNCTION} == "START" ]] || [[ ${RUN_FUNCTION} == "STOP" ]] || [[ ${RUN_FUNCTION} == "RESTART" ]]; then
  :
else
  echo "Argument number 1 must be STATUS START STOP or RESTART, please run the script again"
  echo
  usage
  exit 1
fi

function app_is_running() {
  #Checks if RUNNING_PID file exists
  if [ -f ../RUNNING_PID ]; then
    return 0
  else
    return 1
  fi
}
function app_status() {
  if app_is_running; then
    echo "Application is running"
  else
    echo "Application is not running"
  fi
}

function stop_app() {
  if app_is_running; then
    echo "Stopping the play application in $(pwd)"
  else
    echo "App is already down, no need to stop"
    return 0
  fi

  /bin/kill "$(cat ../RUNNING_PID)"
  for i in {1..20}
  do
   if app_is_running; then
     echo -n ".."
     sleep 1
   else
    echo "SUCCESS"
    return 0
   fi
  done
  echo "ERROR"
  return 1
}

function start_app() {
  if app_is_running; then
    echo "App is already up, no need to start"
    return 0
  else
    echo "Starting the play application in $(pwd)"
  fi

  nohup ../bin/${project_name} ${JVM_MEMORY_ARGS} -Dconfig.resource=${env_file_name} > /dev/null 2>&1 &

 for i in {1..20}
  do
   if app_is_running; then
     echo "SUCCESS"
     return 0
   else
     echo -n ".."
     sleep 1
   fi
  done
  echo "ERROR"
  return 1
}

function restart_app() {
  echo "Restarting play application"
  stop_app
  retVal=$?
  if [ ! $retVal = 0 ]; then return $retVal; fi #If last command failed, return with error code
  start_app
}

if [[ ${RUN_FUNCTION} == "STATUS" ]]; then
  echo "Calling the status function"
  app_status

elif [[ ${RUN_FUNCTION} == "START" ]]; then
  echo "Calling the start function"
  start_app || exit 1

elif [[ ${RUN_FUNCTION} == "STOP" ]]; then
  echo "Calling the stop function"
  stop_app || exit 1

elif [[ ${RUN_FUNCTION} == "RESTART" ]]; then
  echo "Calling the restart function"
  restart_app || exit 1

else
  echo "Failed: Script failed, please check"
  exit 1
fi
