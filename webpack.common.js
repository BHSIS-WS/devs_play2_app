require("babel-polyfill");
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

// the path(s) that should be cleaned
let pathsToClean = [
    'public/javascripts/*bundle.js'
];

// the clean options to use
let cleanOptions = {
    verbose:  true,
    dry:      false
};

module.exports = {
    entry: './ui/index.js',
    output: {
        path: path.resolve(__dirname, 'public/javascripts'),
        filename: 'dlt_main_bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    performance: {
        hints: 'warning',
        maxEntrypointSize: 500000,
        maxAssetSize: 500000
    },
    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions)
    ],
    target: 'web',
    node: {
        fs: 'empty'
    }

};


