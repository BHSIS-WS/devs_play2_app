#!/bin/bash

#Input validation
if [[ $# -ne 1 ]]
then
  echo "usage: $0 <version_number>"
  exit 1
fi

# Check if first argument is not empty
if [ -n "$1" ]
then
  RELEASE_VERSION=$1
  echo "Version number provided: $1"
else
  echo "Invalid version number provided: $1"
  exit 1
fi

if [ -d "/var/lib/jenkins/workspace" ]
then
  PROJECT_STORE="/var/lib/jenkins/workspace/app_deployments/${PROJECT_NAME}"
else
  PROJECT_STORE="/local/apps/bhsis/workspace/app_deployments/${PROJECT_NAME}"
fi
if [ -d ${PROJECT_STORE} ]
then
  :
else
  mkdir -p ${PROJECT_STORE} && mkdir ${PROJECT_STORE}/${RELEASE_VERSION}
fi



#Saving the release version to version.sbt file to have the package name with release number.
echo version in ThisBuild := "\"${RELEASE_VERSION}"\"  > version.sbt
echo "Saving the release version to version.sbt $(cat version.sbt)"

#Running the yarn install, build and sbt clean and creating the build package. 
echo "Running the yarn install, build and sbt clean and creating the build package." 

cd ui && yarn install && yarn build || exit 1
cd ..
sbt clean && sbt dist || exit 1

echo "Copying the newly created package to proper location for artifact export."
cp -rp ./target/universal/*-${RELEASE_VERSION}.zip ${PROJECT_STORE}/${RELEASE_VERSION}/
mv ./target/universal/*-${RELEASE_VERSION}.zip ./

