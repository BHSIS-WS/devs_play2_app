# DEVS DB Docker image
**You should have installed docker, docker-compose and have a docker account to pull images**

## Docker Postgres 12. Build base image.
*cd to ./docker/db/base from your git project*
```bash
cd ./docker/db/base
docker build -t bhsis02.eagletechva.com/eg_pg12_base:v01 .
```

## Option 1: docker-compose **without volume** 

### Build docker image
```bash
docker-compose -p devs_db build 
```

### Run docker container using docker-compose. Recommended method
```bash
docker-compose -f docker-compose.yaml up -d  
```
### DB script to import the db dump 
*Make sure db dumps are exist on remote server before running the import script.*
* You can create db dumps of remote server using below curl command, and then run the db import script.*
*Wait for the Jenkins success build email before running the import db script.*
```bash
curl -I -u pavank:11a9530d288a8d6d2ccaddc6abbb79dbf7 "http://etinf21.eagletechva.com:8080/view/DEVS/job/devs_test_db_dump/build?token=run-jobs-remotely" 
``` 
```bash
docker exec -it devs_db ./run_db_import.sh db_config.json
```
### DB script to restore the db dump 

```bash
docker exec -it devs_db ./run_db_restore.sh db_config.json
```

### Stop docker container. 
*Stop do not remove the continer, you can use the start to run the contanier again*
```bash
docker-compose -f docker-compose.yaml stop
```
### Start docker container.        
*Start will run the continer, if it was stopped previously*
```bash
docker-compose -f docker-compose.yaml start
```

### Stop and remove containers, networks..
```bash
docker-compose -f docker-compose.yaml down
```

## Option 2: docker-compose **with volume** 
### Build docker image
```bash
docker-compose -p devs_db build            
```
### Run docker container using docker-compose. Recommended method
```bash
docker-compose -f docker-compose-with-volume.yaml up -d
```
### DB script to import the db dump 
*Make sure db dumps are exist on remote server before running the import script*
*You can create db dumps of remote server using below curl command, and then run the db import script*
*Wait for the Jenkins success build email before running the import db script*
```bash
curl -I -u pavank:11a9530d288a8d6d2ccaddc6abbb79dbf7 "http://etinf21.eagletechva.com:8080/view/DEVS/job/devs_test_db_dump/build?token=run-jobs-remotely" 
``` 
```bash
docker exec -it devs_db_vol ./run_db_import.sh db_config.json
```
### DB script to restore the db dump 

```bash
docker exec -it devs_db_vol ./run_db_restore.sh db_config.json
```
### Stop docker container.         
*Stop do not remove the continer, you can use the start to run the contanier again*
```bash
docker-compose -f docker-compose-with-volume.yaml stop
```
### Start docker container. 
*Start will run the continer, if it was stopped previously*
```bash
docker-compose -f docker-compose-with-volume.yaml start
```
### Stop and remove containers, networks..
```bash
docker-compose -f docker-compose-with-volume.yaml down
```

## Option 3: Using docker **run** commands. **without volume**

### Build docker image

```bash
docker build -t devs_db:v01 .
```
### Run docker container
*docker run -d --name "Container Name" -p "Ports"  "Image Name"*

```bash
docker run -d --rm --name devs_db -p 35432:5432 devs_db:v01
```
### DB script to import the db dump 
*Make sure db dumps are exist on remote server before running the import script*
*You can create db dumps of remote server using below curl command, and then run the db import script*
*Wait for the Jenkins success build email before running the import db script*
```bash
curl -I -u pavank:11a9530d288a8d6d2ccaddc6abbb79dbf7 "http://etinf21.eagletechva.com:8080/view/DEVS/job/devs_test_db_dump/build?token=run-jobs-remotely"
``` 
```bash
docker exec -it devs_db ./run_db_import.sh db_config.json
```
### DB script to restore the db dump 

```bash
docker exec -it devs_db ./run_db_restore.sh db_config.json
```

###  Stop docker container
```bash
docker stop devs_db
```
###  Start docker container
```bash
docker start devs_db
```
###  Remove docker container
```bash
docker rm devs_db
```

## Option 4: Using docker **run** commands. **with volume**

### Build docker image

```bash
docker build -t devs_db:v01 .
```
### Run docker container 
*docker run -d --name "Container Name" -p "Ports"  "Image Name"*

```bash
docker run -d --rm --name devs_db_vol --entrypoint "/bin/bash /start_db_vol.sh" -p 25432:5432 -v /Users/<<User Name>>/pg_data/devs:/var/lib/pgsql/12/data/ devs_db_vol:v01
```

### DB script to import the db dump 
*Make sure db dumps are exist on remote server before running the import script.*
*You can create db dumps of remote server using below curl command, and then run the db import script.*
*Wait for the Jenkins success build email before running the import db script.*
```bash
curl -I -u pavank:11a9530d288a8d6d2ccaddc6abbb79dbf7 "http://etinf21.eagletechva.com:8080/view/DEVS/job/devs_test_db_dump/build?token=run-jobs-remotely"
``` 
```bash
docker exec -it devs_db_vol ./run_db_import.sh db_config.json
```
### DB script to restore the db dump 

```bash
docker exec -it devs_db_vol ./run_db_restore.sh db_config.json
```

###  Stop docker container
```bash
docker stop devs_db_vol
```
###  Start docker container
```bash
docker start devs_db_vol
```
###  Remove docker container
```bash
docker rm devs_db_vol
```

###  If need to go inside your running container.
```bash
docker exec -it devs_db /bin/bash
```
```bash
docker exec -it devs_db_vol /bin/bash
```

## Helpful commands.

## Using docker compose 
### Stop services only
```bash
docker-compose stop
```
### Stop and remove containers, networks..
```bash
docker-compose down 
```
### Down and remove volumes
```bash
docker-compose down --volumes 
```

### Down and remove images
```bash
docker-compose down --rmi <all|local>
```

