#!/usr/bin/env python3

'''
Created by Pavan Kumar
Purpose of the script is to run restore the db bump
'''
import os
import subprocess
import datetime
import json
import logging
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import sys
config_file = sys.argv[1]

try:
  with open(config_file, 'r') as js_f:
    config = json.load(js_f)
except Exception as e:
  logging.error(f'Get the Exception FileNotFoundError: {e}: {config_file}')
  sys.exit(f'Get the Exception file not found: FileNotFoundError {e}: {config_file}')

app_name = config['app_name']
db_name = config['db_name']
db_dumps_dir = config['put_db_dumps']
send_email = config['send_email']
email_address = config['email_address']
create_db = config['create_db']
db_globals = config['db_globals']

todays_date = datetime.datetime.now().strftime('%Y%m%d')

if create_db is None:
  create_db = f'{db_dumps_dir}/{db_name}_createdb_{todays_date}.sql'

if db_globals is None:
  db_globals = f'{db_dumps_dir}/{db_name}_globals_{todays_date}.sql'

db_extensions = f'{db_dumps_dir}/{db_name}_extensions_{todays_date}.sql'
db_dump = f'{db_dumps_dir}/{db_name}_FULL_{todays_date}.dump'

db_dump_files = [create_db, db_globals, db_extensions, db_dump]

##Send the message to Email address
def send_notifcation_email(subject, message):
  body = message
  html = (f'<p><strong>{body}</strong></p>')
  msg = MIMEMultipart()
  msg = MIMEText(html, "html")
  msg['From'] = fromaddr
  msg['To'] = ", ".join(toaddrs)
  msg['Subject'] = f'{subject}'
  server = smtplib.SMTP('eagletechva-com.mail.protection.outlook.com')
  server.sendmail(fromaddr, toaddrs, msg.as_string())
  server.quit()

def restore_db_dumps():
  print(f'Running the Create DB script: {create_db}')
  subprocess.run([ '/bin/sudo', '-u', 'postgres', 'psql', '-f', create_db ])
  print(f'Running the db globals: {db_globals}')
  subprocess.run(['/bin/sudo', '-u', 'postgres', 'psql', '-f', db_globals])
  print(f'Running the db extensions: {db_extensions}')
  subprocess.run(['/bin/sudo', '-u', 'postgres', 'psql', '-f', db_extensions])
  print(f'Running the database restore: {db_dump}')
  subprocess.run(['/bin/sudo', '-u', 'postgres', 'pg_restore', '-d', db_name, db_dump, '-v'])

restore_db_dumps()  
