echo "Using the Volume script"
echo 'postgres ALL=(ALL) ALL' >> /etc/sudoers
chown -R postgres.postgres /var/lib/pgsql
sudo -u postgres /usr/pgsql-12/bin/initdb /var/lib/pgsql/12/data/

cp /db_confs/postgresql.conf /var/lib/pgsql/12/data/postgresql.conf
cp /db_confs/pg_hba.conf /var/lib/pgsql/12/data/pg_hba.conf
cp /db_confs/pgsql_profile /var/lib/pgsql/.pgsql_profile

chown -R postgres.postgres /var/lib/pgsql

#sudo -u postgres /usr/pgsql-12/bin/pg_ctl start -D /var/lib/pgsql/12/data -w && \
#./run_db_import.sh ./db_config.json && \ 
#./run_db_restore.sh ./db_config.json
#sudo -u postgres /usr/pgsql-12/bin/pg_ctl stop -D /var/lib/pgsql/12/data -w

sudo -u postgres /usr/pgsql-12/bin/postgres -D /var/lib/pgsql/12/data -p 5432
