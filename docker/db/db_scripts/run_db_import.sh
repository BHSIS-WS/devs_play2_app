#!/bin/bash
##Purpose of this script is to import the database from Test db servers(mostly}. 
##Test db servers and details are saved to db_config.json or <<ap_name>>_db_config.json
##Make sure db_config.json has the right values
##This script should be used in the Docker container only. 
##You can run get_db_dumps.py script on your local system without this script

db_script_dir="."
import_db_dumps_script="get_db_dumps.py"

if [ $# -ne 1 ] #Checking for the argument
then
  echo "Please pass the db_config.json or <<ap_name>>_db_config.json as argument"
  exit 1
fi

##Running the get_db_dumps.py.
##To run this python script on a local system you may need following python libraries.
##Docker image already has these libraries. 
## pip3 install --user paramiko
## pip3 install --user scp 

python3 ${db_script_dir}/${import_db_dumps_script} $1 

