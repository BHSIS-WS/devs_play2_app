--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE cld;
ALTER ROLE cld WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'dk0wner';
CREATE ROLE common;
ALTER ROLE common WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'dk0wner';
CREATE ROLE common_read_only;
ALTER ROLE common_read_only WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION NOBYPASSRLS;
CREATE ROLE deepas;
ALTER ROLE deepas WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'dk0wner';
CREATE ROLE dlt;
ALTER ROLE dlt WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'dk0wner';
CREATE ROLE dlt_app;
ALTER ROLE dlt_app WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'dk0wner';
CREATE ROLE dsscld;
ALTER ROLE dsscld WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'dk0wner';
CREATE ROLE dsscld_app;
ALTER ROLE dsscld_app WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'dk0wner';
CREATE ROLE dsscld_read_only;
ALTER ROLE dsscld_read_only WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION NOBYPASSRLS;
CREATE ROLE dsscld_readonly;
ALTER ROLE dsscld_readonly WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'dk0wner';
CREATE ROLE marcusb;
ALTER ROLE marcusb WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'dk0wner';
CREATE ROLE michaelm;
ALTER ROLE michaelm WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'dk0wner';
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS;
--
-- User Configurations
--

--
-- User Config "cld"
--

ALTER ROLE cld SET search_path TO 'cld', 'common', 'public';
--
-- User Configurations
--

--
-- User Config "common"
--

ALTER ROLE common SET search_path TO 'common', 'public';
--
-- User Configurations
--

--
-- User Config "dlt"
--

ALTER ROLE dlt SET search_path TO 'dlt', 'public';
--
-- User Configurations
--

--
-- User Config "dlt_app"
--

ALTER ROLE dlt_app SET search_path TO 'dlt', 'public';
--
-- User Configurations
--

--
-- User Config "dsscld"
--

ALTER ROLE dsscld SET search_path TO 'dsscld', 'cld', 'common', 'public';
--
-- User Configurations
--

--
-- User Config "dsscld_app"
--

ALTER ROLE dsscld_app SET search_path TO 'dsscld', 'cld', 'public';


--
-- Role memberships
--

GRANT common_read_only TO deepas GRANTED BY postgres;
GRANT dsscld_read_only TO dsscld_readonly GRANTED BY postgres;
GRANT dsscld_read_only TO michaelm GRANTED BY postgres;




--
-- PostgreSQL database cluster dump complete
--

