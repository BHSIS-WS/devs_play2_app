DROP DATABASE IF EXISTS devstest;
create database devstest
    with owner = postgres
    encoding = 'UTF8'
    tablespace = pg_default
    lc_collate = 'en_US.UTF-8'
    lc_ctype = 'en_US.UTF-8'
    connection limit = -1;

alter database devstest set search_path = public,dlt,information_schema,pg_catalog,topology;

