#!/usr/bin/env python3

'''
Created by Pavan Kumar
Purpose of the script is to get the doc-stie and dasis_home page files
'''
import os
import subprocess
import datetime
import paramiko
from scp import SCPClient
import json
import logging
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import sys

config_file = sys.argv[1]

try:
  with open(config_file, 'r') as js_f:
    config = json.load(js_f)
except Exception as e:
  logging.error(f'Get the Exception FileNotFoundError: {e}: {config_file}')
  sys.exit(f'Get the Exception file not found: FileNotFoundError {e}: {config_file}')

app_name = config['app_name']
db_name = config['db_name']
db_hostname = config['db_hostname']
unix_id = config['unix_id']
unix_id_password = config['unix_id_password']
get_db_dumps = config['get_db_dumps']
put_db_dumps = config['put_db_dumps']
send_email = config['send_email']
email_address = config['email_address']

todays_date = datetime.datetime.now().strftime('%Y%m%d')
create_db = f'{db_name}_createdb_{todays_date}.sql'
db_globals = f'{db_name}_globals_{todays_date}.sql'
db_extensions = f'{db_name}_extensions_{todays_date}.sql'
db_dump = f'{db_name}_FULL_{todays_date}.dump'
db_dump_files = [create_db, db_globals, db_extensions, db_dump]

##Send the message to Email address
def send_notifcation_email(subject, message):
  body = message
  html = (f'<p><strong>{body}</strong></p>')
  msg = MIMEMultipart()
  msg = MIMEText(html, "html")
  msg['From'] = fromaddr
  msg['To'] = ", ".join(toaddrs)
  msg['Subject'] = f'{subject}'
  server = smtplib.SMTP('eagletechva-com.mail.protection.outlook.com')
  server.sendmail(fromaddr, toaddrs, msg.as_string())
  server.quit()

def createSSHClient(server, port, user, password):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, port, user, password)
    return client

def scp_db_dumps():
  ssh = createSSHClient(db_hostname, '22', unix_id, unix_id_password)
  with SCPClient(ssh.get_transport()) as scp:
    for db_dump_file in db_dump_files:
      print(f'Running SCP to get the file from path: {get_db_dumps}{db_dump_file} and putting to: {put_db_dumps}') 
      scp.get(get_db_dumps + db_dump_file, local_path=put_db_dumps)

scp_db_dumps()

