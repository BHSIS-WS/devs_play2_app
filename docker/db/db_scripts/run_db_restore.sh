#!/bin/bash
##Purpose of this script is to restore the database dumps to docker db container. 
##Test db servers and details are saved to db_config.json or <<ap_name>>_db_config.json
##Make sure db_config.json has the right values
##This script should be used in the Docker container only. 
##You can run restore_db_dumps.py script on your local system without this script

db_script_dir="."
restore_db_dumps_script="restore_db_dumps.py"

if [ $# -ne 1 ] #Checking for the argument
then
  echo "Please pass the db_config.json or <<ap_name>>_db_config.json as argument"
  exit 1
fi

##Running the restore_db_dumps.py
python3 ${db_script_dir}/${restore_db_dumps_script} $1 

