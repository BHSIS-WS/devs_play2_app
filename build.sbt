name := """devs_play2_app"""
organization := "gov.bhsis"

//version := "1.0-SNAPSHOT"
version := (version in ThisBuild).value

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayScala, PlayEbean)

scalaVersion := "2.12.8"

libraryDependencies += guice
libraryDependencies += jdbc
libraryDependencies += javaJdbc
libraryDependencies += javaJdbc % Test

libraryDependencies += "org.postgresql" % "postgresql" % "42.2.1"

// https://mvnrepository.com/artifact/javax.xml.bind/jaxb-api
libraryDependencies += "javax.xml.bind" % "jaxb-api" % "2.3.1"
// https://mvnrepository.com/artifact/javax.activation/activation
libraryDependencies += "javax.activation" % "activation" % "1.1.1"
// https://mvnrepository.com/artifact/org.glassfish.jaxb/jaxb-runtime
libraryDependencies += "org.glassfish.jaxb" % "jaxb-runtime" % "2.3.2"

libraryDependencies += "org.apache.commons" % "commons-text" % "1.2"

libraryDependencies += "org.apache.poi" % "poi-ooxml" % "3.9"

libraryDependencies += "javax.json" % "javax.json-api" % "1.1.4"
libraryDependencies += "org.glassfish" % "javax.json" % "1.1.4"

libraryDependencies ++= Seq(
  ws,
  ehcache,
  cacheApi
)

libraryDependencies += "com.typesafe.play" %% "play-mailer" % "6.0.1"
libraryDependencies += "com.typesafe.play" %% "play-mailer-guice" % "6.0.1"

libraryDependencies ++= Seq(
  "org.pac4j" %% "play-pac4j" % "7.0.1",
  "org.pac4j" % "pac4j-http" % "3.6.1"
)

//compile sbt with java deprecated code warning.
//javacOptions := Seq("-Xlint", "-deprecation")

PlayKeys.playDefaultPort := 9042

routesGenerator := InjectedRoutesGenerator

mappings in Universal ++=
  (baseDirectory.value / "infra-scripts" * "*" get) map
    (x => x -> ("infra-scripts/" + x.getName))
mappings in Universal ++=
  (baseDirectory.value / "infra-scripts/TEST" * "*" get) map
    (x => x -> ("infra-scripts/TEST/" + x.getName))
mappings in Universal ++=
  (baseDirectory.value / "infra-scripts/DEV" * "*" get) map
    (x => x -> ("infra-scripts/DEV/" + x.getName))

mappings in Universal ++=
  (baseDirectory.value / "videos" * "*" get) map
    (x => x -> ("videos/" + x.getName))

