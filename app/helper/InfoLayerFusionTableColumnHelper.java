/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.util.*;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import io.ebean.Ebean;
import io.ebean.RawSqlBuilder;
import models.FusionMapLayer;
import models.InfoLayerFusionTableColumn;
import models.LayerColumnInfo;
import play.libs.Json;

/**
 *
 * @author pingw
 */
public class InfoLayerFusionTableColumnHelper {

    public static List<FusionMapLayer> assignInfoLayerFusionTableColumnListToFusionMapLayer(List<FusionMapLayer> fusionMapLayerList, 
            List<InfoLayerFusionTableColumn> infoLayerFusionTableColumnList) {
        

        //1. break infoLayerFusionTableColumnList into sublist by fusionLayerId
        Map<Integer, List<InfoLayerFusionTableColumn>> tableColumnListByFusionIdMap = new HashMap<Integer, List<InfoLayerFusionTableColumn>>();
        for (InfoLayerFusionTableColumn tableColumn : infoLayerFusionTableColumnList) {
            Integer fusionMapLayerId = tableColumn.fusionMapLayerId;
            List<InfoLayerFusionTableColumn> columnListOfThisLayer = tableColumnListByFusionIdMap.get(fusionMapLayerId);
            if (columnListOfThisLayer==null){
                columnListOfThisLayer = new ArrayList<InfoLayerFusionTableColumn>();
                tableColumnListByFusionIdMap.put(fusionMapLayerId, columnListOfThisLayer);
            }
            columnListOfThisLayer.add (tableColumn);    
        }
        

        //2.iterate over all fusionMapLayerList
        for (FusionMapLayer fml : fusionMapLayerList) {
            Integer key = fml.fusionMapLayerId;
            List<InfoLayerFusionTableColumn> columnList = tableColumnListByFusionIdMap.get(key);
            if ( columnList!=null ) {
                fml.infoLayerFusionTableColumnList = columnList;
            }
        }

        return fusionMapLayerList;
      
    }

    public List<LayerColumnInfo> getColumnInfo(List<String> orderedLayerIdList) {
        List<LayerColumnInfo> layers = null;
        if (orderedLayerIdList != null && !orderedLayerIdList.isEmpty()) {
            StringBuilder ids = new StringBuilder();
            for (String id : orderedLayerIdList) {
                if (ids.length() > 0) {
                    ids.append(", ");
                }
                ids.append(id);
            }
            String sql = "select string_agg(il.column_name, ',') as db_columns, fml.fusion_map_layer_id, fml.layer_name, " +
                    "fml.db_table_name, "+
                    "string_agg(to_char(il.info_layer_fusion_table_column_id, '99999'), ',') as db_column_ids " +
                    "from info_layer_fusion_table_column il " +
                    "join fusion_map_layer fml " +
                    "on il.fusion_map_layer_id = fml.fusion_map_layer_id " +
                    "where fml.fusion_map_layer_id in (" + ids.toString() + ") " +
                    "and il.disable_ind is null " +
                    "group by fml.fusion_map_layer_id";
            layers = Ebean.find(LayerColumnInfo.class)
                    .setRawSql(RawSqlBuilder
                    .parse(sql)
                    .create()).findList();
        }
        return layers;
    }

    public ArrayNode layerIdsToColumnArrayNode(List<String> orderedLayerIdList) {
        return listColumnToArray(getColumnInfo(orderedLayerIdList));
    }

    private ArrayNode listColumnToArray(List<LayerColumnInfo> infoColumns)  {
        ArrayNode listArrayNode = Json.newArray();
        if (infoColumns != null && !infoColumns.isEmpty()) {
            ObjectNode layerInfo = null;
            ArrayNode colArray = null;
            List<String> columnNameList = null;
            String[] columnNameArr = null;
            String[] columnIdArr = null;
            ObjectNode colInfo = null;
            for (LayerColumnInfo aLayer : infoColumns) {
                colArray = Json.newArray();
                layerInfo = Json.newObject();
                if (aLayer.dbColumns != null && !aLayer.dbColumns.isEmpty()) {
//                    columnNameList = Arrays.asList(aLayer.dbColumns.split(","));
//                    for (String colName : columnNameList) {
//                        colArray.add(colName);
//                    }
                    columnNameArr = aLayer.dbColumns.split(",");
                    columnIdArr = aLayer.dbColumnIds.split(",");
                    for(int i = 0; i < columnNameArr.length; i++) {
                        colInfo = Json.newObject();
                        colInfo.set("columnName", new TextNode(columnNameArr[i].trim()));
                        colInfo.set("columnId", new TextNode(columnIdArr[i].trim()));
                        colArray.add(colInfo);
                    }
                }
                layerInfo.set("columns", colArray);
                layerInfo.set("fusionMapLayerId", new IntNode(aLayer.fusionMapLayerId));
                layerInfo.set("layerName", new TextNode(aLayer.layerName));
                layerInfo.set("tableName", new TextNode(aLayer.dbTableName));
                listArrayNode.add(layerInfo);
            }
        }
        return listArrayNode;
    }


}
