/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import models.DataSet;
import models.GradientMapLayer;

/**
 *
 * @author pingw
 */
public class DataSetHelper {

    public static List<DataSet> assignGradientMapLayersToDataSetList(List<DataSet> dataSetList, List<GradientMapLayer> gradientMapLayerList) {
        //1. create dataSet map with code as key
        Map<String, DataSet> dataSetMap = new HashMap<String, DataSet>();
        for (DataSet dataSet : dataSetList) {
            dataSetMap.put(dataSet.dataSetId.toString(), dataSet);
        }

        //2.iterate over all gradientMapLayerList
        for (GradientMapLayer gml : gradientMapLayerList) {
            String key = gml.dataSetId.toString();
            DataSet dataSet = dataSetMap.get(key);
            if ( null != dataSet ) {
                if (null == dataSet.gradientMapLayers) {
                    dataSet.gradientMapLayers = new 
                      ArrayList<GradientMapLayer>();
                }
                List<GradientMapLayer> subGradientMapLayerList = 
                  dataSet.gradientMapLayers;
                subGradientMapLayerList.add(gml);
            }
        }

        return dataSetList;
    }
    
    public static void assignGradFieldColId(List<DataSet> dataSetList, Map<Integer,String> gradFieldColMap) {
        int id = 1;
        for (DataSet dataSet : dataSetList) {
            for (GradientMapLayer gradLayer : dataSet.gradientMapLayers) {
                gradLayer.fieldColumnId = Integer.valueOf(id);
                gradFieldColMap.put(Integer.valueOf(id), gradLayer.fieldColumnName);
                id++;
            }
        }
    }
    
}
