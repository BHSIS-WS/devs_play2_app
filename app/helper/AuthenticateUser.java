package helper;

import com.fasterxml.jackson.databind.JsonNode;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import models.helper.SecurityConstant;
import org.pac4j.core.context.Pac4jConstants;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.credentials.UsernamePasswordCredentials;
import org.pac4j.core.credentials.authenticator.Authenticator;
import org.pac4j.core.exception.CredentialsException;
import org.pac4j.core.profile.CommonProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.libs.crypto.CookieSigner;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.i18n.Lang;
import play.i18n.MessagesApi;
import play.libs.ws.*;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.CompletionStage;

import static play.mvc.Controller.*;

public class AuthenticateUser implements Authenticator<UsernamePasswordCredentials> {

    private WSClient ws;
    private MessagesApi messagesApi;
    private FormFactory formFactory;
    private Config config = ConfigFactory.load();
    private CookieSigner cookieSigner;
    private final String REG_COOKIE_NAME = "devsRegisterComputer";
    private static final Logger log = LoggerFactory.getLogger(AuthenticateUser.class);

    public AuthenticateUser(WSClient ws, MessagesApi messagesApi, FormFactory formFactory, CookieSigner cookieSigner) {
        this.ws = ws;
        this.messagesApi = messagesApi;
        this.formFactory = formFactory;
        this.cookieSigner = cookieSigner;
    }


    @Override
    public void validate(final UsernamePasswordCredentials credentials, final WebContext context) throws CredentialsException {
        boolean validUser;
        String registerComputer = "";

        if(session("tmploginname") == null) {
            String username = credentials.getUsername();
            String password = credentials.getPassword();
            String demoUser = config.hasPath("application.demo.username") ? config.getString("application.demo.username") : null;

            if (config.hasPath("applicationMode") && config.getString("applicationMode").equals("local") && username.equals(demoUser)) {
                demoAuthenticator(username, password, credentials);
            } else {
                if (ctx().request().cookie(REG_COOKIE_NAME) != null) {
                    registerComputer = ctx().request().cookie(REG_COOKIE_NAME).value();
                }
                JsonNode jsonAuth = authenticateUser(username, password, registerComputer);
                validUser = jsonAuth.get("authenticated") == null ? false : jsonAuth.get("authenticated").asBoolean();
                String authFailMessage = jsonAuth.get("authStatus") == null ? "" : jsonAuth.get("authStatus").asText();

                if (validUser) {
                    if (SecurityConstant.twoFactorAuthEnabled && !registerComputer.equals(registerComputerCookie(username))) {
                        WSRequest request = ws.url(SecurityConstant.SEND_PASSCODE_SVC_URL);
                        request.addQueryParameter("req.userName", username);
                        request.addQueryParameter("req.app", SecurityConstant.APP_ID);
                        request.addQueryParameter("req.appHost", SecurityConstant.APP_HOST);

                        String sendPasscodeStatus;
                        try {
                            CompletionStage<WSResponse> resp = request.post("content");
                            CompletionStage<String> respJsonPromise = resp.thenApply(WSResponse::getBody);
                            sendPasscodeStatus = respJsonPromise.toCompletableFuture().get();
                        } catch (Exception ex) {
                            sendPasscodeStatus = "send_error";
                        }
                        sendPasscodeStatus = (sendPasscodeStatus == null) ? "send_error" : sendPasscodeStatus;
                        if (sendPasscodeStatus.equals("success")) {
                            session("tmploginname", username);
                        } else {
                            flash("error", messagesApi.get(Lang.forCode("en"), "error.web.service.send.email"));
                            log.error("send passcode failed, the status is: " + sendPasscodeStatus);
                        }
                        forwardToLoginPage(true);
                    } else {
                        setAuthenticationSession(username, credentials);
                    }
                } else {
                    flash("error", authFailMessage);
                    throwsException(authFailMessage);
                }
            }
        } else {
            authenticatePasscode(credentials);
        }

    }

    private Collection<String> getUserPermission(final String userName){
        JsonNode respJsonObject;
        Collection<String> permissions;

        WSRequest request = ws.url(SecurityConstant.PERM_SERVICE_URL);
        request.addQueryParameter("userName", userName);
        request.addQueryParameter("app", SecurityConstant.APP_ID);
        try {
            CompletionStage<WSResponse> resp = request.post("content");
            CompletionStage<JsonNode> respJsonPromise = resp.thenApply(WSResponse::asJson);
            respJsonObject = respJsonPromise.toCompletableFuture().get();
            String result = respJsonObject.get("permissions").asText();
            permissions = result.length() > 0 ? new ArrayList<>(Arrays.asList(result.split(","))) : new ArrayList<>();
        } catch (Exception ex){
            ex.printStackTrace();
            permissions = new ArrayList<>();
            permissions.add("error");
        }
        return permissions;
    }

    private JsonNode authenticateUser(final String userId, final String userPwd, final String regComputer) throws CredentialsException {
        JsonNode respJsonObject = null;
        String authenticateCode;
        boolean verifyPasscode = SecurityConstant.twoFactorAuthEnabled;
        if (verifyPasscode && regComputer.equals(registerComputerCookie(userId))) {
            verifyPasscode = false;
        }

        WSRequest request = ws.url(SecurityConstant.AUTH_SERVICE_URL);
        request.addQueryParameter("loginName", userId);
        request.addQueryParameter("password", userPwd);
        request.addQueryParameter("appid", SecurityConstant.APP_ID);
        request.addQueryParameter("verifyPasscode", String.valueOf(verifyPasscode));

        try{
            CompletionStage<WSResponse> resp = request.post("content");
            CompletionStage<JsonNode> respJsonPromise = resp.thenApply(WSResponse::asJson);
            respJsonObject = respJsonPromise.toCompletableFuture().get();
            authenticateCode = respJsonObject.get("authCode").asText();

        } catch(Exception ex){
            ex.getStackTrace();
            authenticateCode = "1";
            flash("error", messagesApi.get(Lang.forCode("en"),"error.auth.webservice.notfound"));
            throwsException(messagesApi.get(Lang.forCode("en"),"error.auth.webservice.notfound"));
        }
        authenticateCode = (authenticateCode == null) ? "1" : authenticateCode;

        switch (authenticateCode) {
            case SecurityConstant.AUTH_ACCOUNT_DISABLED:
                flash("error", messagesApi.get(Lang.forCode("en"),"error.login.account.disabled"));
                throwsException(messagesApi.get(Lang.forCode("en"),"error.login.account.disabled"));
                break;
            case SecurityConstant.AUTH_ACCOUNT_PWD_LOCKED:
                flash("error", messagesApi.get(Lang.forCode("en"),"error.login.account.locked"));
                throwsException(messagesApi.get(Lang.forCode("en"),"error.login.account.locked"));
                break;
            case SecurityConstant.AUTH_NO_ACTIVITY_LOCKED:
                flash("error", messagesApi.get(Lang.forCode("en"),"error.login.account.noactivity"));
                throwsException(messagesApi.get(Lang.forCode("en"),"error.login.account.noactivity"));
                break;
            case SecurityConstant.AUTH_PWD_EXPIRED:
                flash("error", messagesApi.get(Lang.forCode("en"),"error.login.password.expired"));
                throwsException(messagesApi.get(Lang.forCode("en"),"error.login.password.expired"));
                break;
            case SecurityConstant.AUTH_ACCOUNT_ADDED:
                flash("error", messagesApi.get(Lang.forCode("en"),"error.login.account.added"));
                throwsException(messagesApi.get(Lang.forCode("en"),"error.login.account.added"));
                break;
            case SecurityConstant.AUTH_ID_PWD_FAIL:
                flash("error", messagesApi.get(Lang.forCode("en"),"error.login.invalid.uidorpassword"));
                throwsException(messagesApi.get(Lang.forCode("en"),"error.login.invalid.uidorpassword"));
                break;
            default:
        }

        return respJsonObject;
    }

    private void authenticatePasscode(final UsernamePasswordCredentials credentials) throws CredentialsException {
        String userId = session("tmploginname");
        DynamicForm requestData = formFactory.form().bindFromRequest();
        String passCode = requestData.get("verificationCode");
        String tfaAttemps = requestData.get("tfaAttemps");
        int attempts = getTfaAttemps(tfaAttemps);

        WSRequest request = ws.url(SecurityConstant.PASSCODE_SVC_URL);
        request.addQueryParameter("req.userName", userId);
        request.addQueryParameter("req.app", SecurityConstant.APP_ID);
        request.addQueryParameter("req.appHost", SecurityConstant.APP_HOST);
        request.addQueryParameter("req.passCode", passCode);
        request.addQueryParameter("req.failCount", Integer.toString(attempts));

        String passcodeRslt;
        try {
            CompletionStage<WSResponse> resp = request.post("content");
            CompletionStage<String> respJsonPromise = resp.thenApply(WSResponse::getBody);
            passcodeRslt = respJsonPromise.toCompletableFuture().get();
        } catch (Exception ex) {
            log.error(String.format("Verify two factor passcode failed due to exception: %s", ex.getMessage()));
            passcodeRslt = "fail";
        }
        passcodeRslt = (passcodeRslt == null) ? "fail" : passcodeRslt;
        switch (passcodeRslt) {
            case SecurityConstant.PASS_CODE_SUCCESS:
                setAuthenticationSession(userId, credentials);
                session().remove("tmploginname");
                String registerComputer = requestData.get("registerComputer");
                if(registerComputer.equals("personal")) {
                    registerComputer = registerComputerCookie(userId);
                    response().setCookie(
                        Http.Cookie.builder(REG_COOKIE_NAME, registerComputer)
                            .withMaxAge(Duration.of(2592000, ChronoUnit.SECONDS))
                            .withSecure(config.getBoolean("play.http.session.secure"))
                            .withHttpOnly(config.getBoolean("play.http.session.httpOnly"))
                            .build()
                    );
                }
                break;
            case SecurityConstant.PASS_CODE_EXPIRE:
            case SecurityConstant.PASS_CODE_FAIL:
            case SecurityConstant.PASS_CODE_NO_CONFIG:
                attempts++;
                session("tfaAttemps", Integer.toString(attempts));
                session("invalidPassCode", passCode);
                if (attempts >= 5) {
                    session().clear();
                    forwardToLoginPage(false);
                } else {
                    if (SecurityConstant.PASS_CODE_FAIL.equals(passcodeRslt) || SecurityConstant.PASS_CODE_NO_CONFIG.equals(passcodeRslt)) {
                        flash("error", messagesApi.get(Lang.forCode("en"),"error.login.invalid.authcode", passCode));
                    } else if (SecurityConstant.PASS_CODE_EXPIRE.equals(passcodeRslt)) {
                        flash("error", messagesApi.get(Lang.forCode("en"), "error.web.service.expired.pass.code", passCode));
                        log.error("passcode expired, the status is: " + messagesApi.get(Lang.forCode("en"), "error.web.service.expired.pass.code", passCode));
                    }
                    forwardToLoginPage(true);
                }
                break;
            default:
                flash("error", messagesApi.get(Lang.forCode("en"), "error.login.invalid.authcode", passCode));
                forwardToLoginPage(true);
                break;
        }
    }

    private void throwsException(final String message) throws CredentialsException {
        throw new CredentialsException(message);
    }

    private void setAuthenticationSession(String username, UsernamePasswordCredentials credentials) throws CredentialsException {
        Collection<String> permissions = getUserPermission(username);
        final CommonProfile profile = new CommonProfile();
        profile.setId(username);
        profile.addAttribute(Pac4jConstants.USERNAME, username);
        profile.addRoles(permissions);
        credentials.setUserProfile(profile);
    }

    private String registerComputerCookie(String userId) {
        Calendar c = Calendar.getInstance();
        int currentMonth = c.get(Calendar.MONTH);
        int currentYear = c.get(Calendar.YEAR);
        byte[] secretByte = config.getString("play.http.secret.key").getBytes();
        return cookieSigner.sign("personal_" + userId + currentMonth + currentYear, secretByte);
    }


    private Result forwardToLoginPage(boolean isForwardingMessage) {
        if(isForwardingMessage) {
            String forwardMessage = flash("error");
            if(forwardMessage != null) {
                session("forwardMessage", forwardMessage);
            }
        }
        return Results.redirect("/devs2/login");
    }

    private static int getTfaAttemps(String tfaAttemps) {
        int attemps = 0;
        if (tfaAttemps != null && !tfaAttemps.isEmpty()) {
            attemps = Integer.valueOf(tfaAttemps);
        }
        return attemps;
    }

    private void demoAuthenticator(String username, String password, UsernamePasswordCredentials credentials) throws CredentialsException {
        String demoPassword = config.hasPath("application.demo.password") ? config.getString("application.demo.password") : null;
        String demoPermissions = config.hasPath("application.demo.permissions") ? config.getString("application.demo.permissions") : null;

        if (demoPassword != null && demoPassword.equals(password)) {
            Collection<String> permissions = new ArrayList<>();
            permissions.add(demoPermissions);
            CommonProfile profile = new CommonProfile();
            profile.setId(username);
            profile.addAttribute(Pac4jConstants.USERNAME, username);
            profile.addRoles(permissions);
            credentials.setUserProfile(profile);
        } else {
            flash("error", messagesApi.get(Lang.forCode("en"),"error.login.invalid.uidorpassword"));
            throwsException(messagesApi.get(Lang.forCode("en"),"error.login.invalid.uidorpassword"));
        }
    }

}

