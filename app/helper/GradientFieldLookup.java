/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import helper.util.ListMapUtility;

import helper.util.ResultSetConverter;
import io.ebean.Ebean;
import io.ebean.SqlRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author pingw
 */
public class GradientFieldLookup {

    private Map<String, Map<String, String>> lookupMap;
    private static final Logger logger = LoggerFactory.getLogger(GradientFieldLookup.class);

    public GradientFieldLookup() {
    }
    
    public void makeSureLookupMapIsInitialized() throws Exception{
        if (lookupMap==null){
            //MiscUtility.printMemInfo();
            lookupMap = buildLookupMap();
            //MiscUtility.printMemInfo();
        }
    }
    
    
    public Map<String, Map<String, String>> buildLookupMap() throws Exception {

        Map<String, Map<String, String>> ret = new HashMap<String, Map<String, String>>();
        ret.putAll(buildLookupMapForStateLevelFields());
        ret.putAll(buildLookupMapForCountyLevelFields());
        return ret;
    }

    
    //state
    private Map<String, Map<String, String>> buildLookupMapForStateLevelFields() throws SQLException, Exception {

        List<Map<String, String>> gradientMapLayerListMap = getGradientMapLayerDataForStateLevel();
        Map<String, List<Map<String, String>>> grouppedGradientMapLayerListMap =
                ListMapUtility.groupListMapByHeaderName(gradientMapLayerListMap, "db_table_name");
        Set<Map.Entry<String, List<Map<String, String>>>> entrySet = grouppedGradientMapLayerListMap != null ? grouppedGradientMapLayerListMap.entrySet() : null;

        //retMap  = Map<gradientLayerId,<Map<fips_code>,fieldValue>>
        Map< String, Map<String, String>> ret = new HashMap<String, Map<String, String>>();
        if (entrySet != null && !entrySet.isEmpty()) {
            for (Map.Entry<String, List<Map<String, String>>> entry : entrySet) {
                String tableName = entry.getKey();
                List<Map<String, String>> dataListMap = entry.getValue();

                String[] gradientMapLayerIds = ListMapUtility.extractColumn(dataListMap, "gradient_map_layer_id");
                String[] columns = ListMapUtility.extractColumn(dataListMap, "field_column_name");
                Map<String, Map<String, String>> mapForOneTableQuery = fetchGradientDataFieldForOneTableStateLevel(tableName, columns, gradientMapLayerIds);
                ret.putAll(mapForOneTableQuery);
            }
        }

        return ret;
    }

    private Map< String, Map<String, String>> fetchGradientDataFieldForOneTableStateLevel(String tableName, String[] columns, String[] gradientMapLayerIds) throws SQLException {
        StringBuilder sb = new StringBuilder("select state_fips_code");
        for (int i = 0; i < columns.length; i++) {
            sb.append(", ");
            sb.append(columns[i]);
        }
        sb.append(" from ").append(tableName);

        List<SqlRow> sqlRs = Ebean.createSqlQuery(sb.toString()).findList();
        List<Map<String, String>> listMap = ResultSetConverter.convertToListMap(sqlRs);

        //ret = Map<gradientLayerId,<Map<fips_code>,fieldValue>>
        Map< String, Map<String, String>> ret = new HashMap<String, Map<String, String>>();
        for (int i = 0; i < gradientMapLayerIds.length; i++) {
            String gradientMapLayerId = gradientMapLayerIds[i];
            String columnName = columns[i];
            Map<String, String> valueMap = ListMapUtility.createMap(listMap, "state_fips_code", columnName);
            ret.put(gradientMapLayerId, valueMap);
        }

        return ret;
    }

    private List<Map<String, String>> getGradientMapLayerDataForStateLevel() throws SQLException {
        String sql = "select gradient_map_layer_id,  db_table_name, lower(field_column_name) as field_column_name,data_level from gradient_map_layer  gml\n"
                + "inner join data_set ds\n"
                + "on gml.data_set_id = ds.data_set_id\n"
                + "where (gml.disable_ind is null or gml.disable_ind !=1)\n"
                + "and (ds.disable_ind is null or ds.disable_ind !=1)\n"
                + "and data_level = 'state'\n"
                + "order by db_table_name, gradient_map_layer_id";
        List<SqlRow> sqlRs = Ebean.createSqlQuery(sql).findList();
        List<Map<String, String>> listMap = ResultSetConverter.convertToListMap(sqlRs);
        return listMap;
    }

    //county
    private Map<String, Map<String, String>> buildLookupMapForCountyLevelFields() throws SQLException, Exception {

        List<Map<String, String>> gradientMapLayerListMap = getGradientMapLayerDataForCountyLevel();
        Map<String, List<Map<String, String>>> grouppedGradientMapLayerListMap =
                ListMapUtility.groupListMapByHeaderName(gradientMapLayerListMap, "db_table_name");
        Set<Map.Entry<String, List<Map<String, String>>>> entrySet = grouppedGradientMapLayerListMap.entrySet() != null ? grouppedGradientMapLayerListMap.entrySet() : null;

        //retMap  = Map<gradientLayerId,<Map<fips_code>,fieldValue>>
        Map< String, Map<String, String>> ret = new HashMap<String, Map<String, String>>();
        if (entrySet != null && !entrySet.isEmpty()) {
            for (Map.Entry<String, List<Map<String, String>>> entry : entrySet) {
                String tableName = entry.getKey();
                List<Map<String, String>> dataListMap = entry.getValue();

                String[] gradientMapLayerIds = ListMapUtility.extractColumn(dataListMap, "gradient_map_layer_id");
                String[] columns = ListMapUtility.extractColumn(dataListMap, "field_column_name");
                Map<String, Map<String, String>> mapForOneTableQuery = fetchGradientDataFieldForOneTableCountyLevel(tableName, columns, gradientMapLayerIds);
                ret.putAll(mapForOneTableQuery);
            }
        }

        return ret;
    }

    
    private Map< String, Map<String, String>> fetchGradientDataFieldForOneTableCountyLevel(String tableName, String[] columns, String[] gradientMapLayerIds) throws SQLException {
        StringBuilder sb = new StringBuilder("select fips_county_code");
        for (int i = 0; i < columns.length; i++) {
            sb.append(", ");
            sb.append(columns[i]);
        }
        sb.append(" from ").append(tableName);
        logger.info("--------");
        logger.info(sb.toString());
        logger.info("--------");
        List<SqlRow> sqlRs = Ebean.createSqlQuery(sb.toString()).findList();
        List<Map<String, String>> listMap = ResultSetConverter.convertToListMap(sqlRs);

        //ret = Map<gradientLayerId,<Map<fips_code>,fieldValue>>
        Map< String, Map<String, String>> ret = new HashMap<String, Map<String, String>>();
        for (int i = 0; i < gradientMapLayerIds.length; i++) {
            String gradientMapLayerId = gradientMapLayerIds[i];
            String columnName = columns[i];
            Map<String, String> valueMap = ListMapUtility.createMap(listMap, "fips_county_code", columnName);
//                   System.out.println("*** to be removed...'"+columnName);
//        System.out.println(listMap);
            ret.put(gradientMapLayerId, valueMap);
        }
 
        return ret;
    }

    private List<Map<String, String>> getGradientMapLayerDataForCountyLevel() throws SQLException {
        
        String sql = "select gradient_map_layer_id,  db_table_name, lower(field_column_name) as field_column_name,data_level  from gradient_map_layer  gml\n"
                + "inner join data_set ds\n"
                + "on gml.data_set_id = ds.data_set_id\n"
                + "where (gml.disable_ind is null or gml.disable_ind !=1)\n"
                + "and (ds.disable_ind is null or ds.disable_ind !=1)\n"
                + "and data_level = 'county'\n"
                + "order by db_table_name, gradient_map_layer_id";
        List<SqlRow> sqlRs = Ebean.createSqlQuery(sql).findList();
        List<Map<String, String>> listMap = ResultSetConverter.convertToListMap(sqlRs);
        return listMap;
    }
}
