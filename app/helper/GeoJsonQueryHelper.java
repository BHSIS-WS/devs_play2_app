package helper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import helper.util.ListMapUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GeoJsonQueryHelper {

    private Integer layerId;
    private String requestParameterJsonString;
    private JsonNode rootNode;
    private JsonNode layerCriteriaNode;
    private JsonNode allLayersCriteriaNode;
    private JsonNode additionalFields;
    private JsonNode dynamicConditions;
    private static final Logger logger = LoggerFactory.getLogger(GeoJsonQueryHelper.class);

    public GeoJsonQueryHelper(Integer layerId, String requestParameterJsonString) {

        this.layerId = layerId;
        if (requestParameterJsonString == null || requestParameterJsonString.trim().equals("")) {
            return;
        }
        this.requestParameterJsonString = requestParameterJsonString;
        this.rootNode = Json.parse(requestParameterJsonString);
        this.layerCriteriaNode = rootNode.get("layerCriteria");
        this.allLayersCriteriaNode = rootNode.get("allLayersCriteria");
        this.additionalFields = rootNode.get("additionalFields");
        this.dynamicConditions = rootNode.get("dynamicConditions");


//        System.out.println("layerCriteriaNode:\n" + layerCriteriaNode);

    }

    public String getNormalLayerWhereStringThatShowOnlyCommonToAllLayer() throws SQLException {
        FusionLayerQueryHelper fusionLayerQueryHelper = new FusionLayerQueryHelper();

        if (this.allLayersCriteriaNode == null ){
            return getNormalLayerWhereString();
        }

        List<Map<String, String>>allLayerCriteriaListMap
                = fusionLayerQueryHelper.convertInputLayersNodeToListMap( (ArrayNode)this.allLayersCriteriaNode );

        if (allLayerCriteriaListMap.size()<=0){
            return getNormalLayerWhereString();
        }

        List<Map<String, String>> inputListMapWithCorrectOrder
                = fusionLayerQueryHelper.getInputListMapWithCorrectOrderOfLayerWithin(allLayerCriteriaListMap);

        if (inputListMapWithCorrectOrder==null||inputListMapWithCorrectOrder.size()<=0){
            throw new RuntimeException("unable to order input layers based on vw_info_layer_order_within. empty result returned for : "+allLayerCriteriaListMap);
        }

        List<String> orderedLayerIdList = ListMapUtility.extractColumnAsList(inputListMapWithCorrectOrder, "fusion_map_layer_id");

        List<Map<String, String>> gradientLayerListMap=
                fusionLayerQueryHelper.extractGradientLayersFromRequest(this.rootNode);
        boolean invertQuery=false;
        List<Map<String, String>> shapeArrayMap = null;

        logger.info("-----------------------");
        logger.info("****** allLayerCriteriaListMap: "+allLayerCriteriaListMap);
        logger.info("****** inputListMapWithCorrectOrder: "+inputListMapWithCorrectOrder);
        logger.info("****** orderedLayerIdList: "+orderedLayerIdList);
        logger.info("-----------------------");


        Map<String, String> sqlInfoMap = fusionLayerQueryHelper.generateSqlToEliminateOuterLayers(inputListMapWithCorrectOrder
                , gradientLayerListMap, invertQuery, shapeArrayMap, null, null, layerId, null, null);

//        Map<String, String> layerIdTableNameMap = buildLayerIdTableNameMap(inputListMapWithCorrectOrder);
        logger.info("-----------------------");
        logger.info("****** sqlInfoMap:"+sqlInfoMap);
        logger.info("-----------------------");

        String filterSql = sqlInfoMap.get("sql");

        if (filterSql!=null && !filterSql.trim().isEmpty()){
            System.out.println("GeoJsonQueryHelper: FILTER_SQL " + " ogc_fid in (" + filterSql + ")");
            return " ogc_fid in (" + filterSql + ")";
        }

        System.out.println("FINISH WITH: " + getNormalLayerWhereString());
        return getNormalLayerWhereString();
    }



    public JsonNode getLayerCriteriaNode() {
        return layerCriteriaNode;
    }

    public JsonNode getAdditionalFields() {
        return additionalFields;
    }

    public List<String> getAdditionalFieldsJavaList(){
        if (this.additionalFields==null) return null;
        List<String> ret = new ArrayList<String>();
        Iterator<JsonNode> elements = this.additionalFields.elements();
        while (elements.hasNext()) {
            ret.add ( elements.next().asText() );
        }
        return ret;
    }

    public String getNormalLayerWhereString() {

        String finalString = "";

        if (this.layerCriteriaNode == null) {
            return appendDynamicConditionsToNormalLayerWhereString("");
        }
        StringBuilder sb = new StringBuilder();

        JsonNode queryTypeNode = this.layerCriteriaNode.get("queryType");
        if (queryTypeNode==null) return appendDynamicConditionsToNormalLayerWhereString("");

        String queryType = this.layerCriteriaNode.get("queryType").asText();
        Iterator<JsonNode> criteriaCodeEntries = this.layerCriteriaNode.get("criteria").elements();
        JsonNode criteriaCode;
        Iterator<JsonNode> criteriaValueEntries;
        JsonNode firstCriteriaValue;
        JsonNode criteriaValue;
        String subConditionStr = "";
        while (criteriaCodeEntries.hasNext()) {

            criteriaCode = (JsonNode) criteriaCodeEntries.next();
            criteriaValueEntries = criteriaCode.get("criteriaValues").elements();

            if (queryType.equalsIgnoreCase("in")) {
                String criteriaColumnName = criteriaCode.get("criteriaName").asText();
                subConditionStr = "";
                if (criteriaValueEntries.hasNext()) {
                    firstCriteriaValue = (JsonNode) criteriaValueEntries.next();
                    subConditionStr += criteriaColumnName + " = " + "'" + firstCriteriaValue.asText() + "'";
                }
                while (criteriaValueEntries.hasNext()) {
                    criteriaValue = (JsonNode) criteriaValueEntries.next();
                    subConditionStr += " or " + criteriaColumnName + " = '" + criteriaValue.asText() + "'";
                }
            } else if (queryType.equalsIgnoreCase("and")) {
                subConditionStr = "";
                if (criteriaValueEntries.hasNext()) {
                    firstCriteriaValue = (JsonNode) criteriaValueEntries.next();
                    subConditionStr += firstCriteriaValue.asText() + " = '1'";
                }
                while (criteriaValueEntries.hasNext()) {
                    criteriaValue = (JsonNode) criteriaValueEntries.next();
                    subConditionStr += " and " + criteriaValue.asText() + " = '1'";
                }
            }
            if (!subConditionStr.isEmpty()) {
                if (sb.length()>0) {
                    sb.append(" and ");
                }
                sb.append("(").append(subConditionStr).append(")");
            }
        }

        finalString = appendDynamicConditionsToNormalLayerWhereString(sb.toString());

        return finalString;

    }

    public String appendDynamicConditionsToNormalLayerWhereString(String normalLayerWhereString) {

        if (this.dynamicConditions == null) {
            return "true";
        }

        Iterator<JsonNode> dynamicConditionEntries = this.dynamicConditions.elements();
        JsonNode dynamicCondition;
        StringBuilder sb = new StringBuilder();
        sb.append(normalLayerWhereString);

        if (sb.length() > 0 && dynamicConditionEntries.hasNext()) {
            sb.append(" and (");
        } else if (dynamicConditionEntries.hasNext()) {
            sb.append("(");
        }

        while(dynamicConditionEntries.hasNext()) {
            dynamicCondition = dynamicConditionEntries.next();

            JsonNode displayColumnName = dynamicCondition.get("displayColumnName");
            if (displayColumnName != null) {
                String subConditionStr =
                        "NULLIF(" +  displayColumnName.asText() + ",'')::int"  + " >= " + dynamicCondition.get("lowerBound").asInt() + " AND "
                                + "NULLIF(" +  displayColumnName.asText() + ",'')::int" + " <= " + dynamicCondition.get("upperBound").asInt();
                if(!subConditionStr.isEmpty()) {
                    sb.append("(").append(subConditionStr).append(")");

                    if (dynamicConditionEntries.hasNext()) {
                        sb.append(" or ");
                    } else {
                        sb.append(")");
                    }
                }
            }
        }

        return sb.toString();
    }


    public void debugPrint() {
        logger.info("--------------GeoJsonQueryHelper.debugPrint()------------");
        logger.info("\nrequestParameterJsonString : \n" + requestParameterJsonString);
        logger.info("\nrootNode : \n" + rootNode);
        logger.info("\nlayerCriteriaNode : \n" + layerCriteriaNode);
        logger.info("\nadditionalFields : \n" + additionalFields);
        logger.info("\nallLayersCriteriaNode : \n" + allLayersCriteriaNode);
        logger.info("\ndynamicConditions : \n" + dynamicConditions);
    }
}
