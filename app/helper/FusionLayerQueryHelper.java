package helper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import helper.util.JsonSmartUtility;
import helper.util.ResultSetUtil;
import io.ebean.Ebean;
import io.ebean.SqlRow;
import models.DataSet;
import models.GradientMapLayer;
import org.apache.commons.text.StringEscapeUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.*;

import helper.util.ListMapUtility;
import helper.util.ResultSetConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.Play;
import play.i18n.Lang;
import play.i18n.Messages;
import play.i18n.MessagesApi;
import play.libs.Json;

public class FusionLayerQueryHelper {

    private final MessagesApi messagesApi;
    Config config;
    private Map<String, String> sqlInfoMap;
    private int page;
    private int pageSize;
    private List<Map<String, String>> resultListMap;
    private List<String> columnList;
    private Map<String,String> tooltipMap;
    private List<Map<String, Object>> selectColumnListMap;
    private Map<String, String> columnIdNameMap;
    private Map<String, String> columnIdTooltipMap;
    private Map<String, String> layerIdTableNameMap;
    private List<Map<String, String>> gradientLayerListMap;
    private List<Integer> stateLevelOptList;
    private List<Integer> countyLevelOptList;
    private Map<String, String> gradFieldColMap1;
    private Map<String, String> tooltipLookupMapGradientLayer;//map<gradientLayerID,tooltipText>
    private String orderColumnName;
    private String orderColumnDir;


    private static final Logger logger = LoggerFactory.getLogger(FusionLayerQueryHelper.class);
    private final Boolean debugMapReq;
    public static Map<Integer,String> gradFieldColMap = new HashMap<Integer,String>();
    static {
        List<GradientMapLayer> gradientMapLayerList = GradientMapLayer.getAllGradientMapLayer();
        List<DataSet> dataSetList = DataSet.getEnabledGradientLayerDataSet();
        dataSetList = DataSetHelper.assignGradientMapLayersToDataSetList(dataSetList, gradientMapLayerList);
        DataSetHelper.assignGradFieldColId(dataSetList, gradFieldColMap);
    }

    public FusionLayerQueryHelper() {
        messagesApi = Play.current().injector().instanceOf(MessagesApi.class);
        config = ConfigFactory.load();
        debugMapReq = "debug".equals(config.getString("map.req.logging")) ? true : false;
    }

    public List<Map<String, String>> extractInputLayersFromRequestRootNode(JsonNode jsonReq) {
        ArrayNode layersNode = (ArrayNode) jsonReq.get("layers");
        return convertInputLayersNodeToListMap(layersNode);
    }

    public List<Map<String, String>> convertInputLayersNodeToListMap(ArrayNode layersNode) {

        if (layersNode==null) return null;

        List<Map<String, String>> inputLayerListMap = new ArrayList<Map<String, String>>();
        Iterator<JsonNode> iter = layersNode.iterator();
        while (iter.hasNext()) {
            JsonNode aJsonObject = iter.next();
            String fusionMapLayerId = trimAndUnquote(aJsonObject.get("fusion_map_layer_id").toString());
            JsonNode criteriaElement = aJsonObject.get("criteria");
            Map<String, String> map = new HashMap<String, String>();
            map.put("fusion_map_layer_id", fusionMapLayerId);

            if (criteriaElement != null) {
                String criteria = trimAndUnquote(criteriaElement.toString());
                if (!criteria.trim().equals("")) {
                    map.put("criteria", criteria);
                }
            }
            inputLayerListMap.add(map);
        }

        return inputLayerListMap;
    }

    public static void trimAndUnquote(String[] stringArray) {
        if (stringArray == null) {
            return;
        }
        for (int i = 0; i < stringArray.length; i++) {
            stringArray[i] = trimAndUnquote(stringArray[i]);
        }
    }

    public static String trimAndUnquote(String quoteText) {
        String text = quoteText.trim();
        if (text.startsWith("\"") && text.endsWith("\"")) {
            text = text.substring(1, text.length() - 1);
        }
        text = encodeString(text);
//        else if (text.startsWith("\"")) {
//            text = text.substring(1, text.length());
//        } else if (text.endsWith("\"")) {
//            text = text.substring(0, text.length() - 1);
//        }
        return text;
    }

    public static String toCamelCase(String inputName) {
        String wordDelimiter = "[-_]+";
        return toCamelCase(inputName, wordDelimiter);
    }

    public static String capFirstCharacterOfString(String inputName) {
        StringBuilder nameBuilder = new StringBuilder(inputName.length());
        nameBuilder.append(inputName.substring(0, 1).toUpperCase());
        if (inputName.length() > 1) {
            nameBuilder.append(inputName.substring(1));
        }
        return nameBuilder.toString();
    }

    public static String toCamelCase(String inputName, String wordDelimiter) {
        String[] words = inputName.split(wordDelimiter);
        StringBuilder nameBuilder = new StringBuilder(inputName.length());
        if (words == null || words.length <= 0) {
            return inputName;
        }

        nameBuilder.append(words[0].trim().toLowerCase());// handle first
        for (int i = 1; i < words.length; i++) {
            String word = words[i].trim().toLowerCase();
            nameBuilder.append(word.substring(0, 1).toUpperCase());
            nameBuilder.append(word.substring(1));
        }
        return nameBuilder.toString();
    }

    public static String toCamelCaseWithFirstCapped(String inputName) {
        inputName = toCamelCase(inputName);
        if (inputName!=null){
            inputName=capFirstCharacterOfString(inputName);
        }
        return inputName;
    }

    public static String generateMd5Sum(String text) throws NoSuchAlgorithmException, FileNotFoundException, IOException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(text.getBytes());
        byte[] mdbytes = md.digest();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mdbytes.length; i++) {
            sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString().toUpperCase();
    }

    //Encode input string to prevent SQL injection to the database
    private static String encodeString(String formText) {
        //Remove any injection to SQL comands
        formText = formText.replaceAll("(?i)delete", "");
        formText = formText.replaceAll("(?i)insert", "");
        formText = formText.replaceAll("(?i)update", "");
        //Remove any javascript code can be executed
        formText = formText.replaceAll("(?i)eval\\((.*)\\)", "");
        formText = formText.replaceAll("[\\\"\\\'][\\s]*((?i)javascript):(.*)[\\\"\\\']", "\"\"");
        formText = formText.replaceAll("((?i)script)", "");
        //Escape any HTML code except for input criteria < and > surrounded by spaces
        formText = StringEscapeUtils.escapeHtml4(formText);
        if(formText.contains(" &gt; ")) formText = formText.replaceAll(" &gt; ", " > ");
        if(formText.contains(" &lt; ")) formText = formText.replaceAll(" &lt; ", " < ");
        return formText;
    }

    /**
     * Convert string parameter to integer, return null if error happened
     * @return integer
     * @param stringParam: string we need to cast to integer
     */
    public static Integer convertStringToInteger(String stringParam) {
        Integer integerParam;
        try {
            integerParam = Integer.parseInt(stringParam);
        }
        catch (NumberFormatException nfe) {
            //nfe.printStackTrace();
            integerParam = null;
        }
        return integerParam;
    }

    /**
     * This method to generate a random text code
     * @return random text
     * @param length: input text
     */
    public static String getRandomCode(int length) {
        char[] charsArray = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789".toCharArray();
        Random random = new Random(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(charsArray[random.nextInt(charsArray.length)]);
        }
        return sb.toString();
    }

    public JsonNode checkInputLayersForException(List<Map<String, String>> inputLayerListMap) throws SQLException {
        if (inputLayerListMap==null || inputLayerListMap.size()<=0){
            return null;//we are okay
        }
        List<Map<String, String>> innerMostGroupLayers = getInnerMostGroupOfLayers(inputLayerListMap);
        if ( innerMostGroupLayers==null || innerMostGroupLayers.size()<=1 ){
            return null;//we are okay
        };
        Collection<Lang> candidates = Collections.singletonList(new Lang(Locale.US));
        Messages messages = messagesApi.preferred(candidates);

        String exceptionType = messages.at("check.input.layers.for.exception.type");
        String exceptionMessage = messages.at("check.input.layers.for.exception.message") + " "
                +ListMapUtility.extractColumnAsList(innerMostGroupLayers, "layer_name") +".";

        ObjectNode json = Json.newObject();
        json.put("exceptionSummary", messages.at("check.input.layers.for.exception"));
        json.put("exceptionCode", "555");
        json.put("exceptiontype", exceptionType);
        json.put("exceptionmessage", exceptionMessage );

        return Json.toJson(json);

    }

    public List<Map<String, String>> extractGradientLayersFromRequest(JsonNode jsonRequestObject) {

        ArrayNode jsonLayerArray = (ArrayNode) jsonRequestObject.get("gradientLayers");
        Iterator<JsonNode> iter = jsonLayerArray.iterator();
        List<Map<String, String>> layerListMap = new ArrayList<Map<String, String>>();
        while (iter.hasNext()) {
            JsonNode aJsonObject = iter.next();
            String fusionMapLayerId = trimAndUnquote(aJsonObject.get("gradientMapLayerId").toString());
            JsonNode criteriaElement = aJsonObject.get("criteria");
            Map<String, String> map = new HashMap<String, String>();
            map.put("gradientMapLayerId", fusionMapLayerId);

            if (criteriaElement != null) {
                String criteria = trimAndUnquote(criteriaElement.toString());
                if (!criteria.trim().equals("")) {
                    map.put("criteria", criteria);
                }
            }
            layerListMap.add(map);
        }

        return layerListMap;

    }

    private List<Map<String, String>> getInnerMostGroupOfLayers(List<Map<String, String>> inputListMap) throws SQLException {
        logger.debug("input:  "+inputListMap);//debug
        List<String> layerIdList = ListMapUtility.extractColumnAsList(inputListMap, "fusion_map_layer_id");

        StringBuilder layerIdCriteriaSb = new StringBuilder();
        layerIdCriteriaSb.append(" fusion_map_layer_id in (");
        for (int i = 0; i < layerIdList.size(); i++) {
            if (i != 0) {
                layerIdCriteriaSb.append(",");
            }
            layerIdCriteriaSb.append(layerIdList.get(i));
        }
        layerIdCriteriaSb.append(") ");
        String layerIdCriteriaString = layerIdCriteriaSb.toString();

        String sql = "select  fusion_map_layer_id, layer_name, fusion_map_layer_group_id, display_order "
                + "from fusion_map_layer where" + layerIdCriteriaString + " "
                + "and fusion_map_layer_group_id in ( "
                + "	select  fusion_map_layer_group_id  "
                + "	from fusion_map_layer where"  +layerIdCriteriaString + " "
                + "	and display_order = ("
                + "	  select min(display_order) from fusion_map_layer where"+ layerIdCriteriaString+ " "
                + "	) "
                + ") "
                + "order by display_order";

        List<SqlRow> sqlRs = Ebean.createSqlQuery(sql).findList();

        List<Map<String, String>> listMap = ResultSetConverter.convertToListMap(sqlRs);

        if (debugMapReq) {
            logger.info("***listmap : "+listMap);
        }
        return listMap;

    }

    public List<Integer> extractStateLevelOptionsFromRequest(JsonNode jsonRequestObject) {
        ArrayNode jsonLayerArray = (ArrayNode) jsonRequestObject.get("stateLevelOpts");
        Iterator<JsonNode> iter = jsonLayerArray.iterator();
        List<Integer> fieldIdList = new ArrayList<Integer>();
        Integer fieldId;
        while (iter.hasNext()) {
            try {
                fieldId = ((IntNode) iter.next()).intValue();
                fieldIdList.add(fieldId);
            }
            catch (NumberFormatException nfe) {
                //nfe.printStackTrace();
            }

        }
        return fieldIdList;
    }

    public List<Integer> extractCountyLevelOptionsFromRequest(JsonNode jsonRequestObject) {
        ArrayNode jsonLayerArray = (ArrayNode) jsonRequestObject.get("countyLevelOpts");
        Iterator<JsonNode> iter = jsonLayerArray.iterator();
        List<Integer> fieldIdList = new ArrayList<Integer>();
        Integer fieldId;
        while (iter.hasNext()) {
            try {
                fieldId = ((IntNode)iter.next()).intValue();
                fieldIdList.add(fieldId);
            }
            catch (NumberFormatException nfe) {
                //nfe.printStackTrace();
            }
        }
        return fieldIdList;
    }

    public List<Map<String, Object>> extractTableColumnsFromRequest(JsonNode jsonRequestObject) {
        ArrayNode array = (ArrayNode) jsonRequestObject.get("tableColumns");

        List<Map<String, Object>> retListMap = new ArrayList<Map<String, Object>>();
        Iterator<JsonNode> iter = array.iterator();
        while (iter.hasNext()) {
            Map<String, Object> map = new HashMap<String, Object>();
            JsonNode layerObj = iter.next();
//            TextNode fusionLayerId = (TextNode)layerObj.get("fusionMapLayerId");
            IntNode fusionLayerId = (IntNode)layerObj.get("fusionMapLayerId");
            logger.info("fusionLayerId=" + fusionLayerId.asText());
            if (debugMapReq) {
                logger.info("fusionLayerId=" + fusionLayerId.asText());
            }
//            map.put("fusionMapLayerId", fusionLayerId.textValue());
            map.put("fusionMapLayerId", fusionLayerId.asText());
            ArrayNode tableColumnArray = (ArrayNode) layerObj.get("selectedTableColumnIds");
            List<String> aColumnIdList = new ArrayList<String>();
            for (int i = 0; i < tableColumnArray.size(); i++) {
                logger.info("\t" + tableColumnArray.get(i));
                aColumnIdList.add(tableColumnArray.get(i).toString().replaceAll("\"", ""));
            }
            map.put("selectedTableColumnIds", aColumnIdList);
            retListMap.add(map);
        }

        return retListMap;

    }

    public List<Map<String, String>> extractShapeArray(JsonNode jsonRequestString) {
        ArrayNode array = (ArrayNode) jsonRequestString.get("shapeArray");
        List<Map<String, String>> retListMap = new ArrayList<Map<String, String>>();
        Iterator<JsonNode> iter = array.iterator();
        while (iter.hasNext()) {
            Map<String, String> map = new HashMap<String, String>();
            JsonNode shapeObj = iter.next();
            String shapeType = ((TextNode)shapeObj.get("type")).textValue();
            map.put("type", shapeType.toLowerCase().toLowerCase());
            if (shapeType.equalsIgnoreCase("polygon")){
                map.put("vertices", ((TextNode)shapeObj.get("vertices")).textValue() );
            }else {
                throw new RuntimeException ("extractShapeArray failed () unknown shape type '"+shapeType+"'");
            }

            retListMap.add(map);
        }

        return retListMap;

    }

    public ObjectNode fetchLayerQueryResult(List<Map<String, String>> inputListMap, List<Map<String, String>> gradientLayerListMap,
                                        List<Integer> stateLevelOptList, List<Integer> countyLevelOptList, Map<Integer, String> gradFieldColMap, int page, int pageSize,
                                        boolean invertQuery, List<Map<String, Object>> selectColumnListMap,List<Map<String, String>> shapeArrayMap, String lat, String lng,
                                        String search, String orderColumnName, String orderColumnDir, String infoWindowSelectedId) throws Exception {
        try {
            return fetchLayerQueryResult0(inputListMap, gradientLayerListMap, stateLevelOptList, countyLevelOptList
                    , gradFieldColMap, page, pageSize, invertQuery, selectColumnListMap,shapeArrayMap, lat, lng, search, orderColumnName, orderColumnDir, infoWindowSelectedId);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }

    }

    public Map<String, Object> fetchLayerQueryResultMap(List<Map<String, String>> inputListMap, List<Map<String, String>> gradientLayerListMap,
                                                        List<Integer> stateLevelOptList, List<Integer> countyLevelOptList, Map<Integer, String> gradFieldColMap,
                                                        int page, int pageSize, boolean invertQuery, List<Map<String, Object>> selectColumnListMap
            ,List<Map<String, String>> shapeArrayMap) throws SQLException, Exception {


        List<Map<String, String>> inputListMapWithCorrectOrder = getInputListMapWithCorrectOrderOfLayerWithin(inputListMap);

        List<String> orderedLayerIdList = ListMapUtility.extractColumnAsList(inputListMapWithCorrectOrder, "fusion_map_layer_id");
        Map<String, String> sqlInfoMap = generateSqlToEliminateOuterLayers(inputListMapWithCorrectOrder
                , gradientLayerListMap, invertQuery,shapeArrayMap, null, null, null, null, null);

        Map<String, String> layerIdTableNameMap = buildLayerIdTableNameMap(inputListMapWithCorrectOrder);

        this.sqlInfoMap = sqlInfoMap;
        this.layerIdTableNameMap = layerIdTableNameMap;
        this.gradientLayerListMap = gradientLayerListMap;
        this.stateLevelOptList = stateLevelOptList;
        this.countyLevelOptList = countyLevelOptList;
        this.gradFieldColMap1 = new GradientFieldNameLookup().buildLookupMap();
        this.tooltipLookupMapGradientLayer = new GradientFieldTooltipLookup().buildLookupMap();

        this.page = page;
        this.pageSize = pageSize;
        this.orderColumnName = null;
        this.orderColumnDir = null;
        this.selectColumnListMap = selectColumnListMap;
        //build columnname Id map:
        this.columnIdNameMap = buildColumnIdNameMap();
        //build map for columnTooltip lookup by id
        this.columnIdTooltipMap = buildColumnIdTooltipMap();

        this.resultListMap = buildResultData();

        Map<String, Object> retDataMap = new HashMap<String, Object>();
        retDataMap.put("listMap", this.resultListMap);
        retDataMap.put("columns", this.columnList);

        return retDataMap;
    }

    public ObjectNode fetchLayerQueryResult0(List<Map<String, String>> inputListMap, List<Map<String, String>> gradientLayerListMap,
                                         List<Integer> stateLevelOptList, List<Integer> countyLevelOptList, Map<Integer, String> gradFieldColMap,
                                         int page, int pageSize, boolean invertQuery, List<Map<String, Object>> selectColumnListMap,List<Map<String, String>> shapeArrayMap,
                                         String lat, String lng, String search, String orderColumnName, String orderColumnDir, String infoWindowSelectedId) throws SQLException, Exception {


        List<Map<String, String>> inputListMapWithCorrectOrder = getInputListMapWithCorrectOrderOfLayerWithin(inputListMap);

        if (inputListMapWithCorrectOrder==null||inputListMapWithCorrectOrder.size()<=0){
            throw new RuntimeException("unable to order input layers based on vw_info_layer_order_within. empty result returned for : "+inputListMap);
        }

        List<String> orderedLayerIdList = ListMapUtility.extractColumnAsList(inputListMapWithCorrectOrder, "fusion_map_layer_id");

        ArrayNode columnnArray = new InfoLayerFusionTableColumnHelper().layerIdsToColumnArrayNode(orderedLayerIdList);

        Map<String, String> sqlInfoMap = generateSqlToEliminateOuterLayers(inputListMapWithCorrectOrder
                , gradientLayerListMap, invertQuery, shapeArrayMap, lat, lng, null, search, infoWindowSelectedId);

        Map<String, String> layerIdTableNameMap = buildLayerIdTableNameMap(inputListMapWithCorrectOrder);
        if (debugMapReq) {
            logger.info("-----------------------\n");
            logger.info("sqlInfoMap:"+sqlInfoMap);
            logger.info("-----------------------\n");
        }
        if (sqlInfoMap==null){
            throw new RuntimeException("unable to generate sqlInfoMap. check layer query cache setup.");
        }

        //recordCount
        SqlRow sqlR = Ebean.createSqlQuery(sqlInfoMap.get("sql_count")).findOne();
        Long recordCount = ResultSetUtil.grabSingleNumberLong(sqlR);

        Long totalPage = 1 + (recordCount / pageSize);
        if (page > totalPage) {
            page = totalPage.intValue();
        }


        if (sqlInfoMap.get("sql_page_find") != null) {
            String pageFindQuery = sqlInfoMap.get("sql_page_find");
            SqlRow pageFindRow = Ebean.createSqlQuery(pageFindQuery).findOne();
            if (pageFindRow != null && !pageFindRow.isEmpty()) {
                Long pageNumL = ResultSetUtil.grabSingleNumberLong(pageFindRow);
                page = pageNumL != null ? pageNumL.intValue() : page;
            }
        }

        this.sqlInfoMap = sqlInfoMap;
        this.layerIdTableNameMap = layerIdTableNameMap;
        this.gradientLayerListMap = gradientLayerListMap;
        this.stateLevelOptList = stateLevelOptList;
        this.countyLevelOptList = countyLevelOptList;
        this.gradFieldColMap1 = new GradientFieldNameLookup().buildLookupMap();
        this.tooltipLookupMapGradientLayer = new GradientFieldTooltipLookup().buildLookupMap();

        this.page = page;
        this.pageSize = pageSize;
        this.orderColumnName = orderColumnName;
        this.orderColumnDir = orderColumnDir;
        this.selectColumnListMap = selectColumnListMap;
        //build columnname Id map:
        this.columnIdNameMap = buildColumnIdNameMap();
        //build map for columnTooltip lookup by id
        this.columnIdTooltipMap = buildColumnIdTooltipMap();

        this.resultListMap = buildResultData();

        if (this.resultListMap != null) {
            logger.info("fetchLayerQueryResult0: resultListMap.size:" + this.resultListMap.size());
        } else {
            logger.info("fetchLayerQueryResult0: resultListMap: NULL");
        }

        //build json string
        ObjectNode json = Json.newObject();
        //orderedLayerIds

        ArrayNode layerIdArrayNode = Json.newArray();
        for (String layerId : orderedLayerIdList) {
            layerIdArrayNode.add(layerId);
        }

        json.set("orderedLayerIds", getArrayFromList(orderedLayerIdList));
        //columns
        json.set("columns", getArrayFromList(this.columnList));
        json.set("tooltipInfo", JsonSmartUtility.stringMapToJson(this.tooltipMap) );
//        json.merge(JsonSmartUtility.mapToJsonString(this.tooltipMap));
        //rows
        json.set("rows", JsonSmartUtility.stringMapListToJson(this.resultListMap));
        //page
        json.put("page", page);
        json.put("totalPages", totalPage);
        json.put("recordCount", recordCount);

        json.set("tableColumnsSelected", JsonSmartUtility.mapListToJsonArray(this.selectColumnListMap));
        json.set("stateOptions", JsonSmartUtility.intListToArray(this.stateLevelOptList));
        json.set("countyOptions", JsonSmartUtility.intListToArray(this.countyLevelOptList));
        json.set("layerColumns", columnnArray);

        return json;
    }

    private ArrayNode getArrayFromList(List<String> listString) {
        ArrayNode listArrayNode = Json.newArray();
        if (listString != null && !listString.isEmpty()) {
            for (String item: listString) {
                listArrayNode.add(item);
            }
        }
        return listArrayNode;
    }

    List<Map<String, String>> getInputListMapWithCorrectOrderOfLayerWithin(List<Map<String, String>> inputListMap) throws SQLException {

        List<String> layerIdList = ListMapUtility.extractColumnAsList(inputListMap, "fusion_map_layer_id");

        StringBuilder layerIdCriteriaSb = new StringBuilder();
        layerIdCriteriaSb.append(" fusion_map_layer_id in (");
        for (int i = 0; i < layerIdList.size(); i++) {
            if (i != 0) {
                layerIdCriteriaSb.append(",");
            }
            layerIdCriteriaSb.append(layerIdList.get(i));
        }
        layerIdCriteriaSb.append(") ");
        String layerIdCriteriaString = layerIdCriteriaSb.toString();

        String sql
                = "select fusion_map_layer_id, db_table_name from vw_info_layer_order_within where" + layerIdCriteriaString + " "
                + "and compare_set=( "
                + "	select min(compare_set) as compare_set from "
                + "	( "
                + "		select compare_set,count(*) as numCount from "
                + "		(  "
                + "		 select  compare_set from vw_info_layer_order_within where" + layerIdCriteriaString + " "
                + "		)a "
                + "		group by compare_set "
                + "	)b "
                + "	where numcount = ( "
                + "		select max(numcount) from "
                + "		("
                + "			select compare_set,count(*) as numCount from "
                + "			( "
                + "			 select  compare_set from vw_info_layer_order_within where" + layerIdCriteriaString + " "
                + "			)a "
                + "			group by compare_set "
                + "		)c "
                + "	) "
                + ") "
                + "order by order_inner_outer asc";

        List<SqlRow> sqlRs = Ebean.createSqlQuery(sql).findList();
        List<Map<String, String>> listMap = ResultSetConverter.convertToListMap(sqlRs);

        addOtherInputFieldsToCorrectlyOrderedListMap(listMap, inputListMap);

        return listMap;

    }

    private List<Map<String, String>> executeSql(String sql) throws SQLException {
        List<SqlRow> sqlRs = Ebean.createSqlQuery(sql).findList();
        return ResultSetConverter.convertToListMap(sqlRs);
    }

    private String executeSqlAsJsonStringJqGridStyle(String sql, String sqlCount, int page, int pageSize) throws SQLException {
        //recordCount
        SqlRow sqlR = Ebean.createSqlQuery(sql).findOne();
        Long recordCount = ResultSetUtil.grabSingleNumberLong(sqlR);
        Long totalPage = 1 + (recordCount / pageSize);
        if (page > totalPage) {
            page = totalPage.intValue();
        }
        //sql
        if (page <= 0) {
            page = 1;
        }
        String sqlWithPagiation = sql + "\nlimit " + pageSize + "\noffset " + (page - 1) * pageSize;
//        System.out.println("sqlWithPagiation\n"+sqlWithPagiation);
        List<SqlRow> sqlRs = Ebean.createSqlQuery(sqlWithPagiation).findList();
        if (sqlRs != null && !sqlRs.isEmpty()) {
            String[] headers = ResultSetUtil.grabColumnNames(sqlRs.get(0));
            String json = ResultSetUtil.getAsJsonStringJqGridStyleWithHeaders(headers, sqlRs, (long) page, pageSize, recordCount);
            return json;
        }
        return null;
    }

    private String executeSqlAsJsonStringJqGridStyle(String sql) throws SQLException {
//        System.out.println("sql\n"+sql);
        List<SqlRow> sqlRs = Ebean.createSqlQuery(sql).findList();
        if (sqlRs != null && !sqlRs.isEmpty()) {
            String[] headers = ResultSetUtil.grabColumnNames(sqlRs.get(0));
            String json = ResultSetUtil.getAsJsonStringJqGridStyleWithHeaders(sqlRs, headers);
            return json;
        }
        return null;
    }

    private void addOtherInputFieldsToCorrectlyOrderedListMap(List<Map<String, String>> correctOrderListMap, List<Map<String, String>> inputListMap) {
        Map<String, Map<String, String>> lookupOldInputListMap = ListMapUtility.indexListMapByHeaderName(inputListMap, "fusion_map_layer_id");

        for (Map<String, String> map : correctOrderListMap) {
            String key = map.get("fusion_map_layer_id");
            Map<String, String> oldMap = lookupOldInputListMap.get(key);
            String criteria = oldMap.get("criteria");
            if (criteria != null) {
                String dbTableName = map.get("db_table_name");
                map.put("criteria", criteria);
                map.put("criteria_query_full", "select ogc_fid from " + dbTableName + " where " + criteria);
            }
        }

    }

    private List<String> getFusionMapLayerTableColumnList(String mostInnerLayerId) throws SQLException {
        return getFusionMapLayerTableColumnListAndPopulateTooltipMap(mostInnerLayerId,null);
    }

    private List<String> getFusionMapLayerTableColumnListAndPopulateTooltipMap(String mostInnerLayerId,Map<String,String> tooltipMap) throws SQLException {
//        System.out.println("getFusionMapLayerTableColumnListAndPopulateTooltipMap: mostInnerLayerId = " + mostInnerLayerId + ", tooltipMap = " + tooltipMap);
        String sql = "SELECT  \n"
                + "   column_name, description\n"
                + "FROM \n"
                + "  dlt.info_layer_fusion_table_column\n"
                + "where fusion_map_layer_id=" + mostInnerLayerId + "\n"
                + "and (disable_ind is null or disable_ind=0)\n"
                + "order by display_order,info_layer_fusion_table_column_id";


        List<Map<String, String>> listMap1 = executeSql(sql);
        //System.out.println("getFusionMapLayerTableColumnListAndPopulateTooltipMap: listMap1: " + listMap1);
        if (listMap1 == null) {
            return null;
        }
        List<String> columnNameList = ListMapUtility.extractColumnAsList(listMap1, "column_name");
        //System.out.println("getFusionMapLayerTableColumnListAndPopulateTooltipMap: columnNameList: " + columnNameList);
        if (tooltipMap!=null){
            List<String> columnDescriptionList = ListMapUtility.extractColumnAsList(listMap1, "description");
//            System.out.println("getFusionMapLayerTableColumnListAndPopulateTooltipMap: columnDescriptionList: " + columnDescriptionList);
            if (columnDescriptionList!=null){
                int i=0;
                for (String description : columnDescriptionList){
                    String columnName = columnNameList.get(i++);
                    tooltipMap.put(columnName, description);
                }
            }

        }
        return columnNameList;
    }

    private String createSqlWithDisplayColumns(String mostInnerTableName, List<String> columnListToDisplay, String sql, boolean invertQuery, String searchCondition) {
        StringBuilder columnListSb = new StringBuilder("ogc_fid as _ogc_fid1_, st_y(ST_Centroid(wkb_geometry)) as _lat_,  st_x(ST_Centroid(wkb_geometry)) as _lng_");
        columnListSb.append(", fips_code_state as _fips_code_state_, fips_code_county as _fips_code_county_");
        for (String column : columnListToDisplay) {
            columnListSb.append(", ");
            columnListSb.append(column);
        }

        StringBuilder sqlRet = new StringBuilder();
        sqlRet.append("select ").append(columnListSb.toString()).append("\n")
                .append("from ").append(mostInnerTableName).append("\n")
                .append("where ogc_fid ");

        if (invertQuery) {
            sqlRet.append("not ");
        }

        sqlRet.append("in (\n");
        sqlRet.append("  " + sql + "\n");
        sqlRet.append(")");

        if(searchCondition.length() > 0) {
            sqlRet.append("and (" + searchCondition + ")\n");
        }

        return sqlRet.toString();
    }

    private String createSqlForPageFind(String mostInnerTableName, String sql, boolean invertQuery, String lat, String lng, String searchCondition) {
        if (lat == null || lng == null || lat.isEmpty() || lng.isEmpty()) {
            return null;
        }
        StringBuilder sqlRet = new StringBuilder();
        sqlRet.append("select ceiling(rowNumber::decimal / 10) \n");
        sqlRet.append("from (select \"_lat_\" as lat, \"_lng_\" as lng, row_number() over (order by ogc_fid) as rowNumber from ").append(mostInnerTableName).append("\n");
        sqlRet.append("where ogc_fid ");
        if (invertQuery) {
            sqlRet.append("not ");
        }
        sqlRet.append("in (\n");
        sqlRet.append("  ").append(sql).append("\n");
        sqlRet.append(")");
        if(searchCondition.length() > 0) {
            sqlRet.append("and (" + searchCondition + ")\n");
        }
        sqlRet.append(") rowNum ");
        sqlRet.append("where lat = ").append(lat).append(" and lng = ").append(lng);
        sqlRet.append(" limit 1");

        return sqlRet.toString();
    }

    private String createSqlForPageFindById(String mostInnerTableName, String sql, boolean invertQuery, String infoWindowSelectedId, String searchCondition) {
        if (infoWindowSelectedId == null || infoWindowSelectedId.isEmpty()) {
            return null;
        }
        StringBuilder sqlRet = new StringBuilder();
        sqlRet.append("select ceiling(rowNumber::decimal / 10) \n");
        sqlRet.append("from (select ogc_fid, row_number() over (order by ogc_fid) as rowNumber from ").append(mostInnerTableName).append("\n");
        sqlRet.append("where ogc_fid ");
        if (invertQuery) {
            sqlRet.append("not ");
        }
        sqlRet.append("in (\n");
        sqlRet.append("  ").append(sql).append("\n");
        sqlRet.append(")");
        if(searchCondition.length() > 0) {
            sqlRet.append("and (\n");
            sqlRet.append(searchCondition);
            sqlRet.append(")\n");
        }
        sqlRet.append(") rowNum ");
        sqlRet.append("where ogc_fid = ").append(infoWindowSelectedId);
        sqlRet.append(" limit 1");

        return sqlRet.toString();
    }

    private String createSqlForRecordCount(String mostInnerTableName, String sql, boolean invertQuery, String searchCondition) {

        StringBuilder sqlRet = new StringBuilder();
        sqlRet.append("select count(*) \n");
        sqlRet.append("from ").append(mostInnerTableName).append("\n");
        sqlRet.append("where ogc_fid ");
        if (invertQuery) {
            sqlRet.append("not ");
        }
        sqlRet.append("in (\n");
        sqlRet.append("  ").append(sql).append("\n");
        sqlRet.append(")");
        if(searchCondition.length() > 0) {
            sqlRet.append("and (" + searchCondition + ")\n");
        }

        return sqlRet.toString();
    }

    private List<Map<String, String>> executeFirstLayerSqlAsListMap(Map<String, String> sqlInfoMap, int page, int pageSize, String orderColumnName, String orderColumnDir) throws SQLException {
        String sql = sqlInfoMap.get("sql_full");
        List<String> columnDisplayList = Arrays.asList(sqlInfoMap.get("display_column_list").split(", "));

        if (page <= 0) {
            page = 1;
        }
        StringBuilder orderBySb = new StringBuilder();
        orderBySb.append("\norder by ");
        if (orderColumnName != null && orderColumnName.length() > 0 && columnDisplayList.stream().anyMatch(str -> str.trim().equalsIgnoreCase(orderColumnName))) {
            orderBySb.append(orderColumnName);
            if (orderColumnDir != null && orderColumnDir.length() > 0) {
                orderBySb.append(" ").append(orderColumnDir);
            }
            orderBySb.append("\n");
        } else {
            orderBySb.append("ogc_fid\n");
        }
        String orderBy = orderBySb.toString();
        String sqlWithPagiation = sql + orderBy + "limit " + pageSize + "\noffset " + (page - 1) * pageSize;

        if (debugMapReq) {
            logger.info("--------------- first layer sql------------\n");
            logger.info("sqlWithPagiation:"+sqlWithPagiation);
            logger.info("------------------\n");
        }
        List<SqlRow> sqlRs = Ebean.createSqlQuery(sqlWithPagiation).findList();
        return ResultSetConverter.convertToListMapWithAddedFields(sqlRs, (page - 1) * pageSize + 1);
    }

    private Map<String, String> buildColumnIdTooltipMap() throws SQLException {
        //build a map of ID as KEY and Column name as value
        String sql = "SELECT info_layer_fusion_table_column_id, description FROM info_layer_fusion_table_column";
        List<Map<String, String>> result = executeSql(sql);
//        Map<String, Map<String, String>> columnIdMap = ListMapUtility.indexListMapByHeaderName(result, "info_layer_fusion_table_column_id");
        Map<String, String> columnIdTooltipMap = new LinkedHashMap<String, String>();
        for (Map<String, String> record : result) {
            columnIdTooltipMap.put(record.get("info_layer_fusion_table_column_id"), record.get("description"));
        }

        return columnIdTooltipMap;
    }

    private List<String> buildColumnTooltipListFromColumnIdList(List<String> columIdList) {
        List<String> columnTooltipList = new ArrayList<String>();
        if (columIdList != null) {
            for (String columnId : columIdList) {
                String columnTooltip = this.columnIdTooltipMap.get(columnId);
//                if (columnTooltip != null && !columnTooltip.trim().equals("")) {
                columnTooltipList.add(columnTooltip);
//                }
            }
        }
        return columnTooltipList;
    }

    private Map<String, String> assembleTooltipMapFromColumnNamesAndTooltips(List<String> columnNameList, List<String> columnTooltipList) {
        if (columnNameList==null){
            return null;
        }
        if (debugMapReq) {
            logger.info("columnTooltipList: "+columnTooltipList);
        }
        Map<String, String> ret = new HashMap<String, String>();
        int i=0;
        for (String columnName : columnNameList){
            String tooltip = columnTooltipList.get(i++);
            ret.put(columnName, tooltip);
        }

        return ret;
    }


    private Map<String, String> buildColumnIdNameMap() throws SQLException {
        //build a map of ID as KEY and Column name as value
        String sql = "SELECT info_layer_fusion_table_column_id, column_name FROM info_layer_fusion_table_column";
        List<Map<String, String>> result = executeSql(sql);
//        Map<String, Map<String, String>> columnIdMap = ListMapUtility.indexListMapByHeaderName(result, "info_layer_fusion_table_column_id");
        Map<String, String> columnIdNameMap = new HashMap<String, String>();
        for (Map<String, String> record : result) {
            columnIdNameMap.put(record.get("info_layer_fusion_table_column_id"), record.get("column_name"));
        }

        return columnIdNameMap;
    }

    private List<String> buildColumnNameListFromColumnIdList(List<String> columnIdList) {
        List<String> columnNameList = new ArrayList<String>();
        //System.out.println("buildColumnNameListFromColumnIdList: columnIdList: " + columnIdList);
        //System.out.println("buildColumnNameListFromColumnIdList: columnIdNameMap: " + this.columnIdNameMap);
        if (columnIdList != null) {
            for (String columnId : columnIdList) {
                Integer colId = Integer.valueOf(columnId);
                String columnName = this.columnIdNameMap.get(columnId);
                //System.out.println("columnId: " + columnId + ", columnName: " + columnName + ", colId: " + colId);
                if (columnName != null && !columnName.trim().equals("")) {
                    columnNameList.add(columnName);
                }
            }
        }
        return columnNameList;
    }

    private String buildSqlForNonFirstLayer(String thisFusionLayerId, Map<String, String> sqlInfoMap,
                                            List<String> columnNameList, List<String> compareColumnNameList, Map<String, String> renamedColumnMap) {

        //build columnNameString
        StringBuilder columnListSb = new StringBuilder("t2.ogc_fid1 as _ogc_fid1");
        for (String column : columnNameList) {
            columnListSb.append(", ");
            for (String columnName : compareColumnNameList) {
                if (columnName.equalsIgnoreCase(column)) {
                    renamedColumnMap.put(column, column + "1");
                    column = column + " as " + column + "1";
                }
            }
            columnListSb.append(column);
        }

        //sql
        String coreInnerSql = sqlInfoMap.get("sql");
        String firstLayerId = sqlInfoMap.get("fusion_map_layer_id");

        coreInnerSql = coreInnerSql + "\norder by ogc_fid1\nlimit " + pageSize + "\noffset " + (page - 1) * pageSize;

        StringBuilder sbSqlT2 = new StringBuilder();
        sbSqlT2.append("\tselect info.ogc_fid1, info.ogc_fid2 from info_layer_geom_within info\n");
        sbSqlT2.append("\tinner join (\n");
        sbSqlT2.append("\t\t").append(coreInnerSql).append("\n");
        sbSqlT2.append("\t)t1\n");
        sbSqlT2.append("\ton info.ogc_fid1=t1.ogc_fid1\n");
        sbSqlT2.append("\twhere fusion_map_layer_id1=").append(firstLayerId).append("\n");
        sbSqlT2.append("\tand fusion_map_layer_id2=").append(thisFusionLayerId).append("\n");
        //sql main
        StringBuilder sb = new StringBuilder();

        String layerName = lookupTableNameByLayerId(thisFusionLayerId);

        sb.append("select distinct ").append(columnListSb).append(" from ").append(layerName).append(" layer \n");
        sb.append("inner join (");
        sb.append(sbSqlT2);
        sb.append(") t2 \n");
        sb.append("on t2.ogc_fid2=layer.ogc_fid\n");

        if (debugMapReq) {
            logger.info("**************Non-first Layer SQL****************\n");
            logger.info(sb.toString());
            logger.info("*************************************************\n");
        }
        return sb.toString();
    }

    private Map<String, String> buildLayerIdTableNameMap(List<Map<String, String>> inputListMapWithCorrectOrder) {
        Map<String, String> retMap = new HashMap<String, String>();
        for (Map<String, String> map : inputListMapWithCorrectOrder) {
            String layerTableName = map.get("db_table_name");
            String layerId = map.get("fusion_map_layer_id");
            retMap.put(layerId, layerTableName);
        }
        return retMap;
    }

    private String lookupTableNameByLayerId(String layerId) {
        return this.layerIdTableNameMap.get(layerId);
    }

    private String generateSqlConditionFromGradientFiters(List<Map<String, String>> gradientLayerListMap, String mostInnerLayerId) throws SQLException {
        String ogcfidString = "ogc_fid1";
        return generateSqlConditionFromGradientFiters(gradientLayerListMap, mostInnerLayerId, ogcfidString);
    }

    private String generateSqlConditionFromGradientFiters(List<Map<String, String>> gradientLayerListMap, String mostInnerLayerId, String ogcfidString) throws SQLException {

        getAdditionalInfoOnGradientLayerListMap(gradientLayerListMap);

        StringBuilder sb = new StringBuilder();

        for (Map<String, String> map : gradientLayerListMap) {
            sb.append(getSqlConditionForOneGradientLayer(map, mostInnerLayerId, ogcfidString));
        }

        return sb.toString();

    }

    private void getAdditionalInfoOnGradientLayerListMap(List<Map<String, String>> gradientLayerListMap) throws SQLException {

        if (gradientLayerListMap == null || gradientLayerListMap.size() <= 0) {
            return;
        }

        StringBuilder sbLayerIds = new StringBuilder();
        boolean isFirst = true;
        for (Map<String, String> aLayerMap : gradientLayerListMap) {
            String layerId = aLayerMap.get("gradientMapLayerId");
            if (layerId == null || layerId.trim().equals("")) {
                continue;
            }
            if (isFirst) {
                sbLayerIds.append("(").append(layerId);
                isFirst = false;
            } else {
                sbLayerIds.append(",").append(layerId);
            }
        }
        if (sbLayerIds.length() > 0) {
            sbLayerIds.append(")");
        }

        String sql = "select gradient_map_layer_id, ds.data_set_id, db_table_name,ds.data_level "
                + "from gradient_map_layer gml\n"
                + "inner join data_set ds on gml.data_set_id = ds.data_set_id\n"
                + "where gradient_map_layer_id in " + sbLayerIds + "\n"
                + "order by gradient_map_layer_id";

//        System.out.println("getAdditionalInfoOnGradientLayerListMap: sql=\n------\n");
//        System.out.println(sql);
//        System.out.println("-----");
        List<SqlRow> sqlRs = Ebean.createSqlQuery(sql).findList();
        List<Map<String, String>> listMap = ResultSetConverter.convertToListMap(sqlRs);
        Map<String, Map<String, String>> gradientInfoLookup = ListMapUtility.indexListMapByHeaderName(listMap, "gradient_map_layer_id");

        for (Map<String, String> theLayerMap : gradientLayerListMap) {
            String layerId = theLayerMap.get("gradientMapLayerId");
            Map<String, String> lookupMap = gradientInfoLookup.get(layerId);
            theLayerMap.put("db_table_name", lookupMap.get("db_table_name"));
            theLayerMap.put("data_level", lookupMap.get("data_level"));
        }

//        System.out.println("*****gradientInfoLookup="+gradientInfoLookup);
//                 System.out.println("********returning mocked gradientLayerListMap() ******");
//        Map<String, String> map = gradientLayerListMap.get(0);
//        map.put("db_table_name","gradient_crime_emission_social_county");
//        map.put("filter_string","arson>40");
//        map.put("data_level", "county");
    }

    private String getSqlConditionForOneGradientLayer(Map<String, String> map, String mostInnerLayerId) {
        return getSqlConditionForOneGradientLayer(map, mostInnerLayerId, "ogc_fid1");
    }

    private String getSqlConditionForOneGradientLayer(Map<String, String> map, String mostInnerLayerId, String ogcfidString) {

        String dataLevel = map.get("data_level");
        boolean isStateLevel = dataLevel.equalsIgnoreCase("state");
        boolean isCountyLevel = dataLevel.equalsIgnoreCase("county");

        if (!isCountyLevel && !isStateLevel) {
            logger.warn("WARNING:: data level for input gradient layer is unknown, default to county level!!! ");
            isCountyLevel = true;
        }

        String fusionLayerIdForGradientBoundary = "13";//default is layer_county,13
        if (isStateLevel) {
            fusionLayerIdForGradientBoundary = "5";
        }

        boolean simplifySqlForInnerLayerIdSameAsBoundaryLayer = fusionLayerIdForGradientBoundary.equals(mostInnerLayerId.trim());

        StringBuilder sb = new StringBuilder();

        if (ogcfidString == null || ogcfidString.trim().equals("")) {
            ogcfidString = "ogc_fid1";
        }
        sb.append("  and ");
        sb.append(ogcfidString).append(" in (\n");
        //
        if (!simplifySqlForInnerLayerIdSameAsBoundaryLayer) {
            sb.append("        select  ogc_fid1 from info_layer_geom_within\n");
            sb.append("	where fusion_map_layer_id1 = ").append(mostInnerLayerId).append("\n");
            sb.append("	and  fusion_map_layer_id2=").append(fusionLayerIdForGradientBoundary).append("\n");
            sb.append("	and ogc_fid2 in (\n");
        }
        //
        if (isCountyLevel) {
            sb.append("		select ogc_fid from layer_county where stateid||countyid in (\n");
            sb.append("		  select fips_county_code from ");
        } else {
            sb.append("		select ogc_fid from layer_states where state_fips_code in (\n");
            sb.append("		  select state_fips_code from ");
        }
        sb.append(map.get("db_table_name"));
        String criteria = map.get("criteria");
        if (criteria != null && !criteria.trim().equals("")) {
            sb.append(" where ").append(map.get("criteria")).append("\n");
        }
        //
        if (!simplifySqlForInnerLayerIdSameAsBoundaryLayer) {
            sb.append("		)\n");
        }
        //
        sb.append("	)\n");
        sb.append("   )\n");

        return sb.toString();
    }

    private List<Map<String, String>> getResultSetForOneStateCountyOption(String stCntyOption, String columnName)
            throws SQLException {
        List<Map<String, String>> result = null;
        StringBuilder sb = new StringBuilder();
        sb.append("select db_table_name\n");
        sb.append("from gradient_map_layer\n");
        sb.append("where field_column_name ilike \'" + stCntyOption + "\'\n");
        result = executeSql(sb.toString());
        String tableName = result.get(0).get("db_table_name").toLowerCase();
        sb.setLength(0);
        sb.append("select ogc_fid as _ogc_fid3, " + stCntyOption.toLowerCase() + " as " + columnName + "\n");
        sb.append("from " + tableName + "\n");
        result = executeSql(sb.toString());
        return result;
    }

//    private void addColumnToDisplayList(String fieldColName, List<String> columnNameList) {
//
//        fieldColName = fieldColName.toLowerCase();
//
//        Set columnNameSet = new HashSet();
//        for (String col : columnNameList) {
//            columnNameSet.add(col.toLowerCase());
//        }
//        boolean headerNameAlreadyInList = columnNameSet.contains(fieldColName);
//        while (headerNameAlreadyInList) {
//            fieldColName = fieldColName + "1";
//            headerNameAlreadyInList = columnNameSet.contains(fieldColName);
//        }
//
//        columnNameList.add(fieldColName);
//
//    }

    private void populateOneColumnDisplayNameAndTooltipText(String fieldColName, String tooltipText, List<String> columnNameList, Map<String, String> tooltipMap) {
        //1. column Name
        fieldColName = fieldColName.toLowerCase();
        Set columnNameSet = new HashSet();
        for (String col : columnNameList) {
            columnNameSet.add(col.toLowerCase());
        }
        boolean headerNameAlreadyInList = columnNameSet.contains(fieldColName);
        while (headerNameAlreadyInList) {
            fieldColName = fieldColName + "1";
            headerNameAlreadyInList = columnNameSet.contains(fieldColName);
        }
        columnNameList.add(fieldColName);

        //2. Tooltip - store tooltip text in the this.tooltipMap
        tooltipMap.put(fieldColName, tooltipText);


    }

    private void addOneCountyLevelDataFieldByFipsCode(List<Map<String, String>> resultListMapFirstLayer, Map<String, Map<String, String>> fipsDataLookupMap, String gradientLayerId, String fieldColName) {

        if (resultListMapFirstLayer == null) {
            return;
        }
        if (fipsDataLookupMap == null || fipsDataLookupMap.size() <= 0) {
            throw new RuntimeException("addOneCountyLevelDataFieldByFipsCode failed! fipsDataLookupMap is empty!");
        }

        Map<String, String> fipsDataLookupMapForTheField = fipsDataLookupMap.get(gradientLayerId);
        if (fipsDataLookupMapForTheField == null) {
            throw new RuntimeException("addOneCountyLevelDataFieldByFipsCode failed! gradientLayerId not found in the fipsDataLookupMap! gradientLayerId=" + gradientLayerId);
        }
        for (Map<String, String> dataMap : resultListMapFirstLayer) {
            String fipsCodeCounty = dataMap.get("_fips_code_county_");
            if (fipsCodeCounty == null) {
                continue;
            }
            String value = fipsDataLookupMapForTheField.get(fipsCodeCounty);
            if (value != null) {
                dataMap.put(fieldColName, value);
            }
        }
    }

    private void addOneStateLevelDataFieldByFipsCode(List<Map<String, String>> resultListMapFirstLayer, Map<String, Map<String, String>> fipsDataLookupMap, String gradientLayerId, String fieldColName) {

        if (resultListMapFirstLayer == null) {
            return;
        }
        if (fipsDataLookupMap == null || fipsDataLookupMap.size() <= 0) {
            throw new RuntimeException("addOneStateLevelDataFieldByFipsCode failed! fipsDataLookupMap is empty!");
        }

        Map<String, String> fipsDataLookupMapForTheField = fipsDataLookupMap.get(gradientLayerId);
        if (fipsDataLookupMapForTheField == null) {
            logger.info("fipsDataLookupMap keys="+fipsDataLookupMap.keySet());
            throw new RuntimeException("addOneStateLevelDataFieldByFipsCode failed! gradientLayerId not found in the fipsDataLookupMap! gradientLayerId='" + gradientLayerId+"'");
        }
        for (Map<String, String> dataMap : resultListMapFirstLayer) {
            String fipsCodeState = dataMap.get("_fips_code_state_");
            if (fipsCodeState == null) {
                continue;
            }
            String value = fipsDataLookupMapForTheField.get(fipsCodeState);
            if (value != null) {
                dataMap.put(fieldColName, value);
            }
        }
    }

    public Map<String, String> generateSqlToEliminateOuterLayers(List<Map<String, String>> inputListMapWithCorrectOrder,
                                                                 List<Map<String, String>> gradientLayerListMap, boolean invertQuery,
                                                                 List<Map<String, String>> shapeArrayMap, String lat, String lng, Integer layerId, String search, String infoWindowSelectedId) throws SQLException {
        if (inputListMapWithCorrectOrder == null) {
            logger.info("generateSqlToEliminateOuterLayers skipped 1");
            return null;
        }

        if (inputListMapWithCorrectOrder.size() == 1) {
            logger.info("generateSqlToEliminateOuterLayers return just for 1");
            return generateSqlInfoForJustOneLayerInput(inputListMapWithCorrectOrder.get(0), gradientLayerListMap, invertQuery,shapeArrayMap, lat, lng, search, infoWindowSelectedId);
        }

        List<String> sortedLayerIdList = ListMapUtility.extractColumnAsList(inputListMapWithCorrectOrder, "fusion_map_layer_id");


        logger.info("****** sortedLayerIdList: "+sortedLayerIdList);

        int nLayerId = sortedLayerIdList.size();
        if (nLayerId == 0) {
            return null;
        }

        Map retMap = new HashMap<String, String>();

        int startIndex = 0;
        if (layerId != null) {
            for (int i = 0; i < sortedLayerIdList.size() - 1; i++) {
                if (sortedLayerIdList.get(i).equalsIgnoreCase(layerId.toString())) {
                    startIndex = i;
                    break;
                }
//            else if (i == sortedLayerIdList.size() - 1 &&
//                    !sortedLayerIdList.get(i).equalsIgnoreCase(layerId.toString())) {
//                return null;
//            }
            }
        }

        //first two layer
        String layerIdInner = sortedLayerIdList.get(startIndex);
        String layerIdOuter = sortedLayerIdList.get(startIndex + 1);
        String criteriaQueryFullInner = inputListMapWithCorrectOrder.get(startIndex).get("criteria_query_full");
        String criteriaQueryFullOuter = inputListMapWithCorrectOrder.get(startIndex + 1).get("criteria_query_full");
        boolean hasCriteriaInner = (criteriaQueryFullInner != null && !criteriaQueryFullInner.trim().equals(""));
        boolean hasCriteriaOuter = (criteriaQueryFullOuter != null && !criteriaQueryFullOuter.trim().equals(""));

        String sql
                = "select distinct ogc_fid1 from info_layer_geom_within\n"
                + "	where fusion_map_layer_id1 = " + layerIdInner + "\n"
                + "	  and  fusion_map_layer_id2=" + layerIdOuter + "\n";
        if (hasCriteriaInner) {
            sql = sql + "	  and ogc_fid1 in (\n"
                    + criteriaQueryFullInner + "\n"
                    + "	  )";
        }
        if (hasCriteriaOuter) {
            sql = sql + "	  and ogc_fid2 in ( " + criteriaQueryFullOuter + " )";
        }

        for (int i = startIndex + 2; i < sortedLayerIdList.size(); i++) {
            layerIdOuter = sortedLayerIdList.get(i);
            sql = "select  ogc_fid1 from info_layer_geom_within\n"
                    + "	where fusion_map_layer_id1 = " + layerIdInner + "\n"
                    + "	  and  fusion_map_layer_id2=" + layerIdOuter + "\n"
                    + "   and ogc_fid1 in (\n"
                    + sql
                    + ")\n";
            criteriaQueryFullOuter = inputListMapWithCorrectOrder.get(i).get("criteria_query_full");
            hasCriteriaOuter = (criteriaQueryFullOuter != null && !criteriaQueryFullOuter.trim().equals(""));
            if (hasCriteriaOuter) {
                sql = sql + "and ogc_fid2 in ( " + criteriaQueryFullOuter + " )";
            }
        }


        //get display column list
        String mostInnerLayerId = sortedLayerIdList.get(startIndex);
        List<String> columnListToDisplay = getFusionMapLayerTableColumnList(mostInnerLayerId);

        //gradient layer
        if (gradientLayerListMap != null && gradientLayerListMap.size() > 0) {
            String gradientSql = generateSqlConditionFromGradientFiters(gradientLayerListMap, mostInnerLayerId);
            sql = sql + "\n" + gradientSql;
        }

        //add SQL snippet for drawn shape array
        if (shapeArrayMap != null && shapeArrayMap.size() > 0) {
            String shapeArraySql = generateSqlConditionFromShapeListMap(shapeArrayMap);
            sql = sql +"\n"+ shapeArraySql;
//            System.out.println("**** gradientSql ...\n"+gradientSql);//debug
        }

        //get search condition
        String searchCondition = getSearchCriteria(columnListToDisplay, search);

        //full sql
        String mostInnerTableName = inputListMapWithCorrectOrder.get(startIndex).get("db_table_name");
        String sql_full = createSqlWithDisplayColumns(mostInnerTableName, columnListToDisplay, sql, invertQuery, searchCondition);

        //sql_count
        String sql_count = createSqlForRecordCount(mostInnerTableName, sql, invertQuery, searchCondition);
        String sql_page_find;
        if (infoWindowSelectedId != null) {
            sql_page_find = createSqlForPageFindById(mostInnerTableName, sql, invertQuery, infoWindowSelectedId, searchCondition);
        } else {
            sql_page_find = createSqlForPageFind(mostInnerTableName, sql, invertQuery, lat, lng, searchCondition);
        }

        retMap.put("display_column_list", String.join(", ", columnListToDisplay));
        retMap.put("fusion_map_layer_id", mostInnerLayerId);
        retMap.put("sql", sql);
        retMap.put("sql_full", sql_full);
        retMap.put("sql_count", sql_count);
        retMap.put("ordered_layerids", inputListMapWithCorrectOrder);
        retMap.put("sql_page_find", sql_page_find);

        if (debugMapReq) {
            logger.info("-----------------------\n");
            logger.info("retMap:" + retMap);
            logger.info("-----------------------\n");
        }

//        System.out.println("**************** sqlMap" + retMap);
        return retMap;

    }

    private Map<String, String> generateSqlInfoForJustOneLayerInput(Map<String, String> layerInfoMap, List<Map<String, String>> gradientLayerListMap,
                                                                    boolean invertQuery,List<Map<String, String>> shapeListMap, String lat, String lng, String search, String infoWindowSelectedId) throws SQLException {
        Map<String, String> retMap = new HashMap<String, String>();
        //first two layer
        String layerId = layerInfoMap.get("fusion_map_layer_id");

        String criteria = layerInfoMap.get("criteria");

        boolean hasCriteria = (criteria != null && !criteria.trim().equals(""));

        String sql
                = "select  ogc_fid from " + layerInfoMap.get("db_table_name") + " \n";
        if (hasCriteria) {
            sql = sql + " where " + criteria + "\n";
        }else {
            sql = sql + " where 1=1\n";
        }
//        sql = sql + " order by ogc_fid ";//tocheck
        //add SQL snippet for Gradient Layer
        if (gradientLayerListMap != null && gradientLayerListMap.size() > 0) {
            String gradientSql = generateSqlConditionFromGradientFiters(gradientLayerListMap, layerId, "ogc_fid");
            sql = sql + gradientSql;
//            System.out.println("**** gradientSql ...\n"+gradientSql);//debug
        }


        //add SQL snippet for drawn shape array
        if (shapeListMap != null && shapeListMap.size() > 0) {
            String shapeArraySql = generateSqlConditionFromShapeListMap(shapeListMap);
            sql = sql + shapeArraySql;
//            System.out.println("**** gradientSql ...\n"+gradientSql);//debug
        }

        //get display column list
        List<String> columnListToDisplay = getFusionMapLayerTableColumnList(layerId);

        //get search condition
        String searchCondition = getSearchCriteria(columnListToDisplay, search);

        //full sql
        String tableName = layerInfoMap.get("db_table_name");
        String sql_full = createSqlWithDisplayColumns(tableName, columnListToDisplay, sql, invertQuery, searchCondition);

        //sqll_count
        String sql_count = createSqlForRecordCount(tableName, sql, invertQuery, searchCondition);
        String sql_page_find;
        if (infoWindowSelectedId != null) {
            sql_page_find = createSqlForPageFindById(tableName, sql, invertQuery, infoWindowSelectedId, searchCondition);
        } else {
            sql_page_find = createSqlForPageFind(tableName, sql, invertQuery, lat, lng, searchCondition);
        }

        retMap.put("display_column_list", String.join(", ", columnListToDisplay));
        retMap.put("fusion_map_layer_id", layerId);
        retMap.put("sql", sql);
        retMap.put("sql_full", sql_full);
        retMap.put("sql_count", sql_count);
        retMap.put("sql_page_find", sql_page_find);
//        System.out.println("Just for one fusionLayer: retMap:\n" + retMap);
        return retMap;
    }

    private String getSearchCriteria(List<String> columnListToDisplay, String searchValue){
        StringBuilder searchCriteria = new StringBuilder();

        if (searchValue != null && searchValue.length() > 0) {
            for (String column : columnListToDisplay) {
                if (searchCriteria.length() > 0) searchCriteria.append(" or ");
                searchCriteria.append(" (UPPER(")
                        .append(column)
                        .append(")")
                        .append(" LIKE '%")
                        .append(searchValue.toUpperCase().replace("'", "''"))
                        .append("%')");
            }
        }

        return searchCriteria.toString();
    }

    private String generateSqlConditionFromShapeListMap(List<Map<String, String>> shapeListMap) {
        String ret = "  and st_contains(\n" +
                "			(\n" +
                "\n" +
                "			SELECT ST_GeomFromText('MULTIPOLYGON\n" +
                "				(\n" +
                "\n" +
                "					((-99.5361328125 44.276671273775186,-95.712890625 43.61221676817573,-95.7568359375 42.00032514831621,-97.294921875 41.31082388091818\n" +
                "					,-99.66796875 41.475660200278206,-100.7666015625 42.32606244456202\n" +
                "					,-100.37109375 43.77109381775648,-99.5361328125 44.276671273775186))\n" +
                "					,\n" +
                "					((-109.3798828125 44.68427737181224,-107.6220703125 43.100982876188546,-105.46875 44.08758502824516\n" +
                "					,-105.6884765625 44.96479793033101,-106.9189453125 45.42929873257377\n" +
                "					,-108.10546875 45.460130637921004,-109.3798828125 44.68427737181224))\n" +
                "\n" +
                "\n" +
                "				)'\n" +
                "			,4326) \n" +
                "			) \n" +
                ",\n" +
                "		wkb_geometry\n" +
                ")";
        boolean firstMap=true;
        StringBuilder wktSb = new StringBuilder();
        wktSb.append(" and st_contains(\n");
        wktSb.append("          (\n");
        wktSb.append("select ST_GeomFromText('MULTIPOLYGON\n(");
        for (Map<String,String> shapeMap : shapeListMap){
            logger.info("****shape Map ="+shapeMap);
            if (debugMapReq) {
                logger.info("****shape Map ="+shapeMap);
            }
            String type = shapeMap.get("type");
            if (!type.trim().equalsIgnoreCase("polygon")){
                logger.info("WARNING: unknown shape type : '"+type+"' detected in generateSqlConditionFromShapeListMap.");
            }
            String vertices = shapeMap.get("vertices");
            if (vertices==null||vertices.trim().equals("")){
                continue;
            }
            if (!firstMap){
                wktSb.append(", ");
            }
            wktSb.append("(");
            wktSb.append(vertices);
            wktSb.append(")\n");

            firstMap=false;
        }
        wktSb.append(")', 4326");
        wktSb.append("   )), wkb_geometry\n");
        wktSb.append(")");
        return wktSb.toString();

    }

    public List<Map<String, String>> createDefaultInputListMapForGradientLayers(List<Map<String, String>> gradientLayerListMap) throws SQLException {
        List<Map<String, String>> inputLayerListMap = new ArrayList<Map<String, String>>();
        if (gradientLayerListMap==null||gradientLayerListMap.size()<=0 ){
            return inputLayerListMap;
        }

        boolean hasCountyLevelGradientLayer = checkIfAnyGradientLayerContainsCountyLevel(gradientLayerListMap);

        //add default fusion_map_layer
        Map<String,String> map = new HashMap<String,String>();
        if (hasCountyLevelGradientLayer) {
            map.put("fusion_map_layer_id", "13");//county
        } else {
            map.put("fusion_map_layer_id", "5");//state
        }

        inputLayerListMap.add (map);

        return inputLayerListMap;
    }

    private boolean checkIfAnyGradientLayerContainsCountyLevel(List<Map<String, String>> gradientLayerListMap) throws SQLException {

        String [] idColumnArray = ListMapUtility.extractColumn(gradientLayerListMap, "gradientMapLayerId");

        if (idColumnArray==null) return false;

        StringBuilder sbSqlIdCondition = new StringBuilder();
        sbSqlIdCondition.append(" and gml.gradient_map_layer_id in (99999");
        for (String id : idColumnArray){
            sbSqlIdCondition.append(",").append(id);
        }
        sbSqlIdCondition.append(")");

        String sql = "select count(*) from gradient_map_layer gml\n"
                + "inner join data_set ds\n"
                + "on gml.data_set_id = ds.data_set_id\n"
                + "where data_level ilike '%county%'\n"
                + sbSqlIdCondition.toString() ;

        SqlRow sqlR = Ebean.createSqlQuery(sql).findOne();
        Long count = ResultSetUtil.grabSingleNumberLong(sqlR);

        return (count!=null && count>0);

    }

    /**
     * @return the tooltipMap
     */
    public Map<String,String> getTooltipMap() {
        return tooltipMap;
    }

    private List<Map<String, String>> buildResultData() throws SQLException, Exception {

        //main, first layer Map
        List<Map<String, String>> resultListMap = executeFirstLayerSqlAsListMap(sqlInfoMap, page, pageSize, orderColumnName, orderColumnDir);

        this.tooltipMap = new LinkedHashMap<String,String>();

        logger.info("buildResultData: sqlInfoMap=" + sqlInfoMap + ", tooltipMap=" + this.tooltipMap);
        this.columnList = getFusionMapLayerTableColumnListAndPopulateTooltipMap(sqlInfoMap.get("fusion_map_layer_id"),this.tooltipMap);
        //need to populate from the first layer later


        //case 2: there is a selected column list from http request
        if (selectColumnListMap != null&&selectColumnListMap.size()>0) {


            List<Map<String, Object>> selectColumnListMapTmp = new ArrayList<Map<String, Object>>(selectColumnListMap);

            //updateReturnTableColumns for first layer
            this.columnList = new ArrayList<String>();
            this.tooltipMap = new LinkedHashMap<String,String>();

            Map<String, Object> firstLayerColumnMap = selectColumnListMapTmp.get(0);
            String firstLayerIdFromColumnInfo = (String) firstLayerColumnMap.get("fusionMapLayerId");
            String layerIdFromSqlInfo = this.sqlInfoMap.get("fusion_map_layer_id");
            if (firstLayerIdFromColumnInfo.equals(layerIdFromSqlInfo)) {

                List<String> columnIdList = (List<String>) firstLayerColumnMap.get("selectedTableColumnIds");
                List<String> firstLayerColumnNameList = buildColumnNameListFromColumnIdList(columnIdList);
                List<String> firstLayerColumnTooltipList = buildColumnTooltipListFromColumnIdList(columnIdList);
                Map<String,String> firstLayerToolTipMap = assembleTooltipMapFromColumnNamesAndTooltips(firstLayerColumnNameList,firstLayerColumnTooltipList);
                this.columnList.addAll(firstLayerColumnNameList);
                this.tooltipMap.putAll(firstLayerToolTipMap);
                selectColumnListMapTmp.remove(0);//remove first layer information off the map
            }


            //iterate over all other layers
            Map<String, String> renamedColumnMap = new HashMap<String, String>(); //<original name> = <new name>
            for (Map<String, Object> aLayerColumnMap : selectColumnListMapTmp) {

                String thisFusionLayerId = (String) aLayerColumnMap.get("fusionMapLayerId");
                List<String> thisColumIdList = (List<String>) aLayerColumnMap.get("selectedTableColumnIds");
                List<String> thisColumnNameList = buildColumnNameListFromColumnIdList(thisColumIdList);
                List<String> thisColumnTooltipList = buildColumnTooltipListFromColumnIdList(thisColumIdList);


                //a. build sql
                String thisLayerSql = buildSqlForNonFirstLayer(thisFusionLayerId, this.sqlInfoMap, thisColumnNameList, this.columnList, renamedColumnMap);
                //b. execute sql to ListMap
                List<Map<String, String>> thisLayerResultListMap = executeSql(thisLayerSql);

                //c. stitch resultSet
                List<String> addtnlColumnList = new ArrayList<String>();
                String newName;
                int index=0;
//                Map<String,String> columnNameChangeMap = new HashMap<String,String>();
                for (String column : thisColumnNameList) {
//                    if (!column.contains("_ogc_fid")) {
                    newName = renamedColumnMap.get(column);
                    if (newName != null) {
                        addtnlColumnList.add(newName);
                        this.tooltipMap.put(newName,thisColumnTooltipList.get(index++));
                    } else {
                        addtnlColumnList.add(column);
                        this.tooltipMap.put(column,thisColumnTooltipList.get(index++));
                    }
//                    }
                }
                String[] columnsToAdd = new String[addtnlColumnList.size()];
                addtnlColumnList.toArray(columnsToAdd);
                resultListMap = ListMapUtility.leftJoin(resultListMap, thisLayerResultListMap, "_ogc_fid1_", "_ogc_fid1", columnsToAdd,renamedColumnMap);

                //d. update returnTableColumns
                for (int i = 0; i < columnsToAdd.length; i++) {
                    this.columnList.add(columnsToAdd[i]);
                }
            }
        }

//        System.out.println("$$$$$$$$$$$$$$$$$$$");
//        System.out.println(" selectColumnListMap ="+selectColumnListMap);
////        System.out.println("-------------------");
//
//        System.out.println("*************1111*************");//debug
//        System.out.println("this.gradFieldColMap1="+this.gradFieldColMap1);
//        System.out.println("--------------------------");//debug


        //Prepare additional Data Field to add to Result
        Set<String> countyLevelFieldIds = new LinkedHashSet<String>();
        Set<String> stateLevelFieldIds = new LinkedHashSet<String>();

        for (Map<String, String> aGradientLayerInfo : this.gradientLayerListMap) {
            String dataLevel = aGradientLayerInfo.get("data_level");
            String gradientLayerId = aGradientLayerInfo.get("gradientMapLayerId");
            if (dataLevel.equalsIgnoreCase("county")) {
                countyLevelFieldIds.add(gradientLayerId);
            } else if (dataLevel.equalsIgnoreCase("state")) {
                stateLevelFieldIds.add(gradientLayerId);
            }
        }
        for (Integer countyOpt : this.countyLevelOptList) {
            countyLevelFieldIds.add(countyOpt.toString());
        }
        for (Integer stateOpt : this.stateLevelOptList) {
            stateLevelFieldIds.add(stateOpt.toString());
        }

        Map<String, Map<String, String>> fipsDataLookupMap = new GradientFieldLookup().buildLookupMap();

        //
        //Update this.columnList (ie. Output Field headers)
        //& data for column for tooltip
        //CountyLevel field
        for (String fieldId : countyLevelFieldIds) {
            String fieldColName = this.gradFieldColMap1.get(fieldId);
            String tooltipText = this.tooltipLookupMapGradientLayer.get(fieldId);
            populateOneColumnDisplayNameAndTooltipText (fieldColName, tooltipText, this.columnList, this.tooltipMap);
            addOneCountyLevelDataFieldByFipsCode(resultListMap, fipsDataLookupMap, fieldId, fieldColName);
        }
        //stateLeveld
        for (String fieldId : stateLevelFieldIds) {
            String fieldColName = this.gradFieldColMap1.get(fieldId);
            String tooltipText = this.tooltipLookupMapGradientLayer.get(fieldId);
            populateOneColumnDisplayNameAndTooltipText (fieldColName, tooltipText, this.columnList, this.tooltipMap);
            addOneStateLevelDataFieldByFipsCode(resultListMap, fipsDataLookupMap, fieldId, fieldColName);
        }

        return resultListMap;
        //        tableColumns":[{"fusionMapLayerId":"25","selectedTableColumnIds":["11","12","13"]},{"fusionMapLayerId":"5","selectedTableColumnIds":["314"]}]

    }
}
