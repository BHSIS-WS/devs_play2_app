/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import io.ebean.Ebean;
import io.ebean.SqlRow;
import helper.util.ListMapUtility;
import helper.util.ResultSetConverter;

/**
 *
 * @author pingw
 */

public class GradientFieldTooltipLookup {

    private Map<String, String> lookupMap;

    public GradientFieldTooltipLookup() {
    }
  
    public Map<String,String> buildLookupMap() throws SQLException {
        
        String sql = "select gml.gradient_map_layer_id,"
                + "lower(gml.name )|| ' (' || ds.name ||')' as tool_tip_text from gradient_map_layer gml\n" 
                + "inner join data_set ds on gml.data_set_id = ds.data_set_id";
        List<SqlRow> sqlRs = Ebean.createSqlQuery(sql).findList();
        List<Map<String, String>> listMap = ResultSetConverter.convertToListMap(sqlRs);
        this.lookupMap = ListMapUtility.createMap(listMap, "gradient_map_layer_id", "tool_tip_text");
        return lookupMap;
    }
}