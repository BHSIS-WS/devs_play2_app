/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import models.CriteriaCode;
import models.FusionMapLayer;
import models.LayerFilteringCriteria;

/**
 *
 * @author AshwiniR
 */
public class CriteriaCodeHelper {
    public static List<CriteriaCode> assignFusionLayerCriteriaToCriteriaCode(List<CriteriaCode> criteriaCodeList, List<LayerFilteringCriteria> layerFilterList){
        Map<String, CriteriaCode> criteriaCodeMap = new HashMap<String, CriteriaCode>();
        for (CriteriaCode criteriaCode : criteriaCodeList){
            criteriaCodeMap.put(criteriaCode.criteriaCodeId.toString(), criteriaCode);
        }
        
        for(LayerFilteringCriteria lfc : layerFilterList){
            String key = lfc.criteriaCodeId.toString();
            CriteriaCode criteriaCode = criteriaCodeMap.get(key);
            if(criteriaCode.filteringCriteriaGroup == null){
                criteriaCode.filteringCriteriaGroup = new ArrayList<LayerFilteringCriteria>();

            }
            List<LayerFilteringCriteria> subLFC = criteriaCode.filteringCriteriaGroup;
            subLFC.add(lfc);
        }
        return criteriaCodeList;
    }
    
    public static List<FusionMapLayer> assignCriteriaCodeListToFusionLayer(List<FusionMapLayer> fusionMapLayerList, Map<Integer, List<CriteriaCode>> criteriaCodeByFusionIdMap) {
        for(FusionMapLayer fml: fusionMapLayerList){
            Integer fmlId = fml.fusionMapLayerId;
            List<CriteriaCode> ccList = criteriaCodeByFusionIdMap.get(fmlId);

            if(ccList != null ){
                for(CriteriaCode cc : ccList){
                    if(fml.criteriaCodeGroup == null){
                    fml.criteriaCodeGroup = new ArrayList<CriteriaCode>();
                }
                    List<CriteriaCode> subcc = fml.criteriaCodeGroup;
                    subcc.add(cc);
                }
            }
        }
               
        return fusionMapLayerList;
    }
    
    public static Map<Integer, List<CriteriaCode>> breakCriteriaCodeListByFusionMapId(List<CriteriaCode> criteriaCodeList, Map<Integer, List<CriteriaCode>> fusionMapIdAndCriteriaCodeIdMap) {
        Map<Integer, List<CriteriaCode>> ccListFusionLayerIdMap = new HashMap<Integer, List<CriteriaCode>>();
        List<CriteriaCode> ccListForFusionLayer = new ArrayList();
        
        Map<Integer, CriteriaCode> criteriaCodeMap = new HashMap<Integer, CriteriaCode>();
        for (CriteriaCode criteriaCode : criteriaCodeList){
            criteriaCodeMap.put(criteriaCode.criteriaCodeId, criteriaCode);
        }
                
        for(Integer key: fusionMapIdAndCriteriaCodeIdMap.keySet()){
            List<CriteriaCode> ccList = new ArrayList<CriteriaCode>();
            ccListForFusionLayer = fusionMapIdAndCriteriaCodeIdMap.get(key);
            
            for(int i=0;i<ccListForFusionLayer.size();i++){
                ccList.add(criteriaCodeMap.get(ccListForFusionLayer.get(i)));
            }
            ccListFusionLayerIdMap.put(key, ccList);
        }
        
        return ccListFusionLayerIdMap;
    }
    
    public static Map<Integer, Integer> getFusionMapIdAndCriteriaCodeIdMap(){
        List<Integer[]> fusionMapLayerIdCriteriaCodeIdList = CriteriaCode.getFusionMapLayerIdAndCriteriaCodeId();
        Map<Integer, Integer> fusionMapIdAndCriteriaCodeIdMap = new HashMap<>();
        
        for(int i = 0; i< fusionMapLayerIdCriteriaCodeIdList.size(); i++){
            fusionMapIdAndCriteriaCodeIdMap.put((Integer)fusionMapLayerIdCriteriaCodeIdList.get(i)[0], (Integer)fusionMapLayerIdCriteriaCodeIdList.get(i)[1]);
        }
        return fusionMapIdAndCriteriaCodeIdMap;
    }
    
}
