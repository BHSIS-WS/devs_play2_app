/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper.util;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

/**
 *
 * @author pingw
 */
public class JsonSmartUtility {

    public static JsonNode stringMapToJson(Map<String, String> map) {
        if (map == null) {
            return null;
        }

        ObjectNode obj = Json.newObject();
        Set<Map.Entry<String, String>> entrySet = map.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (value != null) {
                obj.put(key, value);
            }
        }

        return Json.toJson(obj);
    }

    public static JsonNode mapToJson(Map<String, Object> map) {
        if (map == null) {
            return null;
        }

        ObjectNode obj = Json.newObject();
        Set<Map.Entry<String, Object>> entrySet = map.entrySet();
        for (Map.Entry<String, Object> entry : entrySet) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value != null) {
                obj.putPOJO(key, value);
            }
        }

        return Json.toJson(obj);
    }

    public static ArrayNode stringMapListToJson(List<Map<String, String>> mapList) {

        ArrayNode listArrayNode = Json.newArray();
        JsonNode jsonItem = null;
        if (mapList != null && !mapList.isEmpty()) {
            for (Map<String, String> map : mapList) {
                jsonItem = stringMapToJson(map);
                listArrayNode.add(jsonItem);
            }
        }
        return listArrayNode;
    }

    public static ArrayNode mapListToJsonArray(List<Map<String, Object>> mapList) {

        ArrayNode listArrayNode = Json.newArray();
        JsonNode jsonItem = null;
        if (mapList != null && !mapList.isEmpty()) {
            for (Map<String, Object> map : mapList) {
                jsonItem = mapToJson(map);
                listArrayNode.add(jsonItem);
            }
        }
        return listArrayNode;
    }

    public static ArrayNode stringListToArray(List<String> listString) {
        ArrayNode listArrayNode = Json.newArray();
        if (listString != null && !listString.isEmpty()) {
            for (String item: listString) {
                listArrayNode.add(item);
            }
        }
        return listArrayNode;
    }

    public static ArrayNode intListToArray(List<Integer> listInt) {
        ArrayNode listArrayNode = Json.newArray();
        if (listInt != null && !listInt.isEmpty()) {
            for (Integer item: listInt) {
                listArrayNode.add(item);
            }
        }
        return listArrayNode;
    }

}
