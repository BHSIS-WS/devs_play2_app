package helper.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author pingw
 */
public class ListMapUtility {

    private static final Logger log = LoggerFactory.getLogger(ListMapUtility.class);

    public static void main(String[] args) throws Exception {
        //new ListMapUtility().testGroupListMap();
    }

    public static List<String> extractColumnAsList(List<Map<String, String>> listMap, String columnHeaderName) {
        if (columnHeaderName == null) {
            return null;
        }
        int n = listMap.size();
        List<String> ret = new ArrayList<String>();
        
        for (Map<String, String> map : listMap) {

            String value = map.get(columnHeaderName);
            ret.add(value);
        }
        return ret;

    }
    
    public static String[] extractColumn(List<Map<String, String>> listMap, String columnHeaderName) {
        if (columnHeaderName == null) {
            return null;
        }
        int n = listMap.size();
        String[] ret = new String[n];
        int i = 0;
        for (Map<String, String> map : listMap) {

            String value = map.get(columnHeaderName);
            ret[i++] = value;
        }

//        for(Map.Entry <String, Map<String, String>> m : retMap.entrySet() ){
//        	System.out.println(" II " + m.getKey() + ".... " + m.getValue());
//        }

        return ret;

    }

    public static List<String> grabHeaderNameAsList(List<Map<String, String>> listMapData) {
        if (listMapData == null || listMapData.isEmpty()) {
            return null;
        }

        List<String> ret = new ArrayList<String>();
        Map<String, String> firstRecord = listMapData.get(0);
        Set<Map.Entry<String, String>> entrySet = firstRecord.entrySet();

        for (Map.Entry<String, String> entry : entrySet) {
            ret.add(entry.getKey());
        }
        return ret;
    }

    public static String[] grabHeaderName(List<Map<String, String>> listMapData) {
        if (listMapData == null || listMapData.isEmpty()) {
            return null;
        }
        //
        Map<String, String> firstRecord = listMapData.get(0);
        Set<Map.Entry<String, String>> entrySet = firstRecord.entrySet();
        int n = entrySet.size();
        String[] ret = new String[n];
        int i = 0;
        for (Map.Entry<String, String> entry : entrySet) {
            ret[i++] = entry.getKey();
        }
        return ret;
    }

    public static List<String> getCommonColumnHeaders(List<Map<String, String>> leftListMap, List<Map<String, String>> rightListMap) {
        //based on the first record only
        if (leftListMap == null || leftListMap.size() <= 0) {
            return null;
        }
        if (rightListMap == null || rightListMap.size() <= 0) {
            return null;
        }
        Map<String, String> leftMap = leftListMap.get(0);
        Map<String, String> rightMap = rightListMap.get(0);
        Set<String> leftKeySet = leftMap.keySet();
        Set<String> rightKeySet = rightMap.keySet();

        List<String> ret = new ArrayList<String>();
        for (String key : leftKeySet) {
            if (rightKeySet.contains(key)) {
                ret.add(key);
            }
        }
        return ret;
    }

    public static void addHashField(List<Map<String, String>> listMap,
            List<String> hashingOrder,
            String hashFieldName) {

        if (listMap == null) {
            return;
        }
        if (hashingOrder == null || hashingOrder.isEmpty()) {
            return;
        }
        if (hashFieldName == null || hashFieldName.trim().equals("")) {
            hashFieldName = "#HASH#";
        }
        int nHeader = hashingOrder.size();
        String[] headerArray = hashingOrder.toArray(new String[nHeader]);

        for (Map<String, String> map : listMap) {
            try {
                //String sumValue = "";
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.reset();
                md.digest(new byte[]{1, 2});
                for (String header : headerArray) {
                    String value = map.get(header);
                    if (value != null) {
                        md.update(value.getBytes("UTF-8"));
                    } else {
                        md.update((byte) 1);
                    }
                } //for .. headerArray
                byte messageDigest[] = md.digest();
                StringBuilder hexString = new StringBuilder();
                for (int i = 0; i < messageDigest.length; i++) {
                    hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
                }
                map.put(hashFieldName, hexString.toString());
            } //        return listMap;
            catch (NoSuchAlgorithmException ex) {
                throw new RuntimeException("RunTimeException: addHashField() failed :" + ex);
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException("RunTimeException: addHashField() failed :" + ex);
            }
        }

    }

    public static void addSquenceField(List<Map<String, String>> listMap, String sequenceFieldName) {

        if (listMap == null) {
            return;
        }

        if (sequenceFieldName == null || sequenceFieldName.trim().equals("")) {
            sequenceFieldName = "#SEQ_NUM#";
        }
        int i = 1;
        for (Map<String, String> map : listMap) {
            map.put(sequenceFieldName, "" + i++);
        }
    }

    public static List<Map<String, String>> getSubListMapByKeySet(Map<String, Map<String, String>> indexedListMap, Set<String> keySet) {
        if (indexedListMap == null) {
            return null;
        }
        if (keySet == null) {
            return null;
        }
        List<Map<String, String>> ret = new ArrayList<Map<String, String>>();

        for (String key : keySet) {
            Map<String, String> map = indexedListMap.get(key);
            if (map != null) {
                ret.add(map);
            } else {
                log.info("WARNING: getSubListMapByKeySet() record not found for Key=" + key);
            }
        }
        return ret;
    }

    public static List<Map<String, String>> leftJoin(List<Map<String, String>> listMapLeft,
            List<Map<String, String>> listMapRight,
            String keyColumnLeft, String keyColumnRight, String[] columnsToAdd) {
        Map<String, String> columnRenameMap = null;
        return leftJoin(listMapLeft,listMapRight, keyColumnLeft, keyColumnRight, columnsToAdd,columnRenameMap);
    }
    
    public static List<Map<String, String>> leftJoin(List<Map<String, String>> listMapLeft,
            List<Map<String, String>> listMapRight,
            String keyColumnLeft, String keyColumnRight, String[] columnsToAdd,
            Map<String,String> columnRenameMap) {
        if (listMapLeft == null) {
            return null;
        }
        if (listMapRight == null) {
            return new ArrayList<Map<String, String>>(listMapLeft);
        }
        if (columnsToAdd == null || columnsToAdd.length < 1) {
            return new ArrayList<Map<String, String>>(listMapLeft);
        }

        //System.out.println("listMapRight------------");
        //System.out.println(listMapRight);
        //System.out.println("================");
       
        Map<String, Map<String, String>> indexedListMapRight = indexListMapByHeaderNameAddendDuplicates(listMapRight, keyColumnRight);
        List<Map<String, String>> ret = new ArrayList<Map<String, String>>(listMapLeft);
        for (Map<String, String> leftRecord : ret) {
            String keyValue = leftRecord.get(keyColumnLeft);
            if (keyValue == null || keyValue.trim().equals("")) {
                continue;
            }
            Map<String, String> rightMatchingRecord = indexedListMapRight.get(keyValue);
            if (rightMatchingRecord == null) {
                continue;
            }
            for (String column : columnsToAdd) {
                String columnKey = column;
                if ( columnRenameMap!=null ){
                    String columnFromMap = columnRenameMap.get(column);
                    if (columnFromMap!=null){
                        columnKey =columnFromMap;
                    }
                }
                String v = rightMatchingRecord.get(column);
                if (v != null) {
                    leftRecord.put(columnKey, v);
                }

            }
        }
        return ret;
    }

    public static Map<String, String> getHashofChildGroupHash(List<Map<String, String>> grantProgramListMap, Map<String, String> grantProgramChildGroupMap, Map<String, String> grantProgramFacilityChildGroupMap, String pkColumnName) {
        String hashChildValue = null;
        Map<String, String> hashMap = new HashMap<String, String>();

        //Map<String, String> parentMap = (Map<String, String>) iter.next();
        for (Map<String, String> parentMap : grantProgramListMap) {
            String key = parentMap.get(pkColumnName);
            if (grantProgramChildGroupMap.containsKey(key) && grantProgramFacilityChildGroupMap.containsKey(key)) {
                hashChildValue = ListMapUtility.getHashofHashValue(grantProgramChildGroupMap.get(key), grantProgramFacilityChildGroupMap.get(key));
            } else if (grantProgramChildGroupMap.containsKey(key)) {
                hashChildValue = grantProgramChildGroupMap.get(key);
            } else if (grantProgramFacilityChildGroupMap.containsKey(key)) {
                hashChildValue = grantProgramFacilityChildGroupMap.get(key);
            }
            hashMap.put(key, hashChildValue);
        }
        return hashMap;
    }

  

    public static void debugPrint(List<Map<String, String>> listMap) {
        if (listMap == null) {
            log.info("ListMapUtility.debugPrint() listMap is null...");
            return;
        }
        int i = 0;
        log.info("* * * ListMapUtility.debugPrint * * *");

        log.info(" size of listMap = " + listMap.size());
        for (Map<String, String> map : listMap) {
            log.info(++i + " : " + map);
        }
        log.info("* * * * * * * * * * * * * * * * * *");
    }

    public static void debugPrint(Map<String, List<Map<String, String>>> groupedListMap) {
        if (groupedListMap == null) {
            log.info("ListMapUtility.debugPrint() groupedListMap is null...");
            return;
        }
        int iGroup = 0;
        log.info("* * * ListMapUtility.debugPrint * * *");
        log.info(" number of listMapGroup = " + groupedListMap.size());

        for (Map.Entry<String, List<Map<String, String>>> entry : groupedListMap.entrySet()) {
            log.info(++iGroup + " : Group Name: " + entry.getKey());
            List<Map<String, String>> groupMember = entry.getValue();
            int i = 0;
            for (Map<String, String> record : groupMember) {
                log.info("\t " + ++i + " : " + record);
            }
        }
        log.info("* * * * * * * * * * * * * * * * * *");
    }

    public static Map<String, List<Map<String, String>>> groupListMapByHeaderName(List<Map<String, String>> listMap, String headerToGroup)
            throws Exception {

        Map<String, List<Map<String, String>>> retMap = new LinkedHashMap<String, List<Map<String, String>>>();
        if (listMap != null && !listMap.isEmpty()) {
            for (Map<String, String> map : listMap) {

                String valueToGroup = map.get(headerToGroup);
                if (valueToGroup == null || valueToGroup.trim().equals("")) {
                    throw new RuntimeException("record missing: key=" + headerToGroup);
                }

                List<Map<String, String>> ownListMap = retMap.get(valueToGroup);

                if (ownListMap == null) {
                    ownListMap = new ArrayList<Map<String, String>>();
                    retMap.put(valueToGroup, ownListMap);
                }
                ownListMap.add(map);
            }
        }


        return retMap;
    }

    public static Map<String, Map<String, String>> indexListMapByHeaderName(List<Map<String, String>> listMap, String headerToGroup) {

        Map<String, Map<String, String>> retMap = new TreeMap<String, Map<String, String>>();

        for (Map<String, String> map : listMap) {
            String key = map.get(headerToGroup);

            if (key == null || key.trim().equals("")) {
                log.info("ListMapUtility.indexListMapByHeaderName: record missing: key=" + headerToGroup);
                continue;
            }
            Map<String, String> oldEntry = retMap.get(key);

            if (oldEntry != null) {
                throw new RuntimeException("ListMapUtility.indexListMapByHeaderName faild: key is not unique: " + key);
            }
            retMap.put(key, map);
        }

        return retMap;
    }

    public static Map<String, Map<String, String>> indexListMapByHeaderNameAddendDuplicates(List<Map<String, String>> listMap, String headerToGroup) {

        Map<String, Map<String, String>> retMap = new TreeMap<String, Map<String, String>>();

        for (Map<String, String> map : listMap) {
            String key = map.get(headerToGroup);

            if (key == null || key.trim().equals("")) {
                log.info("ListMapUtility.indexListMapByHeaderNameAddendDuplicates: record missing: key=" + headerToGroup);
                continue;
            }
            Map<String, String> oldEntry = retMap.get(key);

            if (oldEntry != null) {
                Map<String, String> combinedMap = new HashMap<>();
                combinedMap.put(headerToGroup, key);
                for (String mapKey : map.keySet()) {
                    if (!mapKey.equalsIgnoreCase(headerToGroup)) {
                        String currMapStr = map.get(mapKey);
                        String oldMapStr = oldEntry.get(mapKey);
                        String combinedStr;
                        if (map.get(mapKey) == null || map.get(mapKey).equalsIgnoreCase("null")) {
                            combinedStr = oldMapStr;
                        } else if (oldMapStr == null || oldMapStr.equalsIgnoreCase("null")) {
                            combinedStr = currMapStr;
                        } else {
                            combinedStr = oldMapStr + ", " + currMapStr;
                        }
                        combinedMap.put(mapKey, combinedStr);
                    }
                    
                }
                retMap.put(key, combinedMap);
            } else {
                retMap.put(key, map);
            }
        }

        return retMap;
    }

    public static List<Map<String, String>> transformAssigmentDataIntoMatrixForm(
            List<Map<String, String>> assignDataListMap,
            String rowKey, String columnKey, String markerForAssignment) throws Exception {

        //1. group the by rowKey
        Map<String, List<Map<String, String>>> dataGrouppedByRowKey =
                ListMapUtility.groupListMapByHeaderName(assignDataListMap, rowKey);
        if (dataGrouppedByRowKey == null) {
            return null;
        }

        //2. declear a return object
        List<Map<String, String>> retListMap = new ArrayList<Map<String, String>>();

        //3. looping over each rowIdValue
        Iterator<Entry<String, List<Map<String, String>>>> iterator =
                dataGrouppedByRowKey.entrySet().iterator();
        while (iterator.hasNext()) {
            Map<String, String> map = new HashMap<String, String>();
            Entry<String, List<Map<String, String>>> entry = iterator.next();

            //3.1 add rowIdValue to the map
            String rowIdValue = entry.getKey();
            map.put(rowKey, rowIdValue);

            List<Map<String, String>> listMap = entry.getValue();
            //3.2 inner looping over (column)
            for (Map<String, String> assignDataRecord : listMap) {
                String columnIdValue = assignDataRecord.get(columnKey);
                //3.2.1 - add X data with key=roleName
                map.put(columnIdValue, markerForAssignment);
            }

            //3.3 - add to return listMap
            retListMap.add(map);
        }

        return retListMap;


    }

    public static Map<String, String> computeHashOfHashValues(Map<String, List<Map<String, String>>> mapOfListMap, String hashFieldName) {
        Map<String, String> hashMap = new HashMap<String, String>();

        if (mapOfListMap == null) {
            throw new RuntimeException("List conatining the records cannot be null");
        }
        if (hashFieldName == null || hashFieldName.trim().equals("")) {
            hashFieldName = "#HASH#";
        }

        Iterator it = mapOfListMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            List<Map<String, String>> listMap = (List<Map<String, String>>) pairs.getValue();
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.reset();
                md.digest(new byte[]{1, 2});
                for (Map<String, String> map : listMap) {
                    String value = map.get(hashFieldName);
                    if (value != null) {
                        md.update(value.getBytes("UTF-8"));
                    } else {
                        md.update((byte) 1);
                    }
                }
                byte messageDigest[] = md.digest();
                StringBuilder hexString = new StringBuilder();
                for (int i = 0; i < messageDigest.length; i++) {
                    hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
                    hashMap.put(pairs.getKey().toString(), hexString.toString());
                }
            } catch (NoSuchAlgorithmException ex) {
                throw new RuntimeException("RunTimeException: addHashField() failed :" + ex);
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException("RunTimeException: addHashField() failed :" + ex);
            }
        }

        return hashMap;
    }

    public static String getHashofHashValue(String s1, String s2) {
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.digest(new byte[]{1, 2});
            if (s1 != null && s2 != null) {

                md.update(s1.getBytes("UTF-8"));
                md.update(s2.getBytes("UTF-8"));
            } else if (s1 != null) {
                md.update(s1.getBytes("UTF-8"));
                md.update((byte) 1);
            } else if (s2 != null) {
                md.update(s2.getBytes("UTF-8"));
                md.update((byte) 1);
            } else {
                md.update((byte) 1);
                md.update((byte) 1);
            }
            byte messageDigest[] = md.digest();
            
            for (int i = 0; i < messageDigest.length; i++) {
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            }
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException("RunTimeException: addHashField() failed :" + ex);
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("RunTimeException: addHashField() failed :" + ex);
        }
        return hexString.toString();
    }

    public static Map<String, String> createMap(List<Map<String, String>> listMap,String keyColumnName, String valueColumnName) {
        if (listMap==null) return null;
        Map<String,String> ret = new HashMap<String, String>();
        for (Map<String,String> record : listMap){
            String key = record.get (keyColumnName);
            String value = record.get (valueColumnName);
            ret.put(key, value);
        }
        return ret;
    }


}
