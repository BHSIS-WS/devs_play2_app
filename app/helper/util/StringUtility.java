/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper.util;

import org.apache.commons.text.StringEscapeUtils;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 *
 * @author PingW1
 */
public class StringUtility {

    public static int findArryIndexByValue(String[] array, String value) {
        int ret = -1;
        if (array == null) {
            return ret;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(value)) {
                return i;
            }
        }
        return ret;
    }



//    public static List<String> arrayToList(String[] array) {
//        if (array == null) {
//            return null;
//        }
//        return ;
//
//    }
    private StringUtility() {
    }

    public static void trimAndUnquote(String[] stringArray) {
        if (stringArray == null) {
            return;
        }
        for (int i = 0; i < stringArray.length; i++) {
            stringArray[i] = trimAndUnquote(stringArray[i]);
        }
    }

    public static String trimAndUnquote(String quoteText) {
        String text = quoteText.trim();
        if (text.startsWith("\"") && text.endsWith("\"")) {
            text = text.substring(1, text.length() - 1);
        }
        text = encodeString(text);
//        else if (text.startsWith("\"")) {
//            text = text.substring(1, text.length());
//        } else if (text.endsWith("\"")) {
//            text = text.substring(0, text.length() - 1);
//        }
        return text;
    }

    public static String toCamelCase(String inputName) {
        String wordDelimiter = "[-_]+";
        return toCamelCase(inputName, wordDelimiter);
    }

    public static String capFirstCharacterOfString(String inputName) {
        StringBuilder nameBuilder = new StringBuilder(inputName.length());
        nameBuilder.append(inputName.substring(0, 1).toUpperCase());
        if (inputName.length() > 1) {
            nameBuilder.append(inputName.substring(1));
        }
        return nameBuilder.toString();
    }

    public static String toCamelCase(String inputName, String wordDelimiter) {
        String[] words = inputName.split(wordDelimiter);
        StringBuilder nameBuilder = new StringBuilder(inputName.length());
        if (words == null || words.length <= 0) {
            return inputName;
        }

        nameBuilder.append(words[0].trim().toLowerCase());// handle first
        for (int i = 1; i < words.length; i++) {
            String word = words[i].trim().toLowerCase();
            nameBuilder.append(word.substring(0, 1).toUpperCase());
            nameBuilder.append(word.substring(1));
        }
        return nameBuilder.toString();
    }
    
   public static String toCamelCaseWithFirstCapped(String inputName) {
        inputName = toCamelCase(inputName);
        if (inputName!=null){
            inputName=capFirstCharacterOfString(inputName);
        }
        return inputName;
    }
   
    public static String generateMd5Sum(String text) throws NoSuchAlgorithmException, FileNotFoundException, IOException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(text.getBytes());
        byte[] mdbytes = md.digest();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mdbytes.length; i++) {
            sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString().toUpperCase();
    }
    
    //Encode input string to prevent SQL injection to the database
    private static String encodeString(String formText) {
        //Remove any injection to SQL comands
        formText = formText.replaceAll("(?i)delete", "");
        formText = formText.replaceAll("(?i)insert", "");
        formText = formText.replaceAll("(?i)update", "");
        //Remove any javascript code can be executed
        formText = formText.replaceAll("(?i)eval\\((.*)\\)", "");
        formText = formText.replaceAll("[\\\"\\\'][\\s]*((?i)javascript):(.*)[\\\"\\\']", "\"\"");
        formText = formText.replaceAll("((?i)script)", "");
        //Escape any HTML code except for input criteria < and > surrounded by spaces
        formText = StringEscapeUtils.escapeHtml4(formText);
        if(formText.contains(" &gt; ")) formText = formText.replaceAll(" &gt; ", " > ");
        if(formText.contains(" &lt; ")) formText = formText.replaceAll(" &lt; ", " < ");
        return formText;
    }
    
     /**
     * Convert string parameter to integer, return null if error happened
     * @return integer
     * @param stringParam: string we need to cast to integer
     */
    public static Integer convertStringToInteger(String stringParam) {
        Integer integerParam;
        try {
            integerParam = Integer.parseInt(stringParam);
        }
        catch (NumberFormatException nfe) {
            //nfe.printStackTrace();
            integerParam = null;
        }
        return integerParam;
    }
    
    /**
     * This method to generate a random text code
     * @return random text
     * @param length: input text     
     */
    public static String getRandomCode(int length) {
        char[] charsArray = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789".toCharArray();
        Random random = new Random(System.currentTimeMillis());
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(charsArray[random.nextInt(charsArray.length)]);
        }
        return sb.toString();
    }
}
