/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper.util;

import io.ebean.SqlRow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 *
 * @author PingW1
 */
public class ResultSetUtil {

    private static final Logger log = LoggerFactory.getLogger(ResultSetUtil.class);

    public static String getAsJsonObjectArray(ResultSet rs) throws SQLException {
        if (rs == null) {
            return null;
        }

        String[] columnNames = ResultSetUtil.grabColumnNames(rs);

        if (columnNames == null || columnNames.length <= 0) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        sb.append("[\n");


        int nColumn = columnNames.length;

        int iRow = 0;
        while (rs.next()) {
            if (iRow != 0) {
                sb.append("\n,");
            }

            sb.append("  {");
            for (int i = 1; i <= nColumn; i++) {
                if (i != 1) {
                    sb.append(",");
                }
                Object val = rs.getObject(i);
                if (val != null) {
                   sb.append("\n\t\"").append(columnNames[i-1]).append("\":");
                    sb.append("\"").append(val).append("\"");
                }
            }
            sb.append("\n  }\n");

            iRow++;
        }
        sb.append("]\n");

        return sb.toString();
    }

    public static String getAsJsonStringArray(ResultSet rs, int columnIndex) throws SQLException {
        if (rs == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        sb.append("[\n");
        
        int iRow = 0;
        while (rs.next()) {
            if (iRow != 0) {
                sb.append(",");
            }

            Object val = rs.getObject(columnIndex);
            if (val != null) {

                sb.append("\"").append(val.toString()).append("\"\n");
            }

            iRow++;
        }
        sb.append("]");


        return sb.toString();


    }

    //{table:{cols:[col1,col2], rows:[[val11,val12],[val21,val22]]}};
    public static String getAsJsonString(ResultSet rs) throws SQLException {
        if (rs == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();

        sb.append("{\"table\":{\n");

        String[] columnNames = null;
        int nColumn = 0;
        if (rs != null) {
            sb.append("\"cols\":\n[");
            columnNames = ResultSetUtil.grabColumnNames(rs);
            nColumn = columnNames.length;
            for (int i = 0; i < nColumn; i++) {
                String a = columnNames[i];
                if (i != 0) {
                    sb.append(",");
                }

                sb.append("\"").append(a).append("\"");

            }
            sb.append("]\n");
        }
        sb.append(",\"rows\":\n[");
        int iRow = 0;
        while (rs.next()) {
            if (iRow != 0) {
                sb.append(",");
            }
            sb.append("[");
            for (int i = 1; i <= nColumn; i++) {
                if (i != 1) {
                    sb.append(",");
                }
                Object val = rs.getObject(i);
                if (val != null) {
                    sb.append("\"").append(val).append("\"");
                }else {
                     sb.append("\"\"");
                }
            }
            sb.append("]\n");

            iRow++;
        }
        sb.append("]");
        sb.append("\n}}");

        return sb.toString();
    }
    
    public static String getAsJsonStringJqGridStyle(ResultSet rs) throws SQLException {
        return getAsJsonStringJqGridStyle( rs,1,1,-1);
    }
    
    public static String getAsJsonStringJqGridStyleWithHeaders(List<SqlRow> rs, String[] headers) throws SQLException {
        return getAsJsonStringJqGridStyleWithHeaders(headers,rs, 1, -1, -1);
    }

    public static String getAsJsonStringJqGridStyleWithHeaders(String[] headers, List<SqlRow>rs, long page, long pageSize, long recordCount) throws SQLException {

        if (rs == null) {
            return null;
        }
    
        StringBuilder sb = new StringBuilder();
        
        sb.append("{\"columns\":\n[");
        int iHeaders=0;
        for (String header: headers){
              if (iHeaders != 0) {
                sb.append(", ");
            }
            sb.append("\""+header+"\"");
            iHeaders++;
        }
        sb.append("]\n,");
        String[] columnNames = null;
        if (rs != null && !rs.isEmpty()) {
            columnNames = ResultSetUtil.grabColumnNames(rs.get(0));
        }
        int nColumn = columnNames.length;
        sb.append("\"rows\":\n[");
//        int iRow = 0;
        int iRow = (int)((page-1)*pageSize);
        if (pageSize<0){
            iRow=0;
        }
        
        int iRowStart = iRow;
        for (SqlRow row : rs) {
            if (iRow != iRowStart) {
                sb.append(",");
            }

            sb.append("{");
//            System.out.println("nColumn="+nColumn);//debug
            sb.append("\"_irow\":").append(iRow + 1);
            if (iRow % 2 == 0) {
                sb.append(",\"_rclass\":").append("\"row_odd\"");
            } else {
                sb.append(",\"_rclass\":").append("\"row_even\"");
            }

            for (String aColName : columnNames) {
//                System.out.println("column:="+columnNames[i-1]);//debug
//                if (i != 1) {
                sb.append(",");
//                }
                Object val = row.get(aColName);
                //column name
                sb.append("\"").append(aColName).append("\":");
                //value
                if (val != null) {
                    sb.append("\"").append(escape(val.toString())).append("\"");
                } else {
                    sb.append("\"\"");
                }
//                System.out.println("\tval: "+val);
            }
            sb.append("}\n");

            iRow++;
        }
        
        long totalPage = recordCount/pageSize;
        if (recordCount%pageSize != 0){
            totalPage++;
        }
        
        sb.append("]\n");
        sb.append(",\"page\":").append(page).append("\n");
        sb.append(",\"totalPages\":").append(totalPage).append("\n");
        if (recordCount <= 0) {
            sb.append(",\"recordCount\":").append(iRow);
        } else {
            sb.append(",\"recordCount\":").append(recordCount);
        }
        sb.append("\n}");

        return sb.toString();
    }
    
     public static String getAsJsonStringJqGridStyle(ResultSet rs,long page,long totalPage ,long recordCount) throws SQLException {
         
        if (rs == null) {
            return null;
        }
        
        StringBuilder sb = new StringBuilder();

        String[] columnNames = null;
        if (rs != null) {
            columnNames = ResultSetUtil.grabColumnNames(rs);
        }
        int nColumn = columnNames.length;
        sb.append("{\"rows\":\n[");
        int iRow = 0;
        while (rs.next()) {
            if (iRow != 0) {
                sb.append(",");
            }
            
            sb.append("{");
//            System.out.println("nColumn="+nColumn);//debug
               sb.append("\"_irow\":").append (iRow+1);
               if ( iRow%2 == 0){
                   sb.append(",\"_rclass\":").append ("\"row_odd\""); 
               }else {
                    sb.append(",\"_rclass\":").append ("\"row_even\"");
               }
              
            for (int i = 1; i <= nColumn; i++) {
//                System.out.println("column:="+columnNames[i-1]);//debug
//                if (i != 1) {
                    sb.append(",");
//                }
                Object val = rs.getObject(i);
                //column name
                sb.append("\"").append(columnNames[i-1]).append("\":");
                //value
                if (val != null) {
                    sb.append("\"").append(escape(val.toString())).append("\"");
                }else {
                     sb.append("\"\"");
                }
//                System.out.println("\tval: "+val);
            }
            sb.append("}\n");
            
            iRow++;
        }
        sb.append("]\n");
        sb.append(",\"page\":").append(page).append("\n");
        sb.append(",\"totalPages\":").append(totalPage).append("\n");
        if (recordCount<=0){
          sb.append(",\"recordCount\":").append(iRow);
        }else {
          sb.append(",\"recordCount\":").append(recordCount);
        }
        sb.append("\n}");

        return sb.toString();
     }
    
    public static String createInsertSql(ResultSet rs, String tableName) throws SQLException {

        String[] columns = grabColumnNames(rs);
        if (columns == null || columns.length <= 0) {
            log.info("ResultSetUtil.creatInsertsql: Table has no columns");
            return null;
        }

        StringBuffer sb = new StringBuffer("Insert into ");
        sb.append(tableName).append(" (");

        for (int i = 0; i < columns.length; i++) {
            sb.append(columns[i]);
            if (i < columns.length - 1) {
                sb.append(", ");
            }
        }
        sb.append(") ");
        sb.append("values (");

        for (int i = 0; i < columns.length; i++) {
            sb.append("?");
            if (i < columns.length - 1) {
                sb.append(", ");
            }
        }
        sb.append(") ");
        return sb.toString();
        //Insert into ORGANIZATION (ORGANIZATION_ID,SYS_GRANTEE_ID) values (214,436);
    }

    public static String getContentAsString(ResultSet rs) throws SQLException {
        if (rs==null) return null;
        while (rs.next()){
            return rs.getString(1);
        }
        return null;
    }
    
    public static String[] grabColumnNames(ResultSet rs) throws SQLException {

        ResultSetMetaData rmd = rs.getMetaData();

        int nColumn = rmd.getColumnCount();
        if (nColumn <= 0) {
            return null;
        }
        String[] ret = new String[nColumn];

        for (int i = 0; i < rmd.getColumnCount(); i++) {
            ret[i] = rmd.getColumnName(i + 1);
        }
        return ret;
    }

    public static String[] grabColumnNames(SqlRow rs) throws SQLException {
        if (rs == null) {
            return null;
        }

        Set<String> keys = rs.keySet();
        String[] columns = null;
        if (keys != null && !keys.isEmpty()) {
            columns = (String[])keys.toArray();
        }
        return columns;
    }

    public static int[] grabColumnTypes(ResultSet rs) throws SQLException {

        ResultSetMetaData rmd = rs.getMetaData();

        int nColumn = rmd.getColumnCount();
        if (nColumn <= 0) {
            return null;
        }
        int[] ret = new int[nColumn];

        for (int i = 1; i < rmd.getColumnCount(); i++) {
            ret[i] = rmd.getColumnType(i + 1);
        }
        return ret;
    }

    public static String grabTableName(ResultSet rs) throws SQLException {
        return grabTableName(rs, 1);
    }

    public static String grabTableName(ResultSet rs, int columnIndexToUse) throws SQLException {
        ResultSetMetaData rmd = rs.getMetaData();
        int numberOfColumns = rmd.getColumnCount();
        log.info("ResultSetUtil.grabTableName: grabTableName() number of column: " + numberOfColumns);
        return rmd.getTableName(columnIndexToUse);
    }

    public static void printResultSet(ResultSet rs) throws SQLException {
        if (rs == null) {
            return;
        }

        int i = 0;
        log.info("starting ...");
        while (rs.next()) {
            log.info("i=" + i++ + ", " + rs.getString(1));

        }

    }

    public static List<Long> grabSingleColumnLong(ResultSet rs, int columnIndex) throws SQLException {
        if (rs == null) {
            return null;
        }

        List<Long> retList = new ArrayList<Long>();
        while (rs.next()) {
            retList.add(rs.getLong(columnIndex));
        }
        return retList;
    }

    public static Long grabSingleNumberLong(SqlRow rs) throws SQLException {
        if (rs == null) {
            return null;
        }
        Set<String> keys = rs.keySet();
        if (keys != null && !keys.isEmpty()) {
            return rs.getLong((((Object[]) keys.toArray())[0]).toString());
        }
        return -1L;
    }
    
    public static String grabDdlScript(ResultSet rs) throws SQLException {
        String NEW_LINE = "\n\r";

        ResultSetMetaData rmd = rs.getMetaData();
        String tableName = rmd.getTableName(1);
        //System.out.println("Table Name=" + tableName);

        StringBuffer sb = new StringBuffer("CREATE TABLE ");
        sb.append(tableName).append("(").append(NEW_LINE);

        for (int i = 1; i <= rmd.getColumnCount(); i++) {
            String name = rmd.getColumnName(i);
            String type = rmd.getColumnTypeName(i);
            int size = rmd.getPrecision(i);
            //System.out.println("   " + name + ", " + type + ", " + size);
            sb.append("  ").append(name).append(" ").append(convertColumnTypeName(type));
            if (i < rmd.getColumnCount()) {
                sb.append(",");
            }
            sb.append(NEW_LINE);
        }
        sb.append(")").append(";").append(NEW_LINE);

        return sb.toString();
    }

    private static String convertColumnTypeName(String srcType) {

        if (srcType.equalsIgnoreCase("INTEGER")) {
            return "NUMBER";
        } else if (srcType.equalsIgnoreCase("BIT")) {
            return "NUMBER";
        } else if (srcType.equalsIgnoreCase("LONGCHAR")) {
            return "CLOB";
        } else if (srcType.equalsIgnoreCase("VARCHAR")) {
            return "VARCHAR2(2048)";
        } else if (srcType.equalsIgnoreCase("DATE")) {
            return "DATE";
        } else if (srcType.equalsIgnoreCase("DATETIME")) {
            return "DATE";
        }
        return "UNKNOWN";

    }

    public static String escape(final String s) {
        if (s != null && s.length() > 0) {
            StringBuilder sb = new StringBuilder();

            int len = s.length();
            for (int i = 0; i < len; i++) {
                char ch = s.charAt(i);
                switch (ch) {
                    case '"':
                        sb.append("\\\"");
                        break;
                    case '\\':
                        sb.append("\\\\");
                        break;
                    case '\b':
                        sb.append("\\b");
                        break;
                    case '\f':
                        sb.append("\\f");
                        break;
                    case '\n':
                        sb.append("\\n");
                        break;
                    case '\r':
                        sb.append("\\r");
                        break;
                    case '\t':
                        sb.append("\\t");
                        break;
                    case '/':
                        sb.append("\\/");
                        break;
                    default:
                        // Reference:
                        // http://www.unicode.org/versions/Unicode5.1.0/
                        if ((ch >= '\u0000' && ch <= '\u001F') || (ch >= '\u007F' && ch <= '\u009F')
                                || (ch >= '\u2000' && ch <= '\u20FF')) {
                            sb.append("\\u");
                            String hex = "0123456789ABCDEF";
                            sb.append(hex.charAt(ch >> 12 & 0x0F));
                            sb.append(hex.charAt(ch >> 8 & 0x0F));
                            sb.append(hex.charAt(ch >> 4 & 0x0F));
                            sb.append(hex.charAt(ch >> 0 & 0x0F));
                        } else {
                            sb.append(ch);
                        }
                }
            }
            return sb.toString();
        } else {
            return s;
        }
    }

}
