package helper.util;

/*
 * Converter.java
 *
 * Created on May 1, 2008, 6:04 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
import io.ebean.SqlRow;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

/**
 *
 * @author PingW
 */
public class ResultSetConverter {

    public static String CR = System.getProperty("line.separator");

    /**
     * Creates a new instance of Converter
     */
    private ResultSetConverter() {
    }

    public static List<String> convertToList(ResultSet rs, int columnIndex) throws SQLException {

        if (rs == null) {
            return null;
        }
        ArrayList<String> ret = new ArrayList<String>();
        //rs.beforeFirst();
        while (rs.next()) {
            ret.add(rs.getString(columnIndex));
        }
        return ret;
    }

    public static List<Object> convertToObjectList(ResultSet rs, int columnIndex) throws SQLException {

        if (rs == null) {
            return null;
        }
        ArrayList<Object> ret = new ArrayList<Object>();
        //rs.beforeFirst();
        while (rs.next()) {
            ret.add(rs.getObject(columnIndex));
        }
        return ret;
    }

    public static String[] convertToArray(ResultSet rs) throws SQLException {
        //debugPrint(rs);
        List<String> list = convertToList(rs, 1);
        if (list == null) {
            return null;
        }
        int n = list.size();
        if (n <= 0) {
            return null;
        }
        return list.toArray(new String[n]);
    }

    public static Object[] convertToObjectArray(ResultSet rs) throws SQLException {
        //debugPrint(rs);
        List<Object> list = convertToObjectList(rs, 1);
        if (list == null) {
            return null;
        }
        int n = list.size();
        if (n <= 0) {
            return null;
        }
        return list.toArray(new Object[n]);
    }

    public static String convertToCsv(ResultSet rs) throws SQLException {

        if (rs == null) {
            return null;
        }
        String[] cloumnHeaders = getColumnNameArray(rs);
        if (cloumnHeaders == null || cloumnHeaders.length <= 0) {
            return null;
        }

        StringBuffer buff = new StringBuffer();

        int nColumn = cloumnHeaders.length;
        for (int i = 0; i < nColumn; i++) {
            if (i > 0) {
                buff.append(",");
            }
            buff.append("\"").append(cloumnHeaders[i]).append("\"");
        }
        buff.append(CR);

        //print header
        //System.out.println("Header: "+buff);//debug
        boolean hasAtLeastOneRecord = false;
        while (rs.next()) {
            hasAtLeastOneRecord = true;
            for (int i = 0; i < nColumn; i++) {
                String value = rs.getString(i + 1);
                if (i > 0) {
                    buff.append(",");
                }
                if (value != null) {
                    buff.append("\"").append(value).append("\"");
                }
            }
            buff.append(CR);
        }

        if (hasAtLeastOneRecord == false) {
            return null;
        }

        return buff.toString();
    }
    
    public static List<Map<String, String>> convertToListMapWithAddedFields(List<SqlRow> rs) throws SQLException {
        return convertToListMapWithAddedFields(rs, 1);
    }
    
    public static List<Map<String, String>> convertToListMapWithAddedFields(List<SqlRow> rs,int iRowStart) throws SQLException {

        if (rs == null || rs.isEmpty()) {
            return null;
        }
        String[] cloumnHeaders = getColumnNameArray(rs.get(0));
        if (cloumnHeaders == null || cloumnHeaders.length <= 0) {
            return null;
        }
        int nColumn = cloumnHeaders.length;

        List<Map<String, String>> ret = new ArrayList<Map<String, String>>();
        int iRow = iRowStart;
        for (SqlRow row: rs) {
            Map<String, String> map = new HashMap<String, String>();

            map.put("_irow", ""+(iRow));
            if (iRow % 2 == 0) {
                map.put("_rclass", "row_odd");
            } else {
                map.put("_rclass", "row_even");
            }
            iRow++;
            for (int i = 0; i < nColumn; i++) {
                String key = cloumnHeaders[i];
                Object val = row.get(key);
                String value = (val != null) ? val.toString() : null;
                map.put(key, value);
            }
            ret.add(map);
        }
        return ret;
    }

   public static List<Map<String, String>> convertToListMap(List<SqlRow> sqlRs) throws SQLException {

        if (sqlRs == null || sqlRs.isEmpty()) {
            return null;
        }
        String[] cloumnHeaders = getColumnNameArray(sqlRs.get(0));
        
        if (cloumnHeaders == null || cloumnHeaders.length <= 0) {
            return null;
        }
        int nColumn = cloumnHeaders.length;

        List<Map<String, String>> ret = new ArrayList<Map<String, String>>();
        for (SqlRow row : sqlRs) {
            Map<String, String> map = new HashMap<String, String>();
            for (int i = 0; i < nColumn; i++) {
                String key = cloumnHeaders[i];
                String value = row.getString(key);
                map.put(key, value);
            }
            ret.add(map);
        }
        return ret;
    }
    

    private static String[] getColumnNameArray(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int nCol = rsmd.getColumnCount();
        if (nCol <= 0) {
            return null;
        }
        String[] ret = new String[nCol];
        for (int i = 0; i < nCol; i++) {
            //System.out.println(i + " : " + rsmd.getColumnLabel(i + 1));
            ret[i] = rsmd.getColumnLabel(i + 1);
        }
        return ret;

    }

    private static String[] getColumnNameArray(SqlRow row) throws SQLException {
        Set<String> keySet = row.keySet();
        String[] ret = keySet.stream().toArray(String[]::new);
        return ret;

    }
//    public static void  debugPrint(ResultSet rs){
//        System.out.println("----DebugPrint ResulstSet rs ---->");
//        if (rs==null) return ;
//        try {
//            //rs.beforeFirst();
//            while( rs.next() ){
//                System.out.println( rs.getString(1));
//            }
//        } catch (SQLException ex) {
//            System.out.println("exception: "+ ex);
//        }
//        System.out.println("<-------------------------------");
//    }
}
