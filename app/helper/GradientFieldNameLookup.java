/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import io.ebean.Ebean;
import io.ebean.SqlRow;
import helper.util.ListMapUtility;
import helper.util.ResultSetConverter;

/**
 *
 * @author pingw
 */

public class GradientFieldNameLookup {

    private Map<String, String> lookupMap;

    public GradientFieldNameLookup() {
    }
  
    public Map<String,String> buildLookupMap() throws SQLException {
        
        String sql = "select gradient_map_layer_id, lower(field_column_name) as field_column_name from gradient_map_layer ";
        List<SqlRow> sqlRs = Ebean.createSqlQuery(sql).findList();
        List<Map<String, String>> listMap = ResultSetConverter.convertToListMap(sqlRs);
        this.lookupMap = ListMapUtility.createMap(listMap, "gradient_map_layer_id", "field_column_name");
        return lookupMap;
    }
}