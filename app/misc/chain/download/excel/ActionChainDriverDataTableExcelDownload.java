/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package misc.chain.download.excel;

import gov.hhs.acf.ohs.hses.model.ents.action.spec.ActionChainSpec;
import gov.hhs.acf.ohs.hses.model.ents.action.spec.ActionChainSpecExecutor;
import gov.hhs.acf.ohs.hses.model.ents.action.spec.ActionChainSpecParser;
import gov.hhs.acf.ohs.hses.model.ents.infra.util.FileUtility;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.constants.AppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pingw
 */
public class ActionChainDriverDataTableExcelDownload {

    //    private String sql;
    private List<Map<String,String>> listMap;
    private String columnArray;
    private List<Map<String, String>> fieldReferenceListMap;
    private static final Logger log = LoggerFactory.getLogger(ActionChainDriverDataTableExcelDownload.class);

    public ActionChainDriverDataTableExcelDownload() {

    }

    public String runReport() throws IOException {
        Date timeRunReportStart = new Date();//debug
        String rawContent = FileUtility.getFileContentFromResource(
                "misc/chain/download/excel/chain_conf_data_listing_download.conf").toString();
        Map<String, String> parameterMap = new HashMap<String,String>();
        ActionChainSpecParser me = new ActionChainSpecParser();
        ActionChainSpec spec = me.parseChainSpec(rawContent);
        Date timeParseChainFileEnd = new Date();//debug
        parameterMap.put("$dbLoginName", AppConfig.config.getString("db.default.username"));
        parameterMap.put("$dbPassword", AppConfig.config.getString("db.default.password"));
        parameterMap.put("$dbUrl", AppConfig.config.getString("db.default.url"));
//        parameterMap.put("$sql", sql);
        parameterMap.put("$columnArray", columnArray);
        //spec.debugPrint();
//        System.out.println("Parameter Map Role Id ::: "+parameterMap.get("$roleIdList"));
        ActionChainSpecExecutor executor = new ActionChainSpecExecutor(spec);
        executor.setParameterMap(parameterMap);


        executor.addActionParameterOverride("excelExportData", "listMapData", this.listMap, List.class);

        executor.addActionParameterOverride("excelExportFieldReference", "listMapData", this.fieldReferenceListMap, List.class);


        Object chainReturnObject = executor.execute();
        Date timeExecuteChainFileEnd = new Date();//debug
        log.info("ActionChainDriverDataTableExcelDownload.runReport: - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        log.info("-   ");
        log.info("-  total time parsing+loading time = "
                + ((timeParseChainFileEnd.getTime() - timeRunReportStart.getTime()) / 1000));
        log.info("-   ");
        log.info("-  total time execution time = "
                + ((timeExecuteChainFileEnd.getTime() - timeRunReportStart.getTime()) / 1000));
        log.info(chainReturnObject.toString());
        log.info("ActionChainDriverDataTableExcelDownload.runReport: - - - - - - - - - - - - - - - - - - - - - - - - - - -");

        return (String) chainReturnObject;
    }


    /**
     * @param columnArray the columnArray to set
     */
    public void setColumnArray(String columnArray) {
        this.columnArray = columnArray;
    }

    /**
     * @param listMap the listMap to set
     */
    public void setListMap(List<Map<String,String>> listMap) {
        this.listMap = listMap;
    }

    public void setFieldReferenceMap(Map<String, String> fieldNameDescriptionMap) {
        if (fieldNameDescriptionMap==null){
            return;
        }
        Set<Map.Entry<String, String>> entrySet = fieldNameDescriptionMap.entrySet();
        List<Map<String,String>> listMap = new ArrayList<Map<String,String>>();
        for (Map.Entry<String, String> entry : entrySet){
            Map map = new HashMap<String,String>();
            map.put("Field Name", entry.getKey());
            map.put("Field Description", entry.getValue());
            listMap.add(map);
        }
        this.fieldReferenceListMap = listMap;
    }
}
