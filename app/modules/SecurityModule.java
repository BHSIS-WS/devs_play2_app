package modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.typesafe.config.Config;
import controllers.CustomHttpActionAdapter;
import helper.AuthenticateUser;

import org.pac4j.core.authorization.authorizer.RequireAnyRoleAuthorizer;
import org.pac4j.core.client.Clients;
import org.pac4j.http.client.indirect.FormClient;
import org.pac4j.play.CallbackController;
import org.pac4j.play.LogoutController;
import org.pac4j.play.store.PlayCacheSessionStore;
import org.pac4j.play.store.PlaySessionStore;


import play.Environment;
import play.api.libs.crypto.CookieSigner;
import play.cache.SyncCacheApi;
import play.data.FormFactory;
import play.libs.ws.WSClient;
import play.i18n.MessagesApi;


public class SecurityModule extends AbstractModule {

    private final Config configuration;
    private final String baseUrl;

    public SecurityModule(final Environment environment, final Config configuration) {
        this.configuration = configuration;
        this.baseUrl = configuration.getString("baseUrl");
    }


    @Override
    protected void configure() {
        //bind(HandlerCache.class).to(Pac4jHandlerCache.class);
        //bind(Pac4jRoleHandler.class).to(MyPac4jRoleHandler.class);
        final PlayCacheSessionStore playCacheSessionStore = new PlayCacheSessionStore(getProvider(SyncCacheApi.class));
        playCacheSessionStore.setTimeout(28800); // 8 hour = 8 * 3600 seconds
        //bind(PlaySessionStore.class).to(PlayCacheSessionStore.class);
        bind(PlaySessionStore.class).toInstance(playCacheSessionStore);

        // callback
        final CallbackController callbackController = new CallbackController();
        callbackController.setDefaultUrl("/devs2");
        bind(CallbackController.class).toInstance(callbackController);

        // logout
        final LogoutController logoutController = new LogoutController();
        logoutController.setDefaultUrl("/devs2/login");
        logoutController.setDestroySession(true);
        bind(LogoutController.class).toInstance(logoutController);
    }

    @Provides
    protected FormClient provideFormClient(WSClient ws, MessagesApi messagesApi, FormFactory formFactory, CookieSigner cookieSigner) {
        return new FormClient(baseUrl + "/login", new AuthenticateUser(ws, messagesApi, formFactory, cookieSigner));
    }

    @Provides
    protected org.pac4j.core.config.Config provideConfig(FormClient formClient) {
        final Clients clients = new Clients(baseUrl + "/callback", formClient);

        final org.pac4j.core.config.Config config = new org.pac4j.core.config.Config(clients);
        config.addAuthorizer("devsuser", new RequireAnyRoleAuthorizer<>("admin", "regular_user"));
        config.setHttpActionAdapter(new CustomHttpActionAdapter());
        return config;
    }
}
