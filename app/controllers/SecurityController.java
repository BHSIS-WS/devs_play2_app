package controllers;

import models.ServerNotification;
import models.helper.SecurityConstant;
import org.pac4j.core.config.Config;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.store.PlaySessionStore;
import org.pac4j.http.client.indirect.FormClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.filters.csrf.AddCSRFToken;
import play.i18n.Lang;
import play.i18n.Messages;
import play.i18n.MessagesApi;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

public class SecurityController extends Controller {

    @Inject
    private Config config;
    @Inject
    private PlaySessionStore playSessionStore;
    @Inject
    private WSClient ws;
    @Inject
    private MessagesApi messagesApi;
    private static final Logger log = LoggerFactory.getLogger(SecurityController.class);

    @AddCSRFToken
    public Result loginForm(Http.Request request) {
        Messages messages = messagesApi.preferred(request);
        Optional<String> forwardMessage =  request.session().getOptional("forwardMessage");;
        if(forwardMessage.isPresent()) {
            flash("error", forwardMessage.get());
        }
        if(session("tfaAttemps") != null) {
            flash("tfaAttemps", session("tfaAttemps"));
        }
        if(session("invalidPassCode") != null) {
            flash("invalidPassCode", session("invalidPassCode"));
        }
        final PlayWebContext context = new PlayWebContext(ctx(), playSessionStore);
        final String sessionId = context.getSessionStore().getOrCreateSessionId(context);
        final FormClient formClient = (FormClient) config.getClients().findClient("FormClient");
        ServerNotification serverNotification = ServerNotification.getAllActiveServerNotifications();
        String showNotificationText = "";
        if (serverNotification != null) {
            showNotificationText = serverNotification.message;
        }

        return ok(views.html.loginForm.render(sessionId, formClient.getCallbackUrl(), showNotificationText, messages, request))
                .removingFromSession(request, "tfaAttemps", "invalidPassCode", "forwardMessage");
    }

    public Result resend() {
        String userid;

        if(session().get("tmploginname") != null) {
            userid = session().get("tmploginname");
            WSRequest request = ws.url(SecurityConstant.SEND_PASSCODE_SVC_URL);
            request.addQueryParameter("req.userName", userid);
            request.addQueryParameter("req.app", SecurityConstant.APP_ID);
            request.addQueryParameter("req.appHost", SecurityConstant.APP_HOST);

            String resend_status;
            try {
                CompletionStage<WSResponse> resp = request.post("content");
                CompletionStage<String> respJsonPromise = resp.thenApply(WSResponse::getBody);
                resend_status = respJsonPromise.toCompletableFuture().get();
            } catch (Exception ex) {
                resend_status = "resend_error";
            }
            resend_status = (resend_status == null) ? "resend_error" : resend_status;
            if (resend_status.equals("success")) {
                session("tmploginname", userid);
                flash("success", messagesApi.get(Lang.forCode("en"),"message.web.service.resend.email"));
            } else {
                flash("error", messagesApi.get(Lang.forCode("en"),"error.web.service.resend.email"));
                log.error("resend passcode failed, the status is: " + resend_status);
            }
        } else {
            session().clear();
        }

        return Results.redirect("/devs2/login");
    }


}
