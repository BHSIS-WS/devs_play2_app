package controllers;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import io.ebean.Ebean;
import io.ebean.Update;
import models.FusionMapLayer;
import org.pac4j.core.context.Pac4jConstants;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.store.PlayCacheSessionStore;
import org.pac4j.play.store.PlaySessionStore;
import play.i18n.Messages;
import play.i18n.MessagesApi;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class LayerAdminController extends Controller {

    @Inject
    private Config config;

    @Inject
    protected PlaySessionStore playSessionStore;

    @Inject
    private MessagesApi messagesApi;

    public Result index(Http.Request request) {
        Messages messages = messagesApi.preferred(request);
        String baseUrl = config.getString("baseUrl");
        ObjectNode appConfig = Json.newObject();
        appConfig.put("baseUrl", baseUrl);
        List<FusionMapLayer> fusionMapLayerList = FusionMapLayer.getAllFusionMapLayers();
        String userName = getUserName(request);
        return ok(views.html.dltLayerAdmin.render(appConfig, fusionMapLayerList, userName, messages));
    }

    public Result save() {
        Map<String, String[]> formMap = request().body().asFormUrlEncoded();
        String[] ids = formMap.get("fLayerIds[]");
        String updateStatement;
        int rows;
        if (null == ids || ids.length == 0) {
            updateStatement = "UPDATE FusionMapLayer fml SET fml.disableInd = 1";
            Update<FusionMapLayer> update = Ebean.createUpdate(FusionMapLayer.class, updateStatement);
            rows = update.execute();
        } else {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ids.length; i++) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append(ids[i]);
            }
            String commaSeparatedIds = sb.toString();
            updateStatement = "UPDATE FusionMapLayer SET disableInd = 0 WHERE fusionMapLayerId" +
                    " IN ("+commaSeparatedIds+")";
            Update<FusionMapLayer> update1 = Ebean.createUpdate(FusionMapLayer.class, updateStatement);
            updateStatement = "UPDATE FusionMapLayer SET disableInd = 1 WHERE fusionMapLayerId" +
                    " NOT IN ("+commaSeparatedIds+")";
            Update<FusionMapLayer> update2 = Ebean.createUpdate(FusionMapLayer.class, updateStatement);
            rows = update1.execute();
            rows = update2.execute();
        }
        return ok("Changes saved");
    }

    public Result revert() {
        ObjectNode jsonResponse = Json.newObject();
        ArrayNode idJArr = Json.newArray();
        List<FusionMapLayer> enabledFLayers = FusionMapLayer.getEnabledFusionMapLayers();
        for (int i = 0; i < enabledFLayers.size(); i++) {
            idJArr.add(enabledFLayers.get(i).fusionMapLayerId);
        }
        jsonResponse.set("idsOfEnabledLayers", idJArr);
        return ok(Json.toJson(jsonResponse));
    }

    private String getUserName(Http.Request request) {
        Optional<String> sessionId = request.session().getOptional(Pac4jConstants.SESSION_ID);
        PlayCacheSessionStore store = (PlayCacheSessionStore) playSessionStore;

        CommonProfile userProfile = null;

        if (sessionId.isPresent()) {
            final Map<String, Object> values = store.getStore().get(sessionId.get());

            if (values != null && values.size() > 0) {
                Object value = values.get(Pac4jConstants.USER_PROFILES);
                if (value != null && value instanceof LinkedHashMap) {
                    userProfile = ((LinkedHashMap<String, CommonProfile>)value).get("FormClient");
                }
            }
        }
        return userProfile != null ? userProfile.getUsername() : null;
    }
}
