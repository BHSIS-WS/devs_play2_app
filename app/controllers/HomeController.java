package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.*;
import com.typesafe.config.Config;
import gov.hhs.acf.ohs.hses.model.ents.infra.util.DateUtility;
import helper.FusionLayerQueryHelper;
import helper.util.StringUtility;
import misc.chain.download.excel.ActionChainDriverDataTableExcelDownload;
import models.UserSavedLayerConfig;
import models.helper.ExcelDownloadLinkGenerator;
import models.CustomerFeedback;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.pac4j.core.context.Pac4jConstants;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.play.java.Secure;
import org.pac4j.play.store.PlayCacheSessionStore;
import org.pac4j.play.store.PlaySessionStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.cache.SyncCacheApi;
import play.libs.Json;
import play.mvc.*;
import play.mvc.Http.Request;

import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;
import java.io.File;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.io.*;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
@Secure(clients = "FormClient", authorizers = "devsuser")
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */

    @Inject
    private Config config;

    @Inject
    private SyncCacheApi cache;

    @Inject
    protected PlaySessionStore playSessionStore;

    private static final Logger log = LoggerFactory.getLogger(HomeController.class);

    public Result index() {
        return ok(views.html.index.render());
    }

    public Result test() {
        return ok(views.html.test.render());
    }

    public Result devsMain(Request request) {
        String userName = getUserName(request);
        String mapGoogleApiUrl = config.getString("map.google.api.url");
        ObjectNode appConfig = Json.newObject();
        Long sessionMaxAge = config.getDuration("play.http.session.maxAge", TimeUnit.MINUTES);
        Long sessionWarnAge = config.getDuration("play.http.session.warnAge", TimeUnit.MINUTES);
        String reCaptchaSiteKey = config.getString("reCaptcha.site.key");
        String reCaptchaSecretKey = config.getString("reCaptcha.secret.key");
        String cartoApiKey = config.getString("carto.apikey");
        String cartoUsername = config.getString("carto.username");
        String cartoHostname = config.getString("carto.hostname");
        Boolean cartoShowTab = config.getBoolean("carto.showTab");
        Boolean printShowTab = config.getBoolean("print.showTab");
        String googleMapApiKey = config.getString("map.google.api.key");

        // disable the timeout in development (or local) environment
        Boolean isDevEnvironment = config.getBoolean("isDevEnv");

        appConfig.put("mapGoogleApiUrl", mapGoogleApiUrl);
        appConfig.put("sessionMaxAge", sessionMaxAge);
        appConfig.put("sessionWarnAge", sessionWarnAge);
        appConfig.put("isDevEnv", isDevEnvironment);
        appConfig.put("reCaptcha", reCaptchaSiteKey);
        appConfig.put("userName", userName);
        appConfig.put("cartoApiKey", cartoApiKey);
        appConfig.put("cartoUsername", cartoUsername);
        appConfig.put("cartoHostname", cartoHostname);
        appConfig.put("cartoShowTab", cartoShowTab);
        appConfig.put("printShowTab", printShowTab);
        appConfig.put("googleMapApiKey", googleMapApiKey);
        return ok(views.html.devsMain.render(appConfig));
    }

    public Result printableformat() {
        return ok(views.html.printableformat.render());
    }

    public Result fetchLayerQueryResult(Request request) {
        JsonNode fetchReq = request.body().asJson();
        ObjectNode json = Json.newObject();
        FusionLayerQueryHelper helper = new FusionLayerQueryHelper();
        //layers (fusion_map_layer)
        List<Map<String, String>> inputLayerListMap = helper.extractInputLayersFromRequestRootNode(fetchReq);
        try {
            //check to make sure input layers can be used to extract data
            JsonNode jsonExceptionString = helper.checkInputLayersForException(inputLayerListMap);
            if (jsonExceptionString != null) return ok(jsonExceptionString);

            //Gradient layers Eg. "gradientLayers": [ { "layer_key":"58.tab18_var1","gradientMapLayerId":"58","criteria":"tab18_var1 > 2.9"}]
            List<Map<String, String>> gradientLayerListMap = helper.extractGradientLayersFromRequest(fetchReq);

            //state level options, e.g. "stateLevelOpts":["38","39","40"]
            List<Integer> stateOptList = helper.extractStateLevelOptionsFromRequest(fetchReq);

            //county level options, e.g. "countyLevelOpts":["1","2","3"]
            List<Integer> countyOptList = helper.extractCountyLevelOptionsFromRequest(fetchReq);

            //tableColumns. Eg. "tableColumns":[{"fusionMapLayerId":"25","selectedTableColumnIds":["11","12","13"]}]
            List<Map<String, Object>> tableColumnListMap = helper.extractTableColumnsFromRequest(fetchReq);

            //drawn polygon
            List<Map<String, String>> shapeArrayMap = helper.extractShapeArray(fetchReq);


            //handle case of : has gradient layers but no fusion layers
            if ( (gradientLayerListMap!=null && gradientLayerListMap.size()>0)&& (inputLayerListMap==null||inputLayerListMap.size()<=0)){
                inputLayerListMap = helper.createDefaultInputListMapForGradientLayers(gradientLayerListMap);
            } else if (inputLayerListMap==null||inputLayerListMap.size() <= 0) {
                return ok(json);
            }

            JsonNode pageElement = fetchReq.get("page");
            int page = 1;
            if ((pageElement != null)) {
                try {
                    page =  pageElement.intValue();
                }
                catch (NumberFormatException nfe) {
                    page = 1;
                }
            }

            JsonNode pageSizeElement = fetchReq.get("pageSize");
            int pageSize = 30;
            if ((pageSizeElement != null)) {
                try {
                    pageSize = pageSizeElement.intValue();
                }
                catch (NumberFormatException nfe) {
                    pageSize = 30;
                }
            }

            ObjectNode order = fetchReq.get("order") != null ? (ObjectNode) fetchReq.get("order") : null;
            String orderColumnName = null;
            String orderColumnDir = null;
            if (order != null) {
                orderColumnName = order.get("name").textValue();
                orderColumnDir = order.get("dir").textValue();
            }

            String search = fetchReq.get("search") != null ? fetchReq.get("search").textValue() : null;
            String lat = fetchReq.get("lat") != null ? fetchReq.get("lat").textValue() : null;
            String lng = fetchReq.get("lng") != null ? fetchReq.get("lng").textValue() : null;
            String infoWindowSelectedId = fetchReq.get("infoWindowSelectedId") != null ? fetchReq.get("infoWindowSelectedId").textValue() : null;

            JsonNode invertQueryElement = fetchReq.get("invertQuery");
            boolean invertQuery = (invertQueryElement != null) ? ((BooleanNode)invertQueryElement).booleanValue() : false;

            json = helper.fetchLayerQueryResult(inputLayerListMap, gradientLayerListMap, stateOptList, countyOptList,
                    FusionLayerQueryHelper.gradFieldColMap, page, pageSize, invertQuery, tableColumnListMap,shapeArrayMap,
                    lat, lng, search, orderColumnName, orderColumnDir, infoWindowSelectedId);
        } catch (Exception e) {
            log.error("fetchLayerQueryResult: {}", e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
            String exceptionMsg = e.getLocalizedMessage();
            if (exceptionMsg != null) {
                exceptionMsg = exceptionMsg.replace("\"", "'");
                exceptionMsg = exceptionMsg.replace("\n", ",");
            } else {
                exceptionMsg = "";
            }
            json.put("exceptiontype", e.getClass().getCanonicalName());
            json.put("exceptionmessage", exceptionMsg);
        }

        return ok(Json.toJson(json));
    }

    public Result downloadExcelFile(Request request) {
        String reqBodyStr = request.body().asText();
        String filePath = null;
        try {
            String requestCacheKey = StringUtility.generateMd5Sum(reqBodyStr);
            Optional<String> linkKeyCached = cache.getOptional(requestCacheKey);
            Optional<Object> filePathObj = linkKeyCached.map(linkKey -> {
                log.debug("*** downloadExcelFile excel url cached..., {}", linkKey);
                return cache.getOptional(linkKey).get();
            });
            if (filePathObj.isPresent()) {
                filePath = filePathObj.get().toString();
            } else {
                JsonNode reqJson = Json.parse(reqBodyStr.substring(reqBodyStr.indexOf('=') + 1));
                FusionLayerQueryHelper helper = new FusionLayerQueryHelper();
                //layers (fusion_map_layer)
                List<Map<String, String>> inputLayerListMap = helper.extractInputLayersFromRequestRootNode(reqJson);

                //Gradient layers Eg. "gradientLayers": [ { "layer_key":"58.tab18_var1","gradientMapLayerId":"58","criteria":"tab18_var1 > 2.9"}]
                List<Map<String, String>> gradientLayerListMap = helper.extractGradientLayersFromRequest(reqJson);

                //state level options, e.g. "stateLevelOpts":["38","39","40"]
                List<Integer> stateOptList = helper.extractStateLevelOptionsFromRequest(reqJson);

                //county level options, e.g. "countyLevelOpts":["1","2","3"]
                List<Integer> countyOptList = helper.extractCountyLevelOptionsFromRequest(reqJson);

                //tableColumns. Eg. "tableColumns":[{"fusionMapLayerId":"25","selectedTableColumnIds":["11","12","13"]}]
                List<Map<String, Object>> tableColumnListMap = helper.extractTableColumnsFromRequest(reqJson);

                //drawn polygon
                List<Map<String, String>> shapeArrayMap = helper.extractShapeArray(reqJson);

                //handle case of : has gradient layers but no fusion layers
                if ( (gradientLayerListMap!=null && gradientLayerListMap.size()>0)&& (inputLayerListMap==null||inputLayerListMap.size()<=0)){
                    inputLayerListMap = helper.createDefaultInputListMapForGradientLayers(gradientLayerListMap);
                } else if (inputLayerListMap==null||inputLayerListMap.size() <= 0) {
                    filePath = createErrorFile("Excel file download failed. Please try again later. ");
                    return ok(new java.io.File(filePath)).withHeader("Content-disposition",  "attachment; filename=Download_Error.txt");
                }

                //page and pageSize
                int page = 1;
                int pageSize = 100000;

                //invertQuery
                boolean invertQuery = false;
                JsonNode invertQueryElement = reqJson.get("invertQuery");
                if (invertQueryElement != null) {
                    invertQuery = invertQueryElement.asBoolean();
                }

                FusionLayerQueryHelper fusionLayerQueryHelper = new FusionLayerQueryHelper();
                Map<String, Object> resultMap = fusionLayerQueryHelper.fetchLayerQueryResultMap(inputLayerListMap, gradientLayerListMap
                        , stateOptList, countyOptList, FusionLayerQueryHelper.gradFieldColMap, page, pageSize
                        , invertQuery, tableColumnListMap,shapeArrayMap);

                ActionChainDriverDataTableExcelDownload ac = new ActionChainDriverDataTableExcelDownload();

                ac.setListMap((List<Map<String,String>>) resultMap.get("listMap")) ;

                String columnsString = resultMap.get("columns").toString();

                ac.setColumnArray(columnsString);

                ac.setFieldReferenceMap(fusionLayerQueryHelper.getTooltipMap());

                filePath = ac.runReport();

                String linkKey = new ExcelDownloadLinkGenerator().newLink();
                cache.set(requestCacheKey, linkKey, 300);
                cache.set(linkKey, filePath, 600);
            }

        } catch (Exception e) {
            log.error("Request /devs2/downloadExcel failed: {}", e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
            filePath = createErrorFile("Excel file download failed. Please try again later. ");
            return ok(new java.io.File(filePath)).withHeader("Content-disposition",  "attachment; filename=Download_Error.txt");
        }
        String attachmentFileName = "Layer_data_"+DateUtility.formatDateAsYearMonthDateHourMinuteSecond()+".xls";
        return ok(new java.io.File(filePath)).withHeader("X-Frame-Options", "SAMEORIGIN").withHeader("Content-disposition",  "attachment; filename="+attachmentFileName);
    }

    private String createErrorFile(String message) {
        String filePath = "/tmp/Download_Error.txt";

        try {
            PrintWriter errorFile = new PrintWriter(new File(filePath), "UTF8");
            errorFile.println(message);
            errorFile.close();
        } catch (Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex));
        }
        return filePath;
    }

    public Result fetchSavedLayerConfigList(Request request) {
        String username = getUserName(request);

        if (username == null) {
            return badRequest("Username not in session");
        }

        List<UserSavedLayerConfig> layerConfigList = UserSavedLayerConfig.getAllSavedLayerConfigsForUser(username);
        ObjectNode result = Json.newObject();
        result.putPOJO("layerConfigList", layerConfigList);
        return ok(Json.toJson(result));
    }


    public Result saveLayerConfigForUser(Request request) {
        String username = getUserName(request);

        if (username == null) {
            return badRequest("Username not in session");
        }

        JsonNode json = request.body().asJson();
        if (json == null) {
            return badRequest("Expecting JSON Data");
        }

        String nameString, descriptionString, configString;
        JsonNode layerConfigNameElement = json.get("layerConfigurationName");
        if (layerConfigNameElement == null) {
            return badRequest("layerConfigurationName param missing");
        }
        JsonNode layerConfigDescriptionElement = json.get("layerConfigurationDescription");
        if (layerConfigDescriptionElement == null) {
            return badRequest("layerConfigurationDescription param missing");
        }
        JsonNode layerConfig = json.get("state");
        if (layerConfig == null) {
            return badRequest("state is required");
        }

        try {
            nameString = layerConfigNameElement.asText();
            descriptionString = layerConfigDescriptionElement.asText();
            configString = layerConfig.toString();
        } catch(Exception e) {
            log.error(e.toString());
            log.error(ExceptionUtils.getStackTrace(e));
            return badRequest("Data error: " + e.toString());
        }

        List<UserSavedLayerConfig> configList = UserSavedLayerConfig.getAllSavedLayerConfigsForUser(username);
        for(UserSavedLayerConfig config : configList) {
            if (config.configName.equals(nameString) && !config.disabledInd) {
                ObjectNode errorResult = Json.newObject();
                errorResult.put("error", true);
                return ok(Json.toJson(errorResult));
            }
        }
        try {
            UserSavedLayerConfig savedConfig = UserSavedLayerConfig.create(username, nameString, descriptionString, configString);
        } catch(Exception e) {
            log.error(e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
            return badRequest("Database error: " + e.toString());
        }

        configList = UserSavedLayerConfig.getAllSavedLayerConfigsForUser(username);
        ObjectNode result = Json.newObject();
        result.putPOJO("layerConfigList", configList);
        return ok(Json.toJson(result));
    }

    public Result deleteLayerConfigForUser(Request request) {
        String username = getUserName(request);

        if (username == null) {
            return badRequest("Username not in session");
        }

        JsonNode json = request.body().asJson();
        if (json == null) {
            return badRequest("Expecting JSON Data");
        }

        Integer userSavedLayerConfigId;
        JsonNode userSavedLayerConfigIdElement = json.get("userSavedLayerConfigId");
        if (userSavedLayerConfigIdElement == null) {
            return badRequest("userSavedLayerConfigId param missing");
        }

        try {
            userSavedLayerConfigId = userSavedLayerConfigIdElement.intValue();
        } catch(Exception e) {
            log.error(e.toString());
            log.error(ExceptionUtils.getStackTrace(e));
            return badRequest("Data error: " + e.toString());
        }

        try {
            UserSavedLayerConfig.deleteSavedLayerConfigForUser(username, userSavedLayerConfigId);
        } catch(Exception e) {
            log.error(e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
            return badRequest("Database error: " + e.toString());
        }

        return ok();
    }

    public Result saveCustomerFeedback(Request request) {
        ObjectNode jsonResponse = Json.newObject();
        JsonNode json = request.body().asJson();
        JsonNode data = json.get("formDataObj");
        String gRecaptchaResponse = json.get("gRecaptchaResponse").toString();
        String feedbackName = StringUtility.trimAndUnquote(data.get("feedbackName").toString());
        String feedbackEmail = StringUtility.trimAndUnquote(data.get("feedbackEmail").toString());
        String feedbackType = StringUtility.trimAndUnquote(data.get("feedbackType").toString());
        String feedbackSubject = StringUtility.trimAndUnquote(data.get("feedbackSubject").toString());
        String feedbackContent = StringUtility.trimAndUnquote(data.get("feedbackContent").toString());

        if(!verifyRecaptcha(gRecaptchaResponse)) {
            jsonResponse.put("result", "wrongCaptchaCode");
            jsonResponse.put("message", "Invalid captcha response, please try again...");
            return ok(Json.toJson(jsonResponse));
        }

        try {
            CustomerFeedback newCustomerFeedback = new CustomerFeedback();
            newCustomerFeedback.senderName = feedbackName;
            newCustomerFeedback.senderEmail = feedbackEmail;
            try {
                newCustomerFeedback.feedbackType = Integer.parseInt(feedbackType);
            }
            catch (NumberFormatException nfe) {
                log.error(ExceptionUtils.getStackTrace(nfe));
                newCustomerFeedback.feedbackType = 1;
            }
            newCustomerFeedback.subject = feedbackSubject;
            newCustomerFeedback.content = feedbackContent;
            newCustomerFeedback.readFlag = 0;

            newCustomerFeedback.save();
        } catch(Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex));
            jsonResponse.put("result", "error");
            jsonResponse.put("message", "Error while submitting the data.");
            return ok(Json.toJson(jsonResponse));
        }
        jsonResponse.put("result", "succeeded");
        jsonResponse.put("message", "Your form has been submitted successfully.");
        return ok(Json.toJson(jsonResponse));
    }


    public boolean verifyRecaptcha(String gRecaptchaResponse) {
        String url = "https://www.google.com/recaptcha/api/siteverify";
        String secret = config.getString("reCaptcha.secret.key");

        if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
            return false;
        }

        String gRecaptchaResponseTrim = gRecaptchaResponse.replaceAll("^\"|\"$", "");
        try {
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Java client");
            String postParams = "secret=" + secret + "&response=" + gRecaptchaResponseTrim;
            // Send post request
            con.setDoOutput(true);
            DataOutputStream dataOutputStream = new DataOutputStream(con.getOutputStream());
            dataOutputStream.writeBytes(postParams);
            dataOutputStream.flush();
            dataOutputStream.close();
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            //parse JSON response and return 'success' value
            StringReader sReader = new StringReader(response.toString());
            JsonReader jReader = javax.json.Json.createReader(sReader);
            JsonObject detailObj = jReader.readObject();
            return detailObj.getBoolean("success");
        } catch (ProtocolException | MalformedURLException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return false;
        } catch (IOException e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return false;
        }
    }

    private String getUserName(Request request) {
        Optional<String> sessionId = request.session().getOptional(Pac4jConstants.SESSION_ID);
        log.info("retrieved sessionId: {}", sessionId.get());
        PlayCacheSessionStore store = (PlayCacheSessionStore) playSessionStore;

        CommonProfile userProfile = null;

        if (sessionId.isPresent()) {
            final Map<String, Object> values = store.getStore().get(sessionId.get());

            if (values != null && values.size() > 0) {
                Object value = values.get(Pac4jConstants.USER_PROFILES);
                if (value != null && value instanceof LinkedHashMap) {
                    userProfile = ((LinkedHashMap<String, CommonProfile>)value).get("FormClient");
                }
                log.info("profile value: {}", value);
            }
        }
        return userProfile != null ? userProfile.getUsername() : null;
    }

}
