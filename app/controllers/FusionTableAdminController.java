package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;

public class FusionTableAdminController extends Controller {

    @Inject
    private Config config;

    public Result adminFusionTableStyles() {
        String adminFusionTableClientId = config.getString("admin.fusion.table.client.id");
        String adminFusionTableRedirect = config.getString("admin.fusion.table.style.redirect");
        ObjectNode appConfig = Json.newObject();
        appConfig.put("clientId", adminFusionTableClientId);
        appConfig.put("redirect", adminFusionTableRedirect);
        return ok(views.html.adminFusionTableStyles.render(appConfig));
    }

    public Result adminFusionTableTemplates() {
        String adminFusionTableClientId = config.getString("admin.fusion.table.client.id");
        String adminFusionTableRedirect = config.getString("admin.fusion.table.template.redirect");
        ObjectNode appConfig = Json.newObject();
        appConfig.put("clientId", adminFusionTableClientId);
        appConfig.put("redirect", adminFusionTableRedirect);
        return ok(views.html.adminFusionTableTemplates.render(appConfig));
    }

}
