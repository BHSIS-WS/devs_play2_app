package controllers;

import com.fasterxml.jackson.databind.node.*;
import org.pac4j.play.java.Secure;
import play.api.Environment;
import play.mvc.Controller;
import play.mvc.Http.Request;
import play.mvc.RangeResults;
import models.DevsHelpTopic;

import play.mvc.Result;
import play.libs.Json;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

@Secure(clients = "FormClient", authorizers = "devsuser")
public class HelpController extends Controller {

    private final Environment environment;

    @Inject
    public HelpController(Environment environment){
        this.environment = environment;
    }

    public Result helpVideo(Request request, Integer id) {
        File videoFile = environment.getFile("videos/Tutorial1.mp4");
        return RangeResults.ofFile(request, videoFile);
    }

    public Result fetchHelpTopicList() {

        List<DevsHelpTopic> helpTopicList =  DevsHelpTopic.getAllDevHelpTopic();
        ObjectNode result = Json.newObject();
        result.putPOJO("HelpTopicDesc", helpTopicList);
        return ok(Json.toJson(result));
    }
}