package controllers;

import org.pac4j.core.context.HttpConstants;
import org.pac4j.play.PlayWebContext;
import org.pac4j.play.http.PlayHttpActionAdapter;
import play.mvc.Result;
import play.mvc.Results;

public class CustomHttpActionAdapter extends PlayHttpActionAdapter {

    @Override
    public Result adapt(int code, PlayWebContext context) {
        if (code ==  HttpConstants.UNAUTHORIZED) {
            return Results.redirect("/devs2/login");
        } else if (code == HttpConstants.FORBIDDEN) {
            return Results.forbidden("Access Denied");
        } else {
            return super.adapt(code, context);
        }
    }

}
