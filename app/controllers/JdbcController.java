package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import models.helper.RefTableHelper;
import org.pac4j.play.java.Secure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http.Request;
import play.mvc.Result;

@Secure(clients = "FormClient", authorizers = "devsuser")
public class JdbcController extends Controller {

    private RefTableHelper helper;

    private static final Logger logger = LoggerFactory.getLogger(JdbcController.class);

    public JdbcController() {
        helper = new RefTableHelper();
    }

    public Result getRefs() {
        JsonNode result = helper.getRefs();
        return ok(result);
    }

    public Result getGeoJson(Request request, Integer layerId) {
        JsonNode reqJson = request.body().asJson();
        JsonNode parameterJsonNode = reqJson.get("parameterJson");
        String parameterJson = parameterJsonNode != null ? parameterJsonNode.asText(): "{}";
        //System.out.println("&&&&&& (layerId: " + layerId + ") - 'getGeoJson()' parameterJson: " + parameterJson);
        JsonNode result = helper.getGeoJson(layerId, parameterJson);
        //System.out.println("&&&&&& result: " + result.toString().substring(0, 200) + "... ");
        return ok(result);
    }

    public Result getGeoJsonForCarto(Integer layerId) {
        //System.out.println("STARTING geoJson FOR CARTO (For layer: " + layerId + ")...");
        JsonNode reqJson = request().body().asJson();
        ArrayNode layerResultArray = Json.newArray();

        System.out.println(reqJson);

        //System.out.println("----- Starting processing for layer " + layerId + " ------");
        JsonNode parameterJsonNode = reqJson;
        String parameterJson = parameterJsonNode != null? parameterJsonNode.asText(): "{}";
        //System.out.println("<><> layerId: " + layerId + " -- parameterJson: " + parameterJson);
        String whereString = helper.getGeoJsonForCarto(layerId, parameterJson);
        JsonNode layerResult = Json.toJson(whereString.toString().replace(" ogc_fid ", " cartodb_id ").replace("\n\t", " "));
        layerResultArray.add(layerResult);
        //System.out.println("Layer Result for layerId = " + layerId + ": " + layerResult.toString());

        return ok(layerResult);
    }

    public Result getGradientGeoJson(Integer layerId, String filterJson, Float simplTolr) {
        JsonNode result = helper.getGradientGeoJson(layerId, filterJson, simplTolr);
        return ok(result);
    }
}



