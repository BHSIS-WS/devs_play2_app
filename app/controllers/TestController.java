package controllers;

import play.mvc.Controller;
import play.mvc.Result;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class TestController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result test() {
        return ok(views.html.test1.test.render());
    }
    public Result test1() {
        return ok(views.html.test1.test1.render());
    }
    public Result test2() {
        return ok(views.html.test1.test2.render());
    }
    public Result test3() {
        return ok(views.html.test1.test3.render());
    }
    public Result test4() { return ok(views.html.test1.test4.render()); }

}
