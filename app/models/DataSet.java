package models;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import io.ebean.*;

/**
 *
 * @author Pingw
 */
@Entity
@Table(name = "data_set", schema = "dlt")
public class DataSet extends Model {
    @Id
    @Column(name = "data_set_id")
    public Integer dataSetId;

    @Column(name = "name")
    public String name;

    @Column(name = "description")
    public String description;

    @Column(name = "comment")
    public String comment;
    
    @Column(name = "disable_ind")
    public Integer disableInd;
    
    @Column(name = "data_level")
    public String dataLevel;

    @Transient
    public List<GradientMapLayer> gradientMapLayers;

    public static List<DataSet> getAllGradientLayerDataSet() {
        String query = "SELECT ds.data_set_id, ds.name, ds.description, ds.comment, ds.disable_ind, ds.data_level " +
                " FROM dlt.data_set ds " +
                "    where ds.data_set_id in ( " +
                "        select g.data_set_id " +
                "        from dlt.gradient_map_layer g " +
                "    ) " +
                "    order by ds.data_set_id";
        RawSql rawSql = RawSqlBuilder.parse(query).create();
        return Ebean.find(DataSet.class).setRawSql(rawSql).findList();
    }

    public static List<DataSet> getEnabledGradientLayerDataSet() {
        return _getEnabledOrDisabledGradientLayerDataSet(true);
    }

    public static List<DataSet> getDisabledGradientLayerDataSet() {
        return _getEnabledOrDisabledGradientLayerDataSet(false);
    }

    /*
    * boolean enabledOrDisabled param - true to get enabled data set, false to
    *                                    get disabled data set
    */
    private static List<DataSet> _getEnabledOrDisabledGradientLayerDataSet(
                           boolean enabledOrDisabled) {
        int disableIndicator = enabledOrDisabled ? 0 : 1;
        String query = "SELECT ds.data_set_id, ds.name, ds.description, ds.comment, ds.disable_ind, ds.data_level " +
                " FROM dlt.data_set ds " +
                "    where ds.data_set_id in ( " +
                "        select g.data_set_id " +
                "        from dlt.gradient_map_layer g" +
                "    ) " +
                "    and ds.disable_ind=:disableInd" +
                "    order by ds.data_set_id";
        RawSql rawSql = RawSqlBuilder.parse(query).create();
        return Ebean.find(DataSet.class).setRawSql(rawSql)
                .setParameter("disableInd", disableIndicator).findList();
    }

    @Override
    public String toString() {
        return "DataSet{" + "name=" + name + ", gradientMapLayers=" + gradientMapLayers + '}';
    }
}
