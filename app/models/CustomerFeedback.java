package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import io.ebean.Model;

@Entity
@Table(name = "customer_feedback", schema="dlt")
public class CustomerFeedback extends Model {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY, generator = "IDSEQ")
    @SequenceGenerator(name="IDSEQ", sequenceName = "customer_feedback_seq", allocationSize = 1)
    @Column(name = "customer_feedback_id")
    public Integer customerFeedbackId;

    @Column(name = "feedback_type")
    public Integer feedbackType;
    
    @Column(name = "sender_name")
    public String senderName;
    
    @Column(name = "sender_email")
    public String senderEmail;

    @Column(name = "subject")
    public String subject;

    @Column(name = "content")
    public String content;

    @Column(name = "read_flag")
    public Integer readFlag;

}