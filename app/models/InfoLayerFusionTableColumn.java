package models;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.ebean.Finder;
import io.ebean.Model;

/**
 *
 * @author WilliamL
 */
@Entity
@Table(name = "info_layer_fusion_table_column", schema="dlt")
public class InfoLayerFusionTableColumn extends Model {

    private static final Finder<Long, InfoLayerFusionTableColumn> find = new Finder<>(InfoLayerFusionTableColumn.class);

    @Id
    @Column(name = "info_layer_fusion_table_column_id")
    public Integer infoLayerFusionTableColumnId;

    @Column(name = "table_name")
    public String tableName;

    @Column(name = "column_name")
    public String columnName;

    @Column(name = "description")
    public String description;
    
    @Column(name = "data_type")
    public String dataType;
    
    @Column(name = "fusion_map_layer_id")
    public Integer fusionMapLayerId;

    @Column(name = "display_order")
    public Integer displayOrder;

    @Column(name = "disable_ind")
    public Integer disableInd;

    @Column(name = "use_for_selection")
    public Boolean useForSelection;

    @Column(name = "disable_info_ind")
    public Integer disableInfoInd;

    @Column(name = "display_info_col_name")
    public String displayInfoColumnName;

    @Column(name = "display_info_order")
    public Integer displayInfoOrder;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append(" infoLayerFusionTableColumnId : ")
                .append(this.infoLayerFusionTableColumnId)
                .append(", tableName : ")
                .append(this.tableName)
                .append(", columnName : ")
                .append(this.columnName)
                .append(", description : ")
                .append(this.description)
                .append(", fusionMapLayerId : ")
                .append(this.fusionMapLayerId)        
                .append(", displayOrder : ")
                .append(this.displayOrder)
                .append(", disable_ind : ")
                .append(this.disableInd)
                .append(", use_for_selection : ")
                .append(this.useForSelection)
                .append(", disable_info_ind : ")
                .append(this.disableInfoInd)
                .append(", display_info_col_name : ")
                .append(this.displayInfoColumnName)
                .append(", display_info_order : ")
                .append(this.displayInfoOrder)
                .append(" }\n");

        return sb.toString();
    }

    public static List<InfoLayerFusionTableColumn> getAllRecords() {
        List<InfoLayerFusionTableColumn> recs = InfoLayerFusionTableColumn.find.query().where()
                .isNull("disable_ind")
                .orderBy("fusion_map_layer_id, display_order, info_layer_fusion_table_column_id")
                .findList();
        return recs;
    }

    public static List<InfoLayerFusionTableColumn> getUseForSelectionRecords(final String tableName) {
        List<InfoLayerFusionTableColumn> recs = InfoLayerFusionTableColumn.find.query().where()
                .isNull("disable_ind")
                .orderBy("fusion_map_layer_id, display_order, info_layer_fusion_table_column_id")
                .findList();
        List<InfoLayerFusionTableColumn> rtn = recs.stream().filter(info -> (info.useForSelection == true && tableName.equalsIgnoreCase(info.tableName))).collect(Collectors.toList());
        return rtn;
    }

    public static List<String> getLayerColumnNames(final Integer layerId) {
        List<InfoLayerFusionTableColumn> recs = InfoLayerFusionTableColumn.find.query().where()
                .eq("fusion_map_layer_id", layerId)
                .and()
                .isNull("disable_ind")
                .orderBy("fusion_map_layer_id, display_order, info_layer_fusion_table_column_id")
                .findList();
        List<String> columns = recs.stream().map(info -> info.columnName).collect(Collectors.toList());
        return columns;
    }
}
