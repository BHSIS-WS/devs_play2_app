package models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import io.ebean.Ebean;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.SqlRow;

/**
 *
 * @author Sunny
 */

@Entity
@Table(name = "devs_help_topic", schema = "dlt")

public class DevsHelpTopic extends Model{
    private static final Finder<Long, DevsHelpTopic> find = new Finder<>(DevsHelpTopic.class);

    @Id
    @Column(name = "devs_help_topic_id")
    public Integer devsHelpTopicId;

    @Column(name = "topic_title")
    public String topicTitle;

    @Column(name = "content")
    public String content;

    @Column(name = "video_id")
    public Integer videoId;

    @Column(name = "display_order")
    public Integer displayOrder;

    @Column(name = "source_url")
    public String sourceUrl;

    @Column(name = "embed_url")
    public String embedUrl;

    public static List<DevsHelpTopic> getAllDevHelpTopic(){
        return DevsHelpTopic.find.query().orderBy("devs_help_topic_id").findList();
    }

    public Integer getDevsHelpTopicId() {
        return devsHelpTopicId;
    }
    public String getTopicTitle(){
        return topicTitle;
    }
    public String getContent(){
        return content;
    }
    public Integer getVideoId() {
        return videoId;
    }
    public Integer getDisplayOrder() {
        return displayOrder;
    }
    public String getSourceUrl() { return sourceUrl; }
    public String getEmbedUrl() { return embedUrl; }
}
