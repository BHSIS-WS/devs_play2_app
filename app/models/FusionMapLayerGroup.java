package models;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import io.ebean.Ebean;
import io.ebean.Model;
import io.ebean.RawSql;
import io.ebean.RawSqlBuilder;

/**
 *
 * @author Pingw
 */
@Entity
@Table(name = "fusion_map_layer_group", schema = "dlt")
public class FusionMapLayerGroup extends Model {
    @Id
    @Column(name = "fusion_map_layer_group_id")
    public Integer fusionMapLayerGroupId;

    @Column(name = "name")
    public String name;

    @Column(name = "description")
    public String description;
    
    @Column(name = "layer_type")
    public String layerType;

    @Transient
    public List<FusionMapLayer> fusionMapLayers;

    public static List getAllFusionMapLayerGroup() {
        String query = "SELECT fusion_map_layer_group_id, name, description, layer_type " +
                " FROM dlt.fusion_map_layer_group " +
                "    where fusion_map_layer_group_id in ( " +
                "    select fusion_map_layer_group_id " +
                "    from dlt.fusion_map_layer where disable_ind is null or disable_ind = 0) " +
                "    order by fusion_map_layer_group_id";

        RawSql rawSql = RawSqlBuilder.parse(query).create();
        return Ebean.find(FusionMapLayerGroup.class).setRawSql(rawSql).findList();
    }

 
    @Override
    public String toString() {
        return "FusionMapLayerGroup{" + "name=" + name + ", fusionMapLayers=" + fusionMapLayers + '}';
    }
}
