package models;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import io.ebean.Finder;
import io.ebean.Model;

/**
 *
 * @author williaml
 */
@Entity
@Table(name = "gradient_map_layer", schema = "dlt")
public class GradientMapLayer extends Model {
    public static final Finder<Long, GradientMapLayer> find = new Finder<>(GradientMapLayer.class);
    public static List<GradientMapLayer> getAllGradientMapLayer() {
        return GradientMapLayer.find.query()
                .where()
                .eq("disable_ind", 0)
                .orderBy("display_order")
                .findList();
    }
    
    @Id
    @Column(name = "gradient_map_layer_id")
    public Integer gradientMapLayerId;

    @Column(name = "name")
    public String name;

    @Column(name = "data_source")
    public String dataSource;
    
     @Column(name = "url_source")
    public String  urlSource;

    @Column(name = "fusion_geom_column")
    public String fusionGeomColumn;

    @Column(name = "db_table_name")
    public String dbTableName;

    @Column(name = "field_column_name")
    public String fieldColumnName;

    @Column(name = "fusion_options_json_string")
    public String fusionOptionsJsonString;

    @Column(name = "fusion_legend_json_string")
    public String fusionLegendJsonString;
    
    @Column(name = "data_set_id")
    public Integer dataSetId;
    
    @Column(name = "disable_ind")
    public Integer disableInd;
    
    @Column(name = "display_order")
    public Integer displayOrder;

    @Column(name = "carto_style_string")
    public String cartoStyleString;

    @Column(name = "layer_info_window_template")
    public String layerInfoWindowTemplate;

    @Transient
    public Integer fieldColumnId;

    @Override
    public String toString() {
        return "GradientMapLayer{" +
                "gradientMapLayerId=" + gradientMapLayerId +
                ", name='" + name + '\'' +
                ", dataSource='" + dataSource + '\'' +
                ", urlSource='" + urlSource + '\'' +
                ", fusionGeomColumn='" + fusionGeomColumn + '\'' +
                ", dbTableName='" + dbTableName + '\'' +
                ", fieldColumnName='" + fieldColumnName + '\'' +
                ", fusionOptionsJsonString='" + fusionOptionsJsonString + '\'' +
                ", fusionLegendJsonString='" + fusionLegendJsonString + '\'' +
                ", dataSetId=" + dataSetId +
                ", disableInd=" + disableInd +
                ", displayOrder=" + displayOrder +
                ", fieldColumnId=" + fieldColumnId +
                ", cartoStyleString=" + cartoStyleString +
                ", layerInfoWindowTemplate=" + layerInfoWindowTemplate +
                '}';
    }
}
