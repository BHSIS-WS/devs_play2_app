package models;

import com.fasterxml.jackson.databind.JsonNode;
import io.ebean.annotation.Sql;
import javax.persistence.Entity;

@Entity
@Sql
public class RefTableJson {
    public JsonNode refs;
}
