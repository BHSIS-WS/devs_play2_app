package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LayerColumnInfo {

    @Id
    @Column(name = "fusion_map_layer_id")
    public Integer fusionMapLayerId;

    @Column(name = "layer_name")
    public String layerName;

    @Column(name = "db_columns")
    public String dbColumns;

    @Column(name = "db_column_ids")
    public String dbColumnIds;

    @Column(name = "db_table_name")
    public String dbTableName;

    @Override
    public String toString() {
        return "LayerColumnInfo{" +
                "fusionMapLayerId=" + fusionMapLayerId +
                ", layerName='" + layerName + "'" +
                ", dbColumns='" + dbColumns + "'" +
                ", dbColumnIds='" + dbColumnIds + "'" +
                ", dbTableName='" + dbTableName + "'" +
                "}";
    }
}
