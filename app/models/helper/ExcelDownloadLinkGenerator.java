/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package models.helper;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 *
 * @author pingw
 */
public class ExcelDownloadLinkGenerator {

    private SecureRandom random = new SecureRandom();

    public String newLink() {
        return new BigInteger(130, random).toString(32);
    }

    public static String test() {
        StringBuilder sb = new StringBuilder();
        ExcelDownloadLinkGenerator me = new ExcelDownloadLinkGenerator();
        sb.append("1\t ").append(me.newLink())
                .append("\t2\t").append(me.newLink())
                .append("\t3\t").append(me.newLink());
        return sb.toString();
    }

}
