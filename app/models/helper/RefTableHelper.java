package models.helper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import helper.GeoJsonQueryHelper;
import io.ebean.Ebean;
import io.ebean.Query;
import io.ebean.RawSql;
import io.ebean.RawSqlBuilder;
import models.FusionMapLayer;
import models.GeoJson;
import models.GradientMapLayer;
import models.RefTableJson;
import models.InfoGradientLayerColumn;
import models.InfoLayerFusionTableColumn;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RefTableHelper {
    private static final Logger logger = LoggerFactory.getLogger(RefTableHelper.class);

    public JsonNode getRefs() {
        Query<RefTableJson> query = Ebean.find(RefTableJson.class);
        RawSql rawSql = RawSqlBuilder.parse(refsQueryString()).columnMapping("refs", "refs").create();

        List<RefTableJson> results = query.setRawSql(rawSql).findList();
        JsonNode ref = Json.newObject();
        ArrayNode refArray = Json.newArray();
        for (RefTableJson oneRef: results) {
            refArray.add(oneRef.refs);
        }
        ((ObjectNode) ref).putPOJO("ref", refArray);

        return ref;
    }

    private String refsQueryString() {
        String union = "\nunion all\n";

        StringBuilder queryString = new StringBuilder();
        queryString.append(criteriaCodesQueryString())
                .append(union).append(layerFilteringCriteriaQueryString())
                .append(union).append(gradientLayerDataSetsQueryString())
                .append(union).append(fusionMapLayerGroupsQueryString())
                .append(union).append(fusionMapLayersQueryString())
                .append(union).append(gradientMapLayersQueryString());

        return queryString.toString();
    }

    public String criteriaCodesQueryString() {
        Map<String, String[]> tableInfo = new HashMap<String, String[]>();
        tableInfo.put("jsonName", new String[]{"criteriacodes"});
        tableInfo.put("tableName", new String[]{"dlt.criteria_code"});
        tableInfo.put("columns",
                new String[] {
                        "criteria_code_id as id",
                        "criteria_name",
                        "criteria_type",
                        "display_order",
                        "display_column_name"
                });
        tableInfo.put("orderBy", new String[] {"criteria_code_id"});
        tableInfo.put("whereClause", null);
        return buildQueryString(tableInfo);
    }

    public String layerFilteringCriteriaQueryString() {
        Map<String, String[]> tableInfo = new HashMap<String, String[]>();
        tableInfo.put("jsonName", new String[]{"layerfilteringcriteria"});
        tableInfo.put("tableName", new String[]{"dlt.fusion_map_layer_filter"});
        tableInfo.put("columns",
                new String[] {
                        "layer_filtering_criteria_id as id",
                        "name",
                        "fusion_map_layer_id",
                        "display_order",
                        "criteria_value",
                        "criteria_code_id"
                });
        tableInfo.put("orderBy", new String[] {"display_order"});
        tableInfo.put("whereClause", null);
        return buildQueryString(tableInfo);
    }

    public String gradientLayerDataSetsQueryString() {
        Map<String, String[]> tableInfo = new HashMap<String, String[]>();
        tableInfo.put("jsonName", new String[]{"gradientlayerdatasets"});
        tableInfo.put("tableName", new String[]{"dlt.data_set"});
        tableInfo.put("columns",
                new String[] {
                        "data_set_id as id",
                        "name",
                        "description",
                        "comment",
                        "disable_ind",
                        "data_level"
                });
        tableInfo.put("orderBy", new String[] {"data_set_id"});
        StringBuilder whereClause = new StringBuilder();
        whereClause.append("WHERE data_set_id In \n");
        whereClause.append("(SELECT data_set_id FROM dlt.gradient_map_layer) \n");
        whereClause.append("AND disable_ind = 0\n");
        tableInfo.put("whereClause", new String[] {whereClause.toString()});
        return buildQueryString(tableInfo);
    }

    public String fusionMapLayerGroupsQueryString() {
        Map<String, String[]> tableInfo = new HashMap<String, String[]>();
        tableInfo.put("jsonName", new String[]{"fusionmaplayergroups"});
        tableInfo.put("tableName", new String[]{"dlt.fusion_map_layer_group"});
        tableInfo.put("columns",
                new String[] {
                        "fusion_map_layer_group_id as id",
                        "name",
                        "description",
                        "layer_type"
                });
        tableInfo.put("orderBy", new String[] {"fusion_map_layer_group_id"});
        StringBuilder whereClause = new StringBuilder();
        whereClause.append("WHERE fusion_map_layer_group_id IN \n");
        whereClause.append("(SELECT fusion_map_layer_group_id FROM dlt.fusion_map_layer\n");
        whereClause.append("WHERE disable_ind IS NULL OR disable_ind = 0)\n");
        tableInfo.put("whereClause", new String[] {whereClause.toString()});
        return buildQueryString(tableInfo);
    }

    public String fusionMapLayersQueryString() {
        Map<String, String[]> tableInfo = new HashMap<String, String[]>();
        tableInfo.put("jsonName", new String[]{"fusionmaplayers"});
        tableInfo.put("tableName", new String[]{"dlt.fusion_map_layer"});
        tableInfo.put("columns",
                new String[] {
                        "fusion_map_layer_id as id",
                        "db_table_name",
                        "layer_desc",
                        "layer_name",
                        "fusion_geom_column",
                        "fusion_legend_json_string",
                        "fusion_options_json_string",
                        "fusion_not_show_where_string",
                        "layer_key",
                        "disable_ind",
                        "display_order",
                        "db_geom_column",
                        "url_source",
                        "query_type",
                        "fusion_map_layer_group_id",
                        "query_type_display",
                        "geom_type",
                        "style_column_json",
                        "layer_config_json_string",
                        "layer_info_window_template",
                        "carto_style_string"
                });
        tableInfo.put("orderBy", new String[] {"display_order"});
        StringBuilder whereClause = new StringBuilder();
        whereClause.append("WHERE disable_ind IS NULL OR disable_ind = 0 \n");
        tableInfo.put("whereClause", new String[] {whereClause.toString()});
        return buildQueryString(tableInfo);
    }

    public String gradientMapLayersQueryString() {
        Map<String, String[]> tableInfo = new HashMap<String, String[]>();
        tableInfo.put("jsonName", new String[]{"gradientmaplayers"});
        tableInfo.put("tableName", new String[]{"dlt.gradient_map_layer"});
        tableInfo.put("columns",
                new String[] {
                        "gradient_map_layer_id as id",
                        "name",
                        "data_source",
                        "fusion_geom_column",
                        "db_table_name",
                        "field_column_name",
                        "fusion_options_json_string",
                        "fusion_legend_json_string",
                        "color_boundaries_json_string",
                        "data_set_id",
                        "disable_ind",
                        "display_order",
                        "url_source",
                        "description",
                        "carto_style_string",
                        "layer_info_window_template"
                });
        tableInfo.put("orderBy", new String[] {"display_order"});
        StringBuilder whereClause = new StringBuilder();
        whereClause.append("WHERE disable_ind = 0\n");
        tableInfo.put("whereClause", new String[] {whereClause.toString()});
        return buildQueryString(tableInfo);
    }

    public JsonNode getGeoJson(Integer layerId, String parameterJson) {
        StringBuilder queryString = new StringBuilder();
        String tableName = tableNameForLayer(layerId);
        GeoJsonQueryHelper queryHelper = new GeoJsonQueryHelper(layerId, parameterJson);
        queryHelper.debugPrint();
        List<String> additionalColumnList = queryHelper.getAdditionalFieldsJavaList();
        List<InfoLayerFusionTableColumn> layerInfo = getLayerInfo(layerId);
        String whereString = "";
        JsonNode result = Json.parse("{}");

        try {
            whereString = queryHelper.getNormalLayerWhereStringThatShowOnlyCommonToAllLayer();
            queryString.append("select '{\"type\": \"FeatureCollection\"\n");
            if (additionalColumnList!=null){
                queryString.append(", \"additionalFields\": ").append( queryHelper.getAdditionalFields()).append("\n");
            }
            queryString.append(",\"features\":'|| json_agg (row_to_json ( data)) || '}' as geoJsonString\n");
            queryString.append("from\n");
            queryString.append("(SELECT\n");
            queryString.append("'Feature' as type,\n");
            queryString.append("ogc_fid as id,\n");
            queryString.append("json_build_object(");
            for(int i = 0; i < layerInfo.size(); i++) {
                queryString.append("'").append(layerInfo.get(i).columnName).append("', json_build_object('displayName', '")
                        .append(layerInfo.get(i).displayInfoColumnName).append("', 'value', fl.").append(layerInfo.get(i).columnName).append(")");
                if(i < layerInfo.size()-1) {
                    queryString.append(",\n\t");
                }
            }
            queryString.append(") as detail,\n");
            queryString.append("geometry_json as geometry\n");
            if (additionalColumnList!=null && additionalColumnList.size()>0){
                for (String column: additionalColumnList) {
                    queryString.append(" ,").append(column);
                }
                queryString.append("\n");
            }
            queryString.append("FROM\n").append("dlt.").append(tableName).append(" as fl\n");
            if (!whereString.isEmpty()) {
                queryString.append("WHERE\n").append(whereString).append("\n");
            }
            queryString.append(")as data");
            Query<GeoJson> query = Ebean.find(GeoJson.class);
            RawSql rawSql = RawSqlBuilder.parse(queryString.toString()).columnMapping("geoJsonString", "geoJsonString").create();
            String geoJsonString = query.setRawSql(rawSql).findOne().geoJsonString;
            result = geoJsonString != null ? Json.parse(geoJsonString) : Json.parse("{}");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public String getGeoJsonForCarto(Integer layerId, String parameterJson) {
        StringBuilder queryString = new StringBuilder();
        String tableName = tableNameForLayer(layerId);
        GeoJsonQueryHelper queryHelper = new GeoJsonQueryHelper(layerId, parameterJson);
        //queryHelper.debugPrint();
        List<String> additionalColumnList = queryHelper.getAdditionalFieldsJavaList();
        List<InfoLayerFusionTableColumn> layerInfo = getLayerInfo(layerId);
        String whereString = " ";
        JsonNode result = Json.parse("{}");
        try {
            String normalLayerWhereString = queryHelper.getNormalLayerWhereStringThatShowOnlyCommonToAllLayer();
            // System.out.println("<->normalLayerWhereString<-> " + normalLayerWhereString);
            if(normalLayerWhereString.isEmpty()) {
                whereString = "";
            } else {
                whereString += normalLayerWhereString;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return whereString;
    }

    public JsonNode getGradientGeoJson(Integer layerId, String filterJson, Float simplTolr) {
        StringBuilder queryString = new StringBuilder();
        GradientMapLayer layer = getLayer(layerId);
        List<InfoGradientLayerColumn> layerInfo = getGradientLayerInfo(layerId);
        JsonNode result = Json.parse("{}");

        float defaultTolerance = .01f;
        String whereString = getGradientLayerWhereString(filterJson);

        queryString.append("select '{\"type\": \"FeatureCollection\",\"features\":'|| json_agg (row_to_json ( data)) || '}' as geoJsonString from \n");
        queryString.append("(\n");
        queryString.append("SELECT\n");
        queryString.append("'Feature' as type,\n");
        queryString.append("ogc_fid as id,\n");
        queryString.append("gl."+layer.fieldColumnName).append(" as field,\n");
        queryString.append("('{");
        for(int i = 0; i < layerInfo.size(); i++) {
            queryString.append("\"" +layerInfo.get(i).columnName + "\": {\"displayName\":' || '\"" + layerInfo.get(i).displayName + "\"' || ',\"value\":\"' || gl." + layerInfo.get(i).columnName + " || '\"}");
            if(i < layerInfo.size()-1) {
                queryString.append(",");
            }
        }
        queryString.append("}')::json as detail,\n");
        queryString.append("geometry_json as geometry\n");
//                    "          st_asGeoJson ( wkb_geometry1)::json as geometry\n" +
        queryString.append("FROM\n");
        queryString.append("dlt." + layer.dbTableName + " as gl\n");
        queryString.append("WHERE ").append(whereString).append("\n");
        queryString.append(")as data");
        RawSql rawSql = RawSqlBuilder.parse(queryString.toString()).columnMapping("geoJsonString", "geoJsonString").create();
        Query<GeoJson> query = Ebean.find(GeoJson.class);
        String geoJsonString = query.setRawSql(rawSql).findOne().geoJsonString;
        result = geoJsonString != null ? Json.parse(geoJsonString) : Json.parse("{}");
        return result;
    }

    public String tableNameForLayer(Integer layerId) {
        String layerTableName = FusionMapLayer.find
                .query()
                .select("dbTableName")
                .where()
                .idEq(layerId)
                .findSingleAttribute();
        return layerTableName;
    }

    public GradientMapLayer getLayer(Integer layerId) {
        Query<GradientMapLayer> query = Ebean.find(GradientMapLayer.class);
        query.where().idEq(layerId);
        GradientMapLayer layer = query.findOne();
        return layer;
    }

    public List<InfoLayerFusionTableColumn> getLayerInfo(Integer layerId) {
        Query<InfoLayerFusionTableColumn> query = Ebean.find(InfoLayerFusionTableColumn.class);
        query.where().eq("fusion_map_layer_id", layerId).and().eq("disable_info_ind", 0).orderBy("display_info_order");
        List<InfoLayerFusionTableColumn> layer = query.findList();
        return layer;
    }

    public List<InfoGradientLayerColumn> getGradientLayerInfo(Integer layerId) {
        Query<InfoGradientLayerColumn> query = Ebean.find(InfoGradientLayerColumn.class);
        query.where().eq("gradient_map_layer_id", layerId).orderBy("display_info_order");
        List<InfoGradientLayerColumn> layer = query.findList();
        return layer;
    }

    private String buildQueryString(Map<String, String[]> tableInfo) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("select row_to_json(rdata) as refs from\n");
        queryString.append("(select json_agg(ref) as ").append(tableInfo.get("jsonName")[0]).append(" from\n");
        queryString.append("(SELECT\n");
        String[] columns = tableInfo.get("columns");
        int colCount = columns.length;
        for (int i = 0; i < colCount; i++) {
            queryString.append(columns[i]);
            if (i < colCount - 1) queryString.append(",");
            queryString.append("\n");
        }
        queryString.append("FROM\n");
        queryString.append(tableInfo.get("tableName")[0]).append("\n");
        if (tableInfo.get("whereClause") != null) {
            queryString.append(tableInfo.get("whereClause")[0]);
        }
        queryString.append("ORDER BY ").append(tableInfo.get("orderBy")[0]).append(")as ref\n");
        queryString.append(") as rdata");

        return queryString.toString();
    }

    private String getGradientLayerWhereString(String filterJsonString) {
        String whereString = "";
        if (!filterJsonString.equals("{}")) {
            JsonNode filter = Json.parse(filterJsonString);
            String fieldColumnName = filter.get("fieldColumnName").asText();
            Iterator<JsonNode> filterEntries = filter.get("filterEntries").elements();
            JsonNode filterEntry = filterEntries.next();
            whereString += fieldColumnName + " " + filterEntry.get("filterSign").asText() + " " +
                    filterEntry.get("filterValue").asText();
            while(filterEntries.hasNext()) {
                filterEntry = filterEntries.next();
                whereString += " and " + fieldColumnName + " " + filterEntry.get("filterSign").asText() + " " +
                        filterEntry.get("filterValue").asText();
            }
        } else {
            whereString = "true";
        }
        return whereString;
    }
}
