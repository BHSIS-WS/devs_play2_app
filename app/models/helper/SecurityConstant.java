package models.helper;

import com.typesafe.config.ConfigFactory;
import com.typesafe.config.Config;

public class SecurityConstant {

    public static final String AUTH_ID_PWD_FAIL ="1",
        AUTH_PWD_EXPIRED = "2",
        AUTH_ACCOUNT_PWD_LOCKED = "3",
        AUTH_ACCOUNT_DISABLED = "5",
        AUTH_NO_ACTIVITY_LOCKED ="9",
        AUTH_ACCOUNT_ADDED = "10",
        PASS_CODE_SUCCESS = "success",
        PASS_CODE_FAIL = "fail",
        PASS_CODE_EXPIRE = "expired",
        PASS_CODE_NO_CONFIG = "no_passcode_config";
    public static final String APP_ID = "12";

    private static Config config = ConfigFactory.load();
    public static final boolean twoFactorAuthEnabled = config.getBoolean("two.factor.auth.enabled");
    public static final String APP_HOST = config.getString("baseUrl");
    public static final String AUTH_BASE_URL = config.getString("auth.service.base.url");
    public static final String AUTH_SERVICE_URL = AUTH_BASE_URL + config.getString("authenticate.service.url");
    public final static String SEND_PASSCODE_SVC_URL = AUTH_BASE_URL + config.getString("sendpasscode.service.url");
    public final static String PASSCODE_SVC_URL = AUTH_BASE_URL + config.getString("passcode.service.url");
    public static final String PERM_SERVICE_URL = AUTH_BASE_URL + config.getString("permission.get.service.url");
}
