package models;

import io.ebean.Expr;
import io.ebean.Model;
import io.ebean.Finder;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "fusion_map_layer", schema = "dlt")
public class FusionMapLayer extends Model {

    public static final Finder<Long, FusionMapLayer> find = new Finder<>(FusionMapLayer.class);

    public static List<FusionMapLayer> getAllFusionMapLayers() {
        return FusionMapLayer.find.query()
                .orderBy("displayOrder")
                .findList();
    }

    public static List<FusionMapLayer> getEnabledFusionMapLayers() {
        return FusionMapLayer.find.query()
                .where()
                .or(Expr.isNull("disableInd"),
                        Expr.eq("disableInd", Integer.valueOf("0")))
                .orderBy("displayOrder")
                .findList();
    }

    @Id
    @Column(name = "fusion_map_layer_id")
    public Integer fusionMapLayerId;

    @Column(name = "layer_name")
    public String layerName;

    @Column(name = "layer_desc")
    public String layerDescription;

    @Column(name = "url_source")
    public String urlSource;

    @Column(name = "db_table_name")
    public String dbTableName;

    @Column(name = "fusion_geom_column")
    public String googleFusionTableGeoColumn;

    @Column(name = "fusion_legend_json_string")
    public String fusionLegendJsonString;

    @Column(name = "fusion_options_json_string")
    public String fusionOptionsJsonString;

    @Column(name = "fusion_not_show_where_string")
    public String fusionNotShowWhereString;

    @Column(name = "layer_key")
    public String layerKey;

    @Column(name = "disable_ind")
    public Integer disableInd;

    @Column(name = "display_order")
    public Integer displayOrder;

    @Column(name = "query_type")
    public String queryType;

    @Column(name = "query_type_display")
    public String queryTypeDisplay;

    @Column(name = "style_column_json")
    public String styleColumnJson;

    @Transient
    public List<CriteriaCode> criteriaCodeGroup;

    @Transient
    public List<InfoLayerFusionTableColumn> infoLayerFusionTableColumnList;

    @Column(name = "fusion_map_layer_group_id")
    public Integer fusionMapLayerGroupId;

    @Column(name = "carto_style_string")
    public String cartoStyleString;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append(" fusionMapLayerId : ")
                .append(this.fusionMapLayerId)
                .append(", layerName : ")
                .append(this.layerName)
                .append(", layerDescription : ")
                .append(this.layerDescription)
                .append(", dbTableName : ")
                .append(this.dbTableName)
//                .append(", googleFusionTableGeoColumn: ")
//                .append(this.googleFusionTableGeoColumn)
//                .append(", fusionLegendJsonString: ")
//                .append(this.fusionLegendJsonString)
//                .append(", fusionOptionsJsonString: ")
//                .append(this.fusionOptionsJsonString)
//                .append(", fusionNotShowWhereString: ")
//                .append(this.fusionOptionsJsonString)
                .append(", layerKey: ")
                .append(this.layerKey)
                .append(", styleColumnJson: ")
                .append(this.styleColumnJson);
//                .append(", infoLayerFusionTableColumnList:  ")
//                .append(infoLayerFusionTableColumnList);
        sb.append("}\n");

        return sb.toString();
    }

    public Boolean isEnabledForAppUsage() {
        return Integer.valueOf(0).equals(disableInd);
    }

}
