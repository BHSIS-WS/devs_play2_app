package models;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.ebean.Expr;
import io.ebean.Finder;
import io.ebean.Model;

@Entity
@Table(name = "info_gradient_layer_column", schema="dlt")
public class InfoGradientLayerColumn extends Model {

    @Id
    @Column(name = "info_gradient_layer_column_id")
    public Integer infoGradientLayerColumnId;

    @Column(name = "data_set_id")
    public Integer dataSetId;

    @Column(name = "gradient_map_layer_id")
    public Integer gradientMapLayerId;

    @Column(name = "gradient_map_layer_name")
    public String gradientMapLayerName;

    @Column(name = "column_name")
    public String columnName;

    @Column(name = "is_field_column")
    public Boolean isFieldColumn;

    @Column(name = "display_name")
    public String displayName;

    @Column(name = "disable_ind")
    public Boolean disableInd;

    @Column(name = "display_info_order")
    public Integer displayInfoOrder;

    private static final Finder<Long, InfoGradientLayerColumn> find = new Finder<>(InfoGradientLayerColumn.class);

    public static List<InfoGradientLayerColumn> getAllRecords() {
        List<InfoGradientLayerColumn> recs = InfoGradientLayerColumn.find.query().where(Expr.eq("disable_ind", "true"))
                .orderBy("gradient_map_layer_id, info_gradient_layer_column_id")
                .findList();
        return recs;
    }

}
