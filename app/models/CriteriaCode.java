package models;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import io.ebean.Ebean;
import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.SqlRow;

@Entity
@Table(name = "criteria_code", schema = "dlt")
public class CriteriaCode extends Model {
    private static final Finder<Long, CriteriaCode> find = new Finder<>(CriteriaCode.class);

    @Id
    @Column(name = "criteria_code_id")
    public Integer criteriaCodeId;

    @Column(name = "criteria_name")
    public String criteriaName;

    @Column(name = "criteria_type")
    public String criteriaType;

    @Column(name = "display_order")
    public Integer displayOrder;
    
    @Column(name = "display_column_name")
    public String displayColumnName;

    @Transient
    public List<LayerFilteringCriteria> filteringCriteriaGroup;

    public static List<Integer[]> getFusionMapLayerIdAndCriteriaCodeId(){
        String query = "select distinct " +
                "f.fusion_map_layer_id, " +
                "c.criteria_code_id " +
                "from criteria_code c " +
                "inner join " +
                "fusion_map_layer_filter f " +
                "on c.criteria_code_id = f.criteria_code_id " +
                "order by f.fusion_map_layer_id" ;
        List<SqlRow> sqlRs = Ebean.createSqlQuery(query).findList();
        List<Integer[]> codeIds = null;
        if (sqlRs != null && !sqlRs.isEmpty()) {
            codeIds = new ArrayList<>();
            Integer[] rowArray = null;
            String[] colNamesArray = (String[])sqlRs.get(0).keySet().toArray();
            for (SqlRow row : sqlRs) {
                rowArray = new Integer[2];
                rowArray[0] = row.getInteger(colNamesArray[0]);
                rowArray[1] = row.getInteger(colNamesArray[1]);
                codeIds.add(rowArray);
            }
        }
        return codeIds;
    }
    
    @Override
    public String toString() {
        return "CriteriaCode{" + "criteriaName=" + criteriaName + ", criteriaCodeId=" + criteriaCodeId + '}';
    }

}
