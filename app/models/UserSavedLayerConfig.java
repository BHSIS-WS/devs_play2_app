package models;

import io.ebean.Model;
import io.ebean.Finder;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "user_saved_layer_config", schema = "dlt")
public class UserSavedLayerConfig extends Model {

    public static final Finder<Long, UserSavedLayerConfig> find = new Finder<>(UserSavedLayerConfig.class);

    @Id
    @Column(name = "user_saved_layer_config_id")
    public Long userSavedLayerConfigId;

    @Column(name = "username")
    public String username;

    @Column(name = "config_name")
    public String configName;

    @Column(name = "config_description")
    public String configDescription;

    @Column(name = "config_json_string")
    public String configJsonString;

    @Column(name = "last_modified")
    public Date lastModified;

    @Column(name = "disabled_ind")
    public Boolean disabledInd;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append(" userSavedLayerConfigId : ").append(userSavedLayerConfigId)
                .append(", username : ").append(username)
                .append(", configName : ").append(configName)
                .append(", configDescription : ").append(configDescription)
                .append(", configJsonString : ").append(configJsonString)
                .append(", lastModified : ").append(lastModified)
                .append(", disabledInd : ").append(disabledInd)
                .append(" }\n");

        return sb.toString();
    }

    public static List<UserSavedLayerConfig> getAllSavedLayerConfigsForUser(String username) {
        return find.query().where()
                .eq("username ", username)
                .eq("disabledInd", false)
                .orderBy("lastModified desc").findList();
    }

    public static void deleteSavedLayerConfigForUser(String username, Integer userSavedLayerConfigId) {
        UserSavedLayerConfig configToDelete = UserSavedLayerConfig.find.query().where()
                .eq("username", username)
                .eq("userSavedLayerConfigId", userSavedLayerConfigId)
                .findOne();
        if (configToDelete != null) {
            configToDelete.disabledInd = true;
            configToDelete.save();
        }
    }

    public static UserSavedLayerConfig create(String username, String name, String description, String config) {
        UserSavedLayerConfig userSavedLayerConfig = new UserSavedLayerConfig();
        userSavedLayerConfig.username = username;
        userSavedLayerConfig.configName = name;
        userSavedLayerConfig.configDescription = description;
        userSavedLayerConfig.configJsonString = config;
        userSavedLayerConfig.lastModified = new Date();
        userSavedLayerConfig.disabledInd = false;
        userSavedLayerConfig.save();
        userSavedLayerConfig.refresh();
        return userSavedLayerConfig;
    }
}
