/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.ebean.Finder;
import io.ebean.Model;

@Entity
@Table(name = "fusion_map_layer_filter", schema = "dlt")
public class LayerFilteringCriteria extends Model {

    private static final Finder<Long, LayerFilteringCriteria> find = new Finder<>(LayerFilteringCriteria.class);

    @Id
    @Column(name = "layer_filtering_criteria_id")
    public Integer layerFilteringCriteriaId;

    @Column(name = "name")
    public String name;

    @Column(name = "fusion_map_layer_id")
    public Integer fusionMapLayerId;

    @Column(name = "display_order")
    public String displayOrder;
    
    @Column(name = "criteria_code_id")
    public Integer criteriaCodeId;

    @Column(name = "criteria_value")
    public String criteriaValue;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append(" layerFilteringCriteriaId : ")
                .append(this.layerFilteringCriteriaId)
                .append(", name : ")
                .append(this.name)
                .append(", fusionMapLayerId : ")
                .append(", displayOrder : ")
                .append(this.displayOrder)
                .append(", criteria_code_id : ")
                .append(", criteria value : ")
                .append(this.criteriaValue)
                .append(" }\n");

        return sb.toString();
    }

    public static List<LayerFilteringCriteria> getAllLayerFilteringCriteria() {
        return LayerFilteringCriteria.find.query()
                .orderBy("display_order").findList();
    }


    public static List<LayerFilteringCriteria> getLayerFilteringCriteriaForFusionLayer(Integer pFusionMapLayerId) {
        return LayerFilteringCriteria.find.query()
                .where()
                .eq("fusion_map_layer_id", pFusionMapLayerId)
                .orderBy("display_order").findList();
    }

}
