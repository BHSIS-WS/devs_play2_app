package models;

import io.ebean.Finder;
import io.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "dlt.server_notification")
public class ServerNotification extends Model {

    @Id
    @Column(name = "server_notification_id")
    public Integer serverNotificationId;

    @Column(name = "message")
    public String message;

    @Column(name = "display_start")
    public Date displayStart;

    @Column(name = "display_end")
    public Date displayEnd;

    @Column(name = "disabled")
    public String disabled;

    @Column(name = "created_by")
    public String createdBy;

    public static final Finder<Long, ServerNotification> find = new Finder<>(ServerNotification.class);

    public static ServerNotification getAllActiveServerNotifications() {
        Date currentDate = new Date();
        return find.query().where()
                .ne("disabled", "1")
                .gt("displayEnd", currentDate)
                .le("displayStart", currentDate).findOne();

    }
}