package models.constants;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import play.libs.Json;

public class AppConfig {
    public static Config config = ConfigFactory.load();

    public AppConfig() {}

    public JsonNode getAppConfig() {
        ObjectNode config = Json.newObject();
        return config;
    }
}

