package models;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GeneratePopulateStyleColumnTest extends BaseModelTest {

    @Test
    public void testGenerateSql() {
        List<FusionMapLayer> layers = FusionMapLayer.getAllFusionMapLayers();
        List<InfoLayerFusionTableColumn> infoLayers = null;

        /*
        String[] cols = { "treatment_type", "aian_name", "st_sanch", "lzip5", "lst" };
        String[] descriptions = { "Treatment Type", "AIAN", "st_sanch", "Zip", "State" };
*/
        /*
        String[] cols = { "name" };
        String[] descriptions = { "States" };
        List<String> colSqls = null;
        List<FusionMapLayer> nssats = layers.stream().filter(layer -> layer.dbTableName.equalsIgnoreCase("layer_states")).collect(Collectors.toList());
        infoLayers = InfoLayerFusionTableColumn.getUseForSelectionRecords("layer_states"); */

        /*
        String[] cols = { "trtype_name", "aian_name", "st", "factext" };
        String[] descriptions = { "Treatment Type", "AIAN", "States", "Types" };
        List<String> colSqls = null;
        List<FusionMapLayer> nssats = layers.stream().filter(layer -> layer.dbTableName.equalsIgnoreCase("layer_bup_otp")).collect(Collectors.toList());
        infoLayers = InfoLayerFusionTableColumn.getUseForSelectionRecords("layer_bup_otp"); */

        /*
        String[] cols = { "description" };
        String[] descriptions = { "description" };
        List<String> colSqls = null;
        List<FusionMapLayer> nssats = layers.stream().filter(layer -> layer.dbTableName.equalsIgnoreCase("layer_metro_micro")).collect(Collectors.toList());
        infoLayers = InfoLayerFusionTableColumn.getUseForSelectionRecords("layer_metro_micro"); */
/*
        String[] cols = { "nchs_scheme_2006" };
        String[] descriptions = { "Types" };
        List<String> colSqls = null;
        List<FusionMapLayer> nssats = layers.stream().filter(layer -> layer.dbTableName.equalsIgnoreCase("layer_urban_rural")).collect(Collectors.toList());
        infoLayers = InfoLayerFusionTableColumn.getUseForSelectionRecords("layer_urban_rural"); */
/*
        String[] cols = { "name" };
        String[] descriptions = { "Types" };
        List<String> colSqls = null;
        List<FusionMapLayer> nssats = layers.stream().filter(layer -> layer.dbTableName.equalsIgnoreCase("layer_hhs_regions")).collect(Collectors.toList());
        infoLayers = InfoLayerFusionTableColumn.getUseForSelectionRecords("layer_hhs_regions"); */

        String[] cols = { "name" };
        String[] descriptions = { "Types" };
        List<String> colSqls = null;
        List<FusionMapLayer> nssats = layers.stream().filter(layer -> layer.dbTableName.equalsIgnoreCase("layer_ihs_regions")).collect(Collectors.toList());
        infoLayers = InfoLayerFusionTableColumn.getUseForSelectionRecords("layer_ihs_regions");
        String layerSql = null;
        for (FusionMapLayer layer : nssats) {
            colSqls = new ArrayList<String>();

            for (int i = 0; i < cols.length; i++) {
                layerSql = getSqlForEachCol(cols[i], descriptions[i], layer.dbTableName);
                colSqls.add(layerSql);
            }
            /*
            for (InfoLayerFusionTableColumn info : infoLayers) {
                layerSql = getSqlForEachCol(info.description, layer.dbTableName);
                colSqls.add(layerSql);
            } */
            layerSql = buildLayerSql(colSqls, layer.fusionMapLayerId);
            System.out.println(layerSql + ";\n\n\n");
        }
    }

    private String getSqlForEachCol(final String col, final String description, final String table) {
        StringBuilder sql = new StringBuilder();
        sql.append("select btrim(btrim(json_agg(style_column)::text, '['), ']') as style_column_json\n");
        sql.append(" from (select '").append(col).append("' as name,\n");
        sql.append(" '").append(description).append("' as description,\n");
        sql.append(" (select array_agg(distinct " +  col + ") from ").append(table).append(") as value) as style_column\n");
        return sql.toString();
    }

    private String buildLayerSql(List<String> layerSql, final Integer layerId) {
        StringBuilder sql = new StringBuilder();
        sql.append("update fusion_map_layer set style_column_json = (\nselect array_to_json(array_agg(style_column_json)) as style_columns\n from ( ");
        int idx = 0;
        for (String layer : layerSql) {
            if (idx > 0) {
                sql.append(" union \n");
            }
            sql.append(layer);
            idx++;
        }
        sql.append(") style_column_jsons\n").append(") where fusion_map_layer_id = ").append(layerId);
        return sql.toString();
    }
}
