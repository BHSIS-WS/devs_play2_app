package models;

import helper.InfoLayerFusionTableColumnHelper;
import models.helper.RefTableHelper;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FusionMapLayerGroupTest extends BaseModelTest{

    @Test
    public void testGetRef() {
        RefTableHelper helper = new RefTableHelper();
        //JsonNode result = helper.getGradientMapLayers();
       // System.out.println(result);
    }

   // @Test
    public void testGetAllFusionMapLayerGroup(){
        List allLayerGroups = FusionMapLayerGroup.getAllFusionMapLayerGroup();
        System.out.println(allLayerGroups);
    }

    //@Test
    public void testGetAllFusionMayLayer() {
        List<FusionMapLayer> layers = FusionMapLayer.getAllFusionMapLayers();

        for(FusionMapLayer layer : layers) {
            System.out.println(layer);
        }
    }

    //@Test
    public void testGet() {
        List<InfoLayerFusionTableColumn> infos = InfoLayerFusionTableColumn.getAllRecords();
        System.out.println(infos + "\n\n");
        infos = InfoLayerFusionTableColumn.getUseForSelectionRecords("layer_facilities_nssats_2011");
        System.out.println(infos + "\n\n");
        infos = InfoLayerFusionTableColumn.getUseForSelectionRecords("layer_facilities_nssats_2011");
        List<String> layer  = InfoLayerFusionTableColumn.getLayerColumnNames(2);
        System.out.println(layer);
        List<String> layerIds = new ArrayList<String>();
        layerIds.add("2");
        layerIds.add("5");
        List<LayerColumnInfo> layers = new InfoLayerFusionTableColumnHelper().getColumnInfo(layerIds);
        System.out.println("layers: ");
        System.out.println(layers);

    }
}
