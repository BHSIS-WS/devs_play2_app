package models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import helper.DataSetHelper;
import helper.FusionLayerQueryHelper;
import org.junit.Test;
import play.libs.Json;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FusionLayerQueryHelperTest extends BaseModelTest {

    @Test
    public void testExtractInputLayersFromRequest() throws Exception {

        //String jsonString = "{\"layers\":[{\"fusion_map_layer_id\":\"39\",\"criteria\":\"\"},{\"fusion_map_layer_id\":\"5\",\"criteria\":\"id in ('24')\"}],\"gradientLayers\":[],\"stateLevelOpts\":[],\"countyLevelOpts\":[],\"invertQuery\":false,\"tableColumns\":[],\"page\":1,\"pageSize\":10,\"shapeArray\":[{\"type\":\"polygon\",\"vertices\":\"(-117.5537109375 45.089035564831036,-99.7119140625 36.87962060502676,-85.869140625 35.17380831799959,-84.5068359375 42.35854391749706,-97.0751953125 45.39844997630408,-117.5537109375 45.089035564831036)\"}]}";
        String jsonString = "{\"layers\":[{\"fusion_map_layer_id\":\"39\",\"criteria\":\"TRTYPE in ('1', '2')\"},{\"fusion_map_layer_id\":\"1\",\"criteria\":\"\"},{\"fusion_map_layer_id\":\"5\",\"criteria\":\"id in ('23', '24')\"}],\"gradientLayers\":[{\"layer_key\":\"8.POP060210\",\"gradientMapLayerId\":\"8\",\"criteria\":\"\"}],\"stateLevelOpts\":[],\"countyLevelOpts\":[],\"invertQuery\":false,\"tableColumns\":[{\"fusionMapLayerId\":\"39\",\"selectedTableColumnIds\":[\"3302\",\"3303\",\"3304\",\"3305\"]},{\"fusionMapLayerId\":\"1\",\"selectedTableColumnIds\":[\"1292\",\"1291\",\"1288\",\"1287\"]},{\"fusionMapLayerId\":\"5\",\"selectedTableColumnIds\":[\"2331\",\"2332\"]}],\"page\":1,\"pageSize\":10,\"shapeArray\":[]}";
        JsonNode json = Json.parse(jsonString);
        FusionLayerQueryHelper helper = new FusionLayerQueryHelper();
        List<Map<String, String>> input = helper.extractInputLayersFromRequestRootNode(json);
        JsonNode jsonExceptionString = helper.checkInputLayersForException(input);

        System.out.println(input);
        System.out.println("------------------");

        System.out.println("exception string: " + jsonExceptionString);
        System.out.println("------------------");

        //Gradient layers Eg. "gradientLayers": [ { "layer_key":"58.tab18_var1","gradientMapLayerId":"58","criteria":"tab18_var1 > 2.9"}]
        List<Map<String, String>> gradientLayerListMap = helper.extractGradientLayersFromRequest(json);

        System.out.println("gradientlayers: " + gradientLayerListMap);
        System.out.println("------------------");

        //state level options, e.g. "stateLevelOpts":["38","39","40"]
        List<Integer> stateOptList = helper.extractStateLevelOptionsFromRequest(json);
        System.out.println("stateOptList: " + stateOptList);
        System.out.println("------------------");

        //county level options, e.g. "countyLevelOpts":["1","2","3"]
        List<Integer> countyOptList = helper.extractCountyLevelOptionsFromRequest(json);
        System.out.println("countyOptList: " + countyOptList);
        System.out.println("------------------");

        //tableColumns. Eg. "tableColumns":[{"fusionMapLayerId":"25","selectedTableColumnIds":["11","12","13"]}]
        List<Map<String, Object>> tableColumnListMap = helper.extractTableColumnsFromRequest(json);
        System.out.println("tableColumnListMap: " + tableColumnListMap);
        System.out.println("------------------");

        //drawn polygon
        List<Map<String, String>> shapeArrayMap = helper.extractShapeArray(json);
        System.out.println("shapeArrayMap: " + shapeArrayMap);
        System.out.println("------------------");

        JsonNode pageElement = json.get("page");
        int page = 1;
        if ((pageElement != null)) {
            try {
                page =  ((IntNode)pageElement).intValue();
                System.out.println("page: " + page);
            }
            catch (NumberFormatException nfe) {
                page = 1;
            }
        }

    }

    @Test
    public void testStaticAssign() {
        Map<Integer,String> gradFieldColMap = new HashMap<Integer,String>();
        List gradientMapLayerList = GradientMapLayer.getAllGradientMapLayer();
        System.out.println("------gradientMapLayerList: ");
        System.out.println(gradientMapLayerList);
        List dataSetList = DataSet.getEnabledGradientLayerDataSet();
        dataSetList = DataSetHelper.assignGradientMapLayersToDataSetList(dataSetList, gradientMapLayerList);
        DataSetHelper.assignGradFieldColId(dataSetList, gradFieldColMap);

        System.out.println("------gradFieldColMap: ");
        System.out.println(gradFieldColMap);
    }

    @Test
    public void testFetch() {
        //String jsonString = "{\"layers\":[{\"fusion_map_layer_id\":\"39\",\"criteria\":\"TRTYPE in ('1', '3')\"},{\"fusion_map_layer_id\":\"1\",\"criteria\":\"\"}],\"gradientLayers\":[],\"stateLevelOpts\":[],\"countyLevelOpts\":[],\"invertQuery\":false,\"tableColumns\":[],\"page\":1,\"pageSize\":10,\"shapeArray\":[]}";
        /*
        {"layers":[{"fusion_map_layer_id":"39","criteria":"TRTYPE in ('1', '3')"},{"fusion_map_layer_id":"1","criteria":""}],"gradientLayers":[],"stateLevelOpts":[],"countyLevelOpts":[],"invertQuery":false,"tableColumns":[],"page":1,"pageSize":10,"shapeArray":[]}
         */

        //String jsonString = "{\"layers\":[{\"fusion_map_layer_id\":\"39\",\"criteria\":\"TRTYPE in ('1', '2')\"},{\"fusion_map_layer_id\":\"1\",\"criteria\":\"\"},{\"fusion_map_layer_id\":\"5\",\"criteria\":\"id in ('23', '24')\"}],\"gradientLayers\":[{\"layer_key\":\"8.POP060210\",\"gradientMapLayerId\":\"8\",\"criteria\":\"\"}],\"stateLevelOpts\":[],\"countyLevelOpts\":[],\"invertQuery\":false,\"tableColumns\":[{\"fusionMapLayerId\":\"39\",\"selectedTableColumnIds\":[\"3302\",\"3303\",\"3304\",\"3305\"]},{\"fusionMapLayerId\":\"1\",\"selectedTableColumnIds\":[\"1292\",\"1291\",\"1288\",\"1287\"]},{\"fusionMapLayerId\":\"5\",\"selectedTableColumnIds\":[\"2331\",\"2332\"]}],\"page\":1,\"pageSize\":10,\"shapeArray\":[]}";
        /*
        {"layers":[{"fusion_map_layer_id":"39","criteria":"TRTYPE in ('1', '2')"},{"fusion_map_layer_id":"1","criteria":""},{"fusion_map_layer_id":"5","criteria":"id in ('23', '24')"}],"gradientLayers":[{"layer_key":"8.POP060210","gradientMapLayerId":"8","criteria":""}],"stateLevelOpts":[],"countyLevelOpts":[],"invertQuery":false,"tableColumns":[{"fusionMapLayerId":"39","selectedTableColumnIds":["3302","3303","3304","3305"]},{"fusionMapLayerId":"1","selectedTableColumnIds":["1292","1291","1288","1287"]},{"fusionMapLayerId":"5","selectedTableColumnIds":["2331","2332"]}],"page":1,"pageSize":10,"shapeArray":[]}
        */

        String jsonString = "{\"layers\":[{\"fusion_map_layer_id\":\"39\",\"criteria\":\"AIAN in ('0', '1')\"},{\"fusion_map_layer_id\":\"1\",\"criteria\":\"\"},{\"fusion_map_layer_id\":\"20\",\"criteria\":\"disaster_type in ('Earthquake', 'Flooding')\"},{\"fusion_map_layer_id\":\"5\",\"criteria\":\"id in ('23')\"}],\"gradientLayers\":[{\"layer_key\":\"1.AGE775212\",\"gradientMapLayerId\":\"1\",\"criteria\":\"\"}],\"stateLevelOpts\":[],\"countyLevelOpts\":[],\"invertQuery\":false,\"tableColumns\":[{\"fusionMapLayerId\":\"39\",\"selectedTableColumnIds\":[\"3302\",\"3303\",\"3304\",\"3305\",\"3306\",\"3307\",\"3308\",\"3309\",\"3310\",\"3311\",\"3312\",\"3313\",\"3314\"]},{\"fusionMapLayerId\":\"20\",\"selectedTableColumnIds\":[\"1771\",\"1787\",\"1781\"]},{\"fusionMapLayerId\":\"5\",\"selectedTableColumnIds\":[\"2331\"]}],\"page\":1,\"pageSize\":10,\"shapeArray\":[]}";
        /*
         {"layers":[{"fusion_map_layer_id":"39","criteria":"AIAN in ('0', '1')"},{"fusion_map_layer_id":"1","criteria":""},{"fusion_map_layer_id":"20","criteria":"disaster_type in ('Earthquake', 'Flooding')"},{"fusion_map_layer_id":"5","criteria":"id in ('23')"}],"gradientLayers":[{"layer_key":"1.AGE775212","gradientMapLayerId":"1","criteria":""}],"stateLevelOpts":[],"countyLevelOpts":[],"invertQuery":false,"tableColumns":[{"fusionMapLayerId":"39","selectedTableColumnIds":["3302","3303","3304","3305","3306","3307","3308","3309","3310","3311","3312","3313","3314"]},{"fusionMapLayerId":"20","selectedTableColumnIds":["1771","1787","1781"]},{"fusionMapLayerId":"5","selectedTableColumnIds":["2331"]}],"page":1,"pageSize":10,"shapeArray":[]}
         */

        /*
        {"layers":[{"fusion_map_layer_id":"39","criteria":"AIAN in ('0', '1') and TRTYPE in ('1', '2', '3')"},{"fusion_map_layer_id":"22","criteria":"chld='1' and pvt='1' and pub='1' and co='1' and gl='1'"},{"fusion_map_layer_id":"1","criteria":""}],"gradientLayers":[],"stateLevelOpts":[],"countyLevelOpts":[],"invertQuery":false,"tableColumns":[],"page":1,"pageSize":10,"shapeArray":[]}
         */
        JsonNode fetchReq = Json.parse(jsonString);
        FusionLayerQueryHelper helper = new FusionLayerQueryHelper();
        //layers (fusion_map_layer)
        List<Map<String, String>> inputLayerListMap = helper.extractInputLayersFromRequestRootNode(fetchReq);
        ObjectNode json = Json.newObject();
        try {
            //check to make sure input layers can be used to extract data
            JsonNode jsonExceptionString = helper.checkInputLayersForException(inputLayerListMap);
            if (jsonExceptionString != null) {
                System.out.println("jsonExceptionString: " + jsonExceptionString);
                return;
            }

            //Gradient layers Eg. "gradientLayers": [ { "layer_key":"58.tab18_var1","gradientMapLayerId":"58","criteria":"tab18_var1 > 2.9"}]
            List<Map<String, String>> gradientLayerListMap = helper.extractGradientLayersFromRequest(fetchReq);

            //state level options, e.g. "stateLevelOpts":["38","39","40"]
            List<Integer> stateOptList = helper.extractStateLevelOptionsFromRequest(fetchReq);

            //county level options, e.g. "countyLevelOpts":["1","2","3"]
            List<Integer> countyOptList = helper.extractCountyLevelOptionsFromRequest(fetchReq);

            //tableColumns. Eg. "tableColumns":[{"fusionMapLayerId":"25","selectedTableColumnIds":["11","12","13"]}]
            List<Map<String, Object>> tableColumnListMap = helper.extractTableColumnsFromRequest(fetchReq);

            System.out.println("FusionLayerQueryHelperTest: tableColumnListMap = " + tableColumnListMap);

            //drawn polygon
            List<Map<String, String>> shapeArrayMap = helper.extractShapeArray(fetchReq);


            //handle case of : has gradient layers but no fusion layers
            if ( (gradientLayerListMap!=null && gradientLayerListMap.size()>0)&& (inputLayerListMap==null||inputLayerListMap.size()<=0)){
                inputLayerListMap = new FusionLayerQueryHelper().createDefaultInputListMapForGradientLayers(gradientLayerListMap);
            } else if (inputLayerListMap==null||inputLayerListMap.size() <= 0) {
                System.out.println("empty inputLayerListMap");
                return;
            }

            JsonNode pageElement = fetchReq.get("page");
            int page = 1;
            if ((pageElement != null)) {
                try {
                    page =  ((IntNode)pageElement).intValue();
                }
                catch (NumberFormatException nfe) {
                    page = 1;
                }
            }

            JsonNode pageSizeElement = fetchReq.get("pageSize");
            int pageSize = 30;
            if ((pageSizeElement != null)) {
                try {
                    pageSize = ((IntNode)pageSizeElement).intValue();
                }
                catch (NumberFormatException nfe) {
                    pageSize = 30;
                }
            }

            JsonNode invertQueryElement = fetchReq.get("invertQuery");
            boolean invertQuery = (invertQueryElement != null) ? ((BooleanNode)invertQueryElement).booleanValue() : false;

            json = new FusionLayerQueryHelper().fetchLayerQueryResult(inputLayerListMap, gradientLayerListMap, stateOptList, countyOptList
                    , FusionLayerQueryHelper.gradFieldColMap, page, pageSize, invertQuery, tableColumnListMap,shapeArrayMap, null, null, null, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            String exceptionMsg = e.getLocalizedMessage();
            if (exceptionMsg != null) {
                exceptionMsg = exceptionMsg.replace("\"", "'");
                exceptionMsg = exceptionMsg.replace("\n", ",");
            } else {
                exceptionMsg = "";
            }
            json.put("exceptiontype", e.getClass().getCanonicalName());
            json.put("exceptionmessage", exceptionMsg);
        }

        System.out.println("fetched json:");
        System.out.println(Json.toJson(json).toString());
    }
}
