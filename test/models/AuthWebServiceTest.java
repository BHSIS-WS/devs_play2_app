package models;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;
import play.api.Play;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;

import java.util.concurrent.CompletionStage;

public class AuthWebServiceTest extends BaseModelTest {

    private WSClient ws = Play.current().injector().instanceOf(WSClient.class);

    @Test
    public void testAuthWS() throws Exception {
        WSRequest request = ws.url("http://etdev1app01.eagletechva.com:9020/auth");
        request.addQueryParameter("loginName", "kaurj");
        request.addQueryParameter("password", "Jaj@04");
        request.addQueryParameter("appid", "10");
        request.addQueryParameter("verifyPasscode", "false");
//        JsonNode queryParams = Json.newObject()
//                .put("loginName", "kaurj")
//                .put("password", "Jasraj@04")
//                .put("appId", 10)
//                .put("verifyPasscode", false);
        CompletionStage<WSResponse> resp = request.post("content");;
        CompletionStage<JsonNode> respJsonPromise = resp.thenApply(WSResponse::asJson);
        JsonNode respJsonObject = respJsonPromise.toCompletableFuture().get();
        System.out.println(respJsonObject + "respJsonObject");
        System.out.println(respJsonObject.get("authCode").asText());

    }
}
